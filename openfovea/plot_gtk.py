#!/usr/bin/env python
# -*- coding: UTF-8 -*-
'''
This module contains the necessary tools to plot graphics in gtk
'''
# TODO: Separate the generic plot from the GTK-only

import pygtk
#import gnome.ui
import gtk
# for script installation
from pkg_resources import resource_filename
#import pdb

import numpy
from scipy import stats
##############
## Graphic library ##
##############
try:
    matplotlib.__version__
except NameError:
    import matplotlib
    matplotlib.use('Agg')
    
from matplotlib.figure import Figure
from matplotlib.backends.backend_gtkagg import \
                                FigureCanvasGTKAgg as FigureCanvas
from matplotlib.backends.backend_gtkagg import \
                                NavigationToolbar2GTKAgg as NavigationToolbar
from matplotlib import cm # colormap
from matplotlib.patches import Circle, Rectangle
from matplotlib.image import imread
from matplotlib.widgets import Cursor
from matplotlib.ticker import NullFormatter
nullfmt = NullFormatter() 

# Internal classes
import classes
import plot_generic
#from fovea_toolbox.post_proc import gauss_fit, compute_gauss

try:
    no_data_filename = resource_filename('openfovea','Icon/NoData.png')
except NotImplementedError:
    no_data_filename = 'openfovea/Icon/NoData.png'

class plot(object):
    """
        This is the generic plot class that will be inherited from the other.
        It has no plot capabilities, but have the standard attribute.
    """
    def __init__(self, window, widget, plot_generic):
        self.window = window
        self.widget = widget
        self.window.set_property('visible', True)
        self.window.connect('destroy', self.on_win_plot_destroy)
        self.plot_generic = plot_generic
        self.axis = plot_generic.axis
        self.figure = plot_generic.figure
        self.canvas = FigureCanvas(self.figure) # a gtk.DrawingArea 
        self.canvas.show() 
        self.graphview = self.widget['plot_area']
        self.graphview.pack_start(self.canvas, True, True)
        # below is optional if you want the navigation toolbar
        if 'box_nav_tool' in self.widget.keys():
            self.navToolbar =  FoveaNavToolbar(self.canvas, self.window)
            self.navToolbar.plot_gen = self.plot_generic
            self.navToolbar.lastDir = '/var/tmp/'
            self.widget['box_nav_tool'].pack_start(self.navToolbar)
            self.navToolbar.show()
            #self.navToolbar.connect('button_press_event', self.press)
        self.destroyed = 0
    def show(self):
        if not self.window.get_property('visible'):
            raise StandardError
        self.figure.subplots_adjust(left=0.2, bottom=0.12)
        self.canvas.draw_idle()
    def on_win_plot_destroy(self, *signal):
        self.destroyed = 1
        self.window.destroy()
    def hold(self, value):
        """
            Change the hold of the figure.
        """
        self.plot_generic.hold(value)
    def set_size(self, size, size2=None):
        """
            Change the size of the figure.
        """
        if size2 == None:
            self.plot_generic.set_size(size[0], size[1])
        else:
            self.plot_generic.set_size(size, size2)
    def set_xscale(self, scale):
        self.plot_generic.set_xscale(scale)
        self.show()
    def get_type(self):
        return self.plot_generic.get_type()
class plotCurves(plot):
    def __init__(self, window, widget, xlabel='Scanner Extention [nm]',
                       ylabel='Cantilever Deflection [nm]',
                       title='Force-Distance curve',
                       win_title='Force Curve',
                       linewidth=1.5):
        _plot_generic = plot_generic.Curve(xlabel, ylabel, title, linewidth)
        plot.__init__(self, window, widget, _plot_generic)
        self.window.set_title(win_title)
        
    def displayPlot(self, X, Y, linestyle='b-', alpha=1):
        """
            Display the plot.
        """
        if self.navToolbar.auto:
            self.plot_generic.lim = None
        self.plot_generic.plot(X, Y, linestyle=linestyle, alpha=alpha)
        self.show()
        
class plotGride(plot):
    '''
    Plots the given matrix corresponding to the type.
    
    Type can be :
        basic = 2D matrix
        
        In the following cases, plot is a forceVolume object
        stiffness = plot the stiffness of the object
        piezzoHeight = plot the piezzo height of the object
        zeroForceImage = plot the zero force Image of the object
    '''
    def __init__(self, window, widget, title = 'Stiffness',
                                                      win_title = 'Gride Plot'):
    
        _plot_generic = plot_generic.Array(title)
        
        self.scaleMax = None
        self.array = {'scan_size' : None,
                      'data' : None}
        
        plot.__init__(self, window, widget, _plot_generic)
        # Set the properties of the GUI
        window.set_title(win_title)
        self.widget['scrollbar_min'].set_property('visible', True)
        self.widget['scrollbar_max'].set_property('visible', True)
        self.widget['scrollbar_min'].connect_object("change-value",
                                                    self.change_scale_min,
                                                    None)
        self.widget['scrollbar_max'].connect_object("change-value",
                                                    self.change_scale_max,
                                                    None)
        
        self.widget['scrollbar_depth'].connect_object("change-value",
                                                    self.change_depth,
                                                    None)
                                                    
        self.widget['button_modify_plot'].set_label('Color scale')
        self.widget['button_modify_plot'].connect_object(
                                            'clicked',
                                            self.color_scale,
                                            self.widget['button_modify_plot'])
        self.widget['button_modify_plot'].set_property('visible', True)
        self.figure.canvas.mpl_connect('button_press_event',
                                         self.on_gride_click)
        self.figure.canvas.mpl_connect('button_release_event', self.on_button_release)
        self.figure.canvas.mpl_connect('motion_notify_event', self.move_mouse)
    def __getattr__(self, att):
        if att == 'path_refresh':
            return self.plot_generic.path_refresh
        elif att == 'path':
            return self.plot_generic.path
    def plot(self, dispArray, scanSize = None, nanVal= 80e9, cmap = 'gray'):
        self.cmap = cmap
        if self.scaleMax == None:
            # The scale hasn't yet been initialized. Then do it.
            try:
                self.scaleMax = numpy.nanmax(dispArray)
            except TypeError:
                # In windows, numpy.nanmax seems to not work in this case...
                self.scaleMax = numpy.max(
                            dispArray[numpy.array((numpy.isnan(dispArray)-1),
                            dtype=bool)])
            if type(self.scaleMax) == numpy.ma.core.MaskedArray:
                self.scaleMax = 0
            self.widget['scrollbar_min'].set_adjustment(
                                gtk.Adjustment(0.0,  0.00,
                                               self.scaleMax,
                                               self.scaleMax / 100,
                                               self.scaleMax / 25,
                                               0))
            self.widget['scrollbar_max'].set_adjustment(
                                gtk.Adjustment(self.scaleMax, 0.00,
                                               self.scaleMax,
                                               self.scaleMax / 100,
                                               self.scaleMax / 25,
                                               0))
        self.array['data'] = dispArray
        if scanSize is not None:
            self.array['scan_size'] = scanSize
        if self.array['data'].ndim == 3:
            self.widget['scrollbar_depth'].set_property('visible', True)
            self.widget['scrollbar_depth'].set_adjustment(
                                gtk.Adjustment(0.0, 0.00,
                                               self.array['data'].shape[2],
                                               1, 5, 1.0))
            self.depth = 0
        self.__plot()
    def __plot(self):
        if self.array['data'].ndim == 3:
            if self.depth >= self.array['data'].shape[2]:
                self.depth = self.array['data'].shape[2] - 1
            elif self.depth < 0:
                self.depth = 0
            disp_array = self.array['data'][:,:,self.depth]
        else:
            disp_array = self.array['data']
        self.plot_generic.cmap = self.cmap
        self.plot_generic.plot(disp_array, scan_size=self.array['scan_size'])
        self.show()
    def get_path(self):
        return self.plot_generic.get_path()
    def change_depth(self, widget, jump, value):
        self.depth = value
        self.__plot()
    def change_scale_min(self, widget, jump, value):
        self.plot_generic.vmin = value
        self.show()
    def change_scale_max(self, widget, jump, value):
        self.plot_generic.vmax = value
        self.show()
    def color_scale(self, widget):
        self.plot_generic.colorbar = not(self.plot_generic.colorbar)
        self.__plot()
        self.show()
    def mark_pos(self, pos_x, pos_y):
        self.plot_generic.marker = (pos_x, pos_y)
        self.show()
    def show_trace(self, trace):
        self.plot_generic.show_trace(trace)
        self.show()
    def on_gride_click(self, event):
        self.plot_generic.on_gride_click(event)
        self.show()
    def on_button_release(self, event):
        self.plot_generic.on_button_release(event)
        self.show()
    def move_mouse(self, event):
        self.plot_generic.move_mouse(event)
        self.show()
class errorBar(plot):
    def __init__(self, window, widget, xlabel='Depth',
                        ylabel='Stiffness',
                        title='mean stiffness',
                        win_title='Mean Stiffness'):
        _plot_generic = plot_generic.ErrorBar(xlabel, ylabel, title)
        plot.__init__(self, window, widget, _plot_generic)
        # Add button to toggle the display of the number of data.
        self.widget['button_modify_plot'].set_label('Hide nbr data')
        self.widget['button_modify_plot'].connect_object(
                                            'clicked',
                                            self.toogle_plot,
                                            self.widget['button_modify_plot'])
        self.widget['button_modify_plot'].set_property('visible', True)
        self.init_data = {'data' : None, 
                          'group' : None, 
                          'group_info' : None,
                          'linestyle' : 'b-'}
       # self.plot_gen = plot_generic.ErrorBar(xlabel, ylabel, title)
    def average_stiffness(self, data, group=None, group_info=None, linestyle='b-'):
        self.init_data['data'] = data
        self.init_data['group'] = group
        self.init_data['group_info'] = group_info
        self.init_data['linestyle'] = linestyle
        self.__refresh_average_stiffness()
    def __refresh_average_stiffness(self):
        # If the button label is set to hide, we have to show it, and inversly.
        if self.widget['button_modify_plot'].get_label() == 'Hide nbr data':
            show_count = True
        elif self.widget['button_modify_plot'].get_label() == 'Show nbr data':
            show_count = False
        self.plot_generic.average_stiffness(self.init_data['data'],
                                           self.init_data['group'],
                                           self.init_data['group_info'],
                                           self.init_data['linestyle'],
                                           show_count=show_count)
        self.show()
    def plot(self, data_x, data_y, y_err, linestyle='b-'):
        """
            Data is organized like this :
        """
        
        self.plot_generic.plot(data_x, data_y, y_err, linestyle)
        self.show()
    def toogle_plot(self, widget):
        """
            Toogle between density and scatter plot
        """
        if widget.get_label() == 'Hide nbr data':
            widget.set_label('Show nbr data')
        elif widget.get_label() == 'Show nbr data':
            widget.set_label('Hide nbr data')
        self.__refresh_average_stiffness()
class TimeLapse(plotCurves):
    def __init__(self, window, widget, xlabel='Time',
                        ylabel='Stiffness',
                        title='Relative Stiffness',
                        win_title='Relative Stiffness'):
        _plot_generic = plot_generic.TimeLapse(xlabel, ylabel, title)
        plot.__init__(self, window, widget, _plot_generic)
        self.window.set_title(win_title)
    def displayPlot(self, data, data_ctl=None, baseline=0):
        """
            Displays the plot.
            
            See plot_generic.TimeLapse documentation for details
        """
        
        self.plot_generic.plot(data, data_ctl, baseline)
        self.show()

class plotMosaic(plotGride):
    '''
    Plots a mosaic of arrays. The input has to be a list of arrays.
    When you create an instance of plotMosaic, you can plot a list of arrays
    in a single window with subplots.
    
    For example, if you have several arrays like array_1, array_2, array_3 put
    them in a single list :
    
    >>> list_of_array = (array_1, array_2, array_3)
    >>> new_mosaic = plotMosaic()
    >>> new_mosaic.plot(list_of_array)
    
    This will open a new window with your array plotted.
    '''
    def __init__(self, window, widget, title = 'Mosaic plot'):
        '''
        Create a new instance of the class plotMosaic.
        window = builder object of the window where you want to plot your data
        widget = builder object of widget that are used for connection.
        title = string that will be used as the name of the window
        '''
        _plot_generic = plot_generic.Mosaic()
        plot.__init__(self, window, widget, _plot_generic)
        self.window.set_title(title)
    def plot(self, list_array, title = 'Mosaic plot', list_name=None,
                                                    cmap = 'gray',
                                                    list_mask=None):
        '''
        Plot your arrays.
        
        list_array = list of the array to be plotted. They do not need to have
                     the same shape or size.
                     
        title = string that will be displayed as the general title of the grid.
        
        list_title = list of string. Each string will be used as a title for
                     each grides. This can take some place and can be places
                     over the above gride. So be carefull for the esthetic of
                     your plot.
        '''
        if cmap in ('gray', 'grey', 'height'):
            cmap = cm.gray
        elif cmap in ('copper', 'afm image'):
            cmap = cm.copper
        elif cmap in ('jet', 'stiffness'):
            cmap = cm.jet
        self.plot_generic.plot(list_array, title, list_name, cmap, list_mask)
        self.plot_generic.figure.canvas.mpl_connect('axes_enter_event',
                                                    self.enter_array)
        self.plot_generic.figure.canvas.mpl_connect('axes_leave_event',
                                                    self.leave_array)
        self.plot_generic.figure.canvas.mpl_connect('motion_notify_event',
                                                    self.enter_array)
#        self.plot_generic.figure.canvas.mpl_connect('pick_event', self.on_pick)
    def enter_array(self, event):
        self.plot_generic.enter_array(event)
        self.show()
    def leave_array(self, event):
        self.plot_generic.leave_array(event)
        self.show()
    def index(self, event):
        return self.plot_generic.index(event)
#    def on_pick(self, event):
#        self.plot_generic.on_pick(event)
#        self.show()
#        print "On plot_gtk"
        
class plotHist(plot):
    '''
    Plot the histogram of the list of values.
    '''
    def __init__(self, window, widget, xlabel = '', title='Histogram', y_rel=False):
        _plot_generic = plot_generic.Histogram(xlabel, title, y_rel=y_rel)
        plot.__init__(self, window, widget, _plot_generic)
        self.window.set_title(title)
    def hist(self, data, data2 = None, label = None, label2 = None,
                            hist_range = None, color_type = 'color', y2=False, ):
        self.plot_generic.plot(data, data2, label, label2,
                                                    hist_range, color_type, y2)
        self.show()
    def gauss(self, prop):
        self.plot_generic.show_fit(prop)
        self.show()
        self.plot_generic.figure.canvas.mpl_connect('motion_notify_event',
                                                    self.move_annotation)
    def move_annotation(self, event):
        self.plot_generic.move_annotation(event)
        self.show()
    def set_y_rel(self, value):
        if type(value) == bool:
            self.plot_generic.relative = value
        
class plotScatter(plot):
    """
        Display the scatter plot of the choosen data.
    """
    
    def __init__(self, window, widget, title='Scatter Plot',
                                                        xlabel='', ylabel=''):
        _plot_generic = plot_generic.Scatter(xlabel, ylabel, title)
        plot.__init__(self, window, widget, _plot_generic)
        self.window.set_title(title)
        
        self.widget['button_modify_plot'].set_label('Density plot')
        self.widget['button_modify_plot'].connect_object(
                                            'clicked',
                                            self.toogle_plot,
                                            self.widget['button_modify_plot'])
        self.widget['button_modify_plot'].set_property('visible', True)
    def display(self, data, data_type):
        """
            Display it.
            Data has to be a PlotData object.
        """
        self.plot_generic.plot(data, data_type)
        self.show()
    def toogle_plot(self, widget):
        """
            Toogle between density and scatter plot
        """
        if widget.get_label() == 'Density plot':
            widget.set_label('Scatter plot')
            self.plot_generic.type = 'density'
            self.plot_generic.refresh()
            self.show()
        elif widget.get_label() == 'Scatter plot':
            widget.set_label('Density plot')
            self.plot_generic.type = 'scatter'
            self.plot_generic.refresh()
            self.show()
        self.show()
class plotTomo(plot):
    '''
    Plot the stiffness tomography slices.
    '''
    def __init__(self, window, widget, tomo_array, scan_size=None,
                                                            title='Tomography'):
        _plot_generic = plot_generic.Tomo(tomo_array, scan_size)
        plot.__init__(self, window, widget, _plot_generic)
        self.window = window
        self.widget = widget
        self.slice = [0, 0, 0]
        self.window.set_property('visible', True)
        self.window.connect('destroy', self.on_win_plot_destroy)
        self.move_cursor = 0
        self.cur_size = 4
        self.destroyed = 0
        self.canvas.mpl_connect('motion_notify_event', self.onmouse_motion)
        self.canvas.mpl_connect('button_press_event', self.on_gride_click)
        
        self.widget['adjust_x'].connect_object("value-changed",
                                                self.on_adjust_x_value_changed,
                                                self.widget['adjust_x'])
        self.widget['adjust_y'].connect_object("value-changed",
                                                self.on_adjust_y_value_changed,
                                                self.widget['adjust_y'])
        self.widget['adjust_z'].connect_object("value-changed",
                                                self.on_spin_z_change_value,
                                                self.widget['adjust_z'])
        self.widget['adjust_x'].upper = tomo_array.shape[0]-1
        self.widget['adjust_y'].upper = tomo_array.shape[1]-1
        self.widget['adjust_z'].upper = tomo_array.shape[2]-1
        
        self.widget['scrollbar_min'].set_property('visible', True)
        self.widget['scrollbar_max'].set_property('visible', True)
        self.colorScaleMax = tomo_array.max()
        self.colorScaleMin = 0
        self.widget['scrollbar_min'].set_adjustment(
                                gtk.Adjustment(0.0,  0.00,
                                               self.colorScaleMax,
                                               self.colorScaleMax / 100,
                                               self.colorScaleMax / 25,
                                               0))
        self.widget['scrollbar_max'].set_adjustment(
                                gtk.Adjustment(self.colorScaleMax,  0.00,
                                               self.colorScaleMax,
                                               self.colorScaleMax / 100,
                                               self.colorScaleMax / 25,
                                               0))
        self.widget['scrollbar_min'].connect_object("change-value",
                                                    self.change_scale_min,
                                                    None)
        self.widget['scrollbar_max'].connect_object("change-value",
                                                    self.change_scale_max,
                                                    None)
        self.plot()
    def on_win_plot_destroy(self, signal):
        self.destroyed = 1
        self.window.destroy()
    def onmouse_motion(self, event):
        '''
        Catch the mouse motion in the axes. self.move_cursor has to be set to
        True to have an action.
        The other axis are refresh according to the movement of the mouse.
        '''
        if not self.move_cursor:
            return
        if event.inaxes == self.plot_generic.axis['display_x']:
            y = self.plot_generic.scale['y'] > event.xdata
            y = y.nonzero()[0][0]
            z = self.plot_generic.scale['z'] > event.ydata
            z = z.nonzero()[0][0]
            self.widget['adjust_y'].set_value(y)
            self.widget['adjust_z'].set_value(z)
        elif event.inaxes == self.plot_generic.axis['display_y']:
            x = self.plot_generic.scale['x'] > event.xdata
            x = x.nonzero()[0][0]
            z = self.plot_generic.scale['z'] > event.ydata
            z = z.nonzero()[0][0]
            x = numpy.floor(event.xdata)
            z = numpy.floor(event.ydata)
            self.widget['adjust_x'].set_value(x)
            self.widget['adjust_z'].set_value(z)
        elif event.inaxes == self.plot_generic.axis['display_z']:
            x = self.plot_generic.scale['x'] > event.xdata
            x = x.nonzero()[0][0]
            y = self.plot_generic.scale['y'] > event.ydata
            y = y.nonzero()[0][0]
            self.widget['adjust_x'].set_value(x)
            self.widget['adjust_y'].set_value(y)
    def on_gride_click(self, event):
        '''
        Catch click in an axis and change the self.move_cursor value.
        '''
        if self.move_cursor:
            self.get_pos()
            self.move_cursor = False
        else:
            self.move_cursor = True
    def plot(self):
        self.plot_generic.plot(self.slice,
                               self.colorScaleMin, self.colorScaleMax)
        self.show()
    def on_spin_z_change_value(self, widget):
        self.slice[2] = widget.get_value()
        self.plot()
    def on_adjust_x_value_changed(self, widget):
        self.slice[0] = widget.get_value()
        self.plot()
    def on_adjust_y_value_changed(self,widget):
        self.slice[1] = widget.get_value()
        self.plot()
    def change_scale_min(self, widget, jump, value):
        self.colorScaleMin = value
        self.plot()
    def change_scale_max(self, widget, jump, value):
        self.colorScaleMax = value
        self.plot()
    def get_pos(self):
        '''
        Function to be overwritten by the writer in order to get the internal
        values.
        '''
        pass

class plotInteractiveCurve(plot):
    """
        Plots several curves in as many subplot as the number of curves.
    """
    def __init__(self, window, widget, win_title='Slices'):
        _plot_generic = plot_generic.InteractiveLinePlot()
        plot.__init__(self, window, widget, _plot_generic)
        self.window.set_title(win_title)
        self.window.resize(500, 500)
    def show(self):
        if not self.window.get_property('visible'):
            raise StandardError
        self.canvas.draw_idle()
    def plot(self, array_x, arrays, labels):
        self.plot_generic.plot(array_x, arrays, labels)
        
        self.plot_generic.figure.canvas.mpl_connect('button_press_event',
                                                    self.button_press_event)
        self.plot_generic.figure.canvas.mpl_connect('button_release_event',
                                                    self.button_release_event)
        self.plot_generic.figure.canvas.mpl_connect('motion_notify_event',
                                                    self.mouse_move)
        self.plot_generic.figure.canvas.mpl_connect('pick_event', self.on_pick)
        self.show()
    def modify_plot(self, array_x, arrays):
        self.plot_generic.modify_plot(array_x, arrays)
        self.show()
    def button_press_event(self, event):
        self.plot_generic.button_press_event(event)
        self.show()
    def mouse_move(self, event):
        self.plot_generic.mouse_move(event)
        self.show()
    def on_pick(self, event):
        self.plot_generic.on_pick(event)
        self.show()
    def button_release_event(self, event):
        self.plot_generic.button_release_event(event)

class FoveaNavToolbar(NavigationToolbar):

    def __init__(self, plotCanvas, plotWindow):
        NavigationToolbar.__init__(self, plotCanvas, plotWindow)
        self.plot_gen = None
        self.fig_size = [800, 800]
        self.auto = 1
    def home(self, event):
        self.auto = 1
        NavigationToolbar.home(self, event)
    def press_zoom(self, event):
        self.auto = 0
        NavigationToolbar.press_zoom(self, event)
    def save_figure(self, event):
        # get the old size...
        old_size = self.plot_gen.figure.get_size_inches().copy()
        self.plot_gen.figure.set_size_inches(self.plot_gen.win_size)
        #self.plot_gen.set_size()
        NavigationToolbar.save_figure(self, event)
        # return to the old size...
        self.plot_gen.figure.set_size_inches(old_size)

def regenerate_plot(self, type='normal'):
    """
        Create the gtk window where to plot the curves.
    """
    
    builder = gtk.Builder()
    if type == 'normal':
        _glade_filename = resource_filename('openfovea', 'glade/plot.glade')
    elif type == 'tomography':
        _glade_filename = resource_filename('openfovea',
                                            'glade/stiffness_plot.glade')
    builder.add_from_file(_glade_filename)
    window = builder.get_object("WindowPlot")
    builder.connect_signals(self)
    if type == 'normal':
        widget = {
        'plot_area' : builder.get_object('boxPlot'),
        'scrollbar_min' : builder.get_object('scrollbar_min'),
        'scrollbar_max' : builder.get_object('scrollbar_max'),
        'scrollbar_depth' : builder.get_object('scrollbar_depth'),
        'box_nav_tool' : builder.get_object('box_nav_tool'),
        'button_modify_plot' : builder.get_object('button_modify_plot')
        }
    elif type == 'tomography':
        widget = {
        'plot_area' : builder.get_object('boxPlot'),
        'display_x': builder.get_object('display_x'),
        'display_y': builder.get_object('display_y'),
        'display_z': builder.get_object('display_z'),
        'adjust_x': builder.get_object('adjust_x'),
        'adjust_y': builder.get_object('adjust_y'),
        'adjust_z': builder.get_object('adjust_z'),
        'scrollbar_min' : builder.get_object('scrollbar_min'),
        'scrollbar_max' : builder.get_object('scrollbar_max')
    }
    
    return [window, widget]
    
def main():
    gtk.main()
    return 0
if __name__=='__main__':
    import sys
    import numpy
    [window, widget] = regenerate_plot('normal')
    window.connect('destroy', gtk.main_quit)
    if sys.argv[1] == 'curve':
        print('Creating vectors...')
        X=numpy.asanyarray([1,2,3,4,5,6,7,8,9,10])
        Y=numpy.asanyarray([1,3,4,3,2,4,5,6,7,8])
        print('Plotting vectors...')
        newPlot=plotCurves(window, widget)
        newPlot.displayPlot(X,Y)
        newPlot.set_size(800, 800)
        main()
    elif sys.argv[1] == 'gride':
        gride = numpy.random.randn(64, 64)+2
        newPlot = plotGride(window, widget)
        newPlot.plot(gride, 2000)
        main()
    elif sys.argv[1] == 'mosaic':
        mosaic = [numpy.random.randn(32, 32)+2 for i in range(10)]
        list_mask = [1,0,0,0,1,1,0,0,1,0]
        newPlot = plotMosaic(window, widget)
        newPlot.plot(mosaic, list_mask=list_mask)
        main()
