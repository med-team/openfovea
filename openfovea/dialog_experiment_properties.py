#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from math import log10, floor

import pygtk
import gtk
# for script installation
from pkg_resources import resource_filename
##############
## Graphic library ##
##############
try:
    matplotlib.__version__
except NameError:
    import matplotlib
    matplotlib.use('Agg', warn=False)
from matplotlib.figure import Figure
from matplotlib.backends.backend_gtkagg import FigureCanvasGTKAgg as FigureCanvas
from matplotlib.backends.backend_gtkagg import NavigationToolbar2GTKAgg as NavigationToolbar

from fovea_toolbox import curve

class main_win():
    def __init__(self, author=None, comment=None, spring_constant=None,
                 inject_index=None, stiff_glass=None, trace_curve=None, 
                 plot_size=None, group=None, _glade_filename=None,
                 linewidth=1.5, histo_y_rel=True, plot_hist_gauss=False,
                 plot_color=None):
        builder = gtk.Builder()
        if _glade_filename == None:
            try:
                _glade_filename = resource_filename(
                                    'openfovea',
                                    'glade/dialog_experiment_properties.glade')
            except NotImplementedError:
                _glade_filename = 'openfovea/glade/dialog_experiment_properties.glade'
        builder.add_from_file(_glade_filename)
        self.window = builder.get_object('ExpProp')
        self.widget = {
            # General tab
            'text_author' : builder.get_object('text_author'),
            'text_description' : builder.get_object('text_description'),
            ## Configuration tab
            # Time Lapse (This is outdated, I should clean it...)
            'spin_inject_index' : builder.get_object('entry_injection_index'),
            # System
            'entry_spring_cst' : builder.get_object('entry_spring_constant'),
            'spin_stiff_glass' : builder.get_object('spin_stiff_glass'),
            # Plot area...
            'plot_area' : builder.get_object('plot_area'),
            'box_nav_tool' : builder.get_object('box_nav_tool'),
            ## Graphs
            # Saved size
            'spin_save_plot_width' : builder.get_object('spin_save_plot_width'),
            'spin_save_plot_height' : builder.get_object('spin_save_plot_height'),
            # Plot options
            'spin_linewidth' : builder.get_object('spin_linewidth'),
            'check_bw_plot' : builder.get_object('check_bw_plot'),
            # Histogram options
            'check_plot_hist_gauss' : builder.get_object('check_plot_hist_gauss'),
            'histo_y_rel' : builder.get_object('histo_y_rel'),
            # Group managment.
            'table_group' : builder.get_object('table_group'),
            'gid' : [], # List of group id
            'glabel' : [], # List of gtk label for group
            'gdisplay' : [], # List of gtk checkbutton for group
        }
        self.widget['spin_stiff_glass'].connect(
                                'value_changed',
                                self.on_stiff_glass_changed)
        adj = gtk.Adjustment(value=800, lower=0, upper=1600, step_incr=1, page_incr=100, page_size=0)
        self.widget['spin_save_plot_width'].set_adjustment(adj)
        adj = gtk.Adjustment(value=800, lower=0, upper=1600, step_incr=1, page_incr=100, page_size=0)
        self.widget['spin_save_plot_height'].set_adjustment(adj)
        adj = gtk.Adjustment(value=linewidth, lower=0.1, upper=5, step_incr=0.1, page_incr=1, page_size=0)
        self.widget['spin_linewidth'].set_adjustment(adj)
        
        builder.connect_signals(self)
        self.author = author
        self.comment = comment
        self.spring_cst = spring_constant
        self.inject_index = inject_index
        self.stiff_glass = stiff_glass
        self.trace_curve = trace_curve
        self.save_size = plot_size
        ### The plotting part :
        self.figure = Figure(figsize=(100, 100), dpi=75, facecolor=(0.93,0.92,0.9,1))
        self.axis = self.figure.add_subplot(111) 
        self.axis.grid(True)
        self.canvas = FigureCanvas(self.figure) # a gtk.DrawingArea 
        self.canvas.show() 
        self.graphview = self.widget['plot_area']
        self.graphview.pack_start(self.canvas, True, True)
        # below is optional if you want the navigation toolbar
        self.navToolbar = NavigationToolbar(self.canvas, self.window)
        self.navToolbar.lastDir = '/var/tmp/'
        self.widget['box_nav_tool'].pack_start(self.navToolbar)
        self.navToolbar.show()
        self.group = None
        # About the groups...
        if group is not None:
            table = builder.get_object('table_group')
            text = gtk.Label('ID')
            text.show()
            table.attach(text, 0, 1, 0, 1)
            text = gtk.Label('Label')
            text.show()
            table.attach(text, 1, 2, 0, 1)
            text = gtk.Label('Display')
            text.show()
            text = table.attach(text, 2, 3, 0, 1)
            self.group_widget = {
                'label' : [],
                'display' : []
            }
            _id_gp = 1
            for gp_id, gp_label, gp_disp in zip(
                                group['id'], group['label'], group['display']):
                self.widget['gid'].append(gp_id)
                text = gtk.Label(gp_id)
                text.show()
                table.attach(text, 0, 1, _id_gp, _id_gp+1)
                text = gtk.Entry(0)
                text.set_text(gp_label)
                text.show()
                table.attach(text, 1, 2, _id_gp, _id_gp+1)
                self.widget['glabel'].append(text)
                check = gtk.CheckButton('Show')
                check.set_active(gp_disp)
                check.show()
                table.attach(check, 2,3,_id_gp, _id_gp+1)
                self.widget['gdisplay'].append(check)
                _id_gp += 1
           #Display Options
        self.linewidth = linewidth
        self.histo_y_rel = histo_y_rel
        self.plot_hist_gauss = plot_hist_gauss
        self.plot_color = plot_color
        self.update()
    def update(self):
        '''
        Update the informations in the window.
        '''
        if self.author is not None:
            self.widget['text_author'].set_text(self.author)
        if self.comment is not None:
            text_buffer = self.widget['text_description'].get_buffer()
            text_buffer.set_text(self.comment)
        if self.spring_cst is not None:
            self.widget['entry_spring_cst'].set_text(str(self.spring_cst))
        if self.inject_index is not None:
            index_adjustment = self.widget['spin_inject_index'].get_adjustment()
            index_adjustment.set_all(value=int(self.inject_index[0]),
                                     upper=int(self.inject_index[1])
                                    )
        if self.stiff_glass is not None:
            self.on_stiff_glass_changed(None)
        if self.save_size is not None:
            self.widget['spin_save_plot_width'].set_value(self.save_size[0])
            self.widget['spin_save_plot_height'].set_value(self.save_size[1])
        if self.histo_y_rel is not None:
            self.widget['histo_y_rel'].set_active(self.histo_y_rel)
        if self.plot_hist_gauss is not None:
            self.widget['check_plot_hist_gauss'].set_active(self.plot_hist_gauss)
        if self.plot_color is not None:
            if self.plot_color is 'bw':
                self.widget['check_bw_plot'].set_active(True)
            else:
                self.widget['check_bw_plot'].set_active(False)
            
    def on_stiff_glass_changed(self, widget):
        stiff_glass_val = self.widget['spin_stiff_glass'].get_value()
        if stiff_glass_val:
            self.stiff_glass = stiff_glass_val
        stiff_glass_adjustment = self.widget['spin_stiff_glass'].get_adjustment()
        lower = self.stiff_glass*2
        if self.stiff_glass:
            n_digits = int(floor(log10(abs(self.stiff_glass))))
        if n_digits < 0:
            self.widget['spin_stiff_glass'].set_digits(-n_digits+2)
        step_increment = -lower / 100
        stiff_glass_adjustment.set_all(value=self.stiff_glass,
                                       upper=0,
                                         lower=lower,
                                         step_increment = step_increment, 
                                         page_increment = step_increment / 4, 
                                         page_size = 0)
        self.display_plot()#update_curve()
    def on_spin_save_plot_width_value_changed(self, widget):
        self.save_size[0] = widget.get_value()
        adjustment = widget.get_adjustment()
        adjustment.set_upper(widget.get_value() + 800)
    def on_spin_save_plot_height_value_changed(self, widget):
        self.save_size[1] = widget.get_value()
        adjustment = widget.get_adjustment()
        adjustment.set_upper(widget.get_value() + 800)
    def on_spin_linewidth_value_changed(self, widget):
        self.linewidth = widget.get_value()
        adjustment = widget.get_adjustment()
    def on_button_validate_clicked(self, widget):
        '''
        Get the information from the window and store them in the following
        attributes :
            self.author
            self.comment
            self.spring_constant
            self.inject_index
            self.save_size
        '''
        self.author = self.widget['text_author'].get_text()
        text_buffer = self.widget['text_description'].get_buffer()
        self.comment = text_buffer.get_text(text_buffer.get_start_iter(),
                                            text_buffer.get_end_iter())
        self.spring_cst = eval(self.widget['entry_spring_cst'].get_text())
        self.inject_index[0] = int(self.widget['spin_inject_index'].get_value())
        # Get values for groups
        self.group = {
                'id' : self.widget['gid'],
                'label' : [it.get_text() for it in self.widget['glabel']],
                'display' : [it.get_active() for it in self.widget['gdisplay']]
                    }
        #Get display options
        self.linewidth = self.widget['spin_linewidth'].get_value()
        self.histo_y_rel = self.widget['histo_y_rel'].get_active()
        self.plot_hist_gauss = self.widget['check_plot_hist_gauss'].get_active()
        if self.widget['check_bw_plot'].get_active():
            self.plot_color = 'bw'
        else:
            self.plot_color = 'color'
        self.validate()
    def on_button_cancel_clicked(self, widget):
        self.destroy(self, widget)
    def display_plot(self):
        if type(self.spring_cst) == list:
            sc = self.spring_cst[0]
        else:
            sc = self.spring_cst
        [iX, iY] = curve.compute_indentation(self.trace_curve[0],
                                             self.trace_curve[1],
                                             self.stiff_glass,
                                             sc)
        self.axis.cla()
        self.axis.grid(True)
        self.axis.plot(iX, iY, 'k-', linewidth=1.5)
        self.canvas.draw_idle()
    def destroy(self, *widget):
        '''
        Pretty quit the dialog.
        '''
        self.window.destroy()
    def validate(self):
        '''
        This function is here to be overriden by the caller in order to send the
        signal through the classes.
        '''
        pass
if __name__ == '__main__':
    group = {'id' : [0, 1, 2],
             'label' : ['First', 'Second', 'Third'],
             'display' : [True, True, True]}
    test = main_win(author='Charles Roduit', comment='I like it...',
                    spring_constant=[0.06, 0.04], inject_index=[0, 3], plot_size=[800, 800],
                    group=group,
                    _glade_filename='glade/dialog_experiment_properties.glade')
    test.window.show()
    gtk.main()
