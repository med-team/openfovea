#!/usr/bin/env python
# -*- coding: utf-8 -*-
'''
This module contains the necessary tools to plot graphics independently from the
gui.

The examples saves the result in a folder named FIGURE_FOLDER. This is a string
that contain the folder where the figure are plotted.
'''

# for script installation
from pkg_resources import resource_filename # pylint: disable-msg=E0611
import os, sys

import pdb        # pdb.set_trace()
import numpy
from scipy import stats

try:
    matplotlib.__version__
except NameError:
    import matplotlib
    matplotlib.use('Agg', warn=False)
    import matplotlib.pyplot as plt

from matplotlib.patches import Circle, Rectangle, FancyArrow
from matplotlib.widgets import Cursor
from matplotlib.ticker import NullFormatter
from mpl_toolkits.axes_grid.anchored_artists import AnchoredText
NULLFMT = NullFormatter()

from fovea_toolbox import post_proc

try:
    NO_DATA_FILENAME = resource_filename('openfovea','Icon/NoData.png')
except NotImplementedError:
    NO_DATA_FILENAME = 'openfovea/Icon/NoData.png'
try:
    NO_DATA_IMAGE = matplotlib.image.imread(NO_DATA_FILENAME)
except IOError:
    NO_DATA_FILENAME = os.path.join(os.path.dirname(sys.executable), 'openfovea/Icon/NoData.png')
    NO_DATA_IMAGE = matplotlib.image.imread(NO_DATA_FILENAME)
class Plot(object):
    """
        This is the generic plot class that will be inherited from the other.
        It has no plot capabilities, but have the standard attribute.
    """
    def __init__(self):
        self.figure = plt.figure()
        self.axis = self.figure.add_subplot(111)
        self.colors = ['blue', 'green', 'red', 'cyan', 'magenta', 'yellow',
                       'black', 'white']
        self.win_size = [8, 6]
        self.scale = ['linear', 'linear']
        self.type = None
    def save(self, filename):
        """
            Save your plot in a file.
        """
        
        self.figure.savefig(filename)
        
    def hold(self, value):
        """
            Change the hold state of the axis.
            Accepted values are :
            
                * 'On' or True to make two plots.
                * 'Off' or False to override the last plot.
        """
        
        if value in ('On', True):
            self.axis.hold(True)
        elif value in ('Off', False):
            self.axis.hold(False)
        
    def set_size(self, width, height, deep=False):
        """
            Change the size of the figure.
        """
        # Figure size is inch * dpi
        #dpi = self.figure.get_dpi()
        #inch = self.figure.get_size_inches()
        #
        ##
        # Normally, the figure size in pixel is the inch * dpi.
        # But I see that it's inch * 100... I force it to 100 and wait for explanations later.
        new_inch = [float(width) / 100, float(height) / 100]
        self.win_size = new_inch
        if deep:
            self.figure.set_size_inches(new_inch)
    def set_xscale(self, scale=None):
        if scale is not None:
            self.scale[0] = scale
        self.axis.set_xscale(self.scale[0])
    def get_type(self):
        return self.type
class Curve(Plot):
    """
        Plot a curve on a window. Stores the standard values required for the
        plot in OpenFovea so that I don't have to think about it later.
        
        >>> import numpy
        
        >>> x_data = numpy.arange(0, 10, 0.1)
        >>> y_data = numpy.sin(x_data)
        >>> y_data_2 = numpy.sin(x_data + 1)
        
        >>> test = Curve(xlabel = 'x axis', ylabel = 'y axis', title = 'plot title')
        >>> test.plot(x_data, y_data)
        >>> test.hold(True)
        >>> test.plot(x_data, y_data_2, linestyle='go')
        >>> test.save(FIGURE_FOLDER + 'plot.svg')
    """
    
    def __init__(self, xlabel='Scanner Extention [nm]',
                       ylabel='Cantilever Deflection [nm]',
                       title='Force-Distance curve',
                       linewidth=1.5):
        Plot.__init__(self)
        
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.title = title
        self.axis.hold(False)
        self.axis.set_xlabel(self.xlabel)
        self.axis.set_ylabel(self.ylabel)
        self.axis.set_title(self.title)
        self.axis.grid(True)
        self.lim = None
        self.linewidth = linewidth
        self.type = 'curve'
    def plot(self, x_data, y_data, linestyle='b-', alpha=1):
        """
            Plot your data with the specified style.
        """
        if self.lim is not None:
            # The plot has already been initialized. Then, we get the current
            # limit in order to restore it after the plot.
            self.lim['x'] = self.axis.get_xlim()
            self.lim['y'] = self.axis.get_ylim()
        self.axis.plot(x_data, y_data, linestyle, linewidth=self.linewidth, alpha=alpha)
        self.__reinitialize()
        
    def __reinitialize(self):
        """
            This is to reinitialize your plot. You don't need to manually call
            it, as it is automatically called when you do a plot.
        """
        
        self.axis.set_xlabel(self.xlabel)
        self.axis.set_ylabel(self.ylabel)
        self.axis.set_title(self.title)
#        if self.ylim is not None:
#            self.axis.set_ylim(self.ylim)
        if self.lim is not None:
            self.axis.set_xlim(self.lim['x'])
            self.axis.set_ylim(self.lim['y'])
        else:
            self.lim = {'x' : self.axis.get_xlim(),
                        'y' : self.axis.get_ylim()}
            
        self.axis.grid(True)
class ErrorBar(Curve):
    def __init__(self, xlabel='Depth [nm]',
                       ylabel='Young Modulus [Pa]',
                       title='Mean Stiffness',):
        Curve.__init__(self, xlabel, ylabel, title)
        self.axis2 = self.axis.twinx()
        self.pretty_axis()
    def average_stiffness(self, data, group=None, group_info=None, linestyle='b-', style='isol', show_count=True):
        """
            Plot the errorbar with the specified style
            
            * Parameters :
                data : list
                group : list
                group_info : list
                linestyle : str
                style : str
                    Defines the style of the average. This is pertinent only
                    with grouped data.
                    'isol' : each item of a group is isolated. The average and
                             standard deviation reflects the population
                             homogeneity. If one file is 245 ± 10 and the other
                             is 255 ± 15, the result will be 250 ± 5.
                    'merge' : Each item of a group is merged. The average and
                              standard deviation reflects the pixels
                              homogeneity. If one file is 245 ± 10 with 10
                              values, the other is 255 ± 15 with 20 values, the
                              result will be 251 ± 13
        """
        self.axis.cla()
        self.axis2.cla()
        if group is not None:
            # data is a set of several measurements each of them belonging to a
            # group
            if group_info is None:
                # no information about the groups...
                group_info = {'id' : numpy.unique(group),
                      'label' : ['group %i'%gid for gid in numpy.unique(group)],
                      'display' : [True for gid in numpy.unique(group)], 
                      }
            group = numpy.array(group)
            list_group = [group_info['id'][i] for i in numpy.nonzero(group_info['display'])[0]]
            for item in list_group:
                #pdb.set_trace()
                gindex = group_info['id'].index(item)
                index_list = numpy.nonzero(group == group_info['id'][gindex])[0]
                # We recover the data that has to be plotted
                _data = [data[i]['mean'] for i in index_list]
                _std = [data[i]['std'] for i in index_list]
                _count = [data[i]['count'] for i in index_list]
                # Here, we have the maximum depth in the files of the current group.
                _deep = [data[i]['deep'] for i in index_list]
                _tot_size = [len(item) for item in _deep]
                #_max_size = max([len(item) for item in _deep])
                deep = _deep[_tot_size.index(max(_tot_size))]
                if style == 'merge':
                    # We have to fill with 0 the arrays that are smaller.
                    _data = numpy.array([list(item) + [0] * (max(_tot_size) - len(item))
                                                            for item in _data])
                    _std = numpy.array([list(item) + [0] * (max(_tot_size) - len(item))
                                                            for item in _std])
                    _count = numpy.array([list(item) + [0] * (max(_tot_size) - len(item))
                                                            for item in _count])
                    new_mean = (_data * _count).sum(0) / _count.sum(0)
                    new_std = (_std ** 2 * (_count - 1)).sum(0) / _count.sum(0) - 1
                    new_std = new_std ** 0.5
                elif style == 'isol':
                    # We have to fill with nan the arrays that are smaller.
                    new_data = numpy.ma.array(numpy.zeros((len(_data), max(_tot_size))),
                                          mask=numpy.ones((len(_data),
                                                           max(_tot_size))))
                    new_count = numpy.zeros((len(_data), max(_tot_size)))
                    for i in range(len(_data)):
                        new_count[i,:len(_count[i])] = _count[i]
                    new_count = new_count.sum(0)
                    for i in range(len(_data)):
                        new_data[i][:len(_data[i])] = _data[i]
                        new_data[i].mask[0:len(_data[i])] = 0
                    new_mean = new_data.mean(0)
                    new_std = new_data.std(0)
#                    _data = numpy.array([list(item) + [nan] * (max(_tot_size) - len(item))
#                                                            for item in _data])
#                    _std = numpy.array([list(item) + [nan] * (max(_tot_size) - len(item))
#                                                            for item in _std])
#                    _count = numpy.array([list(item) + [nan] * (max(_tot_size) - len(item))
#                                                            for item in _count])
#                    new_mean = _data.mean(0)
#                    new_std = _data.std(0)
                self.plot(deep, new_mean, new_std, label=group_info['label'][gindex])
                if show_count:
                    self.plot_y2(deep, new_count)
        else:
            self.plot(data['deep'], data['mean'], data['std'])
            if show_count:
                self.plot_y2(data['deep'], data['count'])
        self.pretty_axis(show_y2=show_count)
    def pretty_axis(self, show_y2=False):
        yaxis = self.axis.get_yaxis()
        yaxis.set_label_position('left')
        yaxis.set_ticks_position('left')
        yaxis = self.axis2.get_yaxis()
        yaxis.set_label_position('right')
        yaxis.set_ticks_position('right')
        yaxis.set_visible(show_y2)
        
    def plot(self, data_x, data_y, y_err, label=None, linestyle='b-'):
        self.axis.hold(True)
        self.axis.errorbar(data_x, data_y, y_err, label=label)
        self.axis.hold(False)
        if label is not None:
            self.axis.legend(loc='best')
    def plot_y2(self, data_x, data_y, linestyle='--'):
        self.axis2.plot(data_x, data_y, linestyle)
        self.axis2.set_ylabel('Number of data')
        
class Array(Plot):
    """
        Plot an array. It can behaves differently corresponding to the array
        type you give.
            
        >>> import numpy
        
        >>> data = numpy.random.randn(10,10)
        >>> test = Array('Random array')
        >>> test.plot(data)
        >>> test.save(FIGURE_FOLDER + 'array_pixel.svg')
        
        If the scan size is specified, the array is recognized as nanometric
        scale image.
        
        >>> test.title = 'Random array in nanometer'
        >>> test.plot(data, scan_size=[1000, 1000])
        >>> test.save(FIGURE_FOLDER + 'array_nm.svg')
        
        A scale bar can be added to show the correlation between color and
        values.
        
        >>> test.colorbar = True
        >>> test.plot(data, scan_size=[1000, 1000])
        >>> test.save(FIGURE_FOLDER + 'array_cb.svg')
        
        If the data is changed, the colormap is updated :
        
        >>> data = data*100
        >>> test.plot(data, scan_size=[1000, 1000])
        >>> test.vmax = 140
        >>> test.save(FIGURE_FOLDER + 'array_cb100.svg')
        
        And you can plot a marker at a position given in pixel :
        
        >>> test.colorbar = False
        >>> test.plot(data, scan_size=[1000, 1000])
        >>> test.marker = (5, 4)
        >>> test.save(FIGURE_FOLDER + 'array_marker.png')
    """
    
    def __init__(self, title='Data array',):
        Plot.__init__(self)
        self.type = 'pcolor'
        # definitions for the axes
        left = 0.15
        bottom = 0.15
        ar_width = 0.7
        cb_width = 0.03
        height = 0.75
        plot_dist = 0.03
        
        left_bar = left + ar_width + plot_dist
        rect_arr = [left, bottom, ar_width, height]
        rect_cb = [left_bar, bottom, cb_width, height]
        
        self.figure.clf()
        self.axis = {'array' : self.figure.add_axes(rect_arr),
                     'colorbar' : self.figure.add_axes(rect_cb)
                     }
        ## Default values:
        self.array = None # stores the array
        self.scale = {'size' : None, # Is the size of the scan
                      'unit' : '[pixel]', # is the unit of the size
                      'array' : None, # Is the array to display the label
                      'voxel_size' : [1, 1], # The size of a pixel
                      'initialized' : False,
                      'show' : True
                      }
        self.color_scale = {'min' : None,
                            'max' : None,
                            'colorbar' : False, # Do we plot or not the
                                                # scalebar.
                            'cmap' : 'gray'}
                            
        self.__init_pmarker()

        self.path = { 'start' : None, # The start point
                      'stop' : None, # The end point
                      'list' : None, # The list of points that define the path
                      'plot' : None, # Contain the curve traced on the plot array.
                      'grab' : None, # Which point to grab = 'start' or 'stop'
                      'refresh' : None, # Contains the list of points that define the path. Is erased when used.
                      }
        #TODO Remove self.trace and self.show_trace
        self.trace = None # Contain the curve traced on the plot array.
        self.title = title

    def __init_pmarker(self):
        self.p_marker = {'display' : True,
                         'indice_x' : None,
                         'indice_y' : None,
                         'patch' : None} # matplotlib patch object to mark the
                                         # position

    def __setattr__(self, att, value):
        if att is 'colorbar':
            self.__dict__['color_scale']['colorbar'] = value
        elif att is 'cmap':
            self.__dict__['color_scale']['cmap'] = value
        elif att is 'vmin':
            self.__dict__['color_scale']['min'] = value
            if self.array is not None:
                self.plot()
        elif att is 'vmax' :
            self.__dict__['color_scale']['max'] = value
            if self.array is not None:
                self.plot()
        elif att is 'marker':
            self.__dict__['p_marker']['display'] = True
            self.__dict__['p_marker']['indice_x'] = value[0]
            self.__dict__['p_marker']['indice_y'] = value[1]
            #self.mark_pos(value)
            self.mark_pos()#plot()
        else:
            self.__dict__[att] = value
    def __getattr__(self, att):
        if att is 'colorbar':
            return self.__dict__['color_scale']['colorbar']
        else:
            return self.__dict__[att]
    def plot(self, array=None, scan_size=None):
        """
            Displays the array.
            The scan size has to be in nanometer.
        """
        ## Initialize the variables
        self.axis['array'].cla()
        self.__init_pmarker()
        if array is not None:
            self.array = array
        if type(self.array) == numpy.ma.core.MaskedArray and \
                                        numpy.all(self.array.mask.all(True)):
            self.axis['array'].imshow(NO_DATA_IMAGE)
            self.axis['array'].set_axis_off()
            self.axis['colorbar'].set_visible(False)
            return
            #raise ValueError, 'Array with only masked values.'
        if not self.scale['initialized']:
            if scan_size in [0, None]:
                scan_size = None
            elif 0 in scan_size:
                scan_size = None
            shape = self.array.shape
            self.scale['array'] = [numpy.arange(0, shape[0]+1) for i in range(2)]
            self.scale['size'] = [1 for i in range(2)]
            self.scale['unit'] =  ['[pixel]' for i in range(2)]
            self.scale['voxel_size'] = [1 for i in range(2)]
        if scan_size is not None:
            shape = self.array.shape
            if isinstance(scan_size, (int, float)):
                scan_size = [float(scan_size), float(scan_size)]
            else:
                scan_size = [float(dim) for dim in scan_size]
            for dim in range(2):
                if scan_size[dim] < 1000:
                    self.scale['unit'][dim] = '[nm]'
                    self.scale['size'][dim] = scan_size[dim]
                elif scan_size[dim] >= 1000:
                    self.scale['unit'][dim] = r'[$\mathregular{\mu}$m]'
                    self.scale['size'][dim] = scan_size[dim] / 1000.
                self.scale['array'][dim] = numpy.arange(
                                0,
                                self.scale['size'][dim] + self.scale['size'][dim] / shape[0],
                                self.scale['size'][dim] / shape[0])
                self.scale['voxel_size'][dim] = self.scale['size'][dim]/shape[dim]
            self.scale['initialized'] = True
#        if not self.scale['initialized']:
#            self.scale['array'] = [numpy.arange(0, shape[0]+1) for i in range(2)]
#            self.scale['unit'] =  ['[pixel]' for i in range(2)]
        # Plot
        self.axis['array'].set_title(self.title)
        image = self.axis['array'].pcolormesh(self.scale['array'][0],
                         self.scale['array'][1],
                         self.array.transpose(),
                         vmin=self.color_scale['min'],
                         vmax=self.color_scale['max'],
                         cmap=self.color_scale['cmap'])
        self.axis['array'].axis([0, self.scale['array'][0][-1],
                                 0, self.scale['array'][1][-1]])
        if self.scale['show']:
            self.axis['array'].set_xlabel('Size ' + self.scale['unit'][0])
            self.axis['array'].set_ylabel('Size ' + self.scale['unit'][1])
        else:
            self.axis['array'].set_xticks([])
            self.axis['array'].set_yticks([])
        if self.color_scale['colorbar']:
            self.axis['colorbar'].set_visible(True)
            self.axis['colorbar'].cla()
            self.figure.colorbar(image, self.axis['colorbar'])
        else:
            self.axis['colorbar'].set_visible(False)
        if self.p_marker['display']:
            self.mark_pos()
            
    def mark_pos(self):
        """
            Mark the position of the cursor.
        """
        if self.p_marker['indice_x'] is None:
            return
        pos_x = self.p_marker['indice_x'] * self.scale['voxel_size'][0]
        pos_y = self.p_marker['indice_y'] * self.scale['voxel_size'][1]
        if self.p_marker['patch'] is not None:
            self.p_marker['patch'].set_xy((pos_x, pos_y))
        else:
            self.p_marker['patch'] = Rectangle((pos_x, pos_y),
                                                self.scale['voxel_size'][0],
                                                self.scale['voxel_size'][1],
                                                zorder=10)
            self.p_marker['patch'].set_antialiased(True)
            self.p_marker['patch'].set_linewidth(1)
            self.p_marker['patch'].set_facecolor((1.0, 1.0, 1.0, 1.0))
            self.p_marker['patch'].set_alpha(0.7)
            self.axis['array'].hold(True)
            self.axis['array'].add_patch(self.p_marker['patch'])

    def on_gride_click(self, event):
        if event.button == 1:
            # Left mouse button ==> goto
            return [event.xdata, event.ydata]
        elif event.button == 3:
            # Right mouse button ==> slice
            posx = int(event.xdata / self.scale['voxel_size'][0])
            posy = int(event.ydata / self.scale['voxel_size'][1])
            self.set_path(posx, posy)
    def show_trace(self, trace):
        """
            Show the trace.
        """
        if self.trace is None:
            self.trace = self.axis['array'].plot(trace[0], trace[1], picker=5,
                                                 linewidth = 4, alpha=0.5)
        else:
            self.trace[0].set_data(trace)
    def set_path(self, posx, posy):
        """
            Create a path on the gride.
        """
        if self.path['start'] is None:
            self.path['start'] = [posx, posy]
        elif self.path['stop'] is None:
            self.path['stop'] = [posx, posy]
            # Create the path and show it
            self.path['list'] = post_proc.generate_path(self.path['start'],
                                                        self.path['stop'])
            self.path['refresh'] = self.path['list']
            plot_list = self.path['list']
            if self.path['plot'] is None:
                self.path['plot'] = self.axis['array'].plot(
                                    plot_list[0] * self.scale['voxel_size'][0],
                                    plot_list[1] * self.scale['voxel_size'][1],
                                    color = 'white', linewidth = 5, alpha = 0.6)
            else: 
                self.path['plot'][0].set_data(plot_list *
                                              self.scale['voxel_size'][0])
        else:
            picker = 3
            if ((posx - picker < self.path['start'][0] < posx + picker) and
                 (posy - picker < self.path['start'][1] < posy + picker)):
            #if [posx, posy] == self.path['start']:
                self.path['grab'] = 'start'
            if ((posx - picker < self.path['stop'][0] < posx + picker) and
                 (posy - picker < self.path['stop'][1] < posy + picker)):
                self.path['grab'] = 'stop'
    def get_path(self):
        """
            Get the path and erase it.
        """
        path, self.path['refresh'] = self.path['refresh'], None
        return path
    def on_button_release(self, event):
        self.path['grab'] = None
    def move_mouse(self, event):
        """
            What to do when the mouse is moved...
        """
        if not event.inaxes:
            return
        if self.path['grab'] == 'start':
            self.path['start'] = [int(event.xdata / self.scale['voxel_size'][0]),
                                  int(event.ydata / self.scale['voxel_size'][1])]
            plot_list = self.path['list']
            self.path['list'] = post_proc.generate_path(self.path['start'], self.path['stop'])
            self.path['plot'][0].set_data(plot_list * self.scale['voxel_size'][0])
            self.path['refresh'] = self.path['list']
        elif self.path['grab'] == 'stop':
            self.path['stop'] = [int(event.xdata / self.scale['voxel_size'][0]),
                                  int(event.ydata / self.scale['voxel_size'][1])]
            plot_list = self.path['list']
            self.path['list'] = post_proc.generate_path(self.path['start'], self.path['stop'])
            self.path['plot'][0].set_data(plot_list * self.scale['voxel_size'][0])
            self.path['refresh'] = self.path['list']

class TimeLapse(Plot):
    """
        Plots the time laps with a barplot displaying the average before and
        after a treatment.

        For example :
        
        >>> import numpy
        
        Let's make some difference between data before 100 and after 100
        
        Data between 0-9 belong to group "0" and between 10-14 to group "1"

        >>> test = TimeLapse()
        
        
        Let's do with DataPlot object.
        
        Our data is made of 15 data points with 10 measurments each. Relative
        stiffness is computed on 3 distances. So let's generate the final list
        like this :
        
        >>> tl_array = numpy.random.randn(15 * 10)
        >>> data = [tl_array for i in range(3)]
        
        >>> ctl_array = numpy.random.randn(15 * 10)
        >>> data_ctl = [ctl_array/(i+1) for i in range(3)]
        
        We generate the file index :
        
        >>> index = numpy.mgrid[0:15, 0:10][0]
        >>> index.shape = 15 * 10
        
        >>> group = [0 for i in range(10*10)] + [1 for i in range(5*10)]
        >>> group = [group for i in range(3)]
        >>> plot_data = PlotData()
        
        >>> plot_data.set_type('relstiff')
        >>> plot_data.set_data('relstiff', data)
        >>> plot_data.set_range('relstiff', [-5, 5, 0.1, 0])
        >>> plot_data.set_group('relstiff', group)
        >>> plot_data.set_fid('relstiff', index)
        
        And the same with the control (and let's add a group to look):
        
        >>> plot_data.set_data('relstiffctl', data_ctl)
        >>> plot_data.set_range('relstiffctl', [-5, 5, 0.1, 0])
        >>> plot_data.set_group('relstiffctl', group)
        >>> plot_data.set_fid('relstiffctl', index)
        
        >>> test.plot(plot_data)
        >>> test.save(FIGURE_FOLDER + 'PlotData_tl_group_ctl')
    """
    #TODO Clean old compatibility for vectors, all should go through DataPlot
    #objects
    def __init__(self, xlabel='Time',
                       ylabel='Stiffness',
                       title='Relative Stiffness'):
        
        Plot.__init__(self)
        self.type = 'plot-bar'
        # definitions for the axes
        #
        #       <----tl_width----> <bar_width>
        #       +----------------+ +---------+ ^
        #       |                | |         | |
        #       |   Time-Lapse   | |   Bar   | height
        #       |                | |         | |
        # left  +----------------+ +---------+ v
        #    bottom              <->
        #                     plot_dist
        
        # definitions for the axes
        left = 0.05
        bottom = 0.1
        tl_width = 0.7
        bar_width = 0.22
        height = 0.8
        plot_dist = 0
        
        left_bar = left + tl_width + plot_dist
        rect_tl = [left, bottom, tl_width, height]
        rect_hist = [left_bar, bottom, bar_width, height]
        
        self.figure = plt.figure(figsize=(10, 2))
        self.axis = {'tl' : self.figure.add_axes(rect_tl),
                     'bar' : self.figure.add_axes(rect_hist)
                     }
        self.sem = 1
    def plot(self, data, data_ctl=None, baseline=0):
        """
            Plots the data
        """
        # pylint: disable-msg=R0913
        # disable the "too many argument" warning from pylint
        
        # Defines the properties of the box plot
        box = {'size' : 0.45, # the size of the box
               'slide' : 0.4}# how far the box slides
                             # (between data and data_ctl)
                             
        ## Get the values to plot :
        if not isinstance(data, PlotData):
            raise AttributeError, "Data has to be a PlotData object."
        indice = data.get_group('relstiff')
        fileid = data.get_fid('relstiff')
        if data_ctl is None:
            fileid_ctl = data.get_fid('relstiffctl')
            data_ctl = data.get_data('relstiffctl')
            group_ctl = data.get_group('relstiffctl')
        data = data.get_data('relstiff')
        time = numpy.unique(fileid)
        
        if indice is None:
            indice = numpy.zeros(data.shape[0])
        res_tl = compute_indice_stat(data, fileid)
        res_bar = compute_indice_stat(data, indice)
        
        # Prepare the axes :
        self.axis['tl'].cla()
        self.axis['bar'].cla()
        # And plot.
        self.axis['tl'].errorbar(time, res_tl['mean'], res_tl['sem'],
                                 fmt='-', color='black')
        
        self.axis['bar'].bar(res_bar['indice'], res_bar['mean'],
                             width=box['size'], color='black', alpha=0.5)
        self.axis['bar'].errorbar(res_bar['indice'] + box['size']/2.,
                                  res_bar['mean'], res_bar['sem'],
                                  fmt=None, ecolor='k')
                                  
        if data_ctl is not None:
            # We have control to plot in dashed line :
            if isinstance(data_ctl, PlotData):
                # Another data set is given for the control.
                indice_ctl = data_ctl.get_group('relstiff')
                fileid_ctl = data_ctl.get_fid('relstiff')
                group_ctl = data_ctl.get_group('relstiff')
                data_ctl = data_ctl.get_data('relstiff')
                
            res_tl_ctl = compute_indice_stat(data_ctl, fileid_ctl)
            res_bar_ctl = compute_indice_stat(data_ctl, group_ctl)
            self.axis['tl'].hold(True)
            time = numpy.unique(fileid_ctl)
            self.axis['tl'].errorbar(time,
                                     res_tl_ctl['mean'], res_tl_ctl['sem'],
                                     fmt='--', color='gray')
            
            self.axis['bar'].bar(
                    res_bar_ctl['indice'] + box['slide'], res_bar_ctl['mean'],
                    width=box['size'], color='white', alpha=0.5)
                    
            self.axis['bar'].errorbar(
                    res_bar_ctl['indice'] + box['slide'] + box['size'] / 2,
                    res_bar_ctl['mean'], res_bar_ctl['sem'],
                    fmt=None, ecolor='k')
            self.axis['tl'].hold(False)
                    
        # Display the baseline in the time-lapse
        self.axis['tl'].hold(True)
        
        baseline = [[time[0]-0.5, time[-1]+0.5],
                    [baseline, baseline]]
        
        self.axis['tl'].plot(baseline[0], baseline[1], 'k-')
        self.axis['tl'].set_xlim(baseline[0][0], baseline[0][1])
        
        # Control the plot axis
        # 1) make y axis the same for both and do not display bar one
        # 2) change the x axis of bar to display 'Before' and 'After'
        tl_y_lim = self.axis['tl'].get_ylim()
        bar_y_lim = self.axis['bar'].get_ylim()
        
        min_y = min(tl_y_lim[0], bar_y_lim[0])
        max_y = max(tl_y_lim[1], bar_y_lim[1])
        # 1)
        self.axis['tl'].set_ylim(min_y, max_y)
        self.axis['bar'].set_ylim(min_y, max_y)
        self.axis['bar'].set_yticks([])
        # 2)
        self.axis['bar'].set_xticks(res_bar['indice'] + box['slide'] +
                                    (box['size'] - box['slide']) / 2)
        self.axis['bar'].set_xticklabels(res_bar['indice'])

class Mosaic(Plot):
    """
        Plots the mosaic of several arrays.
        
        >>> from numpy.random import randn
        >>> import pylab
        
        >>> array_list = [randn(10,10), randn(10,10), randn(15,15)]
        >>> list_name = ['first and a very long text to test it...', 'second', 'third']
        >>> test = Mosaic()
        >>> test.plot(array_list, list_name = list_name)
        >>> test.save(FIGURE_FOLDER + 'mosaic_test.png')
        
        Note that mask annotation is pickable.
    """

    def __init__(self):
        Plot.__init__(self)
        self.type = 'mosaic'
        self.figure.clf()
        self.axis = list()
        self.text_annotation = list()
        self.mask_annotation = list()
        
    
    def plot(self, list_array, title = 'Mosaic plot',
                   list_name = None, cmap = 'gray', list_mask = None):
        """
            Plot the Mosaic of arrays.
        """
        
        #self.__init__()
        size_list = len(list_array)
        nbr_line = numpy.round(numpy.sqrt(size_list))
        nbr_col = numpy.ceil(numpy.sqrt(size_list))
        # Create the list of name
        if list_name is None:
            list_name = ['Image ' + '{0:0>2}'.format(v)
                         for v in xrange(size_list)]
        # Create the list of mask
        if list_mask is None:
            list_mask = [False for i in range(size_list)]
        # Create the list of subplots
        for item in xrange(len(list_array)):
            new_array = None
            self.axis.append(self.figure.add_subplot(nbr_line, nbr_col, item+1))
            # determine what to plot
            if list_array[item] is not None:
                if list_array[item].ndim > 2:
                    # This is a 3d array, then take the first one.
                    if isinstance(list_array[item], numpy.ma.core.MaskedArray):
                        if 0 in list_array[item][:, :, 0].mask:
                            new_array = list_array[item][:, :, 0]
                        else:
                            new_array = None
                else :
                    # This is a 2d array, then take it like this.
                    new_array = list_array[item]
            # Plot it in the subplot.
            if new_array is not None:
                self.axis[item].pcolorfast(new_array.transpose(),
                                           cmap=cmap)
            else:
                self.axis[item].imshow(NO_DATA_IMAGE)
            self.axis[item].set_axis_off()
        for item in xrange(len(list_array)):
            # Annotate with the name given in the list.
            ax_box = self.axis[item].get_position().get_points()
            middle = (ax_box[0][0] + (ax_box[1][0] - ax_box[0][0]) / 2,
                      ax_box[0][1] + (ax_box[1][1] - ax_box[0][1]) / 2)
            self.text_annotation.append(
                    self.axis[-1].annotate(list_name[item],
                           xy=middle, xycoords = 'figure pixels',
                           horizontalalignment = 'center',
                           verticalalignment = 'center',
                           fontsize = 12,
                           fontweight = 'bold',
                           bbox=dict(boxstyle="round", fc="0.8", alpha=0.8),
                           visible = False
                           )
                    )
            # Annotate with 'M' if masked, or nothing if not.
            if list_mask[item]:
                mask_color = 'red'
                alpha = 0.7
            else:
                mask_color = 'green'
                alpha = 0.3
            bottom_right = (0,0)#ax_box[0][0], ax_box[0][1])
            self.mask_annotation.append(
                self.axis[item].annotate('M',
                            xy=bottom_right, xycoords = 'axes fraction',
                            horizontalalignment = 'left',
                            verticalalignment = 'bottom',
                            fontsize = 12,
                            fontweight = 'bold',
                            bbox=dict(boxstyle='round', fc=mask_color, alpha=alpha),
                            visible = True, 
                            picker=20,
                            alpha=alpha)
                )
        self.figure.suptitle(title)
        self.figure.canvas.mpl_connect('axes_enter_event', self.enter_array)
        self.figure.canvas.mpl_connect('axes_leave_event', self.leave_array)
        self.figure.canvas.mpl_connect('motion_notify_event', self.enter_array)
        #self.figure.canvas.mpl_connect('pick_event', self.on_pick)
    def index(self, event):
        """
            Get the index number of the axes and the object type where a mouse event occured.
            
            You can directly access the axis number of a mouse event :
            
            [index, otype] = mosaic.index(event)
        """
        axe = self.axis.index(event.mouseevent.inaxes)
        if self.mask_annotation.count(event.artist):
            # This is the mask artist that was clicked.
            obj = 'mask'
        return [axe, obj]
    def enter_array(self, event):
        """
            Responsible for the mouse motion event, when the cursor enter an
            axis.
        """
        
        try:
            self.text_annotation[self.axis.index(event.inaxes)].set_visible(
                                                            True)
            self.text_annotation[self.axis.index(event.inaxes)].xytext = \
                                                            (event.x, event.y)
        except ValueError:
            pass
    def leave_array(self, event):
        """
            Responsible for the mouse motion event, when the cursor leave an
            axis.
        """
        try:
            self.text_annotation[self.axis.index(event.inaxes)].set_visible(False)
        except ValueError:
            pass
        
class Histogram(Plot):
    """
        Plot the histogram of a data set.
        This is an enhanced version of the hist function of matplotlib.
        
        >>> from numpy.random import randn
        >>> data = randn(1000)
        >>> test = Histogram(xlabel='Random [AU]')
        
        There is a simple way to plot your data just by giving your data array :

        >>> test.plot(data)
        >>> test.save(FIGURE_FOLDER + 'simple_hist.png')
        
        But there is a more advanced way, which is to make an instance of the
        PlotData class, set it's properties and send it to plot :
        
        >>> plot_data = PlotData()
        >>> plot_data.set_data('force', data)
        >>> plot_data.set_range('force', [-5, 5, 0.1])
        >>> test.plot(plot_data)
        >>> test.save(FIGURE_FOLDER + 'PlotData_class_hist.png')
        
        Comparing several datasets is possible. Note that the second set of data
        does not need to set the range. The range of the first one will be used.
        
        >>> plot_data2 = PlotData()
        >>> plot_data2.set_data('force', data + 1.2)
        >>> test.plot(plot_data, plot_data2, color_type='color')
        >>> test.save(FIGURE_FOLDER + 'PlotData_cmpare_hist.png')
        
        We can also plot the kernel density estimation over the plot :
        
        >>> from fovea_toolbox.post_proc import find_gauss_fit
        >>> result = find_gauss_fit(data, 1024)
        >>> test.plot(plot_data)
        >>> test.show_fit(result)
        >>> test.save(FIGURE_FOLDER + 'PlotData_fit.png')
        
        You can also define some groups and plot the data according to the group
        they belong to.
        
        >>> from numpy.random import rand
        >>> data = randn(100)
        >>> data2 = rand(100)
        >>> data3 = randn(100) * rand(100)
        >>> merged = list(data)+list(data2)+list(data3)
        >>> group = [0 for i in range(100)] + \
                    [1 for i in range(100)] + \
                    [2 for i in range(100)]
        
        >>> plot_data = PlotData()
        >>> plot_data.set_data('force', merged)
        >>> plot_data.set_group('force', group)
        >>> plot_data.set_range('force', [-5, 5, 0.1])
        >>> test.plot(plot_data)
        >>> test.save(FIGURE_FOLDER + 'PlotData_with_group.png')
    """
    
    def __init__(self, xlabel = '...', title='Histogram', data_type='', y_rel=False):
        Plot.__init__(self)
        self.type = 'histogram'
        self.xlabel = xlabel
        self.axis2 = None
        self.title = title
        self.data = None
        self.data2 = None
        self.label, self.label2 = None, None
        self.index = None
        self.data_type = data_type
        self.max_hist = None # Stores the maximum of histogram to scale the fit.
        self.range = None # Stores the x_range of the histogram.
        self.relative = y_rel # The y axis in relative or absolute mode.
        self.text_annotation = {'list' : list(),
                                'grabbed' : None,
                                'clicked' : [0,0]}
    def plot(self, data, data2 = None, label = None, label2 = None,
                    hist_range = None, color_type = 'color', y2=False):
        """
            Plots the histogram.
        """
        frequency_factor = 1
        if isinstance(data, PlotData):
            self.range = data.range
            if self.relative and  (data.nbr_curves is not None):
                frequency_factor = 1 / float(data.nbr_curves)
            else:
                frequency_factor = 1
            group = data.group
            group_label = data.get_group_prop('label')
            group_visible = data.get_group_prop('display')
            group_id = data.get_group_prop('id')
            data = data.data
        else:
            group = [0 for i in range(len(data))]
            group_label = None
            group_visible = [True]
            group_id = [0]
            self.range = hist_range
        if isinstance(data2, PlotData):
            if self.relative and (data2.nbr_curves is not None):
                frequency_factor2 = 1 / float(data2.nbr_curves)
            else:
                frequency_factor2 = 1
            group2 = data2.group
            data2 = data2.data
        elif data2 is not None:
            group2 = [0 for i in range(len(data2))]
            frequency_factor2 = 1
        group = numpy.asanyarray(group)
        data = numpy.asanyarray(data)
        index = numpy.isfinite(data)
        self.label = label
        self.label2 = label2
        if self.label is None:
            self.label = '...'
        if self.label2 is None:
            self.label = '...'
        if color_type == 'color':
            color = ['blue', 'red']
            edgecolor = ['black', 'black']
        elif color_type == 'bw':
            color = ['black', 'white']
            edgecolor = ['black', 'black']
        group = group[index]
        list_group = [group_id[i] for i in numpy.nonzero(group_visible)[0]]
        #list_group = numpy.unique(group)
        self.data = data[index]
        if data2 is not None:
            group2 = numpy.asanyarray(group2)
            data2 = numpy.asanyarray(data2)
            index = numpy.isfinite(data2)
            self.data2 = data2[index]
        self.axis.cla()
        self.axis.hold(True)
        if self.axis2 is not None:
            self.axis2.cla()
            self.axis2.hold(True)
            self.axis.cla()
            if data2 is None:
                self.axis2.yaxis.set_visible(False)
            else:
                self.axis2.yaxis.set_visible(True)
        if self.range == None:
            if data2 is None:
                for indice in range(group.min(), group.max()+1):
                    pl_x = self.axis.hist(self.data[group==indice], alpha=0.5)
            else:
                pl_x = self.axis.hist(self.data)
            if data2 is not None:
                pl_x2 = self.axis.hist(self.data2, alpha=0.5)
        else:
            index = (self.range[0] < self.data) * (self.data < self.range[1])
            if data2 is None:
                _i = 0
                for item in list_group:
                    indice = index * (group == item)
                    pl_x = numpy.histogram(self.data[indice],
                                      numpy.arange(self.range[0],
                                                   self.range[1],
                                                   self.range[2]))
                    pl_x = (pl_x[0] * frequency_factor, pl_x[1])
                    self.axis.bar(pl_x[1][:-1],
                                  pl_x[0],
                                  color=self.colors[_i],
                                  width=self.range[2],
                                  label=group_label[item],#'Group ' + str(item),
                                  alpha=0.5)
                    _i += 1
            else:
                pl_x = numpy.histogram(self.data[index],
                                      numpy.arange(self.range[0],
                                                   self.range[1],
                                                   self.range[2]))
                pl_x = (pl_x[0] * frequency_factor, pl_x[1])
                
                if y2:
                    if self.axis2 is None:
                        self.axis2 = self.axis.twinx()
                    axis = self.axis2
                    self.axis.yaxis.tick_left()
                    axis.yaxis.tick_right()
                else:
                    axis = self.axis
                bar1 = self.axis.bar(pl_x[1][:-1],
                                  pl_x[0],
                                  width=self.range[2],
                                  label=self.label,
                                  alpha=0.5,
                                  color=color[0],
                                  edgecolor=edgecolor[0])
                
                index = ((self.range[0] < self.data2) *
                        (self.data2 < self.range[1]))
                self.data2 = self.data2[index]
                
                
                pl_x2 = numpy.histogram(self.data2,
                                       numpy.arange(self.range[0],
                                                   self.range[1],
                                                   self.range[2]))
                pl_x2 = (pl_x2[0] * frequency_factor2, pl_x2[1])
                bar2 = axis.bar(pl_x2[1][:-1],
                                  pl_x2[0],
                                  width=self.range[2],
                                  label=self.label2,
                                  alpha=0.5,
                                  color=color[1],
                                  edgecolor=edgecolor[1])
                __max = max(max(pl_x[0]), max(pl_x2[0]))
                __max = __max + __max*5/100.
                if not y2:
                    self.axis.set_ylim(0, __max)
                #if label2 is not None:
        if len(list_group) > 1 or data2 is not None:
            if y2:
                leg = plt.legend((bar1[0], bar2[0]),
                                 (self.label, self.label2),
                                 'best')
                self.axis2.set_ylabel(self.label2)
                self.axis.set_ylabel(self.label)
            else:
                leg = self.axis.legend(loc='best')
            if leg is not None:
                # If there is a legend... otherwise, make nothing.
                frame  = leg.get_frame()
                frame.set_alpha(0.4)
        if len(pl_x[0]):
            self.max_hist = max(pl_x[0])
        else :
            self.max_hist = self.range[1]
        self.axis.set_xlabel(self.xlabel)
        if self.range is not None:
            self.axis.set_xlim(self.range[0], self.range[1])

    def show_fit(self, prop):
        self.text_annotation['list'] = list()
        prop['kde'][1] = (prop['kde'][1] * self.max_hist /
                          max(prop['kde'][1]) * 0.9)
        prop['maxima'][1] = (prop['maxima'][1] * self.max_hist /
                             max(prop['maxima'][1]) * 0.9)
        self.axis.plot(prop['kde'][0], prop['kde'][1], linewidth = 1.5)
        for [p_x, p_y, mean, std, count] in zip(prop['maxima'][0],
                                            prop['maxima'][1],
                                            prop['mean'],
                                            prop['std'],
                                            prop['count']):
                                            
            text = "Mean : " + str(round(mean, 4)) + "\n" +\
                   "Std : " + str(round(std, 4)) + "\n" +\
                   "Count : " + str(count)
            self.text_annotation['list'].append(
                self.axis.annotate("Peak : " + str(round(p_x, 4)) + "\n" + text,
                                xy=(p_x, p_y), xytext=(p_x, p_y + p_y/20),
                                textcoords='data',
                                arrowprops=dict(arrowstyle="->",
                                                connectionstyle="arc3"),
                                                bbox=dict(boxstyle="round",
                                                          fc="0.8",
                                                          alpha=0.5)
                                )
                                )
        if self.range is not None:
            self.axis.set_xlim(self.range[0], self.range[1])
        self.figure.canvas.mpl_connect('button_press_event',
                                        self.grab_annotation)
        self.figure.canvas.mpl_connect('button_release_event',
                                        self.release_annotation)
        self.figure.canvas.mpl_connect('motion_notify_event',
                                        self.move_annotation)
    def grab_annotation(self, event):
        for item in self.text_annotation['list']:
            text_pos = item.get_position()
            bbox = item.get_bbox_patch()
            if text_pos[0] < event.x < text_pos[0] + bbox.get_width():
                if text_pos[1] < event.y < text_pos[1] + bbox.get_height():
                    self.text_annotation['grabbed'] = item
                    if event.inaxes:
                        self.text_annotation['clicked'] = [
                                                   event.xdata - item.xytext[0],
                                                   event.ydata - item.xytext[1]]
                    else:
                        self.text_annotation['clicked'] = [0,0]
    def release_annotation(self, event):
        self.text_annotation['grabbed'] = None
    def move_annotation(self, event):
        #print "...", self.text_annotation['grabbed']
        if self.text_annotation['grabbed'] is not None:
            try:
                if event.inaxes:
                    pos_x = event.xdata - self.text_annotation['clicked'][0]
                    pos_y = event.ydata - self.text_annotation['clicked'][1]
                    self.text_annotation['grabbed'].xytext = (pos_x , pos_y)
            except TypeError:
                pass
class Scatter(Plot):
    """
        Displays a scatter plot of the data provided.
        The data are PlotData objects.
        
        >>> from numpy.random import randn
        >>> data = randn(1000)
        >>> data2 = randn(1000)*2 + 1.5
        
        >>> plot_data = PlotData()
        >>> plot_data.set_data('force', data)
        >>> plot_data.set_data('lr', data2)
        >>> plot_data.set_range('force', [-5, 5, 0.1])
        >>> plot_data.set_range('lr', [-5, 10, 0.2])
        >>> plot_data.set_group_prop('all', {'id': [0], 'label' : ['0'], 'display' : [True]})
        
        >>> test = Scatter(xlabel='data1', ylabel='data2', title='test of scatter')
        >>> #print plot_data.get_data('force')
        >>> test.plot(plot_data, ['force', 'lr'])
        >>> test.save(FIGURE_FOLDER + 'PlotData_Scatter.png')
        >>> test.type = "Density"
        >>> test.refresh()
        >>> test.save(FIGURE_FOLDER + 'PlotData_Density.png')
        
        You can also define some groups and plot the data according to the group
        they belong to.
        
        >>> from numpy.random import rand
        >>> data = randn(100)
        >>> data2 = rand(100)
        >>> data3 = randn(100) * rand(100)
        >>> merged = list(data)+list(data2)+list(data3)
        >>> group = [0 for i in range(100)] + \
                    [1 for i in range(100)] + \
                    [2 for i in range(100)]
        
        >>> data4 = randn(100)
        >>> data5 = rand(100)
        >>> data6 = randn(100) * rand(100)
        >>> merged2 = list(data4)+list(data5)+list(data6)
        >>> group2 = [0 for i in range(100)] + \
                    [1 for i in range(100)] + \
                    [2 for i in range(100)]
        
        >>> plot_data = PlotData()
        
        >>> plot_data.set_data('force', merged)
        >>> plot_data.set_group('force', group)
        >>> plot_data.set_range('force', [-5, 5, 0.1])
        
        >>> plot_data.set_data('lr', merged2)
        >>> plot_data.set_group('lr', group2)
        >>> plot_data.set_range('lr', [-5, 5, 0.1])
        >>> plot_data.set_group_prop('all', {'id': [0], 'label' : ['0'], 'display' : [True]})
        
        >>> test = Scatter(xlabel='data1', ylabel='data2', title='test of scatter')
        >>> test.plot(plot_data, ['force', 'lr'])
        >>> test.save(FIGURE_FOLDER + 'PlotData_Scatter_with_group.png')
    """
    
    def __init__(self, xlabel='...', ylabel='...', title=''):
        Plot.__init__(self)
        self.xlabel = xlabel
        self.ylabel = ylabel
        self.type = 'scatter'
        self.markers = ['o', # circle
                        's', # square
                        '^', # triangle up
                        '>', # triangle right
                        'v', # triangle down
                        '<', # triangle left
                        'd', # diamond
                        'p', # pentagram
                        'h', # hexagon
                        '8', # octagon
                        '+', # plus
                        'x', # cross
                        ]
        self.data_x, self.data_y = None, None # Store the variables
        self.range_x, self.range_y = None, None # Store the ranges
        self.group_x, self.group_y = None, None # Store the groups
        
        # definitions for the axes
        
        #                          h-size
        #       <-----width------> <---->
        #       +----------------+          ^
        #       |  hist_x        |          |
        #       |                |          | h-size
        #       +----------------+          v
        #       +----------------+ +----+   ^
        #       |                | | h  |   |
        #       |                | | i  |   |
        #       |   Scatter      | | s  |   |
        #       |                | | t  |   |
        #       |                | | -  |   |
        #       |                | | y  |   |
        # left  +----------------+ +----+   v
        #    bottom              <->
        #                     plot_dist
        left, width = 0.1, 0.65
        bottom, height = 0.1, 0.65
        h_size = 0.2
        bottom_h = left_h = left + width + 0.02

        rect_scatter = [left, bottom, width, height]
        rect_histx = [left, bottom_h, width, h_size]
        rect_histy = [left_h, bottom, h_size, height]
        
        self.figure.clf()
        
        self.axis_scatter = self.figure.add_axes(rect_scatter)
        self.axis_histx = self.figure.add_axes(rect_histx)
        self.axis_histy = self.figure.add_axes(rect_histy)
        self.clear()
    def clear(self):
        """
            Reinitialize the plots.
        """
        
        self.axis_histx.cla()
        self.axis_histy.cla()
        # no labels
        self.axis_histx.xaxis.set_major_formatter(NULLFMT)
        self.axis_histy.yaxis.set_major_formatter(NULLFMT)
    def plot(self, data, data_type):
        """
            Plot the data.
            The data has to be a PlotData object.
            data_type is a list of two string representing the data to plot.
        """
        self.clear()
        self.data_x = numpy.asarray(data.get_data(data_type[0]))
        self.data_y = numpy.asarray(data.get_data(data_type[1]))
        self.range_x = data.get_range(data_type[0])
        self.range_y = data.get_range(data_type[1])
        self.group_x = data.get_group(data_type[0])
        self.group_y = data.get_group(data_type[1])
        self.group_prop = { 'id' : data.get_group_prop('id'),
                            'label' : data.get_group_prop('label'),
                            'display' : data.get_group_prop('display')}
        if self.type == 'scatter':
            self.__scatter()
        elif self.type == 'density':
            self.__density()
        else:
            return
            
        x_index = ((self.range_x.min < self.data_x) *
                   (self.data_x < self.range_x.max))
        y_index = ((self.range_y.min < self.data_y) *
                   (self.data_y < self.range_y.max))
        self.axis_histx.hist(self.data_x[x_index],
                                 numpy.arange(self.range_x.min,
                                              self.range_x.max,
                                              self.range_x.bin_size)
                                 )
        self.set_xscale(plot='hist')
        self.axis_histy.hist(self.data_y[y_index],
                                 numpy.arange(self.range_y.min,
                                              self.range_y.max,
                                              self.range_y.bin_size),
                                 orientation='horizontal'
                                 )
    def refresh(self):
        """
            Refresh the graph without changing the data. Used to toogle between
            scatter and density plot.
        """
        
        if self.type == 'scatter':
            self.__scatter()
        elif self.type == 'density':
            self.__density()
        else:
            return
        
    def __scatter(self):
        """
            Specific code to display the scatter plot.
        """
        
        #list_group = numpy.unique(self.group_x)
        list_group = [i for i in numpy.nonzero(self.group_prop['display'])[0]]
        list_group = numpy.asarray(list_group)
        if len(list_group) > 1:
            use_group = 1
            x_g_index = [self.group_x == i for i in list_group]
            y_g_index = [self.group_y == i for i in list_group]
        else:
            use_group = 0
        self.axis_scatter.cla()
        if use_group:
            for i in list_group:
                try:
                    index = list(list_group).index(i)
                    self.axis_scatter.scatter(self.data_x[x_g_index[index]],
                                          self.data_y[y_g_index[index]],
                                          label=self.group_prop['label'][i],
                                          #label='Group ' + str(i),
                                          c=self.colors[index],
                                          marker=self.markers[index],
                                          alpha=0.5)
                except ValueError, msg:
                    if msg.args[0] == \
                             'zero-size array to ufunc.reduce without identity':
                        pass
                    else:
                        raise ValueError, msg
                else:
                    leg = self.axis_scatter.legend(loc='best')
                    frame  = leg.get_frame()
                    frame.set_alpha(0.4)
        else:
            self.axis_scatter.scatter(self.data_x, self.data_y, alpha=0.5)
        self.axis_scatter.set_xlabel(self.xlabel)
        self.axis_scatter.set_ylabel(self.ylabel)
        self.set_xscale(plot='scatter')
        self.axis_scatter.axis([self.range_x.min, self.range_x.max,
                                self.range_y.min, self.range_y.max])
        
    def __density(self):
        """
            Specific code to display the density plot.
        """
        
        self.axis_scatter.cla()
        ## make the density plot
        # Beginn...
        finite = numpy.isfinite(self.data_x) * numpy.isfinite(self.data_y)
        in_range = ((self.data_x < self.range_x.max) *
                    (self.data_x > self.range_x.min) *
                    (self.data_y < self.range_y.max) *
                    (self.data_y > self.range_y.min))
        index = finite * in_range
        
        vect_x = ((self.data_x - self.range_x.min) /
             (self.range_x.max - self.range_x.min)) * 128
            
        vect_y = ((self.data_y - self.range_y.min) /
             (self.range_y.max - self.range_y.min)) * 128
             
        lin_vect = numpy.asanyarray([vect_x[index], vect_y[index]])
        kde = stats.kde.gaussian_kde(lin_vect)
        # Regular grid to evaluate kde upon
        mesh = numpy.mgrid[0:128, 0: 128]
        grid_coords = numpy.append(mesh[0].reshape(-1, 1),
                                    mesh[1].reshape(-1, 1),
                                    axis=1)
        arr_z = kde(grid_coords.T)
        arr_z = arr_z.reshape(128, 128)
        self.axis_scatter.imshow(arr_z.transpose(1, 0),
                             origin='lower',
                             extent=(self.range_x.min,
                                     self.range_x.max,
                                     self.range_y.min,
                                     self.range_y.max),
                             aspect='auto')
        self.axis_scatter.set_xlabel(self.xlabel)
        self.axis_scatter.set_ylabel(self.ylabel)
        #self.set_xscale()
    def set_xscale(self, scale=None, plot=None):
        if scale is not None:
            self.scale[0] = scale
        if plot == 'scatter':
            self.axis_scatter.set_xscale(self.scale[0])
            self.axis_scatter.axis([self.range_x.min, self.range_x.max,
                                    self.range_y.min, self.range_y.max])
        elif plot == 'hist':
            self.axis_histx.set_xscale(self.scale[0])
            self.axis_histx.set_xlim(self.range_x.min, self.range_x.max)
class Tomo(Plot):
    """
        Plot the interior of a 3D array.
        
        >>> from numpy.random import randn
        >>> data = randn(64, 64, 100)
        
        Consider that the data are recorded on a square of 2µm per 2µm
        
        >>> test = Tomo(data, [2000, 2000])
        >>> test.plot([20, 10, 50]) # Let's plot the position x=20, y=10, z=50
        >>> test.save(FIGURE_FOLDER + 'Tomography.png')
    """
    
    def __init__(self, tomo_array, scan_size=None):
        Plot.__init__(self)
        self.type='tomography'
        # Defines the axes :
        #                         plot_dist
        #                            <->
        #           <-----width------> <-----width------>
        #           +----------------+                      ^
        #           |                |                      |
        #           |                |                      |
        #           |   display Y    |                      | Height
        #           |                |                      |
        #           |                |                      |
        #           |                |                      | ^
        # bottom_2  +----------------+                      v plot_dist
        #           +----------------+ +----------------+   ^ v
        #           |                | |                |   |
        #           |                | |                |   |
        #           |   display X    | | display Z      |   | Height
        #           |                | |                |   |
        #           |                | |                |   |
        #           |                | |                |   |
        # bottom    +----------------+ +----------------+   v
        #                              ^
        #          left                L left_2
        
        width, height = 0.35, 0.35
        left, bottom = 0.1, 0.1
        plot_dist = 0.1
        bottom_2 = bottom + width + plot_dist
        left_2 = left + height + plot_dist

        rect = {'display_x' : [left, bottom, width, height],
                'display_y' : [left, bottom_2, width, height],
                'display_z' : [left_2, bottom, width, height]}
        self.figure.clf()
        self.axis = dict()
        self.cursor = dict()
        for key, rectangle in zip(rect.keys(), rect.values()):
            self.axis[key] = self.figure.add_axes(rectangle)
            self.cursor[key] = Cursor(self.axis[key], useblit=True,
                                     color='green', alpha=0.3, linewidth=2)
        
        ## Define the labels
        self.unit = ['', '', '']
        self.scale = {'x' : None,
                      'y' : None,
                      'z' : None,
                      'scan_size' : None}
        self.pixel_size = [None, None]
        self.__set_label(tomo_array.shape, scan_size)
        
        ##
        self.tomo_array = tomo_array
        self.slice = [0, 0, 0] # the current slice [x, y, z]
        self.colorScaleMax = self.tomo_array.max()
        self.colorScaleMin = 0
        
    def plot(self, position, color_scale_min=None, color_scale_max=None):
        if color_scale_min is not None:
            self.colorScaleMin = color_scale_min
        if color_scale_max is not None:
            self.colorScaleMax = color_scale_max
        self.slice = position
        self.plot_x()
        self.plot_y()
        self.plot_z()
    def plot_x(self):
        '''
        Refresh the x slice plot
        '''
        self.axis['display_x'].cla()
        self.axis['display_x'].pcolor(
                                self.scale['y'], self.scale['z'],
                                self.tomo_array[self.slice[0],
                                                :,
                                                :].transpose(),
                                vmin = self.colorScaleMin,
                                vmax = self.colorScaleMax)
        self.axis['display_x'].set_xlabel('Size' + self.unit[0])
    def plot_y(self):
        '''
        Refresh the y slice plot
        '''
        self.axis['display_y'].cla()
        self.axis['display_y'].pcolor(
                                self.scale['x'], self.scale['z'],
                                self.tomo_array[:,
                                                self.slice[1],
                                                :].transpose(),
                                vmin = self.colorScaleMin,
                                vmax = self.colorScaleMax)
        self.axis['display_y'].set_xlabel('Size' + self.unit[1])
    def plot_z(self):
        '''
        Refresh the z slice plot
        '''
        self.axis['display_z'].cla()
        self.axis['display_z'].pcolor(
                                self.scale['x'], self.scale['y'],
                                self.tomo_array[:,
                                                :,
                                                self.slice[2]].transpose(),
                                vmin = self.colorScaleMin,
                                vmax = self.colorScaleMax)
        self.axis['display_z'].set_xlabel('Size ' + self.unit[2])
        self.axis['display_z'].set_ylabel('Size ' + self.unit[2])
    def __set_label(self, shape, scan_size=None):
        _scan_size = list(scan_size)
        if scan_size is not None:
            for dim in range(2):
                if scan_size[dim] < 1000:
                    self.unit[dim] = '[nm]'
                elif scan_size[dim] >= 1000:
                    self.unit[dim] = r'[$\mathregular{\mu}$m]'
                    _scan_size[dim] = _scan_size[dim] / 1000.
            self.scale['x'] = numpy.arange(0,
                                           _scan_size[0] + _scan_size[0] / shape[0],
                                           _scan_size[0] / shape[0])
            self.scale['y'] = numpy.arange(0,
                                           _scan_size[1] + _scan_size[1] / shape[1],
                                           _scan_size[1] / shape[1])
            self.scale['z'] = numpy.arange(0, shape[2])
            self.pixel_size = [_scan_size[0] / shape[0], _scan_size[1] / shape[1]]
        else:
            self.scale['x'] = numpy.arange(0, shape[0])
            self.scale['y'] = numpy.arange(0, shape[1])
            self.scale['z'] = numpy.arange(0, shape[2])
            self.pixel_size = [1,1]

class InteractiveLinePlot(Plot):
    """
        Plots several lines in as many plots as the number of lines.
        
        * Parameters :
            array_x : array_like
                The x array that is common for all plots.
            arrays : list of array_like
                The y arrays that defines the plots.
            label : list of str
                The label that will be display to identify the plots.
        
        * Output :
        
        >>> import numpy
        >>> array_x = numpy.arange(0, 6, 0.1)
        >>> array_1 = array_x * 2
        >>> array_2 = numpy.sin(array_x)
        >>> array_3 = numpy.cos(array_x)
        >>> test = InteractiveLinePlot()
        >>> test.plot(array_x, [array_1, array_2, array_3],\
                               ['linear', 'cos', 'sin'])
    """
    
    def __init__(self):
        Plot.__init__(self)
        self.figure.clf()
        
        self.figure.canvas.mpl_connect('button_press_event',
                                       self.button_press_event)
        self.figure.canvas.mpl_connect('motion_notify_event',
                                       self.mouse_move)
        self.figure.canvas.mpl_connect('button_release_event',
                                                    self.button_release_event)
        self.figure.canvas.mpl_connect('pick_event', self.on_pick)
        self.array_x = None
        self.arrays_y = None
        self.picked = None
        self.info = None
        self.plot_list = None
    def button_press_event(self, event):
        """
            Function called when user click on the figure.
            It hides visible graph we click on or display hidden graph we click
            on.
        """
        
        if event.inaxes in self.axis:
            if event.button == 3:
                
                # right click
                modify = self.axis.index(event.inaxes)
                self.toggle_display(modify)
            elif event.button == 1:
                self.info_display(event)
    def button_release_event(self, event):
        self.picked = None
    def toggle_display(self, axis_nbr):
        """
            Display or hide the axis.
        """
        
        if self.axis[axis_nbr].get_visible():
            # We hide this subplot and push all ones below to replace it.
            # The hidden plot goes to the end of the display.
            self.axis[axis_nbr].set_visible(False)
            cur = self.axis[axis_nbr].get_geometry()
            for item in self.axis:
                tgeo = item.get_geometry()
                if tgeo[2] > cur[2]:
                    item.change_geometry(tgeo[0], tgeo[1], tgeo[2]-1)
            
            self.axis[axis_nbr].change_geometry(cur[0], cur[1], 
                                              len(self.axis))
        else:
            self.axis[axis_nbr].set_visible(True)
    def plot(self, array_x, arrays, label):
        """
            Fuction called to display the graph.
        """
        self.figure.clf()
        self.axis = []
        self.array_x = array_x
        self.arrays_y = arrays
        self.arrows = {'begin' : [None for i in range(len(arrays))],
                       'end' : [None for i in range(len(arrays))]}
        tot_nbr = len(arrays)
        self.info = []
        for nbr in range(1, len(arrays)+1):
            self.axis.append(self.figure.add_subplot(
                                int(str(tot_nbr) + str(1) + str(nbr))
                                ))
            self.info.append(
                        self.axis[nbr-1].annotate('Right click on axes to get info',
                               xy=[1.05, 0.7], xycoords = 'axes fraction',
                               horizontalalignment = 'left',
                               verticalalignment = 'center',
                               fontsize = 10,
                               #fontweight = 'bold',
                               bbox=dict(boxstyle="round", fc="0.8", alpha=0.8),
                               visible = True
                               )
                        )
        self.plot_list = list()
        for array, axis, label in zip(arrays, self.axis, label):
            self.plot_list.append(axis.plot(array_x, array))
            at = AnchoredText(label, loc=9, prop=None, pad=0., borderpad=0.5,
                              frameon=False)
            axis.add_artist(at)
        self.figure.subplots_adjust(left=0.05, right=0.6)
    def modify_plot(self, array_x, arrays):
        self.array_x = array_x
        self.arrays_y = arrays
        for plots, array_y, axis in zip(self.plot_list, arrays, self.axis):
            plots[0].set_data(array_x, array_y)
            axis.set_xlim(min(array_x), max(array_x))
            axis.set_ylim(min(array_y), max(array_y))
    def info_display(self, event):
        axis_nbr = self.axis.index(event.inaxes)
        index = numpy.nonzero(self.array_x >= event.xdata)[0][0]
        pos = [self.array_x[index], self.arrays_y[axis_nbr][index]]
        if self.arrows['begin'][axis_nbr] == None:
            self.create_arrow(index, 'begin')
        if self.arrows['end'][axis_nbr] == None:
            self.create_arrow(index, 'end')
        else:
            self.move_arrow(index)
            self.get_info()
    def create_arrow(self, index, side):
        for axis_nbr in range(len(self.axis)):
            lim = self.axis[axis_nbr].get_ylim()
            length = (lim[1]-lim[0]) / 10.
            lim = self.axis[axis_nbr].get_xlim()
            width =  (lim[1]-lim[0]) / 50.
            if type(self.arrays_y[axis_nbr][0]) == numpy.ma.core.MaskedArray:
                pos = [self.array_x[index], self.arrays_y[axis_nbr][0][index]]
            else:
                pos = [self.array_x[index], self.arrays_y[axis_nbr][index]]
            arrows = [FancyArrow(pos[0], pos[1] + length,
                                    0, -length,
                                    length_includes_head=True,
                                    head_length=length, head_width=None,
                                    width=width, picker=5),
                        FancyArrow(pos[0], pos[1] - length,
                                     0, length,
                                     length_includes_head=True,
                                     head_length=length, head_width=None,
                                     width=width, picker=5)]
            self.arrows[side][axis_nbr] = arrows
            self.axis[axis_nbr].add_patch(self.arrows[side][axis_nbr][0])
            self.axis[axis_nbr].add_patch(self.arrows[side][axis_nbr][1])
    def move_arrow(self, index):
        if self.picked == None:
            return
        for axis_nbr in range(len(self.axis)):
            if type(self.arrays_y[axis_nbr][0]) == numpy.ma.core.MaskedArray:
                pos = [self.array_x[index], self.arrays_y[axis_nbr][0][index]]
            else:
                pos = [self.array_x[index], self.arrays_y[axis_nbr][index]]
            init_pos = self.arrows[self.picked][axis_nbr][0].get_xy()
            diff = [pos[0] - init_pos[0][0], pos[1] - init_pos[0][1]]
            init_pos += diff
            init_pos = self.arrows[self.picked][axis_nbr][1].get_xy()
            init_pos += diff
    def mouse_move(self, event):
        if not event.inaxes:
            return
        if event.button == 1:
            self.info_display(event)
    def on_pick(self, event):
        nbr_graph = len(self.axis)
        for gnb in range(nbr_graph):
            res = (self.arrows['begin'][gnb][0] == event.artist) + (self.arrows['begin'][gnb][1] == event.artist)
            if res:
                self.picked = 'begin'
            res = False
            try:
                res = (self.arrows['end'][gnb][0] == event.artist) + (self.arrows['end'][gnb][1] == event.artist)
            except:
                pass
            if res:
                self.picked = 'end'
    def get_info(self):
        xdata = [self.arrows['begin'][0][0].get_xy()[0][0],
                 self.arrows['end'][0][0].get_xy()[0][0]]
        indexes = [numpy.nonzero(self.array_x >= xdata[0])[0][0],
                   numpy.nonzero(self.array_x >= xdata[1])[0][0]]
        for axis in range(len(self.axis)):
            text = self.create_info_text(axis, indexes)
            self.info[axis].set_text(text)
    def create_info_text(self, axis_nbr, indexes):
        if type(self.arrays_y[axis_nbr][0]) == numpy.ma.core.MaskedArray:
            pos = [[self.array_x[indexes[0]], self.arrays_y[axis_nbr][0][indexes[0]]],
                   [self.array_x[indexes[1]], self.arrays_y[axis_nbr][0][indexes[1]]]]
        else:
            pos = [[self.array_x[indexes[0]], self.arrays_y[axis_nbr][indexes[0]]],
                   [self.array_x[indexes[1]], self.arrays_y[axis_nbr][indexes[1]]]]

        text = "Start Point : %.2f, %.2f\n"%(pos[0][0], pos[0][1]) +\
               "End Point : %.2f, %.2f\n"%(pos[1][0], pos[1][1]) +\
               "Horizontal distance : %.2f\n"%(pos[1][0]-pos[0][0]) +\
               "Vertical distance : %.2f"%(pos[1][1]-pos[0][1])
        return text

class PlotData(object):
    """
        Stores the data and the propeties specific to this data set.
    """
    def __init__(self):
        self.group_prop = {'id' : [],
                           'label' : [],
                           'display' : [],
                           'set' : False}
        self.group_dict = dict()
        self.data_dict = dict()
        self.range_dict = dict()
        self.fileid_dict = dict()
        self.coord_dict = dict()
        for item in PLOT_DATA:
            prop = PLOT_DATA[item]
            self.group_dict[item] = prop[0]
            self.data_dict[item] = prop[1]
            self.range_dict[item] = prop[2]
            self.fileid_dict[item] = prop[3]
            self.coord_dict[item] = prop[4]
        self.label = None
        self.nbr_curves = None
        self.type = 'force'

    def __getattr__(self, att):# pylint disable-msg=R0911
        if att is 'data':
            if self.type in PLOT_DATA_TYPE['simple']:
                return self.data_dict[self.type]
            elif self.type in PLOT_DATA_TYPE['dist']:
                return self.data_dict[self.type][
                                                self.range_dict[self.type].dist]
            
        elif att is 'range':
            return self.range_dict[self.type].range
        elif att is 'group':
            if self.type in PLOT_DATA_TYPE['simple']:
                return self.group_dict[self.type]
            elif self.type in PLOT_DATA_TYPE['dist']:
                return self.group_dict[self.type][
                                                self.range_dict[self.type].dist]
    def get_group_prop(self, gtype):
        """
            Get the group property.
                Parameters:
                    gtype : str
                        'id'
                        'label'
                        'display'
        """
        if self.group_prop['set']:
            return self.group_prop[gtype]
        else:
            if gtype == 'id':
                return numpy.unique(self.group)
            elif gtype == 'label':
                return ["Groupe %i"%numpy.unique(self.group)[0]]
            elif gtype == 'display':
                return [True]
            else:
                raise TypeError, "%s is not recognized attribute."
    def set_group_prop(self, gtype, data):
        """
            Set the group property.
                Parameter:
                    gtype : str
                    data : list
        """
        if gtype == 'all':
            if data is not None:
                self.group_prop = data
                self.group_prop['set'] = True
            else:
                self.group_prop = {'id' : [],
                                   'label' : [],
                                   'display' : [],
                                   'set' : False}
        else:
            self.group_prop[gtype] = data
    def set_range(self, data_type, d_range):
        """
            Set the range of the data to plot.
        """
        if data_type in PLOT_DATA.keys():
            self.range_dict[data_type].set_range(d_range)
        else:
            raise TypeError, "Data type %s not known." % data_type
    def set_dist(self, dist):
        """
            Set the distance at which we want to get the relative values.
        """
        for data_type in PLOT_DATA_TYPE['dist']:
            self.range_dict[data_type].dist = dist
    def set_data(self, data_type, data):
        """
            Store the data to plot.
        """
        if type(data) is list:
            if data_type in PLOT_DATA_TYPE['dist']:
                data = [numpy.array(i) for i in data]
            else:
                data = numpy.asanyarray(data)
        if data_type in PLOT_DATA.keys():
            self.data_dict[data_type] = data
            if self.group_dict[data_type] is None:
                # Group is not related to current data, then reinitialize it.
                if data_type in PLOT_DATA_TYPE['dist']:
                    this_shape = [len(i) for i in data]
                    group = []
                    for item in this_shape:
                        group.append([0 for this in range(item)])
                    self.set_group(data_type, group)
                else:
                    self.set_group(data_type, [0 for i in range(len(data))])
        else:
            raise TypeError, "Data type %s not known." % data_type
    def set_group(self, data_type, group):
        """
            Store the group of the data to plot.
        """
        if data_type in PLOT_DATA.keys():
            self.group_dict[data_type] = group
        else:
            raise TypeError, "Data type %s not known." % data_type
    def set_fid(self, data_type, fid):
        """
            Store the file index of the data to plot.
        """
        if data_type in PLOT_DATA.keys():
            self.fileid_dict[data_type] = fid
        else:
            raise TypeError, "Data type %s not known." % data_type
    def set_coord(self, data_type, coord):
        """
            Store the coordinate of the data to plot.
        """
        if data_type in PLOT_DATA.keys():
            self.coord_dict[data_type] = coord
        else:
            raise TypeError, "Data type %s not known." % data_type
        
    def set_type(self, data_type):
        """
            Set the data type you want to plot
        """
        
        if data_type in PLOT_DATA.keys():
            self.type = data_type
        else:
            raise TypeError
    def __totake(self, data_type):
        group = [self.group_prop['id'][i] for i in numpy.nonzero(self.group_prop['display'])[0]]
        if data_type in PLOT_DATA_TYPE['simple']:
            totake = numpy.asarray([sum([i==j for j in group]) for i in self.group_dict[data_type]], dtype=bool)
        elif data_type in PLOT_DATA_TYPE['dist']:
            totake = numpy.asarray([sum([i==j for j in group]) 
                                    for i in self.group_dict[data_type][self.range_dict[data_type].dist]],
                                   dtype=bool)
        return totake
    def get_data(self, data_type, group=None):
        """
            Get the data that is stored.
        """
        totake = self.__totake(data_type)
        if data_type in PLOT_DATA_TYPE['simple']:
            return self.data_dict[data_type][totake]
        elif data_type in PLOT_DATA_TYPE['dist']:
            try:
                return self.data_dict[data_type][
                                                self.range_dict[data_type].dist]#[totake]
            except TypeError, message:
                if str(message) == "'NoneType' object is unsubscriptable":
                    return None
    def get_range(self, data_type):
        """
            Get the range of the data.
        """
        return self.range_dict[data_type]
    def get_group(self, data_type):
        """
            Get the group of the data.
        """
        # change the type and store the old one
        old_data_type, self.type = self.type, data_type
        
        group = self.group
        
        self.type = old_data_type
        
        return group
    def get_fid(self, data_type):
        """
            Get the file index of the data.
        """
        return self.fileid_dict[data_type]
    def get_coord(self, data_type):
        """
            Get the file index of the data.
        """
        return self.coord_dict[data_type]
        
class PlotRange(object): # pylint: disable-msg=R0903
    """
        Stores the range of the display
    """
    def __init__(self):
        self.min = None
        self.max = None
        self.bin_size = None
        self.dist = None
    def __setattr__(self, att, value):
        if att is 'min':
            if self.max is not None:
                self.__dict__[att] = min(value, self.max)
            else:
                self.__dict__[att] = value
        elif att is 'max':
            if self.min is not None:
                self.__dict__[att] = max(value, self.min)
            else:
                self.__dict__[att] = value
        else:
            self.__dict__[att] = value
    def __getattr__(self, att):
        if att is 'range':
            return [self.min, self.max, self.bin_size]
    def set_range(self, d_range):
        """
            set the range of the data.
        """
        if d_range[0] is not None:
            self.min = d_range[0]
        if d_range[1] is not None:
            self.max = d_range[1]
        if d_range[2] is not None:
            self.bin_size = d_range[2]
        try:
            if d_range[3] is not None:
                self.dist = d_range[3]
        except IndexError:
            pass
        
class PlotProperties(object): # pylint: disable-msg=R0903
    """
        Stores the plot properties
    """
    def __init__(self):
        self.main_exp_label = None
        self.comp_exp_label = None
        self.color_type = 'bw'
        self.plot_compare = False
        self.min = None
        self.max = None
        self.bin_size = None
        self.absolute = True
        self.__type = {'Stiffness': 'jet',
                       'Piezo': 'copper',
                       'Topography': 'copper',
                       'Event': 'gray',
                       'Event force': 'gray',
                       'Event distance': 'gray',
                       'Event length': 'gray'}

    def __setattr__(self, att, value):
        if att is 'color_type':
            if value in ['bw', 'black and white', 'BW']:
                self.__dict__[att] = 'bw'
            elif value in ['color', 'Color']:
                self.__dict__[att] = 'color'
        else:
            self.__dict__[att] = value

    def get_cmap(self, ptype):
        if self.color_type == 'bw':
            return 'gray'
        else:
            return self.__type[ptype]


### Some useful functions
def compute_array_stat(data):
    """
        Compute the statistic of the data for each time point.
        For example :
        
        >>> import numpy
        
        # Let's generate an array of data filled with 1 and 2
        
        >>> data = numpy.ones((15,10))
        >>> data[10:] = 2
        
        >>> indice = numpy.zeros(15)
        >>> indice[10:] = 1
        >>> result = compute_array_stat(data)
        
        # here is the result
        
        >>> result['mean']
        array([ 1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  1.,  2.,  2.,  2.,
                2.,  2.])
        >>> result['std']
        array([ 0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,  0.,
                0.,  0.])
        >>> result['count']
        array([ 10.,  10.,  10.,  10.,  10.,  10.,  10.,  10.,  10.,  10.,  10.,
                10.,  10.,  10.,  10.])
    """
    mean = data.mean(1)
    std = data.std(1)
    if type(data) == numpy.ma.core.MaskedArray:
        count = data.count(1)
    else:
        count = numpy.ones(data.shape[0]) * data.shape[1]
    sem = std / numpy.sqrt(count)
    
    result = {'mean' : mean,
              'std' : std,
              'count' : count,
              'sem' : sem}
    return result
    
def compute_indice_stat(data, indice):
    """
        Compute the statistic of the data, according to the indice.
        For example :
        
        >>> import numpy
        
        # Let's generate an array of data filled with 1 and 2
        
        >>> data = numpy.ones((150))
        >>> data[100:] = 2
        
        >>> indice = numpy.zeros(150)
        >>> indice[100:] = 1
        >>> result = compute_indice_stat(data, indice)
        
        # here is the result
        
        >>> result['indice']
        array([ 0.,  1.])
        >>> result['mean']
        array([ 1.,  2.])
        >>> result['std']
        array([ 0.,  0.])
        >>> result['count']
        array([ 100.,   50.])
        
        Let's make the same, but with on dimension vector.
        
        >>> data.shape = 15 * 10
        >>> indice = [0 for i in range(10 * 10)] + [1 for i in range(5 * 10)]
        
        >>> result = compute_indice_stat(data, indice)
        
        # here is the result
        
        >>> result['indice']
        array([0, 1])
        >>> result['mean']
        array([ 1.,  2.])
        >>> result['std']
        array([ 0.,  0.])
        >>> result['count']
        array([ 100.,   50.])
        
    """
    indice_list = numpy.unique(indice)
    mean = list()
    std = list()
    count = list()
    sem = list()
    indice_finite = numpy.isfinite(data)
    for index in indice_list:
        # doi_indice points to data with the good indice and which are finite
        doi_indice = indice_finite * (indice == index)
        doi = data[doi_indice] # doi means data of interrest
        if len(doi):
            mean.append(doi.mean())
            std.append(doi.std())
        else:
            mean.append(numpy.nan)
            std.append(numpy.nan)
        if type(doi) == numpy.ma.core.MaskedArray:
            count.append(doi.count())
        elif len(doi.shape) == 2:
            count.append(doi.shape[0] * doi.shape[1])
        elif len(doi.shape) == 1:
            count.append(doi.shape[0])
    # change list to array_
    mean, std, count = numpy.array(mean), numpy.array(std), numpy.array(count, dtype=float)
    # Exclude values that have no count.
    indice = count == 0.
    mean[indice], std[indice], count[indice] = numpy.nan, numpy.nan, numpy.nan
    sem = std / numpy.sqrt(count)
    result = {'mean' : mean,
              'std' : std,
              'count' : count,
              'sem' : sem,
              'indice' : indice_list}
    return result


# PLOT_DATA stores the properties of standard properties.
# [group_dict, data_dict, range_dict, fileid_dict, coord_dict]
PLOT_DATA = {'force' : [None, None, PlotRange(), None, None],
             'force_base' : [None, None, PlotRange(), None, None],
             'lr' : [None, None, PlotRange(), None, None],
             'dist' : [None, None, PlotRange(),None, None],
             'stiff': [None, None, PlotRange(), None, None],
             'stiff_ev': [None, None, PlotRange(), None, None],
             'stiff_notev': [None, None, PlotRange(), None, None],
             'relstiff' : [None, None, PlotRange(), None, None],
             'relstiffctl' : [None, None, PlotRange(), None, None],
             'young_modulus': [None, None, PlotRange(), None, None],
             'length' : [None, None, PlotRange(), None, None],
             'plength' : [None, None, PlotRange(), None, None]}
PLOT_DATA_TYPE = {'simple' : ['force', 'force_base', 'lr',  'dist', 'stiff',
                              'stiff_ev', 'stiff_notev', 'length', 'plength'],
                  'dist' : ['relstiff', 'relstiffctl']}

if __name__ == '__main__':
    import doctest
    FIGURE_FOLDER = 'Tests/Graphs/'
    
    doctest.testmod()
