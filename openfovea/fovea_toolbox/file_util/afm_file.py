#! /usr/bin/python
# -*- coding: utf-8 -*-
'''
This module is used to parse the files form Digital Instrument AFM
'''
__author__ = "Charles Roduit <charles.roduit@gmail.com>"
__date__ = "01.07.2007"
__license__ = "GNU Public License (GPL) version 3"
__version__ = "0.1"

import os
import csv
import zipfile

import numpy
from struct import unpack
import re

import nanoscope
import jpk
import asylum
import csem


def load(file_name):
    """
        Get the type of the file.

        >>> [header, deflection_av, deflection_re, plotx, piezo] = \
        load(ASYLUM5_FILE)
        >>> print plotx.shape
        (42622,)
        >>> print deflection_av.shape
        (1, 1, 42622)
        >>> [header, deflection_av, deflection_re, plotx, piezo] = \
        load(ASYLUM5_DIR)
        >>> print deflection_av.shape
        (8, 8, 301)

        * Parameters :
            file_name : str
                The file name (with the complete path) to load.

        * Returns :
            header : dict
                The header dictionnary.
            trace : 3D numpy.array
                Contains the 3d array of the trace deflection.
                trace[0,0,:] is the curve at pos_x = 0, pos_y = 0
            retrace : 3D numpy.array
                Contains the 3d array of the retrace deflection.
                retrace[0,0,:] is the curve at pos_x = 0, pos_y = 0
            plotx : 1D numpy.array
                Contains the piezo positions (x scale).
            piezo : 2D array
                The height of the piezo at the end of the indentation.
    """

    value = {
        'header': None,
        'trace_x': None,
        'retrace_x': None,
        'trace_y': None,
        'retrace_y': None,
        'piezo': None,
        'data_fid': None}
    if os.path.isdir(file_name):
        #print('directory')
        # User wants to load a directory...
        # Perhaps it's better to choose a single file...
        if is_asylum_dir(file_name):
            # This is an asylum directory.

            __value = asylum.load(file_name)
            value['piezo'] = __value['piezo']
            value['data_fid'] = __value['fid']
            value['header'] = __value['header']
            header = __value['header']
            #[trace, retrace, value['piezo'], header] = asylum.load(file_name)
            #value['trace_y'] = trace[1]
            #value['retrace_y'] = retrace[1]
            #piezo = numpy.zeros(deflection_av.shape[0:2])
            #value['trace_x'] = trace[0]
            #value['retrace_x'] = retrace[0]
        else:
            raise TypeError('Not a supported AFM file format.')
    elif is_nanoscope(file_name):
        #print('Nanoscope')
        header = nanoscope.lect_header(file_name)
        [value['trace_y'], value['retrace_y']] = nanoscope.load_force_curves(
                                                            file_name, header)
        value['piezo'] = nanoscope.load_image(file_name, header)
        # Creates the x vector
        value['trace_x'] = numpy.arange(header['force_samples_per_curve']) * \
                               header['x_factor']
        value['retrace_x'] = value['trace_x']
        header = nanoscope.clean_header(header)
    elif is_jpk(file_name):
        #print('jpk')
        __value = jpk.load(file_name)
        value['data_fid'] = __value['fid']
        header = __value['header']
#        print header
#        trace_x = trace[0]
#        retrace_x = retrace[0]
#        trace_y = trace[1]
#        retrace_y = retrace[1]
        #value['piezo'] = jpk.load_piezo(header, value['data_fid'])
        #piezo = numpy.zeros(trace_y.shape[0:2])
        value['piezo'] = __value['piezo']
    elif is_jpk_single(file_name):
#        print('jpk_single')
        [trace, retrace, header] = jpk.load_single(file_name)
        value['trace_x'] = trace[0]
        value['retrace_x'] = retrace[0]
        value['trace_y'] = trace[1]
        value['retrace_y'] = retrace[1]
        value['piezo'] = numpy.zeros(value['trace_y'].shape[0:2])
    elif is_asylum(file_name):
        #print('asylum')
        [trace, retrace, value['piezo'], header] = asylum.load(file_name)
        value['trace_x'] = trace[0]
        value['retrace_x'] = retrace[0]
        value['trace_y'] = trace[1]
        value['retrace_y'] = retrace[1]
        value['piezo'] = numpy.zeros(value['trace_y'].shape[0:2])
    elif csem.is_csem(file_name):
        [trace, retrace, header, piezo] = csem.load(file_name)
        value['trace_x'] = trace[0]
        value['retrace_x'] = retrace[0]
        value['trace_y'] = trace[1]
        value['retrace_y'] = retrace[1]
        value['piezo'] = piezo
    else:
        #print ('Not recognize')
        raise TypeError('Not a supported AFM file format.')
    # Add header for OpenFovea
    header['event_dist_thresh'] = 0.0
    header['thresh_event_fit_length'] = [None, None]
    header['thresh_event_fit_plength'] = [None, None]
    header['event_fit_model'] = None

    value['header'] = header
    #print value['piezo']
    return value


def is_nanoscope(file_name):
    """
    Test if the file comes from a veeco di nanoscope microscope

    >>> is_nanoscope(NANOSCOPE_FILE)
    True
    >>> is_nanoscope(JPK_FILE)
    False
    """

    file_id = open(file_name, 'rb')
    line_in_mem = file_id.readline()
    if not '\*Force file list' in line_in_mem:
        # This is not a nanoscoe file
        return False
    else:
        return True


def is_jpk(file_name):
    '''
    Test if the file comes from a jpk microscope.

    >>> is_jpk(JPK_FILE)
    True
    >>> is_jpk(ASYLUM5_FILE)
    False
    '''
    this_file = open(file_name)
    this_csv = csv.reader(this_file, delimiter=' ')
    ftype = None
    try:
        first_line = this_csv.next()
        ftype = 'csv'
    except:  # [csv.Error, StopIteration], reason:
        pass
    #    return False
    try:
        fid = zipfile.ZipFile(file_name)
        ftype = 'zip'
    except:
        pass
    if ftype == 'csv':
        if len(first_line) == 1:
            return False
        if not 'scanner:' in first_line[1]:
            return False
        else:
            return True
    elif ftype == 'zip':
        if 'header.properties' in fid.namelist():
            return True
    else:
        return False


def is_jpk_zip(file_name):
    '''
    Test if the file is a zipped jpk
    '''


def is_jpk_single(file_name):
    """
        Test if the file comes from a jpk microscope and is made of a single
        fc.
    """
    this_file = open(file_name)
    this_csv = csv.reader(this_file, delimiter=' ')
    try:
        first_line = this_csv.next()
    except csv.Error:
        return False
    if len(first_line) == 1:
        return False
    if not 'xPosition:' in first_line[1]:
        return False
    else:
        return True


def is_asylum(file_name):
    """
        Test if the file comes from an asylum microscope.

        >>> is_asylum(ASYLUM5_FILE)
        True
        >>> is_asylum(NANOSCOPE_FILE)
        False
    """
    if os.path.isdir(file_name):
        return False
    file_id = open(file_name)
    ver_igor = unpack('h', file_id.read(2))[0]
    if ver_igor in [1, 2, 3, 5]:
        return True
    else:
        return False


def is_asylum_dir(file_name):
    """
        Test if the directory is from asylum.

        >>> is_asylum_dir(ASYLUM5_DIR)
        True
    """
    if not os.path.isdir(file_name):
        return False
    dir_list = asylum.get_dir_list(file_name)
    if len(dir_list) == 0:
        return False
    file_list = os.listdir(os.path.join(file_name, dir_list[0]))
    _good_shape = [bool(re.match("Line\d+Point\d+.ibw", x)) for x in file_list]
    if True in _good_shape:
        file_list[0]
        file_name = os.path.join(file_name, dir_list[0],
                                 file_list[0])
        return is_asylum(file_name)
    else:
        return False


def load_curve(fid, pos, dtype, header, origin='relative'):
    """
        Load the curve on the fly.

        * Parameters :
            fid : instance of file id (obtained when opening a file)

            pos : list : [pos_x, pos_y]
                  The position of the curve to load.

            dtype : str
                    the curve data type to load.
                    'trace_x', 'trace_y', 'retrace_x', 'retrace_y'
            header : dict
                  The microscope type.
                  Currently supported values are : 'JPK', 'Asylum'
            origin : str
                     If the origin is 'relative', the curve beginns at 0.
                     If the origin is 'absolute', the curve is not modified.

        * Returns :
            curve : array
    """

    if header['Microscope'] == 'JPK':
        curve = jpk.load_curve(fid, pos, dtype)
        #return jpk.load_curve(fid, pos, dtype, header['index_array'])
    elif header['Microscope'] == 'Asylum':
        curve = asylum.load_curve(fid, pos, dtype, header['index_array'])
    else:
        raise TypeError('The microscope type %s is not supported.' %
                        header['Microscope'])
#    if origin == 'relative':
#        if 'x' in dtype:
#            curve = curve - curve[0]
#        elif 'y' in dtype:
#            curve = curve - curve[-1]
    if curve is None:
        return curve
#    if 'x' in dtype and curve[0] > curve[-1]:
#        curve = curve[::-1]
#    elif 'y' in dtype and curve[0] < curve[-1]:
#        curve = curve[::-1]
    return curve


def force_load_array(fid, header):
    """
    This function loads the arrays from the NanoWizard3 zipped files.

            * Parameters :
                fid : instance of file id (obtained when opening a file)

                header : dict
                         The microscope type.
                         Currently supported values are : 'JPK'

            * Returns :
                dictionnary with :
                    'Index' : The index array (i.e. the folder name in the
                              corresponding pixel) This index is generated from
                              the coordinate in the header of each curve. This
                              is useful if the index array automatically
                              generated is wrong.
                    'Piezo' : The piezo array. This is the height of the piezo
                              at the end of the indentation. This array is used
                              to generate the "zero force image" or "Topography
                              image".
    """

    if header['Microscope'] == 'JPK':
        return jpk.load_arrays(fid)

if __name__ == '__main__':
    ASYLUM5_FILE = '../../../docs/data/files/ASYLUM/Cell0005.ibw'
    NANOSCOPE_FILE = '../../../docs/data/files/FR2310B.004'
    JPK_FILE = \
        '../../../docs/data/files/JPK/mappp-data-2008.11.07-11.43.24.map_txt'
    ASYLUM5_DIR = '/home/charles/AFM/Asylum Files/Figures/Archive'
    ASYLUM5_DIR = \
        '/home/charles/AFM/Robert_Ross/cropstack cell1/cell100-Cropped fmap'
    import doctest
    doctest.testmod()
