from xml.dom.minidom import Document, parse
import os
import commands
import tempfile
import zipfile
from datetime import datetime
from distutils.version import StrictVersion
import pdb

import numpy
from numpy import array, Inf, inf, NaN, nan

import succellus_import

from common import Variables
GLOB_VARS = Variables()

version = '0.1a68'

###
# 0.1a68 : Add PoCNoise array.
# 0.1a67 : Add event length and persistent length.


# TODO correct the small error while saving the event force. I get an error of
# ~2e-12 between original and saved data (see aexTest, I had to change a
# assertEqual to assertAlmostEqual)
def save(fv_folder_object, file_name):
    '''
    Save a FVFolder object to an aex file.
    '''
    tmp_dir = tempfile.mkdtemp()
    if not os.path.splitext(file_name)[1] == '.aex':
        file_name = file_name + '.aex'
    save_exp(fv_folder_object, tmp_dir, version)
    start_path = os.path.split(file_name)[0]
    for fv_object in fv_folder_object.file['list']:
        if fv_object.header['data_location'] not in ['local', u'local']:
            curr_path = os.path.split(fv_object.header['data_location'])[0]
            rel_path = os.path.relpath(curr_path, start_path)
            fv_object.header['data_location'] = os.path.join(
                           rel_path,
                           os.path.split(fv_object.header['data_location'])[1])
        save_fv(fv_object, tmp_dir, version)
    mime_type = open(os.path.join(tmp_dir,'mimetype'), 'wb')
    mime_type.write('application/afm.aex.data')
    mime_type.close()
    # Compress all files in the zip
    aex_file = zipfile.ZipFile(file_name,'w')
    
    for item in os.listdir(tmp_dir):
        if os.path.isfile(os.path.join(tmp_dir, item)):
            aex_file.write(os.path.join(tmp_dir, item), item)
            os.remove(os.path.join(tmp_dir, item))
    for item in os.listdir(os.path.join(tmp_dir, 'data')):
        aex_file.write(os.path.join(tmp_dir, 'data', item), 
                       os.path.join('data', item))
        os.remove(os.path.join(tmp_dir, 'data', item))
    aex_file.close()
    os.rmdir(os.path.join(tmp_dir, 'data'))
    os.rmdir(tmp_dir)
    
def save_exp(fv_folder, base_dir, version):
    exp_tree = Document()
    exp_node = exp_tree.createElement('fvfolder')
    exp_node.setAttribute('xmlns:openfovea','initiator_software')
    exp_node.setAttribute('openfovea:version', version)
    
    info_node = exp_tree.createElement('info')
    #####
    # Date node
    date_node = exp_tree.createElement('date')
    date_element = exp_tree.createTextNode(str(fv_folder.date))
    date_node.appendChild(date_element)
    exp_node.appendChild(date_node)
    # End of date node
    #####
    # Author node
    author_node = exp_tree.createElement('author')
    author_element = exp_tree.createTextNode(fv_folder.author)
    author_node.appendChild(author_element)
    exp_node.appendChild(author_node)
    # End of author node
    #####
    # Comment node
    comment_node = exp_tree.createElement('comment')
    comment_element = exp_tree.createTextNode(fv_folder.comment)
    comment_node.appendChild(comment_element)
    exp_node.appendChild(comment_node)
    # End of comment node
    #####
    # Parameters node
    parameters_node = exp_tree.createElement('parameters')
    for key in fv_folder.parameters:
        parameters_node.setAttribute(key, str(fv_folder.parameters[key]))
    exp_node.appendChild(parameters_node)
    # End of parameters node
    #####
    # Group node
    group_node = exp_tree.createElement('group')
    for key in fv_folder.group:
        group_node.setAttribute(key, str(fv_folder.group[key]))
    exp_node.appendChild(group_node)
    # End of group node
    #####
    exp_tree.appendChild(exp_node)
    
    xml_id = open(os.path.join(base_dir, 'experiment.xml'), 'wb')
    xml_id.write(exp_tree.toprettyxml())
    xml_id.close()

def load_exp(file_name):
    xmldoc=parse(file_name)
    child=xmldoc.firstChild
    #exp_dict['OF_version'] = child.getAttribute('openfovea:version')
    exp_dict = {'date' : '',
                'author' : '',
                'comment': '',
                'parameters' : {'event_detect_weight' : None,
                                'event_fit_model' : None,
                                'rel_stiff_dist' : None,
                                'injection_index' : 0,
                                'PoC_threshold' : 2,
                                'histo_y_rel' : False,
                                'plot_size' : [800, 600],
                                'plot_hist_gauss' : True,
                                },
                'group' : { 'id' : [],
                            'label' : [],
                            'display' : []},
                'OF_version' : child.getAttribute('openfovea:version')
                }
    for elm in child.childNodes:
        if elm.nodeName in exp_dict.keys():
            if elm.nodeName == 'parameters':
                for key in exp_dict['parameters'].keys():
                    string = elm.getAttribute(key)
                    if len(string):
                        try:
                            exp_dict['parameters'][key] = eval(string)
                        except NameError:
                            exp_dict['parameters'][key] = str(string)
            elif elm.nodeName == 'group':
                for key in exp_dict['group'].keys():
                    exp_dict['group'][key] = eval(elm.getAttribute(key))
            else:
                string = elm.childNodes[0].data
                string = string.replace('\n\t\t','')
                string = string.replace('\n\t','')
                #for key in exp_dict.keys:
                if elm.nodeName == 'date':
                    exp_dict['date'] = datetime.strptime(string, 
                                                        '%Y-%m-%d %H:%M:%S.%f')
                else:
                    exp_dict[elm.nodeName] = string      
    exp_dict['parameters']['rel_stiff_dist'] = int(exp_dict['parameters']['rel_stiff_dist'])
    return exp_dict
    
def save_fv(fv_object, base_dir, version):
    '''
    Save a single force volume file to an xml file.
    '''
    fv_tree = Document()
    
    force_volume_node = fv_tree.createElement('fvfile')
    force_volume_node.setAttribute('xmlns:array','numpy_array')
    force_volume_node.setAttribute('xmlns:openfovea','initiator_software')
    force_volume_node.setAttribute('openfovea:version', version)
    force_volume_node.setAttribute('filename', fv_object.name)
    
    header_node = fv_tree.createElement('header')
    for key in fv_object.header:
        if key == 'x_factor':
            header_node.setAttribute(key, '%.15f'%fv_object.header[key])
        elif key == 'date':
            header_node.setAttribute(key, str(fv_object.header[key]))
        elif key == 'note':
            header_node.setAttribute(key, fv_object.header[key].__repr__())
        elif key == 'index_array':
            _node = __node_array(fv_tree.createElement('index_array'),
                                 fv_object.header[key],
                                 'index',
                                 fv_object.name,
                                 base_dir)
            if _node is not None:
                force_volume_node.appendChild(_node)
        else:
            header_node.setAttribute(key, str(fv_object.header[key]))
    force_volume_node.appendChild(header_node)

    ## Save the switches
    switch_node = fv_tree.createElement('switch')
    for key in fv_object.get_switch('list'):
        switch_node.setAttribute(key, str(fv_object.get_switch(key)))
    force_volume_node.appendChild(switch_node)
    original_switches = fv_object.get_switch('restore')
    # Force switches to be null in order to save all the data
    fv_object.set_switch('reset')
    ## Piezo array
    try:
        __array = fv_object.get_array('Piezo')
    except Exception as inst:
        if 'Topography array is not loaded' in inst.message:
            pass
        else:
            raise inst
    else:
        _node = __node_array(fv_tree.createElement('piezo_array'),
                             __array,
                             'piezo',
                             fv_object.name,
                             base_dir)
        if _node is not None:
            force_volume_node.appendChild(_node)
    ## Stiffness header
    stiffness_header_node = fv_tree.createElement('stiffness_header')
    for key in fv_object.stiffness:
        stiffness_header_node.setAttribute(key, str(fv_object.stiffness[key]))
    force_volume_node.appendChild(stiffness_header_node)
    ## Stiffness array
    st_array = fv_object.get_array('Stiffness')
    if type(st_array) == numpy.ma.core.MaskedArray:
        stiffness_node = fv_tree.createElement('stiffness')
        #stiffness_node.setAttribute('shape',str(st_array.shape))
#        for key in fv_object.stiffness:
#            stiffness_node.setAttribute(key, str(fv_object.stiffness[key]))
        
        stiff_data_node = fv_tree.createElement('data')
        rel_name = 'data/' + fv_object.name + '_stiffness.data'
        abs_name = os.path.join(base_dir, 'data', fv_object.name + '_stiffness.data')
        stiff_data_node.setAttribute('shape', str(st_array.data.shape))
        stiff_data_node.setAttribute('array:href', rel_name)
        __save_array(st_array.data, abs_name)
        stiffness_node.appendChild(stiff_data_node)
        
        stiff_mask_node = fv_tree.createElement('mask')
        rel_name = 'data/' + fv_object.name + '_stiffness_mask.data'
        abs_name = os.path.join(base_dir, 'data', fv_object.name + '_stiffness_mask.data')
        stiff_mask_node.setAttribute('shape', str(st_array.mask.shape))
        stiff_mask_node.setAttribute('array:href', rel_name)
        __save_array(st_array.mask.astype(float), abs_name)
        stiffness_node.appendChild(stiff_mask_node)
        
        force_volume_node.appendChild(stiffness_node)
        
    # Mask array
    _node = __node_array(fv_tree.createElement('mask_array'),
                             fv_object.get_array('Mask'),
                             'mask', fv_object.name, base_dir)
    if _node is not None:
        force_volume_node.appendChild(_node)
    ## PoC array
    _node = __node_array(fv_tree.createElement('poc_array'),
                             fv_object.get_array('PoCIndice'),
                             'poc_indice', fv_object.name, base_dir)
    if _node is not None:
        force_volume_node.appendChild(_node)
    ## PoC Noise array
    _node = __node_array(fv_tree.createElement('pocnoise_array'),
                            fv_object.get_array('PoCNoise'),
                            'poc_noise', fv_object.name, base_dir)
    if _node is not None:
        force_volume_node.appendChild(_node)
    ## Topography array
    _node = __node_array(fv_tree.createElement('topo_array'),
                             fv_object.get_array('Topography'),
                             'topography', fv_object.name, base_dir)
    if _node is not None:
        force_volume_node.appendChild(_node)
    ## Event array
    _node = __node_array(fv_tree.createElement('event'),
                             fv_object.get_array('Event'),
                             'event', fv_object.name, base_dir)
    if _node is not None:
        # Save the parameters of the events.
        for key in fv_object.event:
            if fv_object.event[key] is not None:
                _node.setAttribute(key, str(fv_object.event[key]))
            else:
                pass
        # Save also the details of the events.
        for event in fv_object.event_list:
            det_event_node = fv_tree.createElement('detail')
            for key in event:
                # TODO Putative dead code : find out when it is necessary
#                if key == 'Stiffness':
#                    det_event_node.setAttribute('Stiffness_data', str(list(event[key].data)))
#                    det_event_node.setAttribute('Stiffness_mask', str(list(event[key].mask)))
                det_event_node.setAttribute(key, str(event[key]))
            _node.appendChild(det_event_node)
        force_volume_node.appendChild(_node)
    ## Random array
    _node = __node_array(fv_tree.createElement('event_rand_array'),
                             fv_object.get_array('Event random'),
                             'event_random', fv_object.name, base_dir)
    if _node is not None:
        force_volume_node.appendChild(_node)
    
    # The force-distance curve :
    curve_dict = fv_object._ForceVolume__fdcurves

    data_location = fv_object.header['data_location']
    if data_location not in ['local', u'local']:
        pass
    else:
        # trace x
        trace_x = fv_tree.createElement('trace_x_array')
        rel_name = 'data/' + fv_object.name + '_trace_x.data'
        abs_name = os.path.join(base_dir, 'data', fv_object.name + '_trace_x.data')
        __save_array(curve_dict['trace_x'], abs_name)
        trace_x.setAttribute('array:href', rel_name)
        trace_x.setAttribute('shape',str(curve_dict['trace_x'].shape))
        force_volume_node.appendChild(trace_x)
        # retrace x
        retrace_x = fv_tree.createElement('retrace_x_array')
        rel_name = 'data/' + fv_object.name + '_retrace_x.data'
        abs_name = os.path.join(base_dir, 'data', fv_object.name + '_retrace_x.data')
        __save_array(curve_dict['retrace_x'], abs_name)
        retrace_x.setAttribute('array:href', rel_name)
        retrace_x.setAttribute('shape',str(curve_dict['retrace_x'].shape))
        force_volume_node.appendChild(retrace_x)
        
        # Trace force curves
        trace_array = fv_tree.createElement('trace_array')
        rel_name = 'data/' + fv_object.name + '_trace.data'
        abs_name = os.path.join(base_dir, 'data', fv_object.name + '_trace.data')
        __save_array(curve_dict['trace_y'], abs_name)
        trace_array.setAttribute('array:href', rel_name)
        trace_array.setAttribute('shape',str(curve_dict['trace_y'].shape))
        force_volume_node.appendChild(trace_array)
            
        # Retrace force curves
        retrace_array = fv_tree.createElement('retrace_array')
        rel_name = 'data/' + fv_object.name + '_retrace.data'
        abs_name = os.path.join(base_dir, 'data', fv_object.name + '_retrace.data')
        __save_array(curve_dict['retrace_y'], abs_name)
        retrace_array.setAttribute('array:href', rel_name)
        retrace_array.setAttribute('shape',str(curve_dict['retrace_y'].shape))
        force_volume_node.appendChild(retrace_array)
        
    fv_tree.appendChild(force_volume_node)
    xml_id = open(os.path.join(base_dir, fv_object.name + '.xml'), 'wb')
    xml_id.write(fv_tree.toprettyxml())
    xml_id.close()
    fv_object.set_switch('restore', original_switches)
def __node_array(array_node, array, array_name, object_name, base_dir):
    """
        Save the array.
        
        * Parameters :
            array_node : xml node
                The node where to put these information.
            array : 2d numpy.array
                The array to save.
            array_name : str
                The name of the array. This will be saved in the xml tree
            object_name : str
                The name of the fv object. This is used to relate the array to
                the correct object when loading.
            parent_node : xml node
                The parent node where to put the array node.
            base_dir : str
                The base directory to save the data.
    """
    
    if type(array) == numpy.ndarray:
        rel_name = 'data/' + object_name + '_' + array_name + '.data'
        abs_name = os.path.join(base_dir, 'data', object_name + '_' + array_name + '.data')
        array_node.setAttribute('shape',str(array.shape))
        array_node.setAttribute('array:href', rel_name)
        __save_array(array, abs_name)
        return array_node
    else:
        return None

def __save_array(array, c_file_name):
    """
        Saves the array to a binary format.
        
        * Parameters :
            array : numpy.ndarray
                The array to save.
            c_file_name : str
                The filename to save it.
                
        * Returns :
            None
    """
    
    [directory, file_name] = os.path.split(c_file_name)
    if not os.path.exists(directory):
        os.makedirs(directory)
    file_id = open(c_file_name, 'wb')
    if type(array) == numpy.ndarray:
        array = array.astype('float64')
        file_id.write(array.tostring('C'))
    file_id.close()

def __load_array(elm, folder_name):
    """
        Loads the array from an xml node of the array type
        
        * Parameters :
            elm : xml node
                xml node with 'shape' and 'array:href' attributes.
            folder_name : str
                The path to the file.
        
        * Returns :
            array : numpy.ndarray
                The dimension of the array accords with 'shape' attribute of the
                elm xml node.
    """
    target = os.path.join(folder_name, elm.getAttribute('array:href'))
    shape = eval(elm.getAttribute('shape'))
    array_id = open(target, 'rb')
    array_str = array_id.read()
    array = numpy.fromstring(array_str, dtype=numpy.float64)
    return array.reshape(shape)
def convert2current(xmldoc):
    """
        Convert a previous version tree in current.
    """
    child = xmldoc.firstChild
    fv_dict = {'OF_version' : child.getAttribute('openfovea:version')}
    if StrictVersion(fv_dict['OF_version']) <= StrictVersion('0.1a63'):
        # There is no stiffness_header element.
        # This element is in stiffness with the arrays.
        elem = child.getElementsByTagName('stiffness')
        if len(elem):
            stiffness_node = elem[0]
            stiff_header = xmldoc.createElement('stiffness_header')
            # get the stiffness properties
            stiff_prop = {'HertzModel' : '', 'PointCarac' : '',  'calibSens' : '',
                          'glass' : '', 'nbParts' : '', 'sizeParts' : '',
                          'relativeMaxDist' : '', 'PoCThreshold' : '',
                          'PoCMethod' : u'deriv', 'recomputePoC' : u'False',
                          'limitSlide' : u'False', 'poisson_ratio' : u'0.3', 
                          'poc_len_slice' : u'0.33'
                          }
            for key in stiff_prop.keys():
                if stiffness_node.hasAttribute(key):
                    val = stiffness_node.getAttribute(key)
                else:
                    val = stiff_prop[key]
                stiff_prop[key] = val
            for item in S_HEADER_064_2_CURRENT:
                val = stiff_prop[S_HEADER_064_2_CURRENT[item]]
                stiff_header.setAttribute(item, val)
            stiff_header.setAttribute('poc_len_slice', 0.33)
            child.appendChild(stiff_header)
    if StrictVersion(fv_dict['OF_version']) == StrictVersion('0.1a64'):
        stiff_header = child.getElementsByTagName('stiffness_header')[0]
        for item in S_HEADER_064_2_CURRENT:
            value = stiff_header.getAttribute(S_HEADER_064_2_CURRENT[item])
            stiff_header.removeAttribute(S_HEADER_064_2_CURRENT[item])
            stiff_header.setAttribute(item, value)
        stiff_header.setAttribute('poc_len_slice', 0.33)
    if StrictVersion(fv_dict['OF_version']) <= StrictVersion('0.1a65'):
        # The change is in the curve_x. Before it was always a 1d vector. Now,
        # it can be also 3d vector. The node name changed from 'curve_x_array'
        # to 'trace_x_array' and 'retrace_y_array' for the trace and retrace
        # curve respectively.
        node = child.getElementsByTagName('curve_x_array')[0]
        target = node.getAttribute('array:href')
        shape = node.getAttribute('shape')
        trace_x = xmldoc.createElement('trace_x_array')
        trace_x.setAttribute('array:href', target)
        trace_x.setAttribute('shape', shape)
        retrace_x = xmldoc.createElement('retrace_x_array')
        retrace_x.setAttribute('array:href', target)
        retrace_x.setAttribute('shape', shape)
        child.appendChild(trace_x)
        child.appendChild(retrace_x)
    if StrictVersion(fv_dict['OF_version']) <= StrictVersion('0.1a66'):
        # The change is in the presence of a wlc fit for events.
        pass
    return xmldoc
def load_fv(file_name, curdir=''):
    '''
    Loads the force volume xml file and returns a dictionnary that contains the
    datas.
    '''
    folder_name = os.path.split(file_name)[0]
    xmldoc=parse(file_name)
    child=xmldoc.firstChild
    fv_dict = {'name' : None,
               'header' : {},
               'stiffness_header' : {},
               'piezo_array' : None,
               'topo_array' : None,
               'poc_array' : None,
               'pocnoise_array' : None,
               'trace_x_array' : None,
               'retrace_x_array' : None,
               'trace_array' : None,
               'retrace_array' : None,
               'event_rand_array' : None,
               'stiffness' : None,
               'mask_array' : None,
               'index_array' : None,
               'switch' : {'mask' : 'False'},
               'event' : {}}
    fv_dict['name'] = child.getAttribute('filename')
    fv_dict['OF_version'] = child.getAttribute('openfovea:version')
    if StrictVersion(fv_dict['OF_version']) < StrictVersion(version):
        xmldoc = convert2current(xmldoc)
        child = xmldoc.firstChild
    for elm in child.childNodes:
        if elm.nodeName in fv_dict.keys():
            if 'array' in elm.nodeName:
                fv_dict[elm.nodeName] = __load_array(elm, folder_name)
                if elm.nodeName == 'event_rand_array':
                    nbr_random = elm.getAttribute('nbr_random')
                    if len(nbr_random):
                        ev_prop = {'nbr_random' : int(nbr_random)}
                if elm.nodeName == 'mask_array' and fv_dict[elm.nodeName] is not None:
                    fv_dict[elm.nodeName] = fv_dict[elm.nodeName].astype('bool')
            elif elm.nodeName == 'stiffness_header':
                stiff_prop = {}
                for key, fct in zip(STIFFNESS_HEADER.keys(), STIFFNESS_HEADER.values()):
                    fv_dict['stiffness_header'][key] = fct(elm.getAttribute(key))
            elif elm.nodeName == 'stiffness':
                # get the data and mask
                for array_node in elm.childNodes:
                    if array_node.nodeName == 'data':
                        stiff_data = __load_array(array_node, folder_name)
                    elif array_node.nodeName == 'mask':
                        stiff_mask = __load_array(array_node, folder_name)
                stiff_array = numpy.ma.MaskedArray(stiff_data,
                                                   mask = stiff_mask)
                fv_dict['stiffness'] = stiff_array
            elif elm.nodeName == 'event':
                if StrictVersion(fv_dict['OF_version']) < StrictVersion('0.1a62'):
                    # Loading rate wasn't computed...
                    pass
                else:
                    # get the event array
                    fv_dict['event']['array'] = __load_array(elm, folder_name)
                    # get the detail of the events
                    detail = list()
                    
                    for array_node in elm.childNodes:
                        if array_node.nodeName == 'detail':
                            ev_det = {}
                            ev_det['Slice'] = array_node.getAttribute('Slice')
                            ev_det['X'] = int(array_node.getAttribute('X'))
                            ev_det['force'] = float(array_node.getAttribute('force'))
                            ev_det['max'] = int(array_node.getAttribute('max'))
                            ev_det['min'] = int(array_node.getAttribute('min'))
                            ev_det['pos_x'] = int(array_node.getAttribute('pos_x'))
                            ev_det['pos_y'] = int(array_node.getAttribute('pos_y'))
                            ev_det['slope'] = float(array_node.getAttribute('slope'))
                            ev_det['loading_rate'] = float(array_node.getAttribute('loading_rate'))
                            ev_det['dist'] = float(array_node.getAttribute('dist'))
                            if StrictVersion(fv_dict['OF_version']) > StrictVersion('0.1a66'):
                                ev_det['fit_length'] = eval(array_node.getAttribute('fit_length'))
                                ev_det['fit_plength'] = eval(array_node.getAttribute('fit_plength'))
                            else:
                                ev_det['fit_length'] = None
                                ev_det['fit_plength'] = None
                            # TODO Putative dead code : find when it's necessary
    #                        if array_node.hasAttribute('Stiffness_mask'):
    #                            mask = eval(array_node.getAttribute('Stiffness_mask'))
    #                            data = eval(array_node.getAttribute('Stiffness_data'))
    #                            ev_det['Stiffness'] = numpy.ma.array(data, mask=mask)
                            detail.append(ev_det)
                    fv_dict['event']['detail'] = detail
            elif elm.nodeName == 'header':
                header_dict = GLOB_VARS.common_header
                for key in header_dict.keys():
                    if elm.getAttribute(key) is not '':
                        header_dict[key] = elm.getAttribute(key)
                    try:
                        header_dict[key] = eval(header_dict[key])
                    except:
                        pass
                try:
                    header_dict['date'] = datetime.strptime(header_dict['date'], 
                                                        '%Y-%m-%d %H:%M:%S.%f')
                except ValueError:
                    header_dict['date'] = datetime.strptime(header_dict['date'], 
                                                        '%Y-%m-%d %H:%M:%S')
                except TypeError:
                    header_dict['date'] = None
                if StrictVersion(fv_dict['OF_version']) < StrictVersion('0.1a63') :
                    header_dict['number_curves'] = header_dict['matrix_length'] ** 2
                if StrictVersion(fv_dict['OF_version']) < StrictVersion('0.1a64') :
                    header_dict['event_dist_thresh'] = 0.0
                if type(header_dict['size']) not in [list, tuple]:
                    header_dict['size'] = (header_dict['size_x'], header_dict['size_y'])
                if type(header_dict['scan_size']) not in [list, tuple]:
                    header_dict['scan_size'] = (header_dict['scan_size'],
                                                header_dict['scan_size'])
                if type(header_dict['pixel_size']) not in [list, tuple]:
                    header_dict['pixel_size'] = (header_dict['pixel_size'],
                                                 header_dict['pixel_size'])
                fv_dict['header'] = header_dict
            elif elm.nodeName == 'switch':
                fv_switch = {}
                for key_nb in range(elm.attributes.length):
                    fv_dict['switch'][elm.attributes.item(key_nb).name] = \
                                            elm.attributes.item(key_nb).value

    if fv_dict['header']['data_location'] not in ['local', u'local']:
        data_location = fv_dict['header']['data_location']

        # Get the correct path.
        try_path = [fv_dict['header']['data_location'],
                    os.path.join(curdir, os.path.split(fv_dict['header']['data_location'])[-1]),
                    os.path.join(curdir, fv_dict['header']['data_location'].split('\\')[-1])]
        
        correct_path = [os.path.exists(__itpath) for __itpath in try_path]
        data_location = try_path[correct_path.index(True)]

        if os.path.isdir(data_location):  # Asylum case
            fv_dict['data_fid'] = data_location
        else :  # JPK case
            fv_dict['data_fid'] = zipfile.ZipFile(data_location, 'r')

        fv_dict['header']['data_location'] = data_location
        fv_dict['header']['index_array'] = fv_dict['index_array']
    return fv_dict
    
def load(aex_name):
    # first unzip the file to a tmp folder
    compressed_file = zipfile.ZipFile(aex_name)
    tmp_dir = tempfile.mkdtemp()
    file_list = []
    directory_list = []
    file_tree = []
    for file_in in compressed_file.filelist:
        directory = os.path.split(file_in.filename)[0]
        if not directory == '':
            if not os.path.isdir(os.path.join(tmp_dir,directory)):
                directory_list.append(directory)
                os.makedirs(os.path.join(tmp_dir, directory))
        outfile = open(os.path.join(tmp_dir, file_in.filename), 'wb')
        outfile.write(compressed_file.read(file_in.filename))
        outfile.close()
        file_list.append(file_in.filename)
    
    if 'mimetype' in file_list:
        # OpenFovea version of aex
        for file_name in file_list:
            c_file_name = os.path.join(tmp_dir, file_name)
            if file_name == 'experiment.xml':
                experiment = load_exp(c_file_name)
            elif os.path.splitext(file_name)[1] == '.xml':
                file_tree.append(load_fv(c_file_name, curdir=os.path.split(aex_name)[0]))
            os.remove(c_file_name)
        for directory in directory_list:
            os.rmdir(os.path.join(tmp_dir,directory))
    else:
        # Succellus version of aex, designed to disapear...
        for file_name in file_list:
            c_file_name = os.path.join(tmp_dir, file_name)
            if file_name == 'experiment.xml':
                experiment = succellus_import.xmlExtractExperiment(c_file_name)
                os.remove(c_file_name)
            else :
                file_tree.append(succellus_import.xmlExtractFV(c_file_name))
                os.remove(c_file_name)
    os.rmdir(tmp_dir)
    return [experiment, file_tree]

STIFFNESS_HEADER_0_64 = {
 'HertzModel' : lambda x: x,
 'PoCMethod' : lambda x: x,
 'PoCThreshold' : lambda x: float(x),
 'PointCarac' : lambda x: float(x),
 'calibSens' : lambda x: float(x),
 'glass' : lambda x: float(x),
 'limitSlide' : lambda x: eval(x),
 'nbParts' : lambda x: int(x),
 'poisson_ratio' : lambda x: float(x),
 'recomputePoC' : lambda x: eval(x),
 'relativeMaxDist' : lambda x: int(x),
 'sizeParts' : lambda x: int(x)}

S_HEADER_064_2_CURRENT = {
 'hertz_model' : 'HertzModel',
 'poc_method' : 'PoCMethod',
 'poc_threshold' : 'PoCThreshold',
 'point_carac' : 'PointCarac',
 'calib_sens' : 'calibSens',
 'glass' : 'glass',
 'limit_slide' : 'limitSlide',
 'nb_parts' : 'nbParts',
 'poisson_ratio' : 'poisson_ratio',
 'recompute_poc' : 'recomputePoC',
 'relative_max_dist' : 'relativeMaxDist',
 'size_parts' : 'sizeParts'}

STIFFNESS_HEADER = {
 'hertz_model' : lambda x: x,
 'fit_method' : lambda x: x,
 'poc_method' : lambda x: x,
 'poc_threshold' : lambda x: float(x),
 'point_carac' : lambda x: float(x),
 'calib_sens' : lambda x: float(x),
 'glass' : lambda x: float(x),
 'limit_slide' : lambda x: eval(x),
 'nb_parts' : lambda x: int(x),
 'poisson_ratio' : lambda x: float(x),
 'recompute_poc' : lambda x: eval(x),
 'relative_max_dist' : lambda x: int(x),
 'size_parts' : lambda x: int(x),
 'poc_len_slice' : lambda x: float(x),
}

if __name__ == '__main__':
    load_fv('../../../docs/data/files/aex_xml/0.1a64/fvfile.xml')
