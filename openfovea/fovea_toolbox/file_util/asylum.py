#! /usr/bin/env python
#-*- coding: iso-8859-1 -*-

"""
    This module imports files from asylum microscopes. To use it make as 
    follow :
    
    >>> [trace, retrace, header, piezo] = load(ASYLUM5_FILE)
    >>> len(trace)
    2
    >>> len(trace[0]) == len(retrace[0])
    True
    
    Trace[0] is the curve x and trace[1] is the array containing the y values
    for all the force distance curves.
    
    >>> len(trace[0]) == len(trace[1][0][0])
    True
    
    If you want to load a complete force spectroscopy data set :
    
    >>> [trace, retrace, header, piezo] = load(ASYLUM5_DIR)
    >>> print trace[1].shape # The y arrays
    (8, 8, 301)
    >>> print trace[0].shape # The x array
    (301,)
"""

__author__  = "Charles Roduit <charles.roduit@gmail.com>"
__date__ = "20.07.2010"
__license__ = "GNU Public License (GPL) version 3"
__version__ = "0.1"

from struct import unpack
import os
from datetime import datetime
import numpy
import re

from common import Variables
import afm_file
#from afm_file import, is_asylum, is_asylum_dir
GLOB_VARS = Variables()

def load(file_name):
    """
        Load asylum file. It detects automatically if the given path is a file
        or is an asylum path.
        
        * Parameters :
            file_name : str
                The complete path to open. The path contains the file or the
                asylum path to open.
        
        * Returns :
            trace : 3D numpy.array
                Contains the 3d array of the trace deflection.
                trace[0,0,:] is the curve at pos_x = 0, pos_y = 0
            retrace : 3D numpy.array
                Contains the 3d array of the retrace deflection.
                retrace[0,0,:] is the curve at pos_x = 0, pos_y = 0
            header : dict
                Contains usefull information to understand the curves.
            piezo : 2D numpy.array
                The piezo position at the end of the indentation.
        
        To detect the type of the file see is_asylum and is_asylum_dir in
        afm_file module.
    """
    value = {
        'trace_x' : None,
        'trace_y' : None,
        'retrace_x' : None,
        'retrace_y' : None,
        'header' : None,
        'fid' : None
    }
    if afm_file.is_asylum(file_name):
        piezo = None
        trace, retrace, header, piezo = get_file(file_name)
        # reshape arrays to be a 3d array.
        for key in trace:
            trace[key].shape = (1, 1, trace[key].shape[0])
        for key in retrace:
            retrace[key].shape = (1, 1, retrace[key].shape[0])
        value['trace_x'] = trace['lvdt']
        value['trace_y'] =  trace['defl']
        value['retrace_x'] = retrace['lvdt']
        value['retrace_y'] =  retrace['defl']
        value['header'] = header
        value['piezo'] = piezo
    elif afm_file.is_asylum_dir(file_name):
        value['fid'], value['header'], value['piezo'] = load_dir(file_name)
        #trace, retrace, header, piezo = load_dir(file_name)
    value['piezo'] = value['piezo'] - value['piezo'].min()
    return value
    
def load_dir(dir_name):
    """
        Load asylum file contained in a directory.
        
        * Parameters :
            dir_name : str
                The complete path to open.
        
        * Returns :
            trace : 3D numpy.array
                Contains the 3d array of the trace deflection.
                trace[0,0,:] is the curve at pos_x = 0, pos_y = 0
            retrace : 3D numpy.array
                Contains the 3d array of the retrace deflection.
                retrace[0,0,:] is the curve at pos_x = 0, pos_y = 0
            header : dict
                Contains usefull information to understand the curves.
            piezo : 2D numpy.array
                The piezo position at the end of the indentation.
    """
    dir_list = get_dir_list(dir_name)
    size = [len(dir_list), 
            len(get_file_list(os.path.join(dir_name, dir_list[0])))]
    piezo = numpy.zeros((size[0], size[1]))
    file_index = [['' for y in xrange(size[1])] for x in xrange(size[0])]#numpy.zeros((size[0], size[1]), dtype=str)
    #trace = []
    #retrace = []
    header = []
    for _item in dir_list:
        folder_name = os.path.join(dir_name, _item)
        file_list = get_file_list(folder_name)
        
        #pos_x = list(dir_list).index(_item)
        for _f_item in file_list:
            # File names are LineXXXXPointYYYY.ibw.
            # We extract the pos_x and pos_y :
            pos_x = int(_f_item[_f_item.rindex('e')+1:_f_item.rindex('P')])
            pos_y = int(_f_item[_f_item.rindex('t')+1:_f_item.rindex('.')])
            file_index[pos_x][pos_y] = os.path.join(folder_name, _f_item)
            # load the file...
            # _item[0] is trace, _item[1] is retrace, _item[2] is header and
            # _itme[3] is piezo
            _item = get_file(os.path.join(folder_name, _f_item))
            #pos_y = list(file_list).index(_f_item)
            #trace.append(_item[0])
            #retrace.append(_item[1])
            header.append(_item[2])
            piezo[int(pos_x), int(pos_y)] = _item[3]
    #length = max([len(item.values()[0]) for item in trace] + 
    #                [len(item.values()[0]) for item in retrace])
    #trace = reorder_curves(trace, size[0], size[1], length)
    #retrace = reorder_curves(retrace, size[0], size[1], length)
    header = header[0]
    header['size_x'] = size[0]
    header['size_y'] = size[1]
    header['size'] = (size[0], size[1])
    header['scan_size'] = (header['scan_size'], header['scan_size'])
    header['number_curves'] = size[0] * size[1]
    header['data_location'] = dir_name
    #return [trace, retrace, header, piezo]
    return file_index, header, piezo

def reorder_curves(data, size_x, size_y, length):
    """
        Reorder the curves to be in two 3d array, one for the trace and one for
        the retrace curve.
        
        * Parameters :
            data : dict
                contains a list of dictionnaries which contains at least 'lvdt'
                and 'defl'.
                e.g. data[0]['defl'] = array_like
            size_x : int
                The x size of the final array.
            size_y : int
                The y size of the final array.
            length : int
                The z size of the final array (the length of the curves).
        
        * Returns :
            data : dict
                data['lvdt'] : numpy.array
                    Contains the 3d array of the piezo position
                    data['lvdt'][0,0,:] is the curve at pos_x = 0, pos_y = 0
                data['defl'] : numpy.array
                    Contains the 3d array of the deflection
                    data['lvdt'][0,0,:] is the curve at pos_x = 0, pos_y = 0
    """
    # Create the dictionnary that will contain arrays of the correct size
    out = {}
    for key in ['lvdt', 'defl']:#data[0].keys():
        out[key] = numpy.empty((size_x, size_y+1, length))
    # Fill them with the complete curves
    pos = -1
    for item in data:
        pos += 1
        for key in out:
            if key == 'lvdt':
                out[key][pos/size_x, pos % size_x, :] = complete_array(
                                                                item[key],
                                                                length,
                                                                'add')
            else:
                out[key][pos/size_x, pos % size_x, :] = complete_array(
                                                                item[key],
                                                                length)
    return out

def complete_array(array, length, add_type='idem'):
    """
        Complete the array with arbitrary values at the end.
        
        * Parameters :
            array : array_like
                The array to be completed.
            length : int
                The final length
            add_type : str
                The type of completion (*default* : 'idem')
            
                * 'idem' : adds the same values at the end of the array
                * 'add' : adds cummulative (n = n-1 + (n-2 - n-1)) so that the
                          arrays increase linearly at the end.
        
        * Returns :
            array : numpy array
                The completed array.
    """
    array = list(array)
    if add_type == 'idem':
        array = numpy.r_[array, [array[-1]] * (length - len(array))]
    elif add_type == 'add':
        step = array[-1] - array[-2]
        start = array[-1] + step
        stop = start + step * (length - len(array))
        array = numpy.r_[array, numpy.arange(start, stop, step)]
    return array
    
def get_file(file_name):
    """
        Get the raw data and header. Need to be ordered to be incorporated in
        OpenFovea.
        
        * Parameters :
            file_name : str
                The complete path for the file to open.
        
        * Returns :
            trace : dict
                Contains the arrays for the trace curve. This dictionnary is
                composed of two 1D numpy array.
                
                * trace['lvdt'] for the x axis
                * trace['defl'] for the deflection
            retrace : dict
                Contains the arrays for the retrace curve. This dictionnary is
                composed of two 1D numpy array.
                
                * retrace['lvdt'] for the x axis
                * retrace['defl'] for the deflection
            header : dict
                The note dictionnary
            piezo : float
                The height of the piezo at the end of the indentation
    """
    header = {}
    file_id = open(file_name, 'rb')
    ver_igor = unpack('=' + 'h', file_id.read(2))[0]
    # Currently, only version 5 is supported.
    if ver_igor == 5:
        order = GLOB_VARS.asylum5_order
        header_list = GLOB_VARS.asylum5_header
    # Get the headers
    for item in order:
        _ud = header_list[item]
        header[item] = _ud[2](unpack('='+_ud[0], file_id.read(_ud[1])))
    # Some more information from the headers
    if header['type'] != 0:
        _ud = TYPE[header['type']]
        data = unpack('=' + str(header['nbr_points']) + _ud[0],
                      file_id.read(header['nbr_points'] * _ud[1]))
        if ver_igor == 5:
            _len = header['formula_size']
            header['formula_data'] = unpack('=' + str(_len)+'s',
                                            file_id.read(_len))[0]
            _len = header['note_size']
            note = unpack('=' + str(_len)+'s', file_id.read(_len))
            _len = header['data_eunit_size']
            header['extended_data_units'] = unpack('=' + str(_len) + 's',
                                                   file_id.read(_len))[0]
            header['dimension_units'] = [
                        unpack('=' + str(_len) + 'c', file_id.read(_len)) 
                        for _len in header['dim_eunit_size']
                        ]
            header['dimension_label'] = [
                        unpack('=' + str(_len) + 's', file_id.read(_len))
                        for _len in header['dim_label_size']
                        ]
            _len = header['string_indice_size']
            header['string_indice'] = unpack('=' + str(_len) + 'l',
                                             file_id.read(_len * 4))
    data = numpy.asanyarray(data)
    if header['n_dim'][2]:
        raise TypeError('Problem in asylum file.\n'+
                        'Want to reshape from array : %s to %s' %
                        (data.shape, (header['n_dim'][1], header['n_dim'][0])))
        
 #       data.shape = (header['n_dim'][1], header['n_dim'][0], header['n_dim'][2])
    else:
        data.shape = (header['n_dim'][1], header['n_dim'][0])
    # I should reorder the header to have the minimal.
    header = parse_note(note[0])
    file_id.close()
    average = average_signal(data[1], data[2], final_length=512)
    # Get the middle point to separate the trace from the retrace curve
    middle_point = numpy.nonzero(average[0] == average[0].max())[0][0]
    trace = {
             'defl' : average[1][:middle_point][::-1] * 1e9,
             'lvdt' : average[0][:middle_point][::-1] * -1e9,
             }
    retrace = {
             'defl' : average[1][middle_point:]* 1e9,
             'lvdt' : average[0][middle_point:]* -1e9,
             }
    return [trace, retrace, header, data[2].max() * -1e9]
    
def parse_note(note):
    """
        Extracts the usefull data from the note.
        
        * Parameters :
            note : dict
                The note extracted from asylum file.
            
        * Returns :
            extracted : dict
                The usefull note.
    """
    # Usefull data, and the way to extract them are contained in the global
    # variable NOTE_INDEX.
    extracted = GLOB_VARS.common_header
    for key in NOTE_INDEX:
        _text = NOTE_INDEX[key][0]
        _extract = NOTE_INDEX[key][1]
        _index = note.index(_text)
        _end = note[_index+1:].index('\r') + _index + 1
        sub_note = note[_index:_end]
        extracted[key] = _extract(sub_note)
    try:
        extracted['date'] = datetime.strptime(
                                extracted['strdate'] + extracted['strtime'],
                                '%a, %b %d, %Y%I:%M:%S %p')
    except ValueError:
        extracted['date'] = datetime.strptime(
                                extracted['strdate'] + extracted['strtime'],
                                '%Y-%m-%d%I:%M:%S %p')
    extracted['Microscope'] = 'Asylum'
    return extracted

def get_dir_list(folder):
    """
        
        Gets the folder list of an asylum foler.
        
        Asylum folders are organized like this :
        
        $ ls FMLine000
        Line0000Point0000.ibw   Line0000Point0001.ibw
        $ ls FMLine001
        Line0001Point0000.ibw   Line0001Point0001.ibw 
        
        get_asylum_folder_list simply returns the ordered folder list that match
        this organization.
        
        * Parameters :
            folder : string
                The path for the folder.
        
        * Returns :
            folder_list : list
                The list of the folders.
    """
    folder_list = os.listdir(folder)
    # we should have the first folder being filenameNNNN
    f_index = [bool(re.match("FMLine\d+", x)) for x in folder_list]
    try:
        basename =  folder_list[f_index.index(True)][:-4]
    except ValueError:
        # There is no True elements in this folder...
        folder_list = []
    else:
        f_index = [bool(x.find(basename) + 1) for x in folder_list]
        f_index = numpy.asarray(f_index)
        folder_list = numpy.asarray(folder_list)
        # Get only the correct items
        folder_list = folder_list[f_index]
        folder_list.sort()
    return folder_list

def get_file_list(folder):
    """
        Gets the file list from an asylum folder.
        
        * Parameters :
            folder : string
                The path for the folder.
            
        * Returns :
            file_list : list
                The list of the files.
    """
    # Sort the file list
    file_list = numpy.asarray(os.listdir(folder))
    file_index = numpy.asarray([bool(x.find('.ibw')) for x in file_list])
    file_list = file_list[file_index]
    file_list.sort()
    return file_list

def load_curve(fid, pos, dtype, curve_index):
    """
        Load the curve on the fly.
        
        * Parameters :
            fid : instance of file id (obtained when opening a file)
            
            pos : list : [pos_x, pos_y]
                  The position of the curve to load.
            
            dtype : str
                    'trace_x', 'trace_y', 'retrace_x', 'retrace_y'
                  
        * Returns :
            curve : arraytrace['lvdt']
    """
    if type(fid) == list:
        data = get_file(fid[pos[0]][pos[1]])
    else:
        fcfolder = os.path.join('FMLine%04i'%pos[0], 'Line%04iPoint%04i.ibw'%(pos[0], pos[1]))
        file_path = os.path.join(fid, fcfolder)
        data = get_file(file_path)
    if 'retrace' in dtype:
        data = data[1]
    elif 'trace' in dtype:
        data = data[0]
    if 'x' in dtype:
        data = data['lvdt']
    elif 'y' in dtype:
        data = data['defl']
    #print fid[pos[0]][pos[1]]
    #print pos
    #print dtype
    #print curve_index
    return data
def average_signal(signal, x_scale, win_size=100, final_length=None):
    """
        Average the signal versus time.
        
        * Parameters :
            signal : array_like
                the array which contains the signal
            x_scale : array_like
                The array which contains the x scale.
            win_size : int
                The size (in pixel) of the window (*Default* = 100)
            
        * Returns :
            average_x : array_like
                The array that contains the new x scale.
            average : array_like
                The array that contains the averaged signal
    """
    begin = 0
    end = len(signal)
    if final_length is not None:
        win_size = int(end / final_length)
    t_slice = numpy.arange(begin, end, win_size)
    average = []
    average_x = []
    for begin, end in zip(t_slice[:-1], t_slice[1:]):
        average.append(signal[begin:end].mean())
        average_x.append(x_scale[begin])
        
    return [numpy.array(average_x), numpy.array(average)]


NOTE_INDEX = {'spring_constant' : ['\rSpringConstant:',
                                    lambda x : float(x[16:])],
              'scan_size' : ['\rScanSize:', lambda x : float(x[10:]) * 1e6],
              'size_x' : ['\rScanPoints:', lambda x : int(x[12:])],
              'size_y' : ['\rScanLines:', lambda x : int(x[11:])],
              'sensit_deflection' : ['\rTriggerDeflection:',
                                     lambda x : float(x[19:])],
              'strdate' : ['\rDate', 
                        lambda x : x[6:]],
              'strtime' : ['\rTime', 
                        lambda x : x[6:]],
              'piezo_pos' : ['\rExtendZ:', lambda x : float(x[9:]) * 1e6],
              'number_curves': ['\rXOPVersion', lambda x : 1], # just to force 1
              }
MIN_HEADER = ['scan_size', 'number_curves', 'size_x', 'size_y',
              'spring_constant', 'sensit_deflection', 'date']
TYPE = {2 : ['f', 4], #'float',
        8 : ['c', 1], #'char',
        16 : ['i', 4], #'int16',
        32 : ['l', 4],#'int32',
        72 : None, #'ubit8',
        80 : None, #'uint16',
        96 : None, #'uint',
        }

if __name__ == '__main__':
#    import sys
#    sys.path.append(os.path.abspath('./'))
    ASYLUM5_FILE = '../../../docs/data/files/Asylum/Cell0005.ibw'
    ASYLUM5_DIR = '/home/charles/AFM/Asylum Files/Figures/Archive'
    ASYLUM5_DIR2 = '/home/charles/AFM/Asylum Files/Figures/remove/CPA22_00'
#    import pylab
#    TRACE, RETRACE, HEADER, PIEZO = load(ASYLUM5_DIR2)
#    pylab.plot(TRACE[0], TRACE[1][0][0], 'ro')
#    pylab.hold(True)
#    pylab.plot(RETRACE[0], RETRACE[1][0][0], 'bo')
#    pylab.xlabel('Scanner extention [nm]')
#    pylab.ylabel('Cantilever deflection [nm]')
#    pylab.show()
#    print header
    import doctest
    doctest.testmod()
