import csv
import os
import zipfile
import tempfile
from copy import deepcopy
#from datetime import datetime
from dateutil import parser as date_parser

import numpy
import Image

from common import Variables
GLOB_VARS = Variables()

#CHANNEL_X = 'vDeflection'
#CHANNEL_X = 'capacitiveSensorHeight' #
CHANNEL_X = 'height'  # 'capacitiveSensorHeight'

def load(file_name):
    '''
    Loads jpk force volume files
    '''
    ftype = get_file_type(file_name)
    value = {
        'adv_mat' : None,
        'ret_mat' : None,
        'header' : None,
        'fid' : None
    }
    
    if ftype == 'txt':
        value['adv_mat'], value['ret_mat'], value['header'] = load_csv(file_name)
        value['piezo'] = numpy.zeros(value['header']['size'])
    elif ftype == 'zip':
        value['fid'], value['header'] = load_zip(file_name)
        value['piezo'] = None
    return value

def load_csv(file_name):
    
    this_file=open(file_name)
    header = get_header_csv(this_file)
    this_file.seek(0)
    this_csv=csv.reader(this_file, delimiter=' ')
    index=-1
    for line in this_csv:
        if len(line) == 3:
            # We are in the header
            if line[1] == 'iLength:':
                x_size = int(line[2])
            elif line[1] == 'jLength:':
                y_size = int(line[2])
            elif line[1] == 'kLength:':
                curve_length = int(line[2])
            elif line[1] == 'index:':
                index = int(line[2])
            elif line[1] == 'iIndex:':
                x_pos = int(line[2])
            elif line[1] == 'jIndex:':
                y_pos = int(line[2])
            elif line[1] == 'direction:':
                direction = line[2]
        elif len(line) == 1:
            # We are between headers
            if index == -1:
                # We are at the very beginning and have to build the matrices
                # that will be used to store the data.
                shape = (x_size, y_size, curve_length)
                adv_mat = [numpy.zeros(shape) for i in range(4)]
                ret_mat = [numpy.zeros(shape) for i in range(4)]
            else:
                data_pos = 0
            pass
        elif len(line) == 4:
            # We are in the datas
            if direction == 'trace':
                (adv_mat[0][x_pos, y_pos, data_pos],
                 adv_mat[1][x_pos, y_pos, data_pos],
                 adv_mat[2][x_pos, y_pos, data_pos],
                 adv_mat[3][x_pos, y_pos, data_pos]) = line
            elif direction == 'retrace':
                (ret_mat[0][x_pos, y_pos, data_pos],
                 ret_mat[1][x_pos, y_pos, data_pos],
                 ret_mat[2][x_pos, y_pos, data_pos],
                 ret_mat[3][x_pos, y_pos, data_pos]) = line
            data_pos += 1
    
    # reorder the force curves...
    adv_mat[0] = reorder_array(adv_mat[0], 1)
    adv_mat[1] = reorder_array(adv_mat[1], header['spring_constant'])
    adv_mat[2] = reorder_array(adv_mat[2], 1)
    adv_mat[3] = reorder_array(adv_mat[3], 1)
    ret_mat[0] = reorder_array(ret_mat[0], 1)
    ret_mat[1] = reorder_array(ret_mat[1], header['spring_constant'])
    ret_mat[2] = reorder_array(ret_mat[2], 1)
    ret_mat[3] = reorder_array(ret_mat[3], 1)
    return [adv_mat, ret_mat, header]

def load_single(file_name):
    """
        Load jpk file made of a single force curve.
    """
    global_header = get_header_csv(file_name)
    this_file=open(file_name)
    this_csv=csv.reader(this_file, delimiter=' ')
    index=-1
    jpk_fv_con = GLOB_VARS.jpk_fc_connector
    header = {}
    vec_trace = [[],[],[],[]]
    vec_retrace = [[],[],[],[]]
    for line in this_csv:
        if len(line) <= 1:
            pass
        elif line[0] == '#': # It's a header
            if jpk_fv_con.has_key(line[1]):
                key = jpk_fv_con[line[1]][0]
                fct = jpk_fv_con[line[1]][1]
                header[key] = fct(line[2])
        else: # It's a data point
            if header['direction'] == 'trace':
                for item, vect in zip(line, vec_trace):
                    vect.append(item)
            if header['direction'] == 'retrace':
                for item, vect in zip(line, vec_retrace):
                    vect.append(item)
    shape = (1,1,len(vec_trace[0]))
    arr_trace = [numpy.zeros(shape) for i in range(4)]
    
    shape = (1,1,len(vec_retrace[0]))
    arr_retrace = [numpy.zeros(shape) for i in range(4)]
    
    for i in range(4):
        arr_trace[i][0,0,:] = numpy.asarray(vec_trace[i])
        arr_retrace[i][0,0,:] = numpy.asarray(vec_retrace[i])
    global_header['scan_size'] = (0, 0)
    global_header['number_curves'] = 1
    return [arr_trace, arr_retrace, global_header]

def reorder_array(this_array, factor):
    for x in range(this_array.shape[0]):
        for y in range(this_array.shape[1]):
            this_array[x,y,:] = this_array[x,y,::-1] * 1e9 / factor
            this_array[x,y,:] = this_array[x,y,:] - min(this_array[x,y,:])
    return this_array
    
def get_header_csv(file_name):
    """
        Get the header from files that have headers and data in the same file.
    """
    this_file = file_name
    this_csv=csv.reader(this_file, delimiter=' ')
    jpk_con = GLOB_VARS.jpk_connector
    header = GLOB_VARS.common_header
    for line in this_csv:
        if len(line) > 4:
            d_string = ''
            for x in line[2:]: d_string = d_string + str(x) + ' '
            d_string = d_string[:-1] # erase the last space
            line = [line[0], line[1], d_string]
        if len(line) == 3:
            # We are in the header
            if jpk_con.has_key(line[1]):
                key = jpk_con[line[1]][0]
                fct = jpk_con[line[1]][1]
                
                header[key] = fct(line[2])
    header['matrix_length'] = header['size_x']
    header['size'] = (header['size_x'], header['size_y'])
    header['scan_size'] = (header['scan_size'], header['scan_size'])
    return header


def load_arrays(fid):
    """
        This function loads the arrays from the NanoWizard3 zipped files.

            * Parameters :
                None

            * Returns :
                dictionnary with :
                    'Index' : The index array (i.e. the folder name in the
                              corresponding pixel) This index is generated from
                              the coordinate in the header of each curve. This
                              is useful if the index array automatically
                              generated is wrong.
                    'Piezo' : The piezo array. This is the height of the piezo
                              at the end of the indentation. This array is used
                              to generate the "zero force image" or "Topography
                              image".
        If the index of curves were missgenerated or lost, we can regenerate
        them from the headers. This take long time.
    """
    
    main_header = get_header_zip(fid.open('header.properties'), htype='main')
    shared_header = get_header_zip(fid.open('shared-data/header.properties'),
                                            htype='shared')
#    ############################################################################ 
#    # Generate an index array :
#    #
#    index_array = numpy.empty((main_header['size_x'], main_header['size_y']),
#                              dtype=int)
#    index_array[:]=-1
    piezo_array = numpy.zeros((main_header['size_x'], main_header['size_y']),
                              dtype=float)
#    
#    pixel_size = [main_header['msize_x'] / main_header['size_x'],
#                  main_header['msize_y'] / main_header['size_y']]
#    # works if center of array is [0, 0]. Here is to modify if we have
#    # different files. 
#    center = [(-main_header['mcenter_x'] +
#               (main_header['msize_x'] / 2))/pixel_size[0],
#              (-main_header['mcenter_y'] + 
#               (main_header['msize_y'] / 2))/pixel_size[1]]
    for index_nb in range(main_header['index_min'], main_header['index_max']+1):
        f_name = 'index/%s/header.properties'%index_nb
        index_header = get_header_zip(fid.open(f_name),
                                      htype='index')#, stop='position')
        # Current pixel position :
        pos = pos_from_index(index_nb,
                             size=[main_header['size_x'], main_header['size_y']],
                             mode=main_header['file_type'])
#        pos = [round(index_header['mpos_x'] / pixel_size[0] + center[0]
#                    - 0.5),
#              round(index_header['mpos_y'] / pixel_size[1] + center[1]
#                    - 0.5)]
#        index_array[pos[0], pos[1]] = index_nb
        gf = _global_factor(shared_header, CHANNEL_X)
        
        f_name = 'index/%i/segments/%i/'%(index_nb, 0)
        
        sgmt_header = get_header_zip(fid.open(f_name+'segment-header.properties'),
                                 htype='segment')
        sgmt_name = sgmt_header['%s.fname' % CHANNEL_X]
        
        curve = load_zip_curve(fid.open(f_name + sgmt_name),
                                dtype='signedinteger',
                                factor=gf)
        piezo_array[pos[0], pos[1]] = curve[-1]*1e9
    return {#'Index' : index_array,
            'Piezo' : piezo_array}


def load_zip(file_name):
    """
        Load file from zipped JPK.
        
        This will return only a header, as the FD curves will be loaded on
        demand.
    """
    
    header = GLOB_VARS.common_header
    fid = zipfile.ZipFile(file_name, 'r')
    
    main_header = get_header_zip(fid.open('header.properties'), htype='main')
    shared_header = get_header_zip(fid.open('shared-data/header.properties'),
                                            htype='shared')
    if main_header['size_unit'] == 'm':
        main_header['msize_x'] = main_header['msize_x']*1e9
        main_header['msize_y'] = main_header['msize_y']*1e9
    
    
    header['number_curves'] = main_header['index_max'] - \
                              main_header['index_min'] + 1
    header['scan_size'] = [main_header['msize_x'], main_header['msize_y']]
    header['size'] = [main_header['size_x'], main_header['size_y']]
    header['size_x'] = main_header['size_x']
    header['size_y'] = main_header['size_y']
    header['data_location'] = file_name
    header['Microscope'] = 'JPK'
    header['sensit_deflection'] = 1
    header['spring_constant'] = shared_header['spring_constant']
    return [fid, header]


def _get_lambda_factor(offset, factor):
    return lambda x: (x+offset)*factor


def _global_factor(header, dtype):
    """
        Header should be the shared header.
        dtype : str
                Is the type of curve you want to load.
                'height', 'vDeflection'
    """
    n_index = header['name'].index(dtype) 
    convert_list = header['conversion_list'][n_index].split(' ')
    gf = header['encode_multiplier'][n_index]
    of = header['encode_offset'][n_index]
    fct = [_get_lambda_factor(of, gf)]
    for conv_item in convert_list:
#        t_fact = header[conv_item+'_multiplier'][n_index]
#        t_off = header[conv_item+'_offset'][n_index]
        if conv_item+'_multiplier' in header:
            if type(header[conv_item+'_multiplier']) == list:
                #print shared_header[conv_item+'_multiplier'][n_index]
                t_fact = header[conv_item+'_multiplier'][n_index]
            else:
                t_fact = header[conv_item+'_multiplier']
        else:
            t_fact=1
        if conv_item+'_offset' in header:
            if type(header[conv_item+'_offset']) == list:
                #print shared_header[conv_item+'_multiplier'][n_index]
                t_off = header[conv_item+'_offset'][n_index]
            else:
                t_off = header[conv_item+'_offset']
        else:
            t_off=0
        fct.append(_get_lambda_factor(t_off, t_fact))
    return fct


def index_from_pos(pos, size, mode):
    """
        Generate the curve index in function of the curve position.
        This can differ corresponding to the imaging mode used.

          qi-map         force-map
            ...             ...
        ---------->     ----------->
        ---------->     <-----------
        ---------->     ----------->
    """
    index = 0
    if mode == 'force-scan-map':
        if pos[1] % 2:
            index = pos[1] * size[0] + (size[0] - pos[0] - 1)
        else:
            index = pos[1] * size[0] + pos[0]
    elif mode == 'quantitative-imaging-map':
        index = pos[1] * size[0] + pos[0]
    else:
        raise TypeError('Mode %s is not known' % mode)
    return index

def pos_from_index(index, size, mode):
    """
        Generate the curve position in function of the curve index.
        This can differ corresponding to the imaging mode used.

          qi-map         force-map
            ...             ...
        ---------->     ----------->
        ---------->     <-----------
        ---------->     ----------->
    """
    pos = [0, 0]
    if mode == 'force-scan-map':
        pos[1] = int(index/(size[0]))
        if pos[1] % 2:
            pos[0] = size[0] - (index) % (size[0]) - 1
        else:
            pos[0] = index % (size[0])
    elif mode == 'quantitative-imaging-map':
        pos[1] = int(index/(size[0]))
        pos[0] = index % (size[0])
    else:
        raise TypeError('Mode %s is not known' % mode)
    return pos

def load_curve(fid, index, dtype):
    """
        Load the curve on the fly.
        
        * Parameters :
            fid : instance of file id (obtained when opening a file)

            mode : str
                   The imaging mode used (see index_from_pos for more details).

            dtype : str
                    'trace_x', 'trace_y', 'retrace_x', 'retrace_y'
                  
        * Returns :
            curve : array
        
    """

    header = get_header_zip(fid.open('header.properties'), htype='main')
    
    c_index = index_from_pos(index,
                       size=[header['size_x'], header['size_y']],
                       mode=header['file_type'])
    # read the shared header...
    shared_header = get_header_zip(fid.open('shared-data/header.properties'),
                                            htype='shared')
    
    # Read the header for this pixel...
    if c_index < 0:
        return None
    sh_name = 'index/%i/header.properties'%c_index
    segment_header=get_header_zip(fid.open(sh_name), htype='index')
    if 'retrace' in dtype:
        sgmt_nbr = 1
    elif 'trace' in dtype:
        sgmt_nbr = 0
    f_name = 'index/%i/segments/%i/'%(c_index, sgmt_nbr)
    sgmt_header = get_header_zip(fid.open(f_name+'segment-header.properties'),
                                 htype='segment')
    if 'x' in dtype:
        sgmt_type = CHANNEL_X
    elif 'y' in dtype:
        sgmt_type = 'vDeflection'
    # Creates the conversion factors :
    gf = _global_factor(shared_header, sgmt_type)
    sgmt_name = sgmt_header[sgmt_type+'.fname']
    sgmt_format = sgmt_header[sgmt_type + '.format']
    curve = load_zip_curve(fid.open(f_name + sgmt_name),
                                dtype='signedinteger',
                                factor=gf)
    #curve = curve - min(curve)
    curve = curve*1e9
    return curve[::-1]
    

def load_zip_curve(fid, stype='raw', dtype='signedinteger', factor=[lambda x:x]):
    '''
    load the curve in the file.
    
    * Parameters :
        fid : file object
              curve file extracted from the zip file.
        stype : str
                save type. 'raw', 'ascii'
                default set to 'raw'
    * Returns :
        curve : list
                list of point that are loaded.
    '''
    dtype = '>' + DTYPES[dtype]
    array_str = fid.read()
    array = numpy.fromstring(array_str, dtype=dtype)
    test = 10
    for item in factor:
        array = item(array)
        test = item(test)
    
    return array
def get_header_zip(header_file, htype='main', stop=None):
    '''
    Get the headers from zip file.
    
    * Parameters :
        header_file : file object
                      header file extracted from the zip file.
        htype : str
                header type. 'main', 'index' or 'segment'.
                default set to 'main'
        
        stop : str or None
                if define, the execution will stop as soon as the information
                wanted is found.
                values are : 'position'
    
    * Returns :
        header : dict
                 dictionnary with the header with the usefull key - value pair.
                 Values that are not usefull for OpenFovea are lost.
    '''
    # Every header has its first line as a date
    date = date_parser.parse(header_file.readline()[1:])
    if htype == 'main':
        __header = ZIP_MAIN_HEADER
    elif htype == 'index':
        __header = ZIP_INDEX_HEADER
    elif htype == 'segment':
        __header = ZIP_SEGMENT_HEADER
    elif htype == 'shared':
        __header = ZIP_SHARED_HEADER
    header = {}
    header['date'] = date
        
    for line in header_file.readlines():
        line_parse = line.split('=')
        if htype == 'shared':
            head_parse = line_parse[0].split('.')
            if head_parse[1].isdigit():
                canal_nb = head_parse[1]
                head_parse[1] = 'xx'
            line_parse[0] = '.'.join(head_parse)
        
        param = __header[line_parse[0]]
        if htype == 'shared':
            head_parse = param[0].split('.')
            if len(head_parse) > 1 and (head_parse[1] == 'xx' or head_parse[1].isdigit()):
                head_parse[1] = canal_nb
                head_parse = '.'.join(head_parse)
                param[0] = head_parse
        if 'time' in line_parse[0]:
            line_parse[1] = ''.join(line_parse[1].split('\\'))
        if line_parse[1] in ['NaN', 'NaN\n']:
            header[param[0]] = numpy.nan
        else:
            header[param[0]] = param[1](line_parse[1])
        del(param)
    if htype == 'shared':
        # We reorder the header, so that we have one 'name' list, which
        # contains the name of the channels and the converters as a 'multiplier'
        # list which contains the corresponding converters.
        new_header = {}
        channel_list = {}
        for item in header:
            splitted = item.split('.')
            if len(splitted) > 1 and splitted[1].isdigit():
                if not new_header.has_key(splitted[0]):
                    new_header[splitted[0]] = \
                                    [1 for i in range(header['channel_count'])]
                new_header[splitted[0]][eval(splitted[1])] = header[item]
                if splitted[0] == 'name':
                    channel_list[splitted[1]]=header[item]
            else:
                new_header[item] = header[item]
        keys = channel_list.keys()
        keys.sort()
        new_header['channel_list'] = [channel_list[i] for i in keys]
        header = new_header
    header_file.close()
    return header
    
def get_file_type(file_name):
    if zipfile.is_zipfile(file_name):
        ftype = 'zip'
    else:
        ftype = 'txt'
    return ftype

DTYPES = {'signedinteger' : 'i',
         'unsignedinteger' : 'I'}

ZIP_MAIN_HEADER = {"jpk-data-file" : ['trash', lambda x: str(x).strip('\n')],
"file-format-version" : ['trash', lambda x: str(x).strip('\n')],
"file-format-features" : ['trash', lambda x: str(x).strip('\n')],
"type" : ['file_type', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.indexes.type" : ['trash', eval],
"quantitative-imaging-map.indexes.min" : ['index_min', eval],
"quantitative-imaging-map.indexes.max" : ['index_max', eval],
"quantitative-imaging-map.scan-number" : ['trash', eval],
"quantitative-imaging-map.start-time" :
                                    ['trash',
                                     lambda x: date_parser.parse(x)],
"quantitative-imaging-map.end-time" : 
                                    ['trash',
                                    lambda x: date_parser.parse(x)],
"quantitative-imaging-map.description.comment" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.description.name" : 
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.description.probe" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.description.user-name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.description.instrument" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.description.source-software" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.description.modification-software" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.settings.force-settings.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.settings.force-settings.relative-setpoint" :
                                    ['trash', eval],
"quantitative-imaging-map.settings.force-settings.control-settings.control-settings-type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.settings.force-settings.control-settings.next-line-additional-delay" :
                                    ['trash', eval],
"quantitative-imaging-map.settings.force-settings.control-settings.next-line-additional-retract" :
                                    ['trash', eval],
"quantitative-imaging-map.settings.force-settings.control-settings.max-retries-per-position" :
                                    ['trash', eval],
"quantitative-imaging-map.settings.force-settings.control-settings.max-bad-adjecent-pixels" :
                                    ['trash', eval],
# Next, we capitalize the first letter => 'false' -> 'False' or 'true' -> 'True'
"quantitative-imaging-map.settings.force-settings.control-settings.closed-loop" :
                                    ['trash', lambda x: eval(x.title())],
"quantitative-imaging-map.settings.force-settings.control-settings.start-option.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.settings.force-settings.control-settings.start-option.additional-retract" :
                                    ['trash', eval],
"quantitative-imaging-map.settings.force-settings.control-settings.start-option.start-time" :
                                    ['trash', eval],
"quantitative-imaging-map.settings.force-settings.control-settings.start-option.motion-time" :
                                    ['trash', eval],
"quantitative-imaging-map.settings.force-settings.control-settings.ttl-outputs.pins.list" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.settings.force-settings.control-settings.baseline-adjust-settings.enabled" :
                                    ['trash', lambda x: eval(x.title())],
"quantitative-imaging-map.settings.force-settings.control-settings.baseline-adjust-settings.interval" :
                                    ['trash', eval],
"quantitative-imaging-map.settings.force-settings.extend.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.settings.force-settings.extend.identifier.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.settings.force-settings.extend.identifier.name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.settings.force-settings.extend.style" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.settings.force-settings.extend.duration" :
                                    ['trash', eval],
"quantitative-imaging-map.settings.force-settings.extend.num-points" :
                                    ['trash', eval],
"quantitative-imaging-map.settings.force-settings.extend.z-start" :
                                    ['trash', eval],
"quantitative-imaging-map.settings.force-settings.extend.z-end" :
                                    ['trash', eval],
"quantitative-imaging-map.settings.force-settings.extend.setpoint" :
                                    ['trash', eval],
"quantitative-imaging-map.settings.force-settings.retract.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.settings.force-settings.retract.identifier.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.settings.force-settings.retract.identifier.name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.settings.force-settings.retract.style" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.settings.force-settings.retract.duration" :
                                    ['trash', eval],
"quantitative-imaging-map.settings.force-settings.retract.num-points" :
                                    ['trash', eval],
"quantitative-imaging-map.settings.force-settings.retract.z-start" :
                                    ['trash', eval],
"quantitative-imaging-map.settings.force-settings.retract.z-end" :
                                    ['trash', eval],
"quantitative-imaging-map.settings.force-settings.retract.setpoint" :
                                    ['trash', eval],
"quantitative-imaging-map.settings.force-settings.data-description.comment" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.settings.force-settings.data-description.name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.settings.force-settings.data-description.probe" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.settings.force-settings.data-description.user-name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.settings.force-settings.data-description.instrument" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.settings.force-settings.data-description.source-software" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.settings.force-settings.data-description.modification-software" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.position-pattern.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.position-pattern.back-and-forth" :
                                    ['trash', lambda x: eval(x.title())],
"quantitative-imaging-map.position-pattern.grid.xcenter" :
                                    ['mcenter_x', eval],
"quantitative-imaging-map.position-pattern.grid.ycenter" :
                                    ['mcenter_y', eval],
"quantitative-imaging-map.position-pattern.grid.ulength" :
                                    ['msize_x', eval],
"quantitative-imaging-map.position-pattern.grid.vlength" :
                                    ['msize_y', eval],
"quantitative-imaging-map.position-pattern.grid.theta" :
                                    ['trash', eval],
"quantitative-imaging-map.position-pattern.grid.reflect" :
                                    ['trash', lambda x: eval(x.title())],
"quantitative-imaging-map.position-pattern.grid.unit.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.position-pattern.grid.unit.unit" :
                                    ['size_unit', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.position-pattern.grid.ilength" :
                                    ['size_x', eval],
"quantitative-imaging-map.position-pattern.grid.jlength" :
                                    ['size_y', eval],
"quantitative-imaging-map.feedback-mode.name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.spm-scanner-mode.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.spm-scanner-mode.software-linearized" :
                                    ['trash', lambda x: eval(x.title())],
"quantitative-imaging-map.spm-scanner-mode.xy-scanner.scanner" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.spm-scanner-mode.xy-scanner.name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.spm-scanner-mode.xy-scanner.fancy-name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"quantitative-imaging-map.spm-scanner-mode.xy-scanner.description" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.indexes.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.indexes.min" :
                                    ['index_min', eval],
"force-scan-map.indexes.max" :
                                    ['index_max', eval],
"force-scan-map.scan-number" :
                                    ['trash', eval],
"force-scan-map.start-time" :
                                    ['trash', lambda x: date_parser.parse(x)],
"force-scan-map.end-time" :
                                    ['trash', lambda x: date_parser.parse(x)],
"force-scan-map.comment" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.description.comment" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.description.name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.description.probe" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.description.user-name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.description.instrument" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.description.source-software" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.description.modification-software" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.settings.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.settings.force-settings.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.settings.force-settings.extend-k-length" :
                                    ['trash', eval],
"force-scan-map.settings.force-settings.retract-k-length" :
                                    ['trash', eval],
"force-scan-map.settings.force-settings.extended-pause-k-length" :
                                    ['trash', eval],
"force-scan-map.settings.force-settings.retracted-pause-k-length" :
                                    ['trash', eval],
"force-scan-map.settings.force-settings.z-start-pause-option.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.settings.force-settings.z-end-pause-option.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.settings.force-settings.extend-scan-time" :
                                    ['trash', eval],
"force-scan-map.settings.force-settings.retract-scan-time" :
                                    ['trash', eval],
"force-scan-map.settings.force-settings.retracted-pause-time" :
                                    ['trash', eval],
"force-scan-map.settings.force-settings.extended-pause-time" :
                                    ['trash', eval],
"force-scan-map.settings.force-settings.data-description.comment" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.settings.force-settings.data-description.name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.settings.force-settings.data-description.probe" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.settings.force-settings.data-description.user-name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.settings.force-settings.data-description.instrument" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.settings.force-settings.data-description.source-software" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.settings.force-settings.data-description.modification-software" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.settings.force-settings.start-with-retract" :
                                    ['trash', lambda x: eval(x.title())],
"force-scan-map.settings.force-settings.control-settings-type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.settings.force-settings.closed-loop" :
                                    ['trash', lambda x: eval(x.title())],
"force-scan-map.settings.force-settings.start-option.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.settings.force-settings.ttl-outputs.pins.list" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.settings.force-settings.pause-before-first.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.settings.force-settings.pause-before-first.identifier.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.settings.force-settings.pause-before-first.identifier.name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.settings.force-settings.pause-before-first.style" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.settings.force-settings.pause-before-first.duration" :
                                    ['trash', eval],
"force-scan-map.settings.force-settings.pause-before-first.num-points" :
                                    ['trash', eval],
"force-scan-map.settings.force-settings.pause-before-first.pause-option.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.settings.force-settings.pause-before-first.height-limit" :
                                    ['trash', eval],
"force-scan-map.settings.force-settings.force-baseline-adjust-settings.enabled" :
                                    ['trash', lambda x: eval(x.title())],
"force-scan-map.settings.force-settings.force-baseline-adjust-settings.interval" :
                                    ['trash', eval],
"force-scan-map.settings.force-settings.force-baseline-adjust-settings.beginOfLine" :
                                    ['trash', lambda x: eval(x.title())],
"force-scan-map.settings.force-settings.force-baseline-adjust-settings.deadtimeBeforeSamples" :
                                    ['trash', eval],
"force-scan-map.settings.force-settings.force-baseline-adjust-settings.averageSamples" :
                                    ['trash', eval],
"force-scan-map.settings.force-settings.line-clock.active.extend" :
                                    ['trash', lambda x: eval(x.title())],
"force-scan-map.settings.force-settings.line-clock.active.retract" :
                                    ['trash', lambda x: eval(x.title())],
"force-scan-map.settings.force-settings.relative-setpoint" :
                                    ['trash', eval],
"force-scan-map.settings.force-settings.relative-z-start" :
                                    ['trash', eval],
"force-scan-map.settings.force-settings.relative-z-end" :
                                    ['trash', eval],
"force-scan-map.position-pattern.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.position-pattern.back-and-forth" :
                                    ['trash', lambda x: eval(x.title())],
"force-scan-map.position-pattern.grid.xcenter" :
                                    ['mcenter_x', eval],
"force-scan-map.position-pattern.grid.ycenter" :
                                    ['mcenter_y', eval],
"force-scan-map.position-pattern.grid.ulength" :
                                    ['msize_x', eval],
"force-scan-map.position-pattern.grid.vlength" :
                                    ['msize_y', eval],
"force-scan-map.position-pattern.grid.theta" :
                                    ['trash', eval],
"force-scan-map.position-pattern.grid.reflect" :
                                    ['trash', lambda x: eval(x.title())],
"force-scan-map.position-pattern.grid.unit.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.position-pattern.grid.unit.unit" :
                                    ['size_unit', lambda x: str(x).strip('\n')],
"force-scan-map.position-pattern.grid.ilength" :
                                    ['size_x', eval],
"force-scan-map.position-pattern.grid.jlength" :
                                    ['size_y', eval],
"force-scan-map.feedback-mode.name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.spm-scanner-mode.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.spm-scanner-mode.software-linearized" :
                                    ['trash', lambda x: eval(x.title())],
"force-scan-map.spm-scanner-mode.xy-scanner.scanner" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.spm-scanner-mode.xy-scanner.name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.spm-scanner-mode.xy-scanner.fancy-name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-map.spm-scanner-mode.xy-scanner.description" :
                                    ['trash', lambda x: str(x).strip('\n')],
}

ZIP_INDEX_HEADER = {
"type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.force-segments.count" :
                                    ['count', eval],
"force-scan-series.header.type" :
                                    ['trash' , lambda x: str(x).strip('\n')],
"force-scan-series.header.position.x" :
                                    ['mpos_x', eval],
"force-scan-series.header.position.y" :
                                    ['mpos_y', eval],
"force-scan-series.header.position-index" :
                                    ['pos_index', eval],
"force-scan-series.header.force-settings.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.header.force-settings.extend-k-length" :
                                    ['trash', eval],
"force-scan-series.header.force-settings.retract-k-length" :
                                    ['trash', eval],
"force-scan-series.header.force-settings.extended-pause-k-length" :
                                    ['trash', eval],
"force-scan-series.header.force-settings.retracted-pause-k-length" :
                                    ['trash', eval],
"force-scan-series.header.force-settings.z-start-pause-option.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.header.force-settings.z-end-pause-option.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.header.force-settings.extend-scan-time" :
                                    ['trash', eval],
"force-scan-series.header.force-settings.retract-scan-time" :
                                    ['trash', eval],
"force-scan-series.header.force-settings.retracted-pause-time" :
                                    ['trash', eval],
"force-scan-series.header.force-settings.extended-pause-time" :
                                    ['trash', eval],
"force-scan-series.header.force-settings.data-description.comment" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.header.force-settings.data-description.name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.header.force-settings.data-description.probe" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.header.force-settings.data-description.user-name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.header.force-settings.data-description.instrument" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.header.force-settings.data-description.source-software" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.header.force-settings.data-description.modification-software" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.header.force-settings.start-with-retract" :
                                    ['trash', lambda x: eval(x.title())],
"force-scan-series.header.force-settings.control-settings-type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.header.force-settings.closed-loop" :
                                    ['trash', lambda x: eval(x.title())],
"force-scan-series.header.force-settings.start-option.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.header.force-settings.ttl-outputs.pins.list" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.header.force-settings.pause-before-first.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.header.force-settings.pause-before-first.identifier.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.header.force-settings.pause-before-first.identifier.name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.header.force-settings.pause-before-first.style" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.header.force-settings.pause-before-first.duration" :
                                    ['trash', eval],
"force-scan-series.header.force-settings.pause-before-first.num-points" :
                                    ['trash', eval],
"force-scan-series.header.force-settings.pause-before-first.pause-option.type" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.header.force-settings.pause-before-first.height-limit" :
                                    ['trash', eval],
"force-scan-series.header.force-settings.force-baseline-adjust-settings.enabled" :
                                    ['trash', lambda x: eval(x.title())],
"force-scan-series.header.force-settings.force-baseline-adjust-settings.interval" :
                                    ['trash', eval],
"force-scan-series.header.force-settings.force-baseline-adjust-settings.beginOfLine" :
                                    ['trash', lambda x: eval(x.title())],
"force-scan-series.header.force-settings.force-baseline-adjust-settings.deadtimeBeforeSamples" :
                                    ['trash', eval],
"force-scan-series.header.force-settings.force-baseline-adjust-settings.averageSamples" :
                                    ['trash', eval],
"force-scan-series.header.force-settings.line-clock.active.extend" :
                                    ['trash', lambda x: eval(x.title())],
"force-scan-series.header.force-settings.line-clock.active.retract" :
                                    ['trash', lambda x: eval(x.title())],
"force-scan-series.header.force-settings.relative-setpoint" :
                                    ['trash', eval],
"force-scan-series.header.force-settings.relative-z-start" :
                                    ['trash', eval],
"force-scan-series.header.force-settings.relative-z-end" :
                                    ['trash', eval],
"force-scan-series.description.comment" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.description.name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.description.probe" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.description.user-name" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.description.instrument" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.description.source-software" :
                                    ['trash', lambda x: str(x).strip('\n')],
"force-scan-series.description.modification-software" :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.force-segments.count' :
                                    ['count', eval],
'quantitative-imaging-series.header.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.header.position.x' :
                                    ['mpos_x', eval],
'quantitative-imaging-series.header.position.y' :
                                    ['mpos_y', eval],
'quantitative-imaging-series.header.position-index' :
                                    ['pos_index', eval],
'quantitative-imaging-series.header.quantitative-imaging-settings.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.header.quantitative-imaging-settings.relative-setpoint' :
                                    ['trash', eval],
'quantitative-imaging-series.header.quantitative-imaging-settings.control-settings.control-settings-type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.header.quantitative-imaging-settings.control-settings.next-line-additional-delay' :
                                    ['trash', eval],
'quantitative-imaging-series.header.quantitative-imaging-settings.control-settings.next-line-additional-retract' :
                                    ['trash', eval],
'quantitative-imaging-series.header.quantitative-imaging-settings.control-settings.max-retries-per-position' :
                                    ['trash', eval],
'quantitative-imaging-series.header.quantitative-imaging-settings.control-settings.max-bad-adjecent-pixels' :
                                    ['trash', eval],
'quantitative-imaging-series.header.quantitative-imaging-settings.control-settings.closed-loop' :
                                    ['trash', lambda x: eval(x.title())],
'quantitative-imaging-series.header.quantitative-imaging-settings.control-settings.start-option.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.header.quantitative-imaging-settings.control-settings.start-option.additional-retract' :
                                    ['trash', eval],
'quantitative-imaging-series.header.quantitative-imaging-settings.control-settings.start-option.start-time' :
                                    ['trash', eval],
'quantitative-imaging-series.header.quantitative-imaging-settings.control-settings.start-option.motion-time' :
                                    ['trash', eval],
'quantitative-imaging-series.header.quantitative-imaging-settings.control-settings.ttl-outputs.pins.list' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.header.quantitative-imaging-settings.control-settings.baseline-adjust-settings.enabled' :
                                    ['trash', lambda x: eval(x.title())],
'quantitative-imaging-series.header.quantitative-imaging-settings.control-settings.baseline-adjust-settings.interval' :
                                    ['trash', eval],
'quantitative-imaging-series.header.quantitative-imaging-settings.extend.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.header.quantitative-imaging-settings.extend.identifier.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.header.quantitative-imaging-settings.extend.identifier.name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.header.quantitative-imaging-settings.extend.style' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.header.quantitative-imaging-settings.extend.duration' :
                                    ['trash', eval],
'quantitative-imaging-series.header.quantitative-imaging-settings.extend.num-points' :
                                    ['trash', eval],
'quantitative-imaging-series.header.quantitative-imaging-settings.extend.z-start' :
                                    ['trash', eval],
'quantitative-imaging-series.header.quantitative-imaging-settings.extend.z-end' :
                                    ['trash', eval],
'quantitative-imaging-series.header.quantitative-imaging-settings.extend.setpoint' :
                                    ['trash', eval],
'quantitative-imaging-series.header.quantitative-imaging-settings.retract.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.header.quantitative-imaging-settings.retract.identifier.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.header.quantitative-imaging-settings.retract.identifier.name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.header.quantitative-imaging-settings.retract.style' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.header.quantitative-imaging-settings.retract.duration' :
                                    ['trash', eval],
'quantitative-imaging-series.header.quantitative-imaging-settings.retract.num-points' :
                                    ['trash', eval],
'quantitative-imaging-series.header.quantitative-imaging-settings.retract.z-start' :
                                    ['trash', eval],
'quantitative-imaging-series.header.quantitative-imaging-settings.retract.z-end' :
                                    ['trash', eval],
'quantitative-imaging-series.header.quantitative-imaging-settings.retract.setpoint' :
                                    ['trash', eval],
'quantitative-imaging-series.header.quantitative-imaging-settings.data-description.comment' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.header.quantitative-imaging-settings.data-description.name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.header.quantitative-imaging-settings.data-description.probe' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.header.quantitative-imaging-settings.data-description.user-name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.header.quantitative-imaging-settings.data-description.instrument' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.header.quantitative-imaging-settings.data-description.source-software' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.header.quantitative-imaging-settings.data-description.modification-software' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.description.comment' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.description.name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.description.probe' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.description.user-name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.description.instrument' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.description.source-software' :
                                    ['trash', lambda x: str(x).strip('\n')],
'quantitative-imaging-series.description.modification-software' :
                                    ['trash', lambda x: str(x).strip('\n')],
}

ZIP_SEGMENT_HEADER = {
#Fri Dec 16 13:09:06 CET 2011
'force-segment-header.force-segment-header-info.*' :
                                    ['trash', eval],
'force-segment-header.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header.num-points' :
                                    ['trash', lambda x: eval(x.title())],
'force-segment-header.duration' :
                                    ['trash', lambda x: eval(x.title())],
'force-segment-header.time-stamp' :
                                    ['trash', lambda x: date_parser.parse(x)],
'force-segment-header.baseline.baseline' :
                                    ['trash', lambda x: eval(x.title())],
'force-segment-header.baseline.measured' :
                                    ['trash', lambda x: eval(x.title())],
'force-segment-header.force-scan-flags.series-done' :
                                    ['trash', lambda x: eval(x.title())],
'force-segment-header.force-scan-flags.done-scanning' :
                                    ['trash', lambda x: eval(x.title())],
'force-segment-header.force-scan-flags.aborted' :
                                    ['trash', lambda x: eval(x.title())],
'force-segment-header.force-scan-flags.data-segment' :
                                    ['trash', lambda x: eval(x.title())],
'force-segment-header.force-scan-flags.z-start-out-of-range' :
                                    ['trash', lambda x: eval(x.title())],
'force-segment-header.force-scan-flags.z-end-out-of-range' :
                                    ['trash', lambda x: eval(x.title())],
'force-segment-header.force-scan-flags.setpoint-out-of-range' :
                                    ['trash', lambda x: eval(x.title())],
'force-segment-header.force-scan-flags.surface-incorrect' :
                                    ['trash', lambda x: eval(x.title())],
'force-segment-header.force-scan-flags.tipsaver-limit-exceeded' :
                                    ['trash', lambda x: eval(x.title())],
'force-segment-header.environment.xy-scanner-position-map.xy-scanners.position-index' :
                                    ['trash', eval],
'force-segment-header.environment.xy-scanner-position-map.xy-scanner.tip-scanner.position.x' :
                                    ['mpos_x', lambda x: eval(x.title())],
'force-segment-header.environment.xy-scanner-position-map.xy-scanner.tip-scanner.position.y' :
                                    ['mpos_y', lambda x: eval(x.title())],
'force-segment-header.environment.xy-scanner-position-map.xy-scanner.motorstage.position.x' :
                                    ['trash', lambda x: eval(x.title())],
'force-segment-header.environment.xy-scanner-position-map.xy-scanner.motorstage.position.y' :
                                    ['trash', lambda x: eval(x.title())],
'channels.list' :
                                    ['channel_list', lambda x: str(x).strip('\n').split(' ')],
'channel.vDeflection.lcd-info.*' :
                                    ['trash', lambda x: eval(x.title())],
'channel.vDeflection.data.file.name' :
                                    ['vDeflection.fname', lambda x: str(x).strip('\n')],
'channel.vDeflection.data.file.format' :
                                    ['vDeflection.format', lambda x: str(x).strip('\n')],
'channel.vDeflection.data.num-points' :
                                    ['vDeflection.num-points', lambda x: eval(x.title())],
'channel.error.lcd-info.*' :
                                    ['trash', lambda x: eval(x.title())],
'channel.error.data.file.name' :
                                    ['error.fname', lambda x: str(x).strip('\n')],
'channel.error.data.file.format' :
                                    ['error.format', lambda x: str(x).strip('\n')],
'channel.error.data.num-points' :
                                    ['error.num-points', lambda x: eval(x.title())],
'channel.hDeflection.lcd-info.*' :
                                    ['trash', lambda x: eval(x.title())],
'channel.hDeflection.data.file.name' :
                                    ['hDeflection.fname', lambda x: str(x).strip('\n')],
'channel.hDeflection.data.file.format' :
                                    ['hDeflection.format', lambda x: str(x).strip('\n')],
'channel.hDeflection.data.num-points' :
                                    ['hDeflection.num-points', lambda x: eval(x.title())],
'channel.capacitiveSensorHeight.lcd-info.*' :
                                    ['trash', lambda x: eval(x.title())],
'channel.capacitiveSensorHeight.data.file.name' :
                                    ['capacitiveSensorHeight.fname', lambda x: str(x).strip('\n')],
'channel.capacitiveSensorHeight.data.file.format' :
                                    ['capacitiveSensorHeight.format', lambda x: str(x).strip('\n')],
'channel.capacitiveSensorHeight.data.num-points' :
                                    ['capacitiveSensorHeight.num-points', lambda x: eval(x.title())],
'channel.height.lcd-info.*' :
                                    ['trash', lambda x: eval(x.title())],
'channel.height.data.file.name' :
                                    ['height.fname', lambda x: str(x).strip('\n')],
'channel.height.data.file.format' :
                                    ['height.format', lambda x: str(x).strip('\n')],
'channel.height.data.num-points' :
                                    ['height.num-points', lambda x: eval(x.title())],
'channel.time.lcd-info.*' :
                                    ['trash', lambda x: eval(x.title())],
'channel.time.data.num-points' :
                                    ['trash', lambda x: eval(x.title())],
'channel.time.data.start' :
                                    ['trash', lambda x: eval(x.title())],
'channel.time.data.step' :
                                    ['trash', lambda x: eval(x.title())],
'channel.seriesTime.lcd-info.*' :
                                    ['trash', lambda x: eval(x.title())],
'channel.seriesTime.data.num-points' :
                                    ['trash', lambda x: eval(x.title())],
'channel.seriesTime.data.start' :
                                    ['trash', lambda x: eval(x.title())],
'channel.seriesTime.data.step' :
                                    ['trash', lambda x: eval(x.title())],
}
ZIP_SHARED_HEADER = {
'force-segment-header-infos.count' :
                                    ['segment_count', eval],
'lcd-infos.count' :
                                    ['channel_count', eval],
'force-segment-header-info.xx.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.approach-id' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.name.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.name.name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.settings.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.settings.style' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.settings.feedback-mode.name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.settings.segment-settings.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.settings.segment-settings.identifier.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.settings.segment-settings.identifier.name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.settings.segment-settings.style' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.settings.segment-settings.duration' :
                                    ['trash', eval],
'force-segment-header-info.xx.settings.segment-settings.num-points' :
                                    ['trash', eval],
'force-segment-header-info.xx.settings.segment-settings.z-start' :
                                    ['trash', eval],
'force-segment-header-info.xx.settings.segment-settings.z-end' :
                                    ['trash', eval],
'force-segment-header-info.xx.settings.segment-settings.setpoint' :
                                    ['trash', eval],
'force-segment-header-info.xx.environment.xy-scanner-position-map.defined' :
                                    ['trash', lambda x: eval(x.title())],
'force-segment-header-info.xx.environment.xy-scanner-position-map.xy-scanners.active-xy-scanner.name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.environment.xy-scanner-position-map.xy-scanner.tip-scanner.xy-scanner.scanner' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.environment.xy-scanner-position-map.xy-scanner.tip-scanner.xy-scanner.name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.environment.xy-scanner-position-map.xy-scanner.tip-scanner.xy-scanner.fancy-name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.environment.xy-scanner-position-map.xy-scanner.tip-scanner.xy-scanner.description' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.environment.xy-scanner-position-map.xy-scanner.tip-scanner.xy-scanner-mode.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.environment.xy-scanner-position-map.xy-scanner.tip-scanner.xy-scanner-mode.software-linearized' :
                                    ['trash', lambda x: eval(x.title())],
'force-segment-header-info.xx.environment.xy-scanner-position-map.xy-scanner.motorstage.xy-scanner.scanner' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.environment.xy-scanner-position-map.xy-scanner.motorstage.xy-scanner.name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.environment.xy-scanner-position-map.xy-scanner.motorstage.xy-scanner.fancy-name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.environment.xy-scanner-position-map.xy-scanner.motorstage.xy-scanner.description' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.environment.xy-scanner-position-map.xy-scanner.motorstage.xy-scanner-mode.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'force-segment-header-info.xx.environment.xy-scanner-position-map.xy-scanner.motorstage.xy-scanner-mode.software-linearized' :
                                    ['trash', lambda x: eval(x.title())],
'force-segment-header-info.xx.environment.xy-scanner-position-map.xy-scanners.list' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.channel.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.channel.name' :
                                    ['name.xx', lambda x: str(x).strip('\n')],
'lcd-info.xx.unit.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.unit.unit' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversions.list' :
                                    ['conversion_list.xx', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversions.default' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversions.base' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.volts.name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.volts.defined' :
                                    ['trash', lambda x: eval(x.title())],
'lcd-info.xx.conversion-set.conversion.distance.name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.distance.defined' :
                                    ['trash', lambda x: eval(x.title())],
'lcd-info.xx.conversion-set.conversion.distance.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.distance.comment' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.distance.base-calibration-slot' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.distance.calibration-slot' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.distance.scaling.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.distance.scaling.style' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.distance.scaling.offset' :
                                    ['distance_offset', eval],
'lcd-info.xx.conversion-set.conversion.distance.scaling.multiplier' :
                                    ['distance_multiplier', eval],
'lcd-info.xx.conversion-set.conversion.distance.scaling.unit.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.distance.scaling.unit.unit' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.force.name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.force.defined' :
                                    ['trash', lambda x: eval(x.title())],
'lcd-info.xx.conversion-set.conversion.force.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.force.comment' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.force.base-calibration-slot' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.force.calibration-slot' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.force.scaling.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.force.scaling.style' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.force.scaling.offset' :
                                    ['force_offset.xx', eval],
'lcd-info.xx.conversion-set.conversion.force.scaling.multiplier' :
                                    ['spring_constant', eval],
'lcd-info.xx.conversion-set.conversion.force.scaling.unit.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.force.scaling.unit.unit' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.absolute.name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.absolute.defined' :
                                    ['trash', lambda x: eval(x.title())],
'lcd-info.xx.conversion-set.conversion.absolute.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.absolute.comment' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.absolute.base-calibration-slot' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.absolute.calibration-slot' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.absolute.scaling.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.absolute.scaling.style' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.absolute.scaling.offset' :
                                    ['force_offset.xx', eval],
'lcd-info.xx.conversion-set.conversion.absolute.scaling.multiplier' :
                                    ['force_offset.xx', eval],
'lcd-info.xx.conversion-set.conversion.absolute.scaling.unit.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.absolute.scaling.unit.unit' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.nominal.name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.nominal.defined' :
                                    ['trash', lambda x: eval(x.title())],
'lcd-info.xx.conversion-set.conversion.nominal.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.nominal.comment' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.nominal.base-calibration-slot' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.nominal.calibration-slot' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.nominal.scaling.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.nominal.scaling.style' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.nominal.scaling.offset' :
                                    ['nominal_offset.xx', eval],
'lcd-info.xx.conversion-set.conversion.nominal.scaling.multiplier' :
                                    ['nominal_multiplier.xx', eval],
'lcd-info.xx.conversion-set.conversion.nominal.scaling.unit.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.nominal.scaling.unit.unit' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.calibrated.name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.calibrated.defined' :
                                    ['trash', lambda x: eval(x.title())],
'lcd-info.xx.conversion-set.conversion.calibrated.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.calibrated.file' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.calibrated.comment' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.calibrated.base-calibration-slot' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.calibrated.calibration-slot' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.calibrated.scaling.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.calibrated.scaling.style' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.calibrated.scaling.offset' :
                                    ['calibrated_offset.xx', eval],
'lcd-info.xx.conversion-set.conversion.calibrated.scaling.multiplier' :
                                    ['calibrated_multiplier.xx', eval],
'lcd-info.xx.conversion-set.conversion.calibrated.scaling.unit.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.calibrated.scaling.unit.unit' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.elapsed.name' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.conversion-set.conversion.elapsed.defined' :
                                    ['trash', lambda x: eval(x.title())],
'lcd-info.xx.encoder.type' :
                                    ['encoder_type.xx', lambda x: str(x).strip('\n')],
'lcd-info.xx.encoder.scaling.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.encoder.scaling.style' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.encoder.scaling.offset' :
                                    ['encode_offset.xx', eval],
'lcd-info.xx.encoder.scaling.multiplier' :
                                    ['encode_multiplier.xx', eval],
'lcd-info.xx.encoder.scaling.unit.type' :
                                    ['trash', lambda x: str(x).strip('\n')],
'lcd-info.xx.encoder.scaling.unit.unit' :
                                    ['trash', lambda x: str(x).strip('\n')],
}
