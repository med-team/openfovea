#! /usr/bin/python
# -*- coding: utf-8 -*-

import csv
import numpy

from common import Variables
GLOB_VARS = Variables()

'''
    Read file for the csem home made microscope.
'''


def is_csem(file_name):
    this_file = open(file_name)
    this_csv = csv.reader(this_file, delimiter='\t')
    try:
        line = this_csv.next()
    except:
        return False
    else:
        if line[0] == '':
            return True
        else:
            return False


def load(file_name):
    '''
        Test if the file is from csem.
    '''
    header = GLOB_VARS.common_header
    header['Microscope'] = 'csem'
    this_file = open(file_name)
    this_csv = csv.reader(this_file, delimiter='\t')
    array = []
    for line in this_csv:
        try:
            float(line[0])
        except:
            if line[0] == '':
                # list of the cantilever
                cantilever = line
                array_x = []
                array_y = numpy.empty
            elif line[0] in ['rad/nm']:
                convertion = [1] + [float(item) for item in line[1:]]
            elif line[0] == 'cste raideur levier N/m':
                spring_cst = [float(item) for item in line[1:]]
            elif line[0] == 'position scanner':
                pos = line
            else:
                print('Unknown line :')
                print line
        else:
            array.append([float(item) * conv
                          for item, conv in zip(line, convertion)])
    # Make headers
    header['spring_constant'] = spring_cst
    header['number_curves'] = len(cantilever) - 1
    # Rearange the arrays
    array_all = numpy.asanyarray(array)
    array_x = -array_all[:, 0]
    array_y = array_all[:, 1:]
    cut = array_x.argmin()
    trace_x = array_x[:cut][::-1]
    retrace_x = array_x[cut:]
    trace_y = array_y[:cut, :][::-1]
    retrace_y = array_y[cut:, :]
    trace_y = trace_y.transpose(1, 0)
    trace_y.shape = (trace_y.shape[0], 1, trace_y.shape[1])
    retrace_y = retrace_y.transpose(1, 0)
    retrace_y.shape = (retrace_y.shape[0], 1, retrace_y.shape[1])
    # Add last header informations
    header['size_x'] = trace_y.shape[0]
    header['size_y'] = trace_y.shape[1]
    header['size'] = [header['size_x'], header['size_y']]
    header['scan_size'] = [header['size_x'], header['size_y']]
    piezo = numpy.ones(trace_y.shape[0:2])
    piezo = piezo * array_x[-1]
    return [trace_x, trace_y], [retrace_x, retrace_y], header, piezo
