# The header of a force volume file
from datetime import datetime
import re


class Variables(object):

    def __init__(self):
        pass

    def __getattribute__(self, key):
        if key == 'common_header':
            return {
                'data_location': 'local',  # The location of the data. If
                # local, all the data are in memory. Otherwise, the relative
                # path to the data file is specified here.
                'index_array': None,  # The index array. If the data are not
                # local, the fd curves are located in separate folders. This
                # index array contains the location for each fd curve.
                'Microscope': None,  # The type of the microscope used.
                'spring_constant': None,  # The spring constant of the
                # cantilever. If the data needs no conversion, value is 1.
                'scan_size': None,  # The size of the scan (in nm).
                # Tuple (x, y)
                'sensit_deflection': None,  # The sensitivity deflection
                'number_curves': None,  # The number of curves that the file
                # contains
                'size_x': 0,  # The size in x direction.
                'size_y': 0,  # The size in y direction.
                'size': None,  # The tuple size (x, y). Goal is to replace the
                # two previous.
                'pixel_size': None,  # The size of the pixel.
                'date': None,  # The date of the exeperiment.
                'note': '/n',  # Some notes that appears on the original file.
                'event_detect_weight': 2,  # The weight of the event
                # detection.
                'group': 0,  # To which group this file belongs.
                'event_dist_thresh': 0.0,  # The event distance threshold.
                'thresh_event_fit_length': [None, None],
                'thresh_event_fit_plength': [None, None],
                # The event fit model. Look at curve.event_fit to see the
                # available models.
                'event_fit_model': None,
                # Tell if the event fit has to be aware or not about the point
                # of contact.
                'event_fit_from_poc': False,
                'Masked': False}  # A switch to mask or not the data.}
        if key == 'fv_header':
            return {'Microscope': None,  # The type of the microscope used.
                'di_version': None,  # TODO Nanoscope Specific
                'spring_constant': None,  # The spring constant of the
                                           # cantilever. If the data needs no
                                           # conversion, value is 1.
                'force_bytes_per_pixel': None,  # TODO Nanoscope Specific
                'hard_z_scale': None,  # TODO Nanoscope Specific
                'image_mode': 0,  # TODO Nanoscope Specific
                # The mode. Only force volume is understood.
                'image_scale': None,  # TODO Nanoscope Specific
                'scan_size': None,  # The size of the scan (in nm)
                'sensit_deflection': None,  # The sensitivity deflection
                'sens_z_scan': None,  # TODO Nanoscope Specific
                'force_samples_per_curve': None,  # TODO Nanoscope Specific
                'image_number_lines': None,  # TODO Nanoscope Specific
                'number_curves': None,  # The number of curves that the file
                 # contains
                'ramp_size': None,  # TODO Nanoscope Specific
                'size_x': None,  # The size in x direction.
                'size_y': None,  # The size in y direction.
                # 'pixel_size': None,  # The size of the pixel.
                'date': None,  # The date of the exeperiment.
                'force_data_length': None,  # TODO Nanoscope Specific
                'force_data_offset': None,  # TODO Nanoscope Specific
                'image_length': None,  # TODO Nanoscope Specific
                'image_offset': None,  # TODO Nanoscope Specific
                'image_samps_per_line': None,  # TODO Nanoscope Specific
                'matrix_length': None,  # TODO Nanoscope Specific
                'x_factor': 1,  # TODO Nanoscope Specific
                'note': '/n',  # Some notes that appears on the original file.
                'event_detect_weight': 2,  # The weight of the event detection.
                'group': 0,  # To which group this file belongs.
                'event_dist_thresh': 0.0,   # The event distance threshold.
                'thresh_event_fit_length': [None, None],
                'thresh_event_fit_plength': [None, None],
                # The event fit model. Look at curve.event_fit to see the
                # available models.
                'event_fit_model': None,
                # Tell if the event fit has to be aware or not about the point
                # of contact.
                'event_fit_from_poc': False,
                'Masked': False}  # A switch to mask or not the data.
        if key == 'jpk_connector':
            return {'fastSize:': ('scan_size', lambda x: eval(x) * 1E6),
                    'numPoints:': ('number_curves', eval),
                    'iLength:': ('size_x', eval),
                    'jLength:': ('size_y', eval),
                    'springConstant:': ('spring_constant', eval),
                    'sensitivity:': ('sensit_deflection', eval),
                    'date:': ('date', lambda x: datetime.strptime(x,
                                            '%a %b %d %H:%M:%S %Z %Y'))}
        if key == 'jpk_zip_connector':
            return {
        'quantitative-imaging-map.position-pattern.grid.ilength': 'size_x',
        'quantitative-imaging-map.position-pattern.grid.jlength': 'size_y',
        'force-scan-map.position-pattern.grid.ilength': 'size_x',
        'force-scan-map.position-pattern.grid.jlength': 'size_y'}
        if key == 'jpk_fc_connector':
            return {'zRelativeEnd:': ('z_end', lambda x: eval(x)),
                    'zRelativeStart:': ('z_start', lambda x: eval(x)),
                    'direction:': ('direction', lambda x: x)
                    }
        if key == 'jpk_header':
            return {'scanner': None,
                    'gridPattern': None,
                    'fastSize': None,
                    'slowSize': None,
                    'xOffset': None,
                    'yOffset': None,
                    'iLength': None,
                    'jLength': None,
                    'theta': None,
                    'reflect': None,
                    'numPoints': None,
                    'forceSettings': None,
                    'zRelativeStart': None,
                    'zRelativeEnd': None,
                    'forceBaseline': None,
                    'forceBaselineMeasured': None,
                    'forceRelativeSetpoint': None,
                    'setpoint': None,
                    'kLength': None,
                    'zStartPauseOption': None,
                    'zEndPauseOption': None,
                    'tipsaverPauseOption': None,
                    'feedbackMode': None,
                    'traceScanTime': None,
                    'retraceScanTime': None,
                    'pauseBeforeFirst': None,
                    'pauseAtStart': None,
                    'pauseAtEnd': None,
                    'pauseOnTipsaver': None,
                    'scanner': None,
                    'date': None}
        if key == 'asylum5_header':
            # The following is inspired from the file read_wave.m from ...
            return {
                    'checksum': ['h', 2, lambda x: x],  # Check sum of header
                    # and wave header.
                    'wfm_size': ['l', 4, lambda x: x],  # Size of wave header.
                    'formula_size': ['l', 4, lambda x: x[0]],  # Size of the
                    # dependency formula (0 if none).
                    'note_size': ['l', 4, lambda x: x[0]],  # Size of the
                    # notes...
                    'data_eunit_size': ['l', 4, lambda x: x[0]],  # Size of the
                    # extended data units.
                    'dim_eunit_size': ['4l', 16, lambda x: x],  # Size of the
                    # extended dimension unit. Need all data
                    'dim_label_size': ['4l', 16, lambda x: x],  # Size of the
                    # dimension labels.
                    'string_indice_size': ['l', 4, lambda x: x[0]],  # Size of
                    # the indices if it's a text.
                    'option1': ['l', 4, lambda x: x],
                    'option2': ['l', 4, lambda x: x],
                    'next': ['l', 4, lambda x: x],
                    'creation_date': ['l', 4, lambda x: x],
                    'modification_date': ['l', 4, lambda x: x],
                    'nbr_points': ['l', 4, lambda x: x[0]],
                    'type': ['h', 2, lambda x: x[0]],  # The type of the data.
                    # See TYPE global.
                    'd_lock': ['h', 2, lambda x: x],  # ??
                    'whpad1': ['c', 1, lambda x: x],
                    'wh_version': ['h', 2, lambda x: x],
                    'fseek': ['5c', 5, lambda x: x],
                    'b_name': ['32c', 32, lambda x: x],
                    'whpad2': ['l', 4, lambda x: x],
                    'd_folder': ['l', 4, lambda x: x],
                    'n_dim': ['4l', 16, lambda x: x],
                    'sfa': ['4d', 32, lambda x: x],
                    'sfb': ['4d', 32, lambda x: x],
                    'data_units': ['4c', 4, lambda x: x],
                    'dim_units0': ['4c', 4, lambda x: x],
                    'dim_units1': ['4c', 4, lambda x: x],
                    'dim_units2': ['4c', 4, lambda x: x],
                    'dim_units3': ['4c', 4, lambda x: x],
                    'fs_valid': ['h', 2, lambda x: x],
                    'whpad3': ['h', 2, lambda x: x],
                    'top_full_scale': ['d', 8, lambda x: x],  # The max and
                    # max full scale value for wave.
                    'bottom_full_scale': ['d', 8, lambda x: x],  # The max and
                    # max full scale value for wave.
                    'data_eunit': ['l', 4, lambda x: x],  # Ignore...
                    'dim_eunits': ['4l', 16, lambda x: x],  # Ignore...
                    'dim_labels': ['4l', 16, lambda x: x],  # Ignore...
                    'wave_note_h': ['l', 4, lambda x: x],  # Ignore...
                    'wh_unused': ['16l', 64, lambda x: x],  # Ignore...
                    'a_modified': ['h', 2, lambda x: x],  # Ignore...
                    'w_modified': ['h', 2, lambda x: x],  # Ignore...
                    'sw_modified': ['h', 2, lambda x: x],  # Ignore...
                    'use_bits': ['c', 1, lambda x: x],  # Ignore...
                    'kind_bits': ['c', 1, lambda x: x],  # Ignore...
                    'formula': ['l', 4, lambda x: x],  # Ignore...
                    'dep_id': ['l', 4, lambda x: x],  # Ignore...
                    'whpad4': ['h', 2, lambda x: x],  # Ignore...
                    'src_folder': ['h', 2, lambda x: x],  # Ignore...
                    'file_name': ['l', 4, lambda x: x],  # Ignore...
                    's_indices': ['l', 4, lambda x: x],  # Ignore...
                    }
        if key == 'asylum5_order':
            # This is the order where the header must be readen.
            return ['checksum', 'wfm_size', 'formula_size', 'note_size',
                    'data_eunit_size', 'dim_eunit_size', 'dim_label_size',
                    'string_indice_size', 'option1', 'option2', 'next',
                    'creation_date', 'modification_date', 'nbr_points', 'type',
                    'd_lock', 'whpad1', 'wh_version', 'fseek', 'b_name',
                    'whpad2', 'd_folder', 'n_dim', 'sfa', 'sfb', 'data_units',
                    'dim_units0', 'dim_units1', 'dim_units2', 'dim_units3',
                    'fs_valid', 'whpad3', 'top_full_scale',
                    'bottom_full_scale', 'data_eunit', 'dim_eunits',
                    'dim_labels', 'wave_note_h', 'wh_unused', 'a_modified',
                    'w_modified', 'sw_modified', 'use_bits', 'kind_bits',
                    'formula', 'dep_id', 'whpad4', 'src_folder', 'file_name',
                    's_indices']

# For informations:
"""
+===========+=======================+=======================+=================+
| Format    | C Type                | Python type           | Standard size   |
+===========+=======================+=======================+=================+
| x         | pad byte              | no value              |                 |
+-----------+-----------------------+-----------------------+-----------------+
| c         | char                  | string of length 1    | 1               |
+-----------+-----------------------+-----------------------+-----------------+
| b         | signed char           | integer               | 1               |
+-----------+-----------------------+-----------------------+-----------------+
| B         | unsigned char         | integer               | 1               |
+-----------+-----------------------+-----------------------+-----------------+
| ?         | _Bool                 | bool                  | 1               |
+-----------+-----------------------+-----------------------+-----------------+
| h         | short                 | integer               | 2               |
+-----------+-----------------------+-----------------------+-----------------+
| H         | unsigned short        | integer               | 2               |
+-----------+-----------------------+-----------------------+-----------------+
| i         | int                   | integer               | 4               |
+-----------+-----------------------+-----------------------+-----------------+
| I         | unsigned int          | integer               | 4               |
+-----------+-----------------------+-----------------------+-----------------+
| l         | long                  | integer               | 4               |
+-----------+-----------------------+-----------------------+-----------------+
| L         | unsigned long         | integer               | 4               |
+-----------+-----------------------+-----------------------+-----------------+
| q         | long long             | integer               | 8               |
+-----------+-----------------------+-----------------------+-----------------+
| Q         | unsigned long long    | integer               | 8               |
+-----------+-----------------------+-----------------------+-----------------+
| f         | float                 | float                 | 4               |
+-----------+-----------------------+-----------------------+-----------------+
| d         | double                | float                 | 8               |
+-----------+-----------------------+-----------------------+-----------------+
| s         | char[]                | string                |                 |
+-----------+-----------------------+-----------------------+-----------------+
| p         | char[]                | string                |                 |
+-----------+-----------------------+-----------------------+-----------------+
| P         | void *                | integer               |                 |
+-----------+-----------------------+-----------------------+-----------------+
"""


def extract_num(file_id, pos=0):
    '''
    Etract number from a string
    '''
    number = re.findall('([0-9]+[.0-9]*)', file_id)[pos]
    try:
        number = int(number)
    except ValueError:
        number = float(number)
    return number


def extract_num_rev(file_id):
    '''
    Etract number from a string
    '''
    number = re.findall('([0-9]+[.0-9]*)', file_id)[-1]
    try:
        number = int(number)
    except ValueError:
        number = float(number)
    return number
