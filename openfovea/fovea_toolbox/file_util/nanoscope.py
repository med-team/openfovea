#! /usr/bin/python
# -*- coding: utf-8 -*-
'''
This module is used to parse the files form Digital Instrument AFM
'''
__author__  = "Charles Roduit <charles.roduit@gmail.com>"
__date__ = "01.07.2007"
__license__ = "GNU Public License (GPL) version 3"
__version__ = "0.1"

import os
from struct import unpack
from datetime import datetime #strptime
import zipfile

import numpy as num

from common import Variables, extract_num, extract_num_rev

GLOB_VARS = Variables()


def clean_header(header):
    new_header = dict()
    for item in GLOB_VARS.common_header:
        try:
            new_header[item] = header[item]
        except KeyError:
            # This key will keep the default value...
            new_header[item] = GLOB_VARS.common_header[item]
    return new_header
        
def lect_header(file_name):
    """
    Cette fonction lit le fichier d'entete du bioscope
    Elle demande le nom du fichier
    Elle retourne un dictionnaire repetroriant toutes
    les information utiles pour traiter les courbes
    forces-volumes ou/et l'image topologique.
    
    >>> header=lect_header(fileName)
    """
    
    file_id = open(file_name,'rb')
    line_in_mem = file_id.readline()

    one_image = 0
    header = GLOB_VARS.fv_header
    header_pos = 'None'
    version = {8.0: 0,
               7.0: 0,
               6.1: 0}
    if not '\*Force file list' in line_in_mem:
        # This is not a nanoscoe file
        raise TypeError
    while not '\*File list end' in line_in_mem:
        ###### Determiner la postition dans l'entete ######
        if '\*Force file list' in line_in_mem:
            header_pos = 'File Description'
        elif '\*Equipment list' in line_in_mem:
            header_pos = 'Equipment list'
        elif '\*Ciao scan list' in line_in_mem:
            header_pos = 'Scan list'
        elif '\*Ciao force list' in line_in_mem:
            header_pos = 'Force list'
        elif '\*Ciao image list' in line_in_mem:
            header_pos = 'Image list'
        elif '\*Ciao force image lis' in line_in_mem:
            header_pos = 'Force image'
        elif header_pos == 'File Description':
            if line_in_mem.find('\Version:') + 1:
                header['di_version'] = extract_num_rev(line_in_mem)
                if 6130000 < header['di_version'] < 6200000:
                    version[6.1] = 1
                elif 7000000 < header['di_version'] < 8000000:
                    version[7.0] = 1
                elif 8000000 < header['di_version'] < 9000000:
                    version[8.0] = 1
            elif line_in_mem.find('\Date:') + 1:
                #find the last digit
                for place, item in enumerate(line_in_mem):
                    if item.isdigit():
                        last=place+1
                date = line_in_mem[7:last]
                
                header['date'] = datetime.strptime(date, 
                                                   '%I:%M:%S %p %a %b %d %Y')
                
        ######### Description de l'equipement ############
        elif header_pos == 'Equipment list':
            if line_in_mem.find('\@Sens. Zscan: V') + 1:
                header['sens_z_scan'] = float(extract_num(line_in_mem))
            elif (version[6.1] or version[7.0] or version[8.0]) and \
                                    line_in_mem.find('\@Sens. Zsens: V') + 1:
                                    
                header['sens_z_scan'] = float(extract_num(line_in_mem))
        ######### des parametres du scan ##################
        elif header_pos == 'Scan list':
            if line_in_mem.find('\Operating mode') + 1:
                if line_in_mem.find('Force Volume') + 1:
                    header['image_mode'] = 'Force Volume'
            elif line_in_mem.find('\Scan size') + 1:
                _ssize = extract_num(line_in_mem)
                header['scan_size'] = (_ssize, _ssize)
            elif (version[7.0] or version[8.0]) and line_in_mem.find('\Scan Size') + 1:
                _ssize = extract_num(line_in_mem)
                header['scan_size'] = (_ssize, _ssize)
            elif line_in_mem.find('\@Sens. Deflection: V') + 1:
                header['sensit_deflection'] = extract_num(line_in_mem)
            elif (version[6.1] or version[7.0] or version[8.0] or version[6.1]) and \
                    line_in_mem.find('\@Sens. DeflSens: V') + 1:
                    
                header['sensit_deflection'] = extract_num(line_in_mem)
                
        ####### Parametre de l'acquisition des courbes de force ######
        elif header_pos == 'Force list':
            if line_in_mem.find('\\force/line') + 1:
                header['matrix_length'] = extract_num(line_in_mem)
                #header['size_x'] = header['matrix_length']
                #header['size_y'] = header['matrix_length']
                header['size'] = (header['matrix_length'], header['matrix_length'])
        ########## Parametres de l'image ########################
        elif header_pos == 'Image list':
            if line_in_mem.find('\Number of lines') + 1:
                header['image_number_lines'] = extract_num(line_in_mem)
            elif line_in_mem.find('\@2:Z scale') + 1:
                header['image_scale'] = extract_num_rev(line_in_mem)
            elif line_in_mem.find('\Samps/line') + 1:
                header['image_samps_per_line'] = extract_num(line_in_mem)
            elif line_in_mem.find('\Data length') + 1:
                header['image_length'] = extract_num(line_in_mem)
            elif line_in_mem.find('\Data offset') + 1:
                if not one_image:
                    header['image_offset'] = extract_num(line_in_mem)
                    one_image = 1
            elif line_in_mem.find('\Note:') + 1:
                header['note'] = line_in_mem[7:]
                while line_in_mem[-3:]=='\r\r\n':
                    line_in_mem = file_id.readline()
                    try:
                        line_in_mem = u'' + line_in_mem
                        header['note'] = header['note'] + line_in_mem
                    except UnicodeDecodeError:
                        line_in_mem = line_in_mem.decode('cp1251')
                        header['note'] = header['note'] + line_in_mem
                header['note'] = header['note'].replace('\r', '')

        ######### Parametre des courbes de force ###############
        elif header_pos == 'Force image':
            if line_in_mem.find('\Data offset:') + 1:
                header['force_data_offset'] = extract_num(line_in_mem)
                
            elif line_in_mem.find('\Data length:') + 1:
                header['force_data_length'] = extract_num_rev(line_in_mem)
                
            elif line_in_mem.find('\Bytes/pixel:') + 1:
                header['force_bytes_per_pixel'] = extract_num_rev(line_in_mem)
                
            elif line_in_mem.find('\Samps/line') + 1:
                header['force_samples_per_curve'] = extract_num(line_in_mem)
                
            elif line_in_mem.find('\Data type: FORCE') + 1:
                pass
                ## Dans la version originale, il y avait un NFJflag=1...
            elif line_in_mem.find('\@4:Z scale: V') + 1:
                #header['z_scale'] = extract_num_rev(file_id)
                header['hard_z_scale'] = extract_num(line_in_mem, pos=1)
                
            #elif line_in_mem.find('\@4:FV scale: V') + 1:
            #    header['fv_scale'] = extract_num_rev(file_id)
                
            elif line_in_mem.find('\@4:Ramp size: V') + 1:
                header['ramp_size'] = extract_num_rev(line_in_mem)
            
            elif line_in_mem.find('\Spring constant') + 1:
                header['spring_constant'] = extract_num_rev(line_in_mem)
                
            elif (version[6.1] or version[7.0] or version[8.0] or version[6.1]) and \
                            line_in_mem.find('\Spring Constant:') + 1:
                header['spring_constant'] = extract_num_rev(line_in_mem)
                
        #####################################################
        line_in_mem = file_id.readline()
    header['x_factor'] = header['ramp_size'] * \
                         header['sens_z_scan'] / header['force_samples_per_curve']

    if header['force_data_length'] != (2 * (header['matrix_length'] ** 2) * 
                                       header['force_bytes_per_pixel'] * 
                                       header['force_samples_per_curve']):
        header['matrix_length'] = int(num.sqrt(header['force_data_length'] /
                                    (header['force_bytes_per_pixel'] * 
                                     header['force_samples_per_curve'] * 
                                     2)))
#        header['size'] = (header['matrix_length'], header['matrix_length'])
#        header['size_x'] = header['matrix_length']
#        header['size_y'] = header['matrix_length']
    header['number_curves'] = header['matrix_length'] ** 2
    return header
def load_force_curves(file_name, header):
    '''
    This function reads the AFM file (file_name) with the help of header 
    information. The header is the dictionnary returned by the function 
    lect_header.It returns the force curves contained in the file in two
    3D numpy arrays. The first one contain the advance force curves and the 
    second one the retraction force curves.

    >>> [im_3d_av,3d_re]=load_force_curves(file_name,header)
    >>> print(im_3d_av[0,0,:])   # print the values of the first advance 
    >>>                          # force curve 
    '''
    
    file_id = open(file_name, 'rb')
    file_id.seek(header['force_data_offset'], 0)
    
    curve_av = num.zeros([header['force_samples_per_curve']])
    curve_re = num.zeros([header['force_samples_per_curve']])
    
    im_3d_av = num.zeros([header['matrix_length'],
                          header['matrix_length'],
                          header['force_samples_per_curve']], num.float)
                          
    im_3d_re = num.zeros([header['matrix_length'],
                          header['matrix_length'],
                          header['force_samples_per_curve']], num.float)
    pos_y = 0
    pos_x = 0
    while pos_y < header['matrix_length']:
        while pos_x < header['matrix_length']:
            # charger la courbe x, y
            curve_av[:] = unpack(str(header['force_samples_per_curve']) + 'h',
                            file_id.read(2 * header['force_samples_per_curve']))
            curve_re[:] = unpack(str(header['force_samples_per_curve']) + 'h',
                            file_id.read(2 * header['force_samples_per_curve']))
            # mettre le minimun a 0
            curve_av = curve_av - min(curve_av)
            curve_re = curve_re - min(curve_re)
            # Correction des echelles
            # (http://web.mit.edu/cortiz/www/MultimodeInstructions.doc)
            curve_av = curve_av * header['hard_z_scale'] * \
                                  header['sensit_deflection']
            curve_re = curve_re * header['hard_z_scale'] * \
                                  header['sensit_deflection']
                                  
            im_3d_av[pos_x, pos_y, ] = correct_curve_drop(curve_av)
            im_3d_re[pos_x, pos_y, ] = correct_curve_drop(curve_re)
            pos_x += 1
        pos_x = 0
        pos_y += 1
    return [im_3d_av, im_3d_re]
    
def correct_curve_drop(curve_y):
    '''
    Corrects the drops that occurs at the beginning of some force-distance
    curves. This function corrects a bug from Veeco AFM.
    '''
    try:
        first_non_zero = curve_y.nonzero()[0][-1]
    except IndexError:
        # The vector is full of zero, then do nothing
        return curve_y
    if not first_non_zero == len(curve_y)-1:
        first_non_zero = curve_y.nonzero()[0][-1]
        for indice in range(first_non_zero, len(curve_y)):
            curve_y[indice] = curve_y[first_non_zero]
        curve_y = curve_y - min(curve_y)
    return curve_y
    
def load_image(file_name, header):
    '''
    This function returns the piezo height image contained in the AFM file
    as a numpy matrix
    
    >>> imageMatrix=load_image(fileName,header)
    '''
    if header.has_key('image_offset') and header['image_offset'] is not None:
        file_id = open(file_name, 'rb')
        im_matrix = num.zeros([header['matrix_length'],
                               header['matrix_length']], num.float)
        pos_y = 0
        pos_x = 0
        while pos_y < header['matrix_length']:
            while pos_x < header['matrix_length']:
                temp_offset = header['image_offset'] + (
                        2 * pos_x * (header['image_number_lines'] / 
                                   header['matrix_length']) * 
                                   header['image_number_lines'] + 
                        2 * pos_y * (header['image_samps_per_line'] / 
                                   header['matrix_length']))
                file_id.seek(temp_offset, 0)
                im_matrix[pos_y, pos_x] = unpack('h', file_id.read(2))[0]
                pos_x += 1
            pos_x = 0
            pos_y += 1
        ## Scale the image
        im_matrix = im_matrix * (header['image_scale'] *
                                 header['sens_z_scan'] / 
                                 (2. ** (2 * 8)))
        pos_x = 0
        minimum = im_matrix.min()
        im_matrix = im_matrix - minimum
    else:
        im_matrix = num.zeros([header['matrix_length'],
                               header['matrix_length']])
    return im_matrix
    
