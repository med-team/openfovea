#!/usr/bin/env python

from xml.dom import minidom
import time
import pylab
import numpy

def parse(file_name):
    xmldoc=minidom.parse("C050614A.002.xml")
    child=xmldoc.firstChild
    FVTags=["fileName", "mainExpIndice", "date", "header", "image",
            "pointOfContact", "topography", "StiffnessesList", "events",
            "CurveX", "CurveYAv", "CurveYRe"]
    dateTag=["year", "month", "day", "hour", "minute", "second"]
    headerTag=["SensZScan", "OpMode", "ScanSize", "NFL", "ScanListScanRate",
               "SensitDeflection", "ScanRate", "ForceListForwVeloc>",
               "ForceListRevVeloc", "NumberCurvesPerLines",
               "ForceListFVScanRate", "Z_SCAN_START", "Z_SCAN_SIZE", "TTD",
               "ImageOffset", "ImageLength", "ImageSampsPerLine",
               "ImageNumberLines", "ImageScale", "ForceOffset",
               "NumberPointsPerCurves", "SpringConstant", "Z_SCALE",
               "HARD_Z_SCALE", "FV_SCALE", "RAMP_SIZE"]
    imageTag=["data"]
    imageAtt=["Contrast"]
    pocTag=["data"]
    pocAtt=["Contrast","Method"]
    topoTag=["data"]
    topoAtt=["Contrast"]
    stiffListAtt=["Contrast","Deep","GlassFit","HertzModel","SegmentNumber",
                 "TipCar-Radius","TipCar-SemiAngle"]
    stiffListTag=["StiffnessMatrix"]
    stiffMatrixAtt=["DeepNbr"]
    stiffMatrixTag=["data"]
    eventAtt=["Type"]
    eventTag=["EventDetail","EventMatrix"]
    eventDetAtt=["PosX","PosY","XValues","YValues","force","jumpSlope","maxX",
                 "maxY","minX","minY"]
    EventMatrixTag=["data"]
    CurveYAvTag=["data"]
    CurveYReTag=["data"]
    result=dict()

def xmlExtractImage(elm):
    nodeDic=dict()
    attDic=dict()
    for att in ["Contrast","Method","DeepNbr"]:
        if elm.hasAttribute(att):
            attDic[att]=elm.getAttribute(att)
    nodeDic["attributes"]=attDic
    matrix=[]
    for data in elm.childNodes:
        if data.nodeName=="data":
            matrix.append([float(s) for s in
                                data.childNodes[0].data.split(",")[0:-1]])
    nodeDic["matrix"]=numpy.array(matrix)
    return nodeDic
    
def xmlExtractVector(elm):
    vectors=list()
    for data in elm.childNodes:
        if data.nodeName=="data":
            vectors.append([float(s) for s in
                                data.childNodes[0].data.split(",")[0:-1]])
    vectors=numpy.array(vectors)
    return vectors

def restructure_header(old_header, version):
    header = dict()
    if not version:
        # we come from the succellus version
        header['FVSCALE']=old_header['FV_SCALE']
        header['ForceListFVScanRate']=old_header['ForceListFVScanRate']
        ## miss ForceListForwVeloc
        header['ForceListForwVeloc']=old_header['ForceListForwVeloc']
        header['ForceListRevVeloc']=old_header['ForceListRevVeloc']
        header['Force Data offset']=old_header['ForceOffset']
        #header[]=old_header['HARD_Z_SCALE']
        header['ImageLength']=old_header['ImageLength']
        header['ImageNumberLines']=old_header['ImageNumberLines']
        #header['ImageOffset']=old_header['ImageOffset']
        header['ImageSampsPerLine']=old_header['ImageSampsPerLine']
        header['ImageScale']=old_header['ImageScale']
        #header['NFL']=old_header['NFL']
        header['matrix_length']=old_header['NumberCurvesPerLines']
        header['size_x'] = header['matrix_length']
        header['size_y'] = header['matrix_length']
        header['ImageSampsPerLine']=old_header['NumberPointsPerCurves']
        header['image mode']=old_header['OpMode']
        header['RAMPSIZE']=old_header['RAMP_SIZE']
        header['ScanListScanRate']=old_header['ScanListScanRate']
        #header['scan rate']=old_header['ScanRate']
        header['scan size']=old_header['ScanSize']
        header['sens z scan']=old_header['SensZScan']
        header['sensit deflection']=old_header['SensitDeflection']
        header['spring_constant']=old_header['SpringConstant']
        header['TTD']=old_header['TTD']
        header['ZSCALE']=old_header['Z_SCALE']
        header['Z_SCAN_SIZE']=old_header['Z_SCAN_SIZE']
        header['ZSCANSTART']=old_header['Z_SCAN_START']
    header['matrix_length'] = int(header['matrix_length'])
    header['size_x'] = int(header['size_x'])
    header['size_y'] = int(header['size_y'])
    header['spring_constant'] = float(header['spring_constant'])
    return header
    
def restructure_stiffness(old_stiffness, version):
    stiffness = dict()
    if not version:
        for key in old_stiffness.keys():
            if key == 'GlassFit':
                stiffness['glass'] = -float(old_stiffness[key][0])
                #print type(stiffness['glass'])
            elif key == 'SegmentNumber':
                stiffness['nbParts'] = int(old_stiffness[key])
                #TODO introduce the stiffness tomography...
                if not stiffness['nbParts']:
                    stiffness['nbParts'] = 4
            elif key == 'Deep':
                stiffness['sizeParts'] = float(old_stiffness[key])
            else:
                stiffness[key] = old_stiffness[key]
    #print stiffness
    return stiffness
            
def restructure_event(old_event, version):
    event = list()
    if not version:
        # we come from the succellus version
        for old_item in old_event:
            item = dict()
            item['pos_x'] = old_item['PosX']-1
            item['pos_y'] = old_item['PosY']-1
            item['slice'] = slice(old_item['XValues'][0],
                                  old_item['XValues'][-1])
            item['min'] = None
            item['max'] = None
            item['slope'] = old_item['jumpSlope']
            item['force'] = old_item['force']
            event.append(item)
    return event
    
def restructure_curves(old_curve_matrix, version):
    if not version:
        #curve_matrix = old_curve_matrix[:,:,::-1]
        pass
    pass
    #return curve_matrix

def xmlExtractFV(fileName):
    xmldoc=minidom.parse(fileName)
    child=xmldoc.firstChild
    FVTags=["fileName","mainExpIndice","date","header","image","pointOfContact",
            "topography","StiffnessesList","events","CurveX","CurveYAv",
            "CurveYRe"]
    dateTag=["year","month","day","hour","minute","second"]
    headerTag=["SensZScan","OpMode","ScanSize","NFL","ScanListScanRate",
               "SensitDeflection","ScanRate","ForceListForwVeloc",
               "ForceListRevVeloc","NumberCurvesPerLines","ForceListFVScanRate",
               "Z_SCAN_START","Z_SCAN_SIZE","TTD","ImageOffset","ImageLength",
               "ImageSampsPerLine","ImageNumberLines","ImageScale","ForceOffset",
               "NumberPointsPerCurves","SpringConstant","Z_SCALE","HARD_Z_SCALE",
               "FV_SCALE","RAMP_SIZE"]
    imageTag=["data"]
    imageAtt=["Contrast"]
    pocTag=["data"]
    pocAtt=["Contrast","Method"]
    topoTag=["data"]
    topoAtt=["Contrast"]
    stiffListAtt=["Contrast","Deep","GlassFit","HertzModel","SegmentNumber",
                 "TipCar-Radius","TipCar-SemiAngle"]
    stiffListTag=["StiffnessMatrix"]
    stiffMatrixAtt=["DeepNbr"]
    stiffMatrixTag=["data"]
    eventAtt=["Type"]
    eventTag=["EventDetail","EventMatrix"]
    eventDetAtt=["PosX","PosY","XValues","YValues","force","jumpSlope","maxX",
                 "maxY","minX","minY"]
    EventMatrixTag=["data"]
    CurveYAvTag=["data"]
    CurveYReTag=["data"]
    result=dict()
    if child.hasAttribute('openfovea:version'):
        aex_version = child.getAttribute('openfovea:version')
    else:
        aex_version = 0
    for elm in child.childNodes:
        if elm.nodeName==FVTags[0]:#fileName
            result["name"]=elm.childNodes[0].data
            
        elif elm.nodeName==FVTags[1]:#mainExpIndice
            result["indice"]=int(elm.childNodes[0].data)
            
        elif elm.nodeName==FVTags[2]:#date
            date=dict()
            for dateElm in elm.childNodes:
                if dateElm.nodeName in dateTag:
                    date[str(dateElm.nodeName)]=int(dateElm.childNodes[0].data)
            result["date"]=date
            del(date)
            
        elif elm.nodeName==FVTags[3]:#header
            header=dict()
            for headerElm in elm.childNodes:
                if headerElm.nodeName in headerTag:
                    header[str(headerElm.nodeName)]=headerElm.childNodes[0].data
            header = restructure_header(header, aex_version)
            result["header"]=header
            del(header)
            
        elif elm.nodeName==FVTags[4]:#Piezo Image
            result['piezo_array']=xmlExtractImage(elm)['matrix']
        elif elm.nodeName==FVTags[5]:#Point of contact
            result["poc_array"]=xmlExtractImage(elm)['matrix']
        elif elm.nodeName==FVTags[6]:#zero force image
            result["topography"]=xmlExtractImage(elm)
            
        elif elm.nodeName==FVTags[7]:#StiffnessesList
            attDic=dict()
            nodeDic=dict()
            #TODO Pretty import values
            for att in stiffListAtt:
                if elm.hasAttribute(att):
                    attDic[att]=elm.getAttribute(att)
            attDic = restructure_stiffness(attDic, aex_version)
            nodeDic["properties"]=attDic
            matrixList=[]
            for childElm in elm.childNodes:
                if childElm.nodeName=="StiffnessMatrix":
                    matrixList.append(xmlExtractImage(childElm))
            matrix=[]
            for depthMatrix in matrixList:
                matrix.append(depthMatrix['matrix'])
            nodeDic["array"]=numpy.array(matrix)
            nodeDic["array"] = nodeDic["array"].transpose(2,1,0)
            nodeDic["array"] = numpy.ma.MaskedArray(
                                        nodeDic["array"], 
                                        mask = nodeDic["array"] == 0)
            result["stiffness"]=nodeDic
            del(attDic,nodeDic,matrix,matrixList)
            
        elif elm.nodeName==FVTags[8]:#events
            events_det=list()
            #TODO Pretty import values
            matrix = None
            for childElm in elm.childNodes:
                if childElm.nodeName=="EventDetail":
                    thisEvt=dict()
                    for att in eventDetAtt:
                        if childElm.hasAttribute(att):
                            thisEvt[att]=childElm.getAttribute(att)
                            if att in ["XValues","YValues"]:
                                thisEvt[att]=[float(s) for s in 
                                            thisEvt[att][0:-1].split(",")]
                            else:
                                thisEvt[att]=float(thisEvt[att])
                    events_det.append(thisEvt)
                elif childElm.nodeName=="EventMatrix":
                    matrix=xmlExtractImage(childElm)
                    matrix=matrix["matrix"]
            events=dict()
            events_det = restructure_event(events_det, aex_version)
            events["detail"]=events_det
            events["matrix"]=matrix
            result["events"]=events
            del(events,matrix)
            
        elif elm.nodeName==FVTags[9]:#CurveX
            result["curve_x_array"]=[float(s) for s in 
                                    elm.childNodes[0].data.split(",")[0:-1]]
            result['curve_x_array'] = numpy.asarray(result['curve_x_array'][::-1])
            
        elif elm.nodeName==FVTags[10]:#CurveYAv
            result["trace_array"]=xmlExtractVector(elm)
            if result.has_key("header"):
                nbLines=int(result["header"]["matrix_length"])
                nbPoints=result["trace_array"].shape[1]
                result["trace_array"]=result["trace_array"].reshape(nbLines,nbLines,nbPoints)
                #result['trace_array'] = result['trace_array'].transpose(1,0,2)
            #result['trace_array'] = restructure_curves(result['trace_array'], 
            #                                           aex_version)
            
        elif elm.nodeName==FVTags[11]:#CurveYRe
            result["retrace_array"]=xmlExtractVector(elm)
            if result.has_key("header"):
                nbLines=int(result["header"]["matrix_length"])
                nbPoints=result["retrace_array"].shape[1]
                result["retrace_array"]=result["retrace_array"].reshape(nbLines,nbLines,nbPoints)
                #result['retrace_array'] = result['retrace_array'].transpose(1,0,2)
            #result['retrace_array'] = restructure_curves(result['retrace_array'], 
            #                                             aex_version)
    result['header']['date'] = result['date']
    return result
    
def xmlExtractExperiment(fileName):
    xmldoc=minidom.parse(fileName)
    child=xmldoc.firstChild
    node=["date","auteur","description","NumberForceVolumeFiles","ForceVolumeNames"]
    result=dict()
    description=list()
    result["ForceVolumeNames"]=list()
    for elm in child.childNodes:
        if elm.nodeName==node[0]:
            result["date"]=time.strptime(elm.childNodes[0].data,'%d-%b-%Y')
        elif elm.nodeName==node[1]:
            result["author"]=elm.childNodes[0].data
        elif elm.nodeName==node[2]:
            description.append(elm.childNodes[0].data)
        elif elm.nodeName==node[3]:
            result["numberFiles"]=int(elm.childNodes[0].data)
        elif elm.nodeName==node[4]:
            for child in elm.childNodes:
                if child.nodeName=='data':
                    result["ForceVolumeNames"].append(child.childNodes[0].data)
    strDesc=str()
    for line in description:
        strDesc=strDesc+line+'\n'
    result['comment']=strDesc[0:-1]
    return result
if __name__=="__main__":
    result=xmlExtractExperiment("experiment.xml")
    
    #result=xmlExtractFV("C050614A.002.xml")
    print result
