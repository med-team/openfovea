import pygtk
pygtk.require('2.0')
import gtk
#from gtk.gtkgl.apputils import *

class FileGroup(object):
    def __init__(self, file_list, group_list=None):
    
        if group_list == None:
            group_list = [0 for i in file_list]
        self.lock = True # defines if we modify all the following value in the list
        self.dialog = gtk.Dialog(title='File Group', buttons=(gtk.STOCK_OK, gtk.RESPONSE_OK,
                gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL))
        self.dialog.set_size_request(300, 300)
        # Create scrolling window
        self.scroll = gtk.ScrolledWindow()
        self.scroll.set_policy(gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)

        self.scroll.show()
        # Create the table.
        self.table = gtk.Table(3, 2)
        self.table.set_border_width(5)
        self.table.set_col_spacings(5)
        self.table.set_row_spacings(5)
        self.table.show()
        check = gtk.CheckButton(label='Lock')
        check.set_active(True)
        check.connect('toggled', self.on_check_toggled)
        check.show()
        self.table.attach(check, 0, 1, 0, 1)
        # Fill the table with label and spin button
        self.spin_button = []
        index_list = range(1, len(file_list)+1)
        for (item, index, group) in zip(file_list, index_list, group_list):
            text = gtk.Label(str=item)
            text.show()
            self.table.attach(text, 0, 1, index, index + 1)
            spin_button = gtk.SpinButton(gtk.Adjustment(value = group,
                                               lower = 0, 
                                               upper = 10, 
                                               step_incr = 1, 
                                               page_incr = 1, 
                                               page_size = 0))
            spin_button.connect('value_changed', self.on_spin_value_changed)
            spin_button.show()
            self.table.attach(spin_button, 1, 2, index, index + 1)
            self.spin_button.append(spin_button)
        self.scroll.add_with_viewport(self.table)
        self.dialog.vbox.pack_start(self.scroll, True, True, 0)
        #self.dialog.vbox.pack_start(self.table, True, True, 0)
    def on_spin_value_changed(self, widget):
        """
            Change the value of the following items in the list
        """
        if not self.lock:
            return
        start = self.spin_button.index(widget)
        if start+1 < len(self.spin_button):
            self.spin_button[start+1].set_value(widget.get_value())
    def on_check_toggled(self, widget):
        self.lock = widget.get_active()
    def run(self):
        """
            Runs the dialog
        """
        response = self.dialog.run()
        if response == gtk.RESPONSE_OK:
            group_list = []
            for item in self.spin_button:
                group_list.append(int(item.get_value()))
            return group_list
    def destroy(self):
        self.dialog.destroy()
        
if __name__ == '__main__':
    file_list = ['File number ' + str(i) for i in range(20)]
    group_list = None
    window = FileGroup(file_list, group_list)
    response = window.run()
    print response
