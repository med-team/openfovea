"""
    This is the module to display the dialog export map.
"""


import pygtk
pygtk.require('2.0')
import gtk
import os
#from pkg_resources import resource_filename # pylint: disable-msg=E0611

    
from matplotlib.backends.backend_gtkagg import FigureCanvasGTKAgg as \
                                               FigureCanvas
import numpy
#from scipy import interpolate, ndimage
import plot_generic

# Graphic library

import matplotlib
matplotlib.use('Agg')
from matplotlib.figure import Figure
from matplotlib.axes import Subplot
from matplotlib.backends.backend_gtkagg import FigureCanvasGTK
from matplotlib import cm # colormap
from matplotlib import pylab
#pylab.hold(False) # This will avoid memory leak

class BoolUnit(object):
    """
    Looks like this :
    
    ::
    +------------------------------------------+
    |                .                         |
    |                . +--------------------+  |
    |                . |                    |  |
    |                . |                    |  |
    | +-------------+. |                    |  |
    | | Select Map  |. |     Display Map    |  |
    | +-------------+. |                    |  |
    |................. |                    |  |
    | +-------------+. |                    |  |
    | | 0 Invert    |..+--------------------+..|
    | +-------------+. | Numerical value    |  |
    |                . +--------------------+  |
    |................:.........................|
    |            +-------------+               |
    |            | Add or bool |               |
    |            +-------------+               |
    +------------------------------------------+
    """
    def __init__(self, fvfile):
        self.fvfile = fvfile
        self.array = {'data' : None, # Store the data array used to generate mask.
                      'mask' : None, # Store the mask itself.
                      'invert' : False} # Invert the mask (or not).
        # Creates the GUI.
        self.map_select = gtk.ComboBox()
        self.map_display = gtk.VBox()
        self.map_display.set_size_request(250,250)
        self.add_bool = gtk.ComboBox()
        self.num_val = gtk.Entry()
        self.invert = gtk.CheckButton(label='Invert')
        self.box = gtk.VBox()
        self.box.show()
        map_box = gtk.HBox()
        map_box.show()
        __box1 = gtk.VBox() # Contains the combobox for mapselect and the invert toggle
        __box1.show()
        
        viz_box = gtk.VBox()
        viz_box.show()

        self.map_select.show()
        self.add_bool.show()
        self.num_val.show()
        self.invert.show()

        viz_box.pack_start(self.map_display)#, expand=False)
        viz_box.pack_start(self.num_val, expand=False)
        __box1.pack_start(self.map_select, expand=False)
        __box1.pack_start(self.invert, expand=False)
        map_box.pack_start(__box1, expand=False)
        #ap_box.pack_start(self.map_select, expand=False)
        map_box.pack_start(viz_box, expand=True)
        self.box.pack_start(map_box, expand=True)
        self.box.pack_start(self.add_bool, expand=False)

        # Create graphics
        self.figure = Figure(figsize=(100, 100), dpi=75)
        self.axis = self.figure.add_subplot(111, axisbg='red')
        self.canvas = FigureCanvas(self.figure)
        self.canvas.show()
        self.map_display.pack_start(self.canvas, True, True)
        self.map_display.show()
        self.canvas.mpl_connect('button_press_event', self.on_array_click)

        # Connect buttons.
        self.map_select.connect('changed', self.change_array)
        self.num_val.connect('activate', self.on_num_val_changed)
        self.invert.connect('toggled', self.on_invert_toggled)
        # bool_list has to be connected in the caller class.
        # Fill the list.
        list_arr = gtk.ListStore(str)
        __list = list()
        for key in self.fvfile.get_array('keys'):
            if type(self.fvfile.get_array(key)) in [numpy.ndarray, numpy.ma.core.MaskedArray]:
                list_arr.append([key])
                __list.append(key)
        self.map_select.set_model(list_arr)
        __cell = gtk.CellRendererText()
        self.map_select.pack_start(__cell, True)
        self.map_select.add_attribute(__cell, 'text', 0)
        self.map_select.set_active(__list.index('Piezo'))
        
        bool_list = gtk.ListStore(str)
        [bool_list.append([key]) for key in ['Add', 'AND','OR','XOR']]
        self.add_bool.set_model(bool_list)
        __cell = gtk.CellRendererText()
        self.add_bool.pack_start(__cell, True)
        self.add_bool.add_attribute(__cell, 'text', 0)
        self.add_bool.set_active(0)
    
    def on_invert_toggled(self, event):
        self.array['invert'] = event.get_active()
        mask = self.array['data'].mask
        mask = numpy.logical_not(mask)
        self.apply_mask(mask)
        self.plot()
    def on_num_val_changed(self, event):
        height = float(event.get_text())
        self.generate_mask(height)
        self.plot()

    def on_array_click(self, event):
        try:
            height = self.array['data'].data[event.ydata, event.xdata]
        except TypeError:
            height = self.array['data'][event.ydata, event.xdata]
        self.generate_mask(height)
        self.plot()
        self.num_val.set_text(str(height))

    def generate_mask(self, height):
        if type(self.array['data']) == numpy.ndarray:
            mask = self.array['data'] < height
        else:
            mask = self.array['data'].data < height
        if self.array['invert']:
            mask = numpy.logical_not(mask)
        self.apply_mask(mask)
    
    def apply_mask(self, mask):
        try:
            self.array['data'].mask = mask
        except AttributeError:
            self.array['data'] = numpy.ma.masked_array(self.array['data'], mask=mask)

    def change_array(self, event):
        self.array['name'] = event.get_active_text()
        self.array['data'] = self.fvfile.get_array(self.array['name'], raw=True)
        if self.array['data'].ndim > 2:
            self.array['data'] = self.array['data'][:,:,0]
        self.plot()

    def plot(self):
        self.axis.pcolor(self.array['data'], cmap=cm.copper)
        self.axis.axis([0, self.array['data'].shape[0],
                        0, self.array['data'].shape[1]])
        self.axis.set_aspect('equal')
        self.canvas.draw_idle()
        
    def get_op(self):
        return self.add_bool.get_active_text()

    def get_mask(self):
        try:
            return self.array['data'].mask
        except:
            return numpy.ones_like(self.array['data'], dtype=bool)
    def destroy(self):
        self.box.destroy()
        return

class MaskProperties(object):
    """
    
    """
    def __init__(self, FVFile):
        self.fvfile = FVFile
        self.mask = None
        self.dialog = gtk.Dialog(title='Mask Propreties',
                                 buttons=(gtk.STOCK_OK, gtk.RESPONSE_OK,
                                          gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL))
        self.dialog.set_default_size(700, 350)

        self.bool_win = list() # This contains the window where each boolean operators are stored
        # Create the first one.
        self.box = {'left' : gtk.HBox(),
                    'bool_view' : gtk.VBox(homogeneous=True),
                    'center' : gtk.HBox()}
        self.bool_win.append(BoolUnit(self.fvfile))
        self.bool_win[0].add_bool.connect('changed', self.add_bool)
        self.bool_win[0].canvas.mpl_connect('button_press_event', self.disp_result)
        self.box['bool_view'].pack_start(self.bool_win[0].box)
        self.scroll_window = gtk.ScrolledWindow()
        self.scroll_window.set_border_width(10)
        self.scroll_window.set_policy(gtk.POLICY_NEVER, gtk.POLICY_AUTOMATIC)
        self.scroll_window.show()
        self.scroll_window.add_with_viewport(self.box['bool_view'])
        self.box['left'].pack_start(self.scroll_window, True, True, 0)
        self.box['center'].pack_start(self.box['left'])#, True, True, 0)
        self.box['center'].show()
        self.box['left'].show()
        self.box['bool_view'].show()
        self.dialog.vbox.pack_start(self.box['center'], True, True, 0)

        # Create graphics
        self.figure = Figure(figsize=(200, 200), dpi=75)
        self.axis = self.figure.add_subplot(111, axisbg='red')
        self.canvas = FigureCanvas(self.figure)
        self.canvas.show()
        self.box['center'].pack_start(self.canvas)#, True, True, 0)
        self.box['center'].set_homogeneous(True)
        
    def add_bool(self, event):
        for bool_unit in self.bool_win:
            if bool_unit.add_bool == event:
                index = self.bool_win.index(bool_unit)
        selected = event.get_active_text()
        print selected
        if selected == 'Add':
            self.bool_win[index].destroy()
            self.bool_win.pop(index)
            return
        if index+1 == len(self.bool_win):
            self.bool_win.append(BoolUnit(self.fvfile))
            self.box['bool_view'].pack_start(self.bool_win[-1].box)
            self.bool_win[-1].add_bool.connect('changed', self.add_bool)
        #self.compute_result()
            self.bool_win[-1].canvas.mpl_connect('button_press_event', self.disp_result)
        self.disp_result()

    def compute_result(self):
        bool_list = [item.get_op() for item in self.bool_win]
        mask_list = [item.get_mask() for item in self.bool_win]
        result = numpy.logical_not(mask_list[0])
        for item in range(len(bool_list)-1):
            if bool_list[item] == 'AND':
                result = numpy.logical_and(result, numpy.logical_not(mask_list[item+1]))
            elif bool_list[item] == 'OR':
                result = numpy.logical_or(result, numpy.logical_not(mask_list[item+1]))
            elif bool_list[item] == 'XOR':
                result = numpy.logical_xor(result, numpy.logical_not(mask_list[item+1]))
        self.mask = numpy.logical_not(result)

    def disp_result(self, event=None):
        self.compute_result()
        self.axis.pcolor(self.mask, cmap=cm.gray)
        self.axis.axis([0, self.mask.shape[0],
                        0, self.mask.shape[1]])
        self.axis.set_aspect('equal')
        self.canvas.draw_idle()

    def run(self):
        """
        Runs the dialog.
        """
        response = self.dialog.run()
        if response == gtk.RESPONSE_OK:
            # Runs the code if button OK is clicked
            self.fvfile.set_array('Mask', self.mask)
            self.fvfile.set_switch('mask', True)
            return True
        else:
            self.fvfile.set_switch('mask', False)
            return False
    def destroy(self):
        """
        Destroy the window.
        """
        self.dialog.destroy()
        return False

if __name__ == '__main__':
    import numpy
    import classes
    FVFOLDER = classes.FVFolder()
    FVFOLDER.load_aex('../docs/data/Stiffness_and_Event_computed.aex')
    DISPLAY_IT = MaskProperties(FVFOLDER.file['list'][1])
    answer = DISPLAY_IT.run()
    print answer
