"""
    This is the module to display the dialog export map.
"""


import pygtk
pygtk.require('2.0')
import gtk
import os
from pkg_resources import resource_filename # pylint: disable-msg=E0611

    
from matplotlib.backends.backend_gtkagg import FigureCanvasGTKAgg as \
                                               FigureCanvas
import numpy
from scipy import interpolate, ndimage
import plot_generic
    
class MapDisplay(object):
    """
        Displays the map with all the button to tune the exportation.
    """
    
    def __init__(self, array, folder=None, depth=1, fvname='', scan_size=None):
        self.array = {'dict' : array,
                      'plot' : None, # contain the current array to plot
                      'type' : None, # contains the type of the current array 
                                     # (correspond to the dict key)
                      'indice' : 0,
                      'depth' : depth, # is the depth between slice
                      'size' : scan_size,
                      'scale' : [0, 0],
                      'do_interp' : 0, # check if we interpolate or not
                      'interp_max' : 0, # Is the maximum value to be displayed in case of interpolation
                      }
                      
        if folder is None:
            folder = os.path.expanduser('~')
        self.file = {'folder' : folder,
                     'name' : 'File_%f_%t_slice_%n_depth_%d.png',
                     'fv_name' : fvname}
        self.dialog = gtk.Dialog(title='Export Maps',
                                 buttons=(gtk.STOCK_OK, gtk.RESPONSE_OK,
                                          gtk.STOCK_CANCEL, gtk.RESPONSE_CANCEL)
                                 )
        self.dialog.set_default_size(500, 350)
        try:
            export_image = resource_filename('openfovea','Icon/export.png')
        except NotImplementedError:
            export_image = 'openfovea/Icon/export.png'
        self.dialog.set_icon(gtk.gdk.pixbuf_new_from_file(export_image))
        
        # The plot
        self.plot_array = plot_generic.Array()
        self.canvas = FigureCanvas(self.plot_array.figure) # a gtk.DrawingArea 
        self.canvas.show() 
        
        self.widget = {'but_folder' : gtk.Button(label=self.file['folder']),
                       'entry_filename' : gtk.Entry(),
                       'entry_title' : gtk.Entry(),
                       'plot_area' : gtk.VBox(),
                       'combo_array' : gtk.ComboBox(),
                       'combo_cmap' : gtk.ComboBox(),
                       'check_auto_filename' : gtk.CheckButton(label='auto'),
                       'check_axis' : gtk.CheckButton(label='show axis label'),
                       'check_cmap' : gtk.CheckButton(label='show color scale'),
                       'check_title' : gtk.CheckButton(label='show title'),
                       'check_interp' : gtk.CheckButton(label='interpolate'),
                       'scale_max' : gtk.HScrollbar(),
                       'scale_min' : gtk.HScrollbar(),
                       'scale_interp' : gtk.HScrollbar(),
                       'size_x' : gtk.SpinButton(),
                       'size_y' : gtk.SpinButton(),
                        }
        self._init_widgets()
        # Everything is displayed, plot the grid:
        self.change_array(self.array['dict'].keys()[0])
        self.change_scale()
        self.plot()
    def _init_scales(self):
        if self.array['plot'] is not None:
            ar_min = self.array['plot'].min()
            ar_max = self.array['plot'].max()
        else:
            ar_min = 0
            ar_max = 0
        interp_max = ar_max + ar_max * 0.1
        # The colorscale
        self.widget['scale_min'].set_adjustment(
                                gtk.Adjustment(ar_min,  ar_min, 
                                               ar_max, 
                                               (ar_max-ar_min) / 100, 
                                               (ar_max-ar_min) / 25, 
                                               0))
        self.widget['scale_max'].set_adjustment(
                                gtk.Adjustment(ar_max,  ar_min, 
                                               ar_max, 
                                               (ar_max-ar_min) / 100, 
                                               (ar_max-ar_min) / 25, 
                                               0))
        self.widget['scale_interp'].set_adjustment(
                                gtk.Adjustment(interp_max,  ar_min, 
                                               interp_max, 
                                               (interp_max-ar_min) / 100, 
                                               (interp_max-ar_min) / 25, 
                                               0))
        self.widget['scale_min'].connect_object("change-value", 
                                                    self.change_scale,
                                                    None)
        self.widget['scale_max'].connect_object("change-value", 
                                                    self.change_scale,
                                                    None)
        self.widget['scale_interp'].connect_object("change-value", 
                                                    self.change_scale,
                                                    None)
    def change_scale(self, widget=None, toto=None, tata=None):
        self.array['scale'] = [self.widget['scale_min'].get_value(),
                               self.widget['scale_max'].get_value()]
        self.array['interp_max'] = self.widget['scale_interp'].get_value()
        self.plot()
    def _init_widgets(self):
        """
            Initialization of the widgets
        """
        # Create the table.
        table = [gtk.Table(2, 2), gtk.Table(2, 5)]
        table[0].set_border_width(5)
        table[0].set_col_spacings(5)
        table[0].set_row_spacings(5)
        table[0].show()
        
        # Button folder
        self.widget['but_folder'].connect('clicked', self.change_folder)
        self.widget['but_folder'].show()
        table[0].attach(self.widget['but_folder'],
                             0, 1, 0, 1, xoptions=0, yoptions=0)
        # Text filename
        self.widget['entry_filename'].set_text(self.file['name'])
        self.widget['entry_filename'].show()
        table[0].attach(self.widget['entry_filename'],
                             1, 2, 0, 1, yoptions=0)
        self.widget['check_auto_filename'].set_active(True)
        self.widget['check_auto_filename'].connect('toggled', self.change_filename)
        table[0].attach(self.widget['check_auto_filename'],
                             2, 3, 0, 1, xoptions=0, yoptions=0)
        # Table which will contain the display and the control of the display
        table[1].set_border_width(5)
        table[1].set_col_spacings(5)
        table[1].set_row_spacings(5)
        table[1].show()
        # The area to plot
        table[1].attach(self.canvas, 0, 1, 0, 5)
        #######
        # The scale bars :
        grid = gtk.Table(rows=3, columns=2, homogeneous=False)
        self.widget['scale_min'].show()
        self.widget['scale_max'].show()
        #self.widget['scale_interp'].show()
        grid.attach(self.widget['scale_max'], 1, 2, 0, 1)
        grid.attach(self.widget['scale_min'], 1, 2, 1, 2)
        grid.attach(self.widget['scale_interp'], 1, 2, 2, 3)
        # the labels
        self.widget['label_scale_min'] = gtk.Label(str='color scale max : ')
        self.widget['label_scale_min'].set_alignment(0,0)
        self.widget['label_scale_min'].show()
        grid.attach(self.widget['label_scale_min'], 0, 1, 0, 1, xoptions=gtk.FILL, yoptions=gtk.FILL)
        self.widget['label_scale_max'] = gtk.Label(str='color scale min : ')
        self.widget['label_scale_max'].set_alignment(0,0)
        self.widget['label_scale_max'].show()
        grid.attach(self.widget['label_scale_max'], 0, 1, 1,2, xoptions=gtk.FILL, yoptions=gtk.FILL)
        self.widget['label_scale_interp'] = gtk.Label(str='crop stiffness : ')
        self.widget['label_scale_interp'].set_alignment(0,0)
        #self.widget['label_scale_interp'].show()
        grid.attach(self.widget['label_scale_interp'], 0, 1, 2,3, xoptions=gtk.FILL, yoptions=gtk.FILL)
        grid.show()
        table[1].attach(grid, 0, 1, 5, 6)
        
        # button for the title
        self.widget['entry_title'].set_text('Title')
        self.widget['entry_title'].show()
        self.widget['entry_title'].connect('changed', self.change_title)
        table[1].attach(self.widget['entry_title'],
                             1, 2, 0, 1, xoptions=0, yoptions=0)
        self.plot_array.title = self.widget['entry_title'].get_text()
        # button to choose the item to plot
        list_arr = gtk.ListStore(str)
        for key in self.array['dict']:
            list_arr.append([key])
        self.widget['combo_array'].set_model(list_arr)
        cell = gtk.CellRendererText()
        self.widget['combo_array'].pack_start(cell, True)
        self.widget['combo_array'].add_attribute(cell, 'text', 0)
        self.widget['combo_array'].set_active(0)
        self.widget['combo_array'].show()
        self.widget['combo_array'].connect('changed', self.change_array)
        table[1].attach(self.widget['combo_array'],
                             1, 2, 1, 2, xoptions=0, yoptions=0)
        # button to choose the scale
        list_cmap = gtk.ListStore(str)
        list_cmap.append(['gray'])
        list_cmap.append(['jet'])
        list_cmap.append(['copper'])
        self.widget['combo_cmap'].set_model(list_cmap)
        cell = gtk.CellRendererText()
        self.widget['combo_cmap'].pack_start(cell, True)
        self.widget['combo_cmap'].add_attribute(cell, 'text', 0)
        self.widget['combo_cmap'].set_active(0)
        self.widget['combo_cmap'].show()
        self.widget['combo_cmap'].connect('changed', self.change_cmap)
        table[1].attach(self.widget['combo_cmap'],
                             1, 2, 2, 3, xoptions=0, yoptions=0)
        # Display the scale ?
        self.widget['check_axis'].show()
        self.widget['check_axis'].set_active(True)
        self.widget['check_axis'].connect('toggled', self.change_axis_label)
        table[1].attach(self.widget['check_axis'],
                             1, 2, 3, 4, xoptions=0, yoptions=0)
        # Display the color map ?
        self.widget['check_cmap'].show()
        self.widget['check_cmap'].set_active(False)
        self.widget['check_cmap'].connect('toggled', self.change_cmap)
        table[1].attach(self.widget['check_cmap'],
                             1, 2, 4, 5, xoptions=0, yoptions=0)
        
        # Interpolate the data ?
        self.widget['check_interp'].show()
        self.widget['check_interp'].set_active(False)
        self.widget['check_interp'].connect('toggled', self.toggle_interp)
        table[1].attach(self.widget['check_interp'],
                            1, 2, 5, 6, xoptions=0, yoptions=0)
        
        # button to choose the size of the image.
        _size_table = gtk.Table(2, 2)
        label = gtk.Label('Width')
        label.show()
        _size_table.attach(label, 1, 2, 0, 1, xoptions=0, yoptions=0)
        label = gtk.Label('Height')
        label.show()
        _size_table.attach(label, 1, 2, 1, 2, xoptions=0, yoptions=0)
        
        adjustment = gtk.Adjustment(value=800, lower=0, upper=1600, step_incr=1, page_incr=100, page_size=0)
        self.widget['size_x'].set_adjustment(adjustment)
        adjustment.connect('value_changed', self.spin_val_changed, 'size_x')
        self.widget['size_x'].show()
        _size_table.attach(self.widget['size_x'],
                            0, 1, 0, 1, xoptions=0, yoptions=0)
        adjustment = gtk.Adjustment(value=800, lower=0, upper=1600, step_incr=1, page_incr=100, page_size=0)
        self.widget['size_y'].set_adjustment(adjustment)
        adjustment.connect('value_changed', self.spin_val_changed, 'size_y')
        self.widget['size_y'].show()
        _size_table.attach(self.widget['size_y'],
                            0, 1, 1, 2, xoptions=0, yoptions=0)
        _size_table.show()
        table[1].attach(_size_table, 1, 2, 6, 7)
        table[0].attach(table[1], 0, 2, 1, 2)
        
        self.dialog.vbox.pack_start(table[0], True, True, 0)
    def spin_val_changed(self, event, wtype):
        """
            Catch the spin val changed.
        """
        adjustment = self.widget[wtype].get_adjustment()
        adjustment.set_upper(event.get_value() + 800)
    def change_folder(self, widget):
        """
            Interface to change the export folder.
        """
        
        chooser = gtk.FileChooserDialog(
                                title='Choose folder',
                                action=gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER,
                                buttons=(gtk.STOCK_CANCEL,
                                         gtk.RESPONSE_CANCEL,
                                         gtk.STOCK_OPEN,
                                         gtk.RESPONSE_OK)
                                        )
        chooser.set_select_multiple(False)
        chooser.set_default_response(gtk.RESPONSE_OK)
        chooser.set_local_only(False)
        chooser.set_filename(self.file['folder'])
        response = chooser.run()
        # Analyse the answer
        if response == gtk.RESPONSE_OK:
            self.file['folder'] = chooser.get_filenames()[0]
            self.widget['but_folder'].set_label(self.file['folder'])
        elif response == gtk.RESPONSE_CANCEL:
            chooser.destroy()
            return
        else:
            return
        chooser.destroy()
    def change_title(self, widget=None):
        """
            Change the title of the plot.
        """
        self.__update_title()
        self.plot()
    def __update_title(self):
        title = self.generate_text(self.widget['entry_title'].get_text())
        self.plot_array.title = title
    def change_filename(self, widget):
        auto = widget.get_active()
        
    def change_cmap(self, widget):
        """
            Modify the colormap of the plot. Displays or not the colorscale
            depending on the state of the checkbutton.
        """
        
        self.plot_array.cmap = self.widget['combo_cmap'].get_active_text()
        self.plot_array.color_scale['colorbar'] = \
                                        self.widget['check_cmap'].get_active()
        self.plot()
    def change_axis_label(self, widget):
        """
            Modify the axis label.
        """
        self.plot_array.scale['show'] = self.widget['check_axis'].get_active()
        self.plot()
    def change_array(self, array=None):
        """
            Modify the array to display.
        """
        if type(array) == str:
            key = array
        elif type(array) == gtk.ComboBox:
            key = array.get_active_text()
        self.array['plot'] = self.array['dict'][key]
        self.array['type'] = key
        self._init_scales()
        self.plot()
    def toggle_interp(self, widget):
        """
            Toogle between interpolate or not.
        """
        self.array['do_interp'] = widget.get_active()
        if self.array['do_interp']:
            self.widget['scale_interp'].show()
            self.widget['label_scale_interp'].show()
        else:
            self.widget['scale_interp'].hide()
            self.widget['label_scale_interp'].hide()
        self.plot()
    
    def plot(self):
        """
            Plot the result.
        """
        if self.array['plot'] is not None:
            if self.array['plot'].ndim == 3:
                data = self.array['plot'][:,:,0]
            else:
                data = self.array['plot']
            if self.array['do_interp']:
                data = compute_interp(data, self.array['interp_max'])
            self.__update_title()
            self.plot_array.plot(data,
                                 scan_size=self.array['size'])
            self.plot_array.vmin = self.array['scale'][0]
            self.plot_array.vmax = self.array['scale'][1]
            self.show()
    def show(self):
        """
            Show the result in the window.
        """
        labels = self.widget['check_cmap'].get_active() + \
               self.widget['check_axis'].get_active() + \
               len(self.widget['entry_title'].get_text())
        if labels:
            self.plot_array.figure.subplots_adjust(left=0.5, bottom=0.5)
        else:
            self.plot_array.figure.subplots_adjust(left=0, bottom=0)
        self.canvas.draw_idle()
    def run(self):
        """
            Runs the dialog.
        """
        response = self.dialog.run()
        self.file['name'] = self.widget['entry_filename'].get_text()
        if response == gtk.RESPONSE_OK:
            # Runs the code if button OK is clicked
            [name, ext] = os.path.splitext(self.file['name'])
            if not len(ext):
                ext = '.png'
            self.export_map(name, ext)
        self.destroy()
    def export_map(self, name, ext):
        """
            Export the maps.
        """
        # Get the size of the image.
        width = self.widget['size_x'].get_value()
        height = self.widget['size_y'].get_value()
        self.plot_array.set_size(width, height, deep=True)
        # Get the displayed item.
        if len(self.array['plot'].shape) == 3:
            # block the min and max
            self.plot_array.vmin = self.array['scale'][0]
            self.plot_array.vmax = self.array['scale'][1]
            for index in range(self.array['plot'].shape[2]):
                self.array['indice'] = index
                self.__update_title()
                arr_to_export = self.array['plot'][:, :, index]
                if self.array['do_interp']:
                    arr_to_export = compute_interp(arr_to_export, self.array['interp_max'])
                self.plot_array.plot(arr_to_export)
                newname = self.generate_text(name)
                f_name = newname + ext
                self.plot_array.save(os.path.join(self.file['folder'], f_name))
        else:
            newname = self.generate_text(name)
            f_name = newname + ext
            self.plot_array.plot(self.array['plot'])
            self.plot_array.save(os.path.join(self.file['folder'], f_name))
    def generate_text(self, pattern):
        """
            Generate the filename from the pattern.
            
            %d means depth
            %n means number
            %f means the filename
            %t means the type
        """
        
        while pattern.find('%d') is not -1:
            indice = pattern.find('%d')
            part_1 = pattern[:indice]
            part_2 = pattern[indice+2:]
            pattern = part_1 + str(self.array['indice']*self.array['depth']) + part_2
        while pattern.find('%n') is not -1:
            indice = pattern.find('%n')
            part_1 = pattern[:indice]
            part_2 = pattern[indice+2:]
            pattern = part_1 + str(self.array['indice']) + part_2
        while pattern.find('%f') is not -1:
            indice = pattern.find('%f')
            part_1 = pattern[:indice]
            part_2 = pattern[indice+2:]
            pattern = part_1 + self.file['fv_name'] + part_2
        while pattern.find('%t') is not -1:
            indice = pattern.find('%t')
            part_1 = pattern[:indice]
            part_2 = pattern[indice+2:]
            pattern = part_1 + self.array['type'] + part_2
        return pattern
    def destroy(self):
        """
            Destroy the window.
        """
        self.dialog.destroy()
def compute_interp(array, interp_max):
        try:
            _inf = array.max()
            _array = array.filled(_inf)
        except AttributeError:
            _array = array
        x, y = array.shape
        x = complex(0, x)
        y = complex(0, y)
        coords = numpy.mgrid[
                        0 : array.shape[0] - 1 : x*10,
                        0 : array.shape[1] - 1 : y*10]
        interp_arr = ndimage.map_coordinates(
                                        _array, coords)
        interp_arr = numpy.ma.array(interp_arr,
                                mask=interp_arr>=interp_max)
        return interp_arr
if __name__ == '__main__':
    import numpy
    TEST = {'Stiffness' : numpy.random.randn(10, 10, 5),
            'Piezo' : numpy.random.randn(10,10),
            'masked' : numpy.ma.array(numpy.random.randn(20,20),
                                      mask=numpy.random.randn(20,20)>0.5,
                                      fill_value=numpy.nan)
            }
    DISPLAY_IT = MapDisplay(TEST, depth=50, scan_size=2000)
    DISPLAY_IT.generate_text('Deep %d is now %d')
    DISPLAY_IT.run()
