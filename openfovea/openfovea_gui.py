#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Module for the GTK gui of OpenFovea.
"""

import gtk
import os.path
import sys
import locale
import threading
import time
import datetime
from glib import GError

import numpy
#####################
## Graphic library ##
#####################
import plot_gtk
# for script installation
from pkg_resources import resource_filename
######################
## Internal library ##
######################
from common import APP_NAME, APP_VERSION
import classes
from classes import NotReadyError
from plot_generic import PlotProperties, PlotData

try:
    from fovea_toolbox.opengl_gtk import TopoWindow as TopoGL
except ImportError:
    # OpenGL is not installed.
    OPENGL_INSTALLED = False
except RuntimeError:
    # OpenGL cannot be loaded...
    OPENGL_INSTALLED = False
else:
    OPENGL_INSTALLED = True

import dialog_experiment_properties
from dialog_export_map import MapDisplay
from dialog_mask_properties import MaskProperties
from dialog_flatten import FlattenDialog
from simple_window_gtk import FileGroup
from fovea_toolbox import misc

DEBUG = 0


def combo_box_set_list(combox, item_list, default=0):
    """
    Sets the combobox list
    """
    preexist = 0
    # Reinitialize or create the model
    model = combox.get_model()
    if model:
        model.clear()
        preexist = 1
    else:
        model = gtk.ListStore(str)

    for item in item_list:
        model.append([item])
    # Pack it if it doesn't preexist
    if not preexist:
        combox.set_model(model)
        cell = gtk.CellRendererText()
        combox.pack_start(cell, True)
        combox.add_attribute(cell, 'text', 0)
    # And set the first item active
    combox.set_active(default)


class OpenFoveaMainGui(object):
    """
        OpenFovea GTK Gui.

        Contains all the buttons and dialog to handle experiment post
        processing.
    """

    def __init__(self, arg=None):
        # Defaults parameters
        if os.name == 'posix':
            _home = os.environ['HOME']
        elif os.name == 'nt':
            _home = os.environ['HOMEPATH']
        self.config = {'current': os.getcwd(),
                          'home': _home,
                          'config': os.path.join(
                                            _home,
                                            '.config',
                                            'OpenFovea.conf'),
                          'folder-fv': os.getcwd(),
                          'folder-aex': os.getcwd(),
                          'folder-export': os.getcwd(),
                          'msgid': datetime.datetime(1900, 1, 1),
                          'linewidth': 1.5,  # The width of the line in the
                                              # curve plots.
                          'connect-server': None}
        self.apply_on_all = 0
        # try to recover preferences...
        if os.path.isfile(self.config['config']):
            config_fid = open(self.config['config'])
            for line in config_fid.readlines():
                splitted_line = line.strip('\n').split(' = ')
                if splitted_line[0] == 'msgid':
                    self.config[splitted_line[0]] = \
                        datetime.datetime.strptime(splitted_line[1],
                                                   '%Y, %m, %d')
                elif splitted_line[0] == 'linewidth':
                    self.config[splitted_line[0]] = float(splitted_line[1])
                elif splitted_line[0] == 'connect-server':
                    self.config[splitted_line[0]] = eval(splitted_line[1])
                elif splitted_line[0] in self.config.keys():
                    self.config[splitted_line[0]] = \
                                            splitted_line[1]
            if 'folder-export' not in self.config:
                self.config['folder-export'] = os.getcwd()
        # Get authorization for connection
        if self.config['connect-server'] is None:
            answer = ask_message(['Allow internet connection ?',
                    '<b>Do you allow OpenFovea to connect server ?</b>\n\n' +
                    'The connection is usefull to display message\n' +
                    'about new release, or important bugs.'])
            if answer == gtk.RESPONSE_YES:
                self.config['connect-server'] = True
            elif answer == gtk.RESPONSE_NO:
                self.config['connect-server'] = False
        ###
        # Get message from the OpenFovea website in order to display news.
        # This will be used for update announce or important news.
        if self.config['connect-server']:
            message, new_id = misc.get_message(self.config['msgid'],
                                               APP_NAME, APP_VERSION)
            if message is not None:
                display_message(message[1:])
                self.config['msgid'] = new_id
        ###
        # Initialize the gui
        ###
        builder = gtk.Builder()
        try:
            glade_filename = resource_filename('openfovea',
                                               'glade/openfovea.glade')
        except NotImplementedError:
            # We are in a windows executable...
            glade_filename = os.path.join(os.path.dirname(sys.executable),
                                          ('openfovea/glade/openfovea.glade'))
        builder.add_from_file(glade_filename)
        self.exp_filename = None
        self.window = builder.get_object("MainWindow")
        self.about_dialog_win = builder.get_object('aboutdialog')
        self.about_dialog = builder.get_object("aboutdialog")
        self.properties_dialog = None
        self.compare_exp_dialog = \
                            builder.get_object('dialog_compare_experiment')
        # connect signals
        builder.connect_signals(self)
        ## The variables
        self.plot_properties = PlotProperties()
        self.plot_properties.color_type = 'color'
        self.data_hist = PlotData()
        self.experiment = None
        self.temp_exp_compare = None
        self.experiment_compare = None
        self.force_volume = None
        self.widget = {
            'progressbar': builder.get_object("progressbar_general"),
            'check_display_fc_trace': builder.get_object(
                                                'check_display_fc_trace'),
            'check_display_fc_retrace': builder.get_object(
                                                'check_display_fc_retrace'),
            'check_display_poc': builder.get_object(
                                                'check_display_poc'),
            'check_display_indent': builder.get_object(
                                                'check_display_indent'),
            'check_display_event': builder.get_object(
                                                'check_display_event'),
            'check_limit_poc_slide': builder.get_object(
                                                'check_limit_poc_slide'),
            'check_compute_poc': builder.get_object('check_compute_poc'),
            'check_stiff_ev_vs_notev': builder.get_object(
                                                'check_stiff_ev_vs_notev'),
            'combo_list_files': builder.get_object('list_file_combo_box'),
            'combo_hertz_model': builder.get_object('combo_hertz_model'),
            'combo_stiff_method': builder.get_object('combo_stiff_method'),
            'combo_event_detect': builder.get_object('combo_event_detect'),
            'combo_event_thresh': builder.get_object('combo_event_thresh'),
            'combo_poc_method': builder.get_object('combo_poc_method'),
            'combo_ev_fit_model': builder.get_object(
                                                    'combo_ev_fit_model'),
            'entry_number_deep': builder.get_object("entry_number_deep"),
            'entry_size_deep': builder.get_object('entry_size_deep'),
            'entry_glass_slope': builder.get_object('entry_glass_slope'),
            'label_pos_x': builder.get_object('label_pos_x'),
            'label_pos_y': builder.get_object('label_pos_y'),
            'label_switch_state': builder.get_object('label_switch_state'),
            'spin_ev_force_hist_min': builder.get_object('ev_force_hist_min'),
            'spin_ev_force_hist_max': builder.get_object('ev_force_hist_max'),
            'spin_ev_force_hist_size': builder.get_object(
                                                    'ev_force_hist_size'),
            'spin_max_rel_stiffness': builder.get_object('max_rel_stiffness'),
            'spin_ev_lr_hist_min': builder.get_object('ev_lr_hist_min'),
            'spin_ev_lr_hist_max': builder.get_object('ev_lr_hist_max'),
            'spin_ev_lr_hist_size': builder.get_object('ev_lr_hist_size'),
            'spin_ev_dist_hist_min': builder.get_object('ev_dist_hist_min'),
            'spin_ev_dist_hist_max': builder.get_object('ev_dist_hist_max'),
            'spin_ev_dist_hist_size': builder.get_object('ev_dist_hist_size'),
            'spin_ev_length_hist_min': builder.get_object(
                                                'ev_length_hist_min'),
            'spin_ev_length_hist_max': builder.get_object(
                                                'ev_length_hist_max'),
            'spin_ev_length_hist_size': builder.get_object(
                                                'ev_length_hist_size'),
            'spin_ev_plength_hist_min': builder.get_object(
                                                'ev_plength_hist_min'),
            'spin_ev_plength_hist_max': builder.get_object(
                                                'ev_plength_hist_max'),
            'spin_ev_plength_hist_size': builder.get_object(
                                                'ev_plength_hist_size'),
            'spin_ev_stiff_hist_min': builder.get_object('ev_stiff_hist_min'),
            'spin_ev_stiff_hist_max': builder.get_object('ev_stiff_hist_max'),
            'spin_ev_stiff_hist_size': builder.get_object(
                                                'ev_stiff_hist_size'),
            'spin_ev_relstiff_hist_min': builder.get_object(
                                                'ev_relstiff_hist_min'),
            'spin_ev_relstiff_hist_max': builder.get_object(
                                                'ev_relstiff_hist_max'),
            'spin_ev_relstiff_hist_size': builder.get_object(
                                                'ev_relstiff_hist_size'),
            'spin_ev_relstiff_hist_dist': builder.get_object(
                                                'ev_relstiff_hist_dist'),
            'spin_rel_stiff_plot_dist': builder.get_object(
                                                'spin_rel_stiff_plot_dist'),
            'spin_nbr_rand_event': builder.get_object('spin_nbr_rand_event'),
            'spin_ev_threshold_dist': builder.get_object(
                                                'spin_ev_threshold_dist'),
            'spin_poisson_ratio': builder.get_object('spin_poisson_ratio'),
            # Option menu
            'apply_on_all': builder.get_object('apply_on_all'),
            # Parameters tab
            'spin_fit_seg_size': builder.get_object('spin_fit_seg_size'),
            'spin_ev_length_thresh_min': builder.get_object(
                                                'spin_ev_length_thresh_min'),
            'spin_ev_length_thresh_max': builder.get_object(
                                                'spin_ev_length_thresh_max'),
            # Mechanical prop.
            'spin_young_modulus_hist_min': builder.get_object(
                                                'spin_ym_hist_min'),
            'spin_young_modulus_hist_max': builder.get_object(
                                                'spin_ym_hist_max'),
            'spin_young_modulus_hist_size': builder.get_object(
                                                'spin_ym_hist_size'),
            'spin_tip_carac': builder.get_object('spin_carac'),
            'spin_ym_hist_depth_nb': builder.get_object(
                                                'spin_ym_hist_depth_nb'),
            }
        self.signal = {
            ### Dictionnary that contains the signal from the object.
            ### This is auto - generated with :
            ### widgetname_fctname
        }
        self.widget_exp_compare = {
            'comp_exp_filename': builder.get_object('comp_exp_filename'),
            'label_main_filename': builder.get_object('label_main_filename'),
            'label_comp_filename': builder.get_object('label_comp_filename'),
            'entry_main_label': builder.get_object('entry_main_label'),
            'entry_comp_label': builder.get_object('entry_comp_label'),
            'combo_plot_color_type': builder.get_object(
                                                'combo_plot_color_type'),
        }
        if not OPENGL_INSTALLED:
            builder.get_object('button_opengl').hide()
        #####
        ## Compute tab
        #####
        combo_box_set_list(self.widget['combo_event_detect'],
                            ['Noise', 'Fuzzy'])
        combo_box_set_list(self.widget['combo_event_thresh'],
                            ['Fixed', 'Auto (Noise)'])
        #####
        ## Parameter tab
        #####
        combo_box_set_list(self.widget['combo_poc_method'],
                           ['deriv', 'curve_fit'])
        self.widget['combo_poc_method'].set_active(1)

        adjustment = gtk.Adjustment(value=0, lower=0, upper=1000,
                                    step_incr=1, page_incr=10, page_size=0)
        self.widget['spin_ev_threshold_dist'].set_adjustment(adjustment)

        adjustment = gtk.Adjustment(value=0, lower=0, upper=2000,
                                    step_incr=1, page_incr=10, page_size=0)
        self.widget['spin_ev_length_thresh_min'].set_adjustment(adjustment)

        adjustment = gtk.Adjustment(value=0, lower=0, upper=1000,
                                    step_incr=1, page_incr=10, page_size=0)
        self.widget['spin_ev_length_thresh_max'].set_adjustment(adjustment)

        combo_box_set_list(self.widget['combo_hertz_model'],
                           ['Cone', 'Sphere'])
        combo_box_set_list(self.widget['combo_stiff_method'],
                           ['Raw', 'Linear', 'Extrema', 'Median'])
        self.widget['combo_hertz_model'].set_active(1)
        self.widget['combo_stiff_method'].set_active(0)
        self.widget['spin_tip_carac'].set_value(40)
        self.widget['spin_poisson_ratio'].set_value(0.3)
        combo_box_set_list(self.widget['combo_ev_fit_model'],
                                                    ['None', 'wlc', 'fjc'])
        #####
        ## Event tab
        #####

        # The windows used for the plots
        self.win_plot = {'Curve Plot': 0,
                    'Indent Plot': 0,
                    'Topography Map': 0,
                    'Piezo Height': 0,
                    'Stiffness Map': 0,
                    'Event Map': 0,
                    'Event Force Map': 0,
                    'Event Distance Map': 0,
                    'Event Length Map': 0,
                    'Stiffness Tomo': 0,
                    'Average Stiffness Tomo': 0,
                    'Mosaic Piezo': 0,
                    'Mosaic Topo': 0,
                    'Mosaic Stiffness': 0,
                    'Mosaic Event': 0,
                    'Mosaic Event Force': 0,
                    'Hist Event Force': 0,
                    'Hist Event LR': 0,
                    'Hist Event Dist': 0,
                    'Hist Event Stiff': 0,
                    'Hist Event Rel Stiff': 0,
                    'Hist Event Length': 0,
                    'Hist Event Persistent Length': 0,
                    'TL Rel Stiffness': 0,
                    'TL Nbr Event': 0,
                    'Scatter_FvsLR': 0,
                    'Scatter_FvsDist': 0,
                    'Scatter_FvsStiff': 0,
                    'Scatter_LRvsDist': 0,
                    'Scatter_LRvsStiff': 0,
                    'Scatter_DistvsStiff': 0,
                    'Scatter_FvsRelStiff': 0,
                    'Scatter_LRvsRelStiff': 0,
                    'Scatter_DistvsRelStiff': 0,
                    'Slice_on_map': 0,
                    'Hist Young Modulus': 0,
                    }
        if arg is not None:
            self.on_open_experiment_activate(arg)
    ##########################################################################
    # Misc part of the window

    # bottom
    def display_progressbar(self, fraction, text=''):
        """
            Display the progress bar.
        """
        if fraction == None:
            self.widget['progressbar'].set_property('visible', True)
            self.widget['progressbar'].set_fraction(0)
            self.widget['progressbar'].set_text('')
        elif type(fraction) != str:
            while gtk.events_pending():
                gtk.main_iteration()
            if not self.widget['progressbar'].get_property('visible'):
                self.widget['progressbar'].set_property('visible', True)
            self.widget['progressbar'].set_fraction(fraction)
            self.widget['progressbar'].set_text(text)
            self.widget['progressbar'].show()
        elif fraction == 'pulse':
            while gtk.events_pending():
                gtk.main_iteration()
            self.widget['progressbar'].pulse()
            self.widget['progressbar'].set_text(text)
            self.widget['progressbar'].show()

    def update_switch(self):
        """
            Update switches in the lower part of the GUI.
        """
        # generate the text
        text = ''
        _fvid = self.experiment.file['current']
        for key in self.experiment.switch_dict:
            text += key + ' : ' + str(self.experiment.get_switch(key)) + ' | '
        self.widget['label_switch_state'].set_text(text)
        _thresh = self.experiment.file['list'][_fvid].get_switch('ev_thresh')
        self.widget['spin_ev_threshold_dist'].set_value(_thresh)
        self.widget['spin_poisson_ratio'].set_value(
           self.experiment.file['list'][_fvid].get_parameter('poisson_ratio'))
        _thresh = self.experiment.file['list'][_fvid].get_switch(
                                                            'ev_fit_length')
        if _thresh[0] is not None:
            self.widget['spin_ev_length_thresh_max'].set_value(_thresh[0])
        else:
            self.widget['spin_ev_length_thresh_max'].set_value(0)
        if _thresh[1] is not None:
            self.widget['spin_ev_length_thresh_max'].set_value(_thresh[1])
        else:
            self.widget['spin_ev_length_thresh_max'].set_value(0)

    def update_hist(self, label, max_val, min_val=0):
        """
            Update the histogram properties.
        """
        meth_name = 'on_button_%s_clicked' % label
        # First disconnect the signal from the widget. Otherwise, it will plot
        # automatically. This is not wanted in case of loading new files, ...
        for item in ['min', 'max', 'size']:
            widget_name = 'spin_%s_%s' % (label, item)
            signal_name = '%s_min_%s' % (widget_name, meth_name)
            if signal_name in self.signal:
                self.widget[widget_name].disconnect(self.signal[signal_name])
        widget_name = 'spin_%s_max' % label
        self.widget[widget_name].set_adjustment(
                        gtk.Adjustment(value=max_val,
                                       lower=min_val,
                                       upper=max_val * 10,
                                       step_incr=max_val / 100,
                                       page_incr=max_val / 25,
                                       page_size=0))
        self.widget[widget_name].set_text(str(max_val))

        widget_name = 'spin_%s_min' % label
        self.widget[widget_name].set_adjustment(
                        gtk.Adjustment(value=min_val,
                                       lower=min_val,
                                       upper=max_val,
                                       step_incr=max_val / 100,
                                       page_incr=max_val / 25,
                                       page_size=0))
        self.widget[widget_name].set_text(str(min_val))

        widget_name = 'spin_%s_size' % label
        self.widget[widget_name].set_adjustment(
                        gtk.Adjustment(value=max_val / 10,
                                       lower=0,
                                       upper=max_val,
                                       step_incr=max_val / 1000,
                                       page_incr=max_val / 250,
                                       page_size=0))
        self.widget[widget_name].set_text(str(max_val / 10))
        meth = getattr(self, meth_name)
        # Last : connect signal from the spins
        for item in ['min', 'max', 'size']:
            widget_name = 'spin_%s_%s' % (label, item)
            signal_name = '%s_min_%s' % (widget_name, meth_name)
            self.signal[signal_name] = \
                    self.widget[widget_name].connect('value_changed', meth)

    def update(self, what):
        """
            Update the GUI.
        """
        if what == 'show gride position':
            self.widget['label_pos_x'].set_text(str(self.force_volume.pos_x))
            self.widget['label_pos_y'].set_text(str(self.force_volume.pos_y))
            try:
                self.win_plot['Piezo Height'].mark_pos(
                                                self.force_volume.pos_x,
                                                self.force_volume.pos_y)
            except (KeyError, StandardError):
                pass
            try:
                self.win_plot['Stiffness Map'].mark_pos(
                                                self.force_volume.pos_x,
                                                self.force_volume.pos_y)
            except (KeyError, StandardError):
                pass
            try:
                self.win_plot['Event Map'].mark_pos(
                                                self.force_volume.pos_x,
                                                self.force_volume.pos_y)
            except (KeyError, StandardError):
                pass
            try:
                self.win_plot['Topography Map'].mark_pos(
                                                self.force_volume.pos_x,
                                                self.force_volume.pos_y)
            except (KeyError, StandardError):
                pass
        elif what == 'show glass slope':
            self.widget['entry_glass_slope'].set_text(
                                    str(self.force_volume.stiffness['glass']))
        elif what == 'gui':
            if self.force_volume is None:
                return
            self.widget['spin_max_rel_stiffness'].set_value(
                                self.experiment.parameters['rel_stiff_dist'])
            self.widget['entry_number_deep'].set_text(
                                str(self.force_volume.stiffness['nb_parts']))
            self.widget['entry_size_deep'].set_text(
                                str(self.force_volume.stiffness['size_parts']))
            self.widget['spin_nbr_rand_event'].set_value(
                                self.force_volume.event['nbr_random'])
            self.widget['spin_fit_seg_size'].set_value(
                                self.force_volume.stiffness['poc_len_slice'])
            if self.force_volume.stiffness['hertz_model'] == 'Cone':
                self.widget['combo_hertz_model'].set_active(0)
            elif self.force_volume.stiffness['hertz_model'] == 'Sphere':
                self.widget['combo_hertz_model'].set_active(1)
            if self.force_volume.stiffness['fit_method'] == 'Raw':
                self.widget['combo_stiff_method'].set_active(0)
            elif self.force_volume.stiffness['fit_method'] == 'Linear':
                self.widget['combo_stiff_method'].set_active(1)
            elif self.force_volume.stiffness['fit_method'] == 'Extrema':
                self.widget['combo_stiff_method'].set_active(2)
            elif self.force_volume.stiffness['fit_method'] == 'Median':
                self.widget['combo_stiff_method'].set_active(3)
            self.widget['spin_tip_carac'].set_value(
                                self.force_volume.stiffness['point_carac'])
            if self.force_volume.header['event_fit_model'] == None:
                self.widget['combo_ev_fit_model'].set_active(0)
            elif self.force_volume.header['event_fit_model'] == 'wlc':
                self.widget['combo_ev_fit_model'].set_active(1)
            elif self.force_volume.header['event_fit_model'] == 'fjc':
                self.widget['combo_ev_fit_model'].set_active(2)

            # Update switch apply_on_all.
            if self.experiment.get_switch('all') == 'Yes':
                self.widget['apply_on_all'].set_active(True)
            elif self.experiment.get_switch('all') == 'No':
                self.widget['apply_on_all'].set_active(False)
            self.update('hist entry')
        elif what == 'hist entry':
            ##
            ## Histograms
            ##
            try:
                max_dist_hist = self.force_volume.trace_x[-1]
            except TypeError:
                max_dist_hist = 10000
            self.update_hist('ev_force_hist', max_val=1.)
            self.update_hist('ev_lr_hist', max_val=1.)

            self.update_hist('ev_dist_hist',
                             max_val=max_dist_hist)
            self.update_hist('ev_relstiff_hist', max_val=10., min_val=-10.)
            # Take the maximum stiffness
            stiff_max = 1.e8
            exp = 7
            for fvo in self.experiment.file['list']:
                if fvo.get_array('Stiffness') is not None:
                    try:
                        stiff_max = fvo.get_array('Stiffness').max()
                    except ValueError:
                        pass
            if stiff_max:
                try:
                    exp = int(numpy.log10(stiff_max)) + 1
                except ValueError:
                    exp = 7
            else:
                exp = 7
            self.update_hist('ev_stiff_hist', max_val=10. ** exp)
            self.update_hist('young_modulus_hist', max_val=10. ** exp)
            self.update_hist('ev_length_hist',
                             max_val=max_dist_hist * 2)
            self.update_hist('ev_plength_hist',
                             max_val=max_dist_hist * 2)
            #fvo.get_array('Stiffness').shape[2]
            # update max depth in for the plot.
            spin_depth = ['spin_ym_hist_depth_nb']
            meth_name = 'on_button_young_modulus_hist_clicked'
            if fvo.get_array('Stiffness') is not None:
                for item in spin_depth:
                    # disconnect the widget from the signal
                    signal_name = '%s_min_%s' % (meth_name, item)
                    if signal_name in self.signal:
                        self.widget[item].disconnect(self.signal[signal_name])
                    self.widget[item].set_adjustment(
                        gtk.Adjustment(
                                value=0,
                                lower=0,
                                upper=fvo.get_array('Stiffness').shape[2],
                                step_incr=1,
                                page_incr=1,
                                page_size=0))
                    # and reconnect it
                    meth = getattr(self, meth_name)
                    # Last : connect signal from the spins
                    self.signal[signal_name] = \
                            self.widget[item].connect('value_changed', meth)

        elif what == 'fv display prop':
            self.force_volume.plot_who['Av'] = \
                           self.widget['check_display_fc_trace'].get_active()
            self.force_volume.plot_who['Re'] = \
                           self.widget['check_display_fc_retrace'].get_active()
            self.force_volume.plot_who['PoC'] = \
                           self.widget['check_display_poc'].get_active()
            self.force_volume.plot_who['Event'] = \
                           self.widget['check_display_event'].get_active()
            self.force_volume.plot_who['Indent'] = \
                           self.widget['check_display_indent'].get_active()
        elif what == 'plots':
            for item in self.win_plot:
                if self.win_plot[item]:
                    self.win_plot[item].set_size(
                                    self.experiment.parameters['plot_size'][0],
                                    self.experiment.parameters['plot_size'][1])
                    if self.win_plot[item].get_type() == 'histogram':
                        self.win_plot[item].set_y_rel(
                                    self.experiment.parameters['histo_y_rel'])

        self.update_switch()

    def regenerate_plot(self, ptype='normal'):
        """
            Regenerate a window plot and returns the window and widgets.
        """
        builder = gtk.Builder()
        if ptype == 'normal':
            try:
                _glade_filename = resource_filename('openfovea',
                                                    'glade/plot.glade')
            except NotImplementedError:
                _glade_filename = 'openfovea/glade/plot.glade'
        elif ptype == 'tomography':
            try:
                _glade_filename = resource_filename('openfovea',
                                                'glade/stiffness_plot.glade')
            except NotImplementedError:
                _glade_filename = 'openfovea/glade/stiffness_plot.glade'
        try:
            builder.add_from_file(_glade_filename)
        except:  # In windows environment...
            print ptype
            if ptype == 'tomography':
                glade_filename = os.path.join(os.path.dirname(sys.executable),
                                        'openfovea/glade/stiffness_plot.glade')
            elif ptype == 'normal':
                glade_filename = os.path.join(os.path.dirname(sys.executable),
                                        'openfovea/glade/plot.glade')
            else:
                glade_filename = os.path.join(os.path.dirname(sys.executable),
                                        'openfovea/glade/plot.glade')
            builder.add_from_file(glade_filename)
        window = builder.get_object("WindowPlot")
        builder.connect_signals(self)
        if ptype == 'normal':
            widget = {
            'plot_area': builder.get_object('boxPlot'),
            'scrollbar_min': builder.get_object('scrollbar_min'),
            'scrollbar_max': builder.get_object('scrollbar_max'),
            'scrollbar_depth': builder.get_object('scrollbar_depth'),
            'box_nav_tool': builder.get_object('box_nav_tool'),
            'button_modify_plot': builder.get_object('button_modify_plot')}
        elif ptype == 'tomography':
            widget = {
            'plot_area': builder.get_object('boxPlot'),
            'display_x': builder.get_object('display_x'),
            'display_y': builder.get_object('display_y'),
            'display_z': builder.get_object('display_z'),
            'adjust_x': builder.get_object('adjust_x'),
            'adjust_y': builder.get_object('adjust_y'),
            'adjust_z': builder.get_object('adjust_z'),
            'scrollbar_min': builder.get_object('scrollbar_min'),
            'scrollbar_max': builder.get_object('scrollbar_max')}
        return [window, widget]

    def modify_window(self, window, how):
        """
            Modify the specified window.

            window = 'Curve Plot' : add the scale bars to modify the events /
                                    PoC detection threshold.
        """

        # Curve Plot window modification to tune the event detection
        if window == 'Curve Plot':
            if how == 'Event detection':
                self.win_plot['Curve Plot'].widget['scrollbar_max'].\
                            set_property('visible', True)
                self.win_plot['Curve Plot'].widget['scrollbar_max'].\
                            set_adjustment(gtk.Adjustment(
                               self.force_volume.header['event_detect_weight'],
                               0.0, 5, 0.1, 0.5, 0))
                self.win_plot['Curve Plot'].widget['scrollbar_max'].\
                            connect_object("change-value",
                                            self.change_event_weight, None)
            if how == 'PoC detection':
                self.win_plot['Curve Plot'].widget['scrollbar_depth'].\
                            set_property('visible', True)
                self.win_plot['Curve Plot'].widget['scrollbar_depth'].\
                            set_adjustment(gtk.Adjustment(
                                self.force_volume.stiffness['poc_threshold'],
                                0.0, 10, 0.1, 0.5, 0))
                self.win_plot['Curve Plot'].widget['scrollbar_depth'].\
                            connect_object("change-value",
                                            self.change_poc_threshold, None)

    def close_all_windows(self):
        """
            Close all the plot windows.
            Usefull when loading new experiment.
        """
        for window in self.win_plot:
            if exist(self.win_plot[window]):
                self.win_plot[window].on_win_plot_destroy('from_gui')

    def on_gride_click(self, event):
        """
            Event when user click on the array.
        """
        if event.inaxes is not None:
            # Create the factor (1 if [0;1000[, 1000 if [1000:1e6[, ...)
            scan_size = self.force_volume.header['scan_size']
            pix_size = self.force_volume.header['pixel_size']
            try:
                factor = [1000 ** (int(numpy.log10(i) / 3)) for i in scan_size]
            except OverflowError:
                # The scan size is set to 0.
                x_pos = numpy.floor(event.xdata)
                y_pos = numpy.floor(event.ydata)
            else:
                x_pos = event.xdata * factor[0] / pix_size[0]
                y_pos = event.ydata * factor[1] / pix_size[1]
            if event.button == 1:
                # Left mouse button ==> goto
                self.force_volume.go_to(int(x_pos), int(y_pos))
                self.plot_curve()
                self.update('show gride position')
            elif event.button == 3:
                # Right mouse button ==> slice
                self.path_gride(event.inaxes)

    def on_gride_motion(self, event):
        """
            Event when mouse is moving on the array.
        """
        if event.inaxes is not None:
            self.path_gride(event.inaxes)

    def path_gride(self, axe_id):
        """
            Slice the array.
        """
        path = None
        for item in self.win_plot:
            # Find the window where user click
            if self.win_plot[item]:
                _axis = self.win_plot[item].axis
                if type(_axis) == dict and 'array' in _axis:
                    if _axis['array'] == axe_id:
                        path = self.win_plot[item].get_path()

        if path is not None:
            # Points list is defined.
            this_slice = self.force_volume.get_path('all', path)

            if not exist(self.win_plot['Slice_on_map']):
                window, widget = self.regenerate_plot()
                self.win_plot['Slice_on_map'] = plot_gtk.plotInteractiveCurve(
                                            window, widget, win_title='Slices')
                                        #xlabel = '[nm]',
                                        #ylabel = '???',
                                        #title = 'Slice of...',
                                        #win_title = 'Slice of...')
                self.win_plot['Slice_on_map'].plot(this_slice[0],
                                                     this_slice[1],
                                                     this_slice[2])
            else:
                self.win_plot['Slice_on_map'].modify_plot(this_slice[0],
                                                            this_slice[1])

    def on_mosaic_click(self, event):
        """
            Change the current FV scan by clicking on the corresponding scan in
            the mosaic view.
        """
        for item in self.win_plot:
            if isinstance(self.win_plot[item], plot_gtk.plotMosaic):
                if self.win_plot[item].axis.count(event.inaxes):
                    _index = self.win_plot[item].axis.index(event.inaxes)
                    self.force_volume = self.experiment.get_file(_index)
                    self.update('fv display prop')
                    self.update('gui')
                    self.widget['combo_list_files'].set_active(_index)

    def on_mosaic_pick(self, event):
        """
            Change the corresponding scan state according to the picked object.
            For example, by clicking on the "M" box, it changes the mask state.
        """
        for item in self.win_plot:
            if isinstance(self.win_plot[item], plot_gtk.plotMosaic):
                if self.win_plot[item].axis.count(event.mouseevent.inaxes):
                    [axis, otype] = self.win_plot[item].index(event)
                    if otype == 'mask':
                        current = \
                          self.experiment.file['list'][axis].get_switch('mask')
                        self.experiment.file['list'][axis].set_switch('mask',
                                                                not(current))

    def change_event_weight(self, widget, jump, value):
        """
            Change the event detection dependency to noise. Done with the scale
            bar.
        """

        if DEBUG:
            print "Changing event weight..."
            print widget, jump
        self.experiment.parameters['event_detect_weight'] = value
        self.plot_curve()

    def change_poc_threshold(self, widget, jump, value):
        """
            Change the point of contact detection threshold. Done with the
            scale bar.
        """

        if DEBUG:
            print "Changing poc threshold..."
            print widget, jump
        self.experiment.PoC_threshold = value
        self.plot_curve()

    #############################################################
    ## File Menu
    def on_new_experiment_activate(self, widget):
        '''
            Creates a new experiment by asking the user to point where the AFM
            files are.
        '''
        if DEBUG:
            print "Make new experiment. Call from :"
            print widget
        # Call the file chooser dialog.
        # In order to open directory, change action in
        # gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER
        chooser = gtk.FileChooserDialog(
                                title='Choose folder',
                                action=gtk.FILE_CHOOSER_ACTION_SELECT_FOLDER,
                                buttons=(gtk.STOCK_CANCEL,
                                         gtk.RESPONSE_CANCEL,
                                         gtk.STOCK_OPEN,
                                         gtk.RESPONSE_OK))
        chooser.set_select_multiple(False)
        chooser.set_default_response(gtk.RESPONSE_OK)
        chooser.set_local_only(False)
        chooser.set_filename(self.config['folder-fv'])
        response = chooser.run()
        # Analyse the answer
        if response == gtk.RESPONSE_OK:
            folder = u'' + chooser.get_filenames()[0]
        elif response == gtk.RESPONSE_CANCEL:
            chooser.destroy()
            return
        else:
            return

        chooser.destroy()
        self.experiment = classes.FVFolder()
        _thread = threading.Thread(
                            target=self.experiment.load_folder, args=(folder,))
        _thread.start()
        while _thread.isAlive():
            self.display_progressbar(self.experiment.counter['num'],
                                     self.experiment.counter['text'])
            time.sleep(0.1)
        self.force_volume = self.experiment.get_file(0)
        # File was correctly loaded, saving the folder in preferences...
        self.config['folder-fv'] = folder
        self.exp_filename = None
        # update file list comboBox...
        combo_box_set_list(self.widget['combo_list_files'],
                           self.experiment.file['name'])
        #self.update('gui')
        self.display_progressbar(None)
        self.close_all_windows()

    def load_exp(self):
        '''
            Loads an aex file. This is the generic function.
        '''
        # Call FileChooser dialog...
        chooser = gtk.FileChooserDialog(title='Choose file to open',
                                        action=gtk.FILE_CHOOSER_ACTION_OPEN,
                                        buttons=(gtk.STOCK_CANCEL,
                                                  gtk.RESPONSE_CANCEL,
                                                  gtk.STOCK_OPEN,
                                                  gtk.RESPONSE_OK))
        chooser.set_select_multiple(False)
        chooser.set_default_response(gtk.RESPONSE_OK)
        chooser.set_local_only(False)
        chooser.set_current_folder(self.config['folder-aex'])
        ext_filter = gtk.FileFilter()
        ext_filter.add_pattern('*.aex')
        ext_filter.set_name('aex - Afm Experiment XML')
        chooser.add_filter(ext_filter)
        response = chooser.run()
        # Analyse output:
        if response == gtk.RESPONSE_OK:
            file_item = u'' + chooser.get_filenames()[0]
        elif response == gtk.RESPONSE_CANCEL:
            chooser.destroy()
            return
        else:
            chooser.destroy()
            return
        chooser.destroy()
        experiment = self.__load_exp(file_item)
        return experiment

    def __load_exp(self, file_item):
        experiment = classes.FVFolder()
        _thread = threading.Thread(target=experiment.load_aex,
                                   args=(file_item, ))
        _thread.start()
        self.display_progressbar('pulse')
        while _thread.isAlive():
            self.display_progressbar('pulse', experiment.counter['text'])
            time.sleep(0.1)
        # File was correctly loaded, saving the folder in preferences...
        self.config['folder-aex'] = os.path.split(file_item)[0]
        experiment.counter['num'] = 0  # reset the counter
        self.display_progressbar(None)
        return experiment

    def on_open_experiment_activate(self, arg):
        """
             Load experiment.
        """

        if DEBUG:
            print "Open experiment. Call from :"
            print arg
        if type(arg) in [str, unicode]:
            _experiment = self.__load_exp(arg)
        else:
            _experiment = self.load_exp()
        if _experiment is not None:
            # If smth is loaded, get it into memory. Else, keep the same.
            self.experiment = _experiment
            self.window.set_title(("%s : %s") % (WIN_TITLE,
                                               self.experiment.short_name))
            self.force_volume = self.experiment.get_file(0)
            self.exp_filename = self.experiment.filename
            # update file list comboBox...
            combo_box_set_list(self.widget['combo_list_files'],
                               self.experiment.file['name'])
            self.update('gui')
            self.update('hist entry')
            self.update_switch()
            self.close_all_windows()

    def on_open_exp_compare_activated(self, widget):
        """
            Load experiment for comparison.
        """

        if DEBUG:
            print "Make experiment to compare. Call from :"
            print widget
        self.experiment_compare = self.load_exp()

    def on_save_experiment_as_activate(self, widget):
        '''
        Saves data and results in an aex file.
        '''
        if DEBUG:
            print "Save experiment. Call from :"
            print widget
        chooser = gtk.FileChooserDialog(title='Save file to ...',
                                        action=gtk.FILE_CHOOSER_ACTION_SAVE,
                                        buttons=(gtk.STOCK_CANCEL,
                                                  gtk.RESPONSE_CANCEL,
                                                  gtk.STOCK_SAVE,
                                                  gtk.RESPONSE_OK))
        chooser.set_default_response(gtk.RESPONSE_OK)
        chooser.set_local_only(False)
        chooser.set_current_folder(self.config['folder-aex'])
        response = chooser.run()
        # Analyse output:
        if response == gtk.RESPONSE_OK:
            self.exp_filename = u'' + chooser.get_filenames()[0]
        else:
            chooser.destroy()
            return
        chooser.destroy()
        self.save_exp()

        self.window.set_title(("%s : %s") % (WIN_TITLE,
                                             self.experiment.short_name))

    def on_save_experiment_activate(self, widget):
        """
            Save the experiment.
        """

        if self.exp_filename is not None and os.path.isfile(self.exp_filename):
            self.save_exp()
        else:
            self.on_save_experiment_as_activate(widget)

    def save_exp(self):
        """
            Save the experiment.
        """
        #self.experiment.save(file_item)
        _thread = threading.Thread(target=self.experiment.save,
                                   args=(self.exp_filename, ))
        _thread.start()
        self.display_progressbar('pulse')
        while _thread.isAlive():
            self.display_progressbar('pulse', self.experiment.counter['text'])
            time.sleep(0.1)
        # File was correctly saved. Saving the folder in preferences...
        self.config['folder-aex'] = os.path.split(self.exp_filename)[0]
        self.experiment.counter['num'] = 0
        self.display_progressbar(None)

    def on_menu_export_stiffness_activate(self, widget):
        """
            Export the computed stiffness on a csv file
        """
        if DEBUG:
            print "Export stiffness. Called from :"
            print widget

        chooser = gtk.FileChooserDialog(title='Export stiffness',
                                        action=gtk.FILE_CHOOSER_ACTION_SAVE,
                                        buttons=(gtk.STOCK_CANCEL,
                                                  gtk.RESPONSE_CANCEL,
                                                  gtk.STOCK_SAVE,
                                                  gtk.RESPONSE_OK))
        chooser.set_default_response(gtk.RESPONSE_OK)
        chooser.set_local_only(False)
        chooser.set_current_folder(self.config['folder-export'])
        response = chooser.run()
        if response == gtk.RESPONSE_OK:
            filename = chooser.get_filenames()[0]
            self.experiment.export_stiffness(filename)
            self.config['folder-export'] = os.path.split(filename)[0]
        elif response == gtk.RESPONSE_CANCEL:
            chooser.destroy()
            return
        else:
            chooser.destroy()
            return
        chooser.destroy()

    def on_menu_export_average_stiffness_tomography_activate(self, widget):
        """
            Export the average stiffness on a csv file
        """
        if DEBUG:
            print "Export average stiffness. Called from :"
            print widget
        chooser = gtk.FileChooserDialog(title='Export average stiffness',
                                        action=gtk.FILE_CHOOSER_ACTION_SAVE,
                                        buttons=(gtk.STOCK_CANCEL,
                                                  gtk.RESPONSE_CANCEL,
                                                  gtk.STOCK_SAVE,
                                                  gtk.RESPONSE_OK))
        chooser.set_default_response(gtk.RESPONSE_OK)
        chooser.set_local_only(False)
        chooser.set_current_folder(self.config['folder-export'])
        response = chooser.run()
        if response == gtk.RESPONSE_OK:
            filename = chooser.get_filenames()[0]
            self.experiment.export_average_stiffness_tomo(filename)
            self.config['folder-export'] = os.path.split(filename)[0]
        elif response == gtk.RESPONSE_CANCEL:
            chooser.destroy()
            return
        else:
            chooser.destroy()
            return
        chooser.destroy()

    def on_menu_export_map_activate(self, widget):
        """
            Export the stiffness map of the file as a png file.
        """
        if DEBUG:
            print "Export map. Called from :"
            print widget
        array_dict = {}
        for item in self.force_volume.get_array('keys'):
            array_dict[item] = self.force_volume.get_array(item)
        export = MapDisplay(array_dict,
                            folder=self.config['folder-export'],
                            depth=self.force_volume.stiffness['size_parts'],
                            fvname=self.force_volume.name,
                            scan_size=self.force_volume.header['scan_size'])
        export.run()

    def on_menu_export_curve_activate(self, widget):
        """
            Export the force indentation curve as csv file.
        """
        if DEBUG:
            print "Export curve. Called from :"
            print widget
        chooser = gtk.FileChooserDialog(title='Export FD curve',
                                        action=gtk.FILE_CHOOSER_ACTION_SAVE,
                                        buttons=(gtk.STOCK_CANCEL,
                                                  gtk.RESPONSE_CANCEL,
                                                  gtk.STOCK_SAVE,
                                                  gtk.RESPONSE_OK))
        chooser.set_default_response(gtk.RESPONSE_OK)
        chooser.set_local_only(False)
        chooser.set_current_folder(self.config['folder-export'])
        response = chooser.run()
        if response == gtk.RESPONSE_OK:
            filename = chooser.get_filenames()[0]
            self.force_volume.export_curve(filename, 'both')
            self.config['folder-export'] = os.path.split(filename)[0]
        elif response == gtk.RESPONSE_CANCEL:
            chooser.destroy()
            return
        else:
            chooser.destroy()
            return
        chooser.destroy()

    def on_menu_export_event_prop_activate(self, widget):
        """
            Export the event properties as csv files.
        """

        if DEBUG:
            print "Export evnt. Called from :"
            print widget
        chooser = gtk.FileChooserDialog(title='Export event properties',
                                        action=gtk.FILE_CHOOSER_ACTION_SAVE,
                                        buttons=(gtk.STOCK_CANCEL,
                                                  gtk.RESPONSE_CANCEL,
                                                  gtk.STOCK_SAVE,
                                                  gtk.RESPONSE_OK))
        chooser.set_default_response(gtk.RESPONSE_OK)
        chooser.set_local_only(False)
        chooser.set_current_folder(self.config['folder-export'])
        response = chooser.run()
        if response == gtk.RESPONSE_OK:
            filename = chooser.get_filenames()[0]
            self.experiment.export_events(filename)
            self.config['folder-export'] = os.path.split(filename)[0]
        elif response == gtk.RESPONSE_CANCEL:
            chooser.destroy()
            return
        else:
            chooser.destroy()
            return
        chooser.destroy()

    def on_menu_export_evt_nbr_activate(self, widget):
        """
            Export the number of evt per curves in csv file.
        """

        if DEBUG:
            print "Export evnt. Called from :"
            print widget
        chooser = gtk.FileChooserDialog(title='Export event number per curves',
                                        action=gtk.FILE_CHOOSER_ACTION_SAVE,
                                        buttons=(gtk.STOCK_CANCEL,
                                                  gtk.RESPONSE_CANCEL,
                                                  gtk.STOCK_SAVE,
                                                  gtk.RESPONSE_OK))
        chooser.set_default_response(gtk.RESPONSE_OK)
        chooser.set_local_only(False)
        chooser.set_current_folder(self.config['folder-export'])
        response = chooser.run()
        if response == gtk.RESPONSE_OK:
            filename = chooser.get_filenames()[0]
            self.experiment.export_nbr_events(filename)
            self.config['folder-export'] = os.path.split(filename)[0]
        elif response == gtk.RESPONSE_CANCEL:
            chooser.destroy()
            return
        else:
            chooser.destroy()
            return
        chooser.destroy()

    def on_menu_mask_activate(self, widget):
        """
            Generate masks.
        """

        if DEBUG:
            print "Activate mask. Called from :"
            print widget
        mask_edit = MaskProperties(self.force_volume)
        answer = mask_edit.run()
        del answer
        mask_edit.destroy()
        self.experiment.update_switch()
        self.update('gui')
    ##
    ## Edition Menu

    ########
    ## dialog experiment properties
    def on_exp_prop_activate(self, widget):
        '''
        Create the properties dialog.
        '''
        if DEBUG:
            print "Property dialog. Called from :"
            print widget

        self.properties_dialog = dialog_experiment_properties.main_win(
            author=self.experiment.author,
            comment=self.experiment.comment,
            spring_constant=self.experiment.parameters['spring_constant'],
            inject_index=[self.experiment.parameters['injection_index'],
                          len(self.experiment.file['list'])],
            stiff_glass=self.experiment.parameters['glass'],
            trace_curve=[self.force_volume.trace_x, self.force_volume.trace_y],
            plot_size=self.experiment.parameters['plot_size'],
            group=self.experiment.group,
            linewidth=self.config['linewidth'],
            histo_y_rel=self.experiment.parameters['histo_y_rel'],
            plot_hist_gauss=self.experiment.parameters['plot_hist_gauss'],
            plot_color=self.plot_properties.color_type
            )
        self.properties_dialog.validate = self.properties_dialog_validate

    def properties_dialog_validate(self):
        '''
        The ending function of the experiment properties dialog.
        The changed data are stored in the experiment
        '''
        # We redefine this function to catch the final signal from the window
        self.experiment.author = self.properties_dialog.author
        self.experiment.comment = self.properties_dialog.comment
        self.experiment.parameters['injection_index'] = \
                                    int(self.properties_dialog.inject_index[0])
        self.experiment.set_parameters('spring_constant',
                                        self.properties_dialog.spring_cst)
        self.experiment.set_parameters('glass',
                                        self.properties_dialog.stiff_glass)
        self.experiment.set_parameters('plot_size',
                                        self.properties_dialog.save_size)
        gprop = self.properties_dialog.group
        for key in ['label', 'display']:
            self.experiment.set_group_prop(key, -1, gprop[key])
        self.config['linewidth'] = self.properties_dialog.linewidth
        self.experiment.parameters['histo_y_rel'] = \
                                       self.properties_dialog.histo_y_rel
        self.experiment.set_parameters('plot_hist_gauss',
                                       self.properties_dialog.plot_hist_gauss)
        self.plot_properties.color_type = self.properties_dialog.plot_color
        self.properties_dialog.destroy()
        self.update('plots')
    ##

    ####
    ## Group
    def on_group_activate(self, widget):
        """
            Manage the groups.
        """

        if DEBUG:
            print "Activate group. Called from :"
            print widget
        win = FileGroup(self.experiment.file['name'],
                        self.experiment.file['group'])
        result = win.run()
        if result is not None:
            self.experiment.set_group(result)
        win.destroy()
    ##

    ###############
    ## Option Menu
    def on_apply_on_all_toggled(self, widget):
        """
            Switch the "apply_on_all" state.
        """

        self.apply_on_all = widget.get_active()
        self.experiment.set_switch('all', widget.get_active())
        self.update('gui')

    def on_show_header_activate(self, widget):
        """
            Show the current FV header in a window.
        """

        if DEBUG:
            print "Show header. Called from :"
            print widget
        if self.force_volume is None:
            # Stop execution here if no files loaded
            display_error('no_fv')
            return
        treestore = create_header_tree(self.force_volume.header)
        treeview = gtk.TreeView(treestore)
        tvcolumn = gtk.TreeViewColumn('File Header')
        treeview.append_column(tvcolumn)
        cell = gtk.CellRendererText()
        tvcolumn.pack_start(cell, True)
        tvcolumn.add_attribute(cell, 'text', 0)
        treeview.set_search_column(0)
        tvcolumn.set_sort_column_id(0)
        treeview.set_reorderable(True)

        # Create a new window
        window = gtk.Window(gtk.WINDOW_TOPLEVEL)
        window.set_title('File Header')

        window.set_size_request(200, 200)

        #window.connect("delete_event", self.delete_event)
        window.add(treeview)
        window.show_all()

    def on_flatten_activate(self, widget):
        """
            Open the flatten dialog
        """
        self.flatten_dialog = FlattenDialog(
                                self.force_volume.get_array('Piezo', raw=True))
        new_array = self.flatten_dialog.run()
        self.force_volume.set_array('Piezo', new_array)
        self.flatten_dialog.destroy()

    ###########################################################################
    ## Display tab
    def on_button_up_clicked(self, widget):
        """
            Navigate in the array to the upper FC.
        """
        if self.force_volume is None:
            # Stop execution here if no files loaded
            display_error('no_fv')
            return
        self.update_stiffness_param(self.force_volume, widget)
        self.force_volume.go_up()
        self.plot_curve()
        self.update('show gride position')

    def on_button_down_clicked(self, widget):
        """
            Navigat in the array to the down FC.
        """

        if self.force_volume is None:
            # Stop execution here if no files loaded
            display_error('no_fv')
            return
        self.update_stiffness_param(self.force_volume, widget)
        self.force_volume.go_down()
        self.plot_curve()
        self.update('show gride position')

    def on_button_next_clicked(self, widget):
        """
            Navigate in the array to the next FC.
        """
        if self.force_volume is None:
            # Stop execution here if no files loaded
            display_error('no_fv')
            return
        self.update_stiffness_param(self.force_volume, widget)
        try:
            self.force_volume.go_next()
        except StopIteration:
            pass
        self.plot_curve()
        self.update('show gride position')

    def on_button_prev_clicked(self, widget):
        """
            Navigate in the array to the previous FC.
        """
        if self.force_volume is None:
            # Stop execution here if no files loaded
            display_error('no_fv')
            return
        self.update_stiffness_param(self.force_volume, widget)
        self.force_volume.go_prev()
        self.plot_curve()
        self.update('show gride position')

    def on_list_file_combo_box_changed(self, signal):
        """
            Change the current FV scan according to the list change.
        """

        self.force_volume = self.experiment.get_file(signal.get_active())
        self.update('fv display prop')
        self.update('gui')

    def on_check_display_fc_trace_clicked(self, widget):
        """
            Switch the display of approach curve.
        """
        if self.force_volume is None:
            # Stop execution here if no files loaded
            display_error('no_fv')
            return
        if widget.get_active():
            self.force_volume.set_to_plot('Av', 1)
        else:
            self.force_volume.set_to_plot('Av', 0)
            if not self.force_volume.to_plot('Re'):
                self.win_plot['Curve Plot'].on_win_plot_destroy()
        self.plot_curve()

    def on_check_display_fc_retrace_clicked(self, widget):
        """
            Switch the display of retraction curve.
        """
        if self.force_volume is None:
            # Stop execution here if no files loaded
            display_error('no_fv')
            return
        if widget.get_active():
            self.force_volume.set_to_plot('Re', 1)
        else:
            self.force_volume.set_to_plot('Re', 0)
            if not self.force_volume.to_plot('Av'):
                self.win_plot['Curve Plot'].on_win_plot_destroy()
        self.plot_curve()

    def on_check_display_poc_clicked(self, widget):
        """
            Switch the display of point of contact on approach curve.
        """
        if self.force_volume is None:
            # Stop execution here if no files loaded
            display_error('no_fv')
            return
        if widget.get_active():
            self.force_volume.set_to_plot('PoC', 1)
        else:
            self.force_volume.set_to_plot('PoC', 0)
        self.plot_curve()

    def on_check_display_event_clicked(self, widget):
        """
            Switch the display of event on retraction curve.
        """
        if self.force_volume is None:
            # Stop execution here if no files loaded
            display_error('no_fv')
            return
        if widget.get_active():
            self.force_volume.set_to_plot('Event', 1)
        else:
            self.force_volume.set_to_plot('Event', 0)
        self.plot_curve()

    def on_check_display_indent_clicked(self, widget):
        """
            Switch the display of indentation curve.
        """
        if self.force_volume is None:
            # Stop execution here if no files loaded
            display_error('no_fv')
            return
        if widget.get_active():
            self.force_volume.set_to_plot('Indent', 1)
        else:
            self.force_volume.set_to_plot('Indent', 0)
            self.win_plot['Indent Plot'].on_win_plot_destroy()
        self.plot_curve()

    def on_button_piezzo_array_clicked(self, widget):
        """
            Display the piezo array.
        """
        if self.force_volume is None:
            display_error('no_fv')
            return
        try:
            array = self.force_volume.get_array('Piezo')
        except NotReadyError:
            answer = ask_message(['Piezo array not loaded',
                                  'Piezo array is not loaded.\n' +
                                  'Do you want to load it ?\n\n' +
                                  'This can take minutes...\n\n' +
                                  'Note that if you compute the stiffness\n' +
                                  'Piezo array is automagically loaded.'])
            if answer == gtk.RESPONSE_YES:
                self.force_volume.force_piezo_loading()
                array = self.force_volume.get_array('Piezo')
            elif answer == gtk.RESPONSE_NO:
                return
        if not exist(self.win_plot['Piezo Height']):
            [window, widget] = self.regenerate_plot()
            self.win_plot['Piezo Height'] = plot_gtk.plotGride(
                                    window, widget,
                                    title=PLOT_WINDOW['piezo array'][2],
                                    win_title=PLOT_WINDOW['piezo array'][1])
            self.win_plot['Piezo Height'].figure.canvas.mpl_connect(
                                                        'button_press_event',
                                                        self.on_gride_click)
            self.win_plot['Piezo Height'].figure.canvas.mpl_connect(
                                                        'motion_notify_event',
                                                        self.on_gride_motion)
        self.win_plot['Piezo Height'].set_size(
                                    self.experiment.parameters['plot_size'])
        self.win_plot['Piezo Height'].plot(
                                    array,
                                    self.force_volume.header['scan_size'],
                                    cmap=self.plot_properties.get_cmap('Piezo'))

    def on_button_topography_array_clicked(self, widget):
        """
            Display the topography (zero-force) array.
        """
        if self.force_volume is None:
            display_error('no_fv')
            return
        if self.force_volume.get_array('Topography') is None:
            display_error('no_compute', 'topography')
            return
        if not exist(self.win_plot['Topography Map']):
            [window, widget] = self.regenerate_plot()
            self.win_plot['Topography Map'] = plot_gtk.plotGride(
                                window, widget,
                                title=PLOT_WINDOW['topography array'][2],
                                win_title=PLOT_WINDOW['topography array'][1])
            self.win_plot['Topography Map'].figure.canvas.mpl_connect(
                                                        'button_press_event',
                                                        self.on_gride_click)
            self.win_plot['Topography Map'].figure.canvas.mpl_connect(
                                                        'motion_notify_event',
                                                        self.on_gride_motion)
        self.win_plot['Topography Map'].set_size(
                                    self.experiment.parameters['plot_size'])
        self.win_plot['Topography Map'].plot(
                                self.force_volume.get_array('Topography'),
                                self.force_volume.header['scan_size'],
                                cmap=self.plot_properties.get_cmap('Topography'))

    def on_button_stiffness_array_clicked(self, widget):
        """
            Display the stiffness array.
        """
        if self.force_volume is None:
            display_error('no_fv')
            return
        if not self.force_volume.has_stiffness:
            display_error('no_compute', 'stiffness')
            return
        if not exist(self.win_plot['Stiffness Map']):
            [window, widget] = self.regenerate_plot()
            self.win_plot['Stiffness Map'] = plot_gtk.plotGride(
                                window, widget,
                                title=PLOT_WINDOW['stiffness array'][2],
                                win_title=PLOT_WINDOW['stiffness array'][1])
            self.win_plot['Stiffness Map'].figure.canvas.mpl_connect(
                                                        'button_press_event',
                                                        self.on_gride_click)
            self.win_plot['Stiffness Map'].figure.canvas.mpl_connect(
                                                        'motion_notify_event',
                                                        self.on_gride_motion)
        self.win_plot['Stiffness Map'].set_size(
                                    self.experiment.parameters['plot_size'])
        self.win_plot['Stiffness Map'].plot(
                                self.force_volume.get_array('Stiffness'),
                                self.force_volume.header['scan_size'],
                                cmap=self.plot_properties.get_cmap('Stiffness'))

    def on_button_event_array_clicked(self, widget):
        """
            Display the event number array.
        """
        if self.force_volume is None:
            display_error('no_fv')
            return
        if self.force_volume.get_array('Event') is None:
            display_error('no_compute', 'event')
            return
        if not exist(self.win_plot['Event Map']):
            [window, widget] = self.regenerate_plot()
            self.win_plot['Event Map'] = plot_gtk.plotGride(
                                    window, widget,
                                    title=PLOT_WINDOW['ev_pos array'][2],
                                    win_title=PLOT_WINDOW['ev_pos array'][1])
            self.win_plot['Event Map'].figure.canvas.mpl_connect(
                                                        'button_press_event',
                                                        self.on_gride_click)
        self.win_plot['Event Map'].set_size(
                                    self.experiment.parameters['plot_size'])
        self.win_plot['Event Map'].plot(
                                        self.force_volume.get_array('Event'),
                                        self.force_volume.header['scan_size'],
                                    cmap=self.plot_properties.get_cmap('Event'))

    def on_button_event_force_array_clicked(self, widget):
        """
            Display the event force array.
        """
        if self.force_volume is None:
            display_error('no_fv')
            return
        if self.force_volume.get_array('Event force') is None:
            display_error('no_compute', 'event')
            return
        if not exist(self.win_plot['Event Force Map']):
            [window, widget] = self.regenerate_plot()
            self.win_plot['Event Force Map'] = plot_gtk.plotGride(
                                    window, widget,
                                    title=PLOT_WINDOW['ev_force array'][2],
                                    win_title=PLOT_WINDOW['ev_force array'][1])
            self.win_plot['Event Force Map'].figure.canvas.mpl_connect(
                                                        'button_press_event',
                                                        self.on_gride_click)
        self.win_plot['Event Force Map'].set_size(
                                    self.experiment.parameters['plot_size'])
        self.win_plot['Event Force Map'].plot(
                            self.force_volume.get_array('Event force'),
                            self.force_volume.header['scan_size'],
                            cmap=self.plot_properties.get_cmap('Event force'))

    def on_button_event_dist_array_clicked(self, widget):
        """
            Display the event force array.
        """
        if self.force_volume is None:
            display_error('no_fv')
            return
        if self.force_volume.get_array('Event distance') is None:
            display_error('no_compute', 'event distance')
            return
        if not exist(self.win_plot['Event Distance Map']):
            [window, widget] = self.regenerate_plot()
            self.win_plot['Event Distance Map'] = plot_gtk.plotGride(
                                    window, widget,
                                    title=PLOT_WINDOW['ev_dist array'][2],
                                    win_title=PLOT_WINDOW['ev_dist array'][1])
            self.win_plot['Event Distance Map'].figure.canvas.mpl_connect(
                                                        'button_press_event',
                                                        self.on_gride_click)
        self.win_plot['Event Distance Map'].set_size(
                                    self.experiment.parameters['plot_size'])
        self.win_plot['Event Distance Map'].plot(
                            self.force_volume.get_array('Event distance'),
                            self.force_volume.header['scan_size'],
                        cmap=self.plot_properties.get_cmap('Event distance'))

    def on_button_event_length_array_clicked(self, widget):
        """
            Display the event number array.
        """
        if self.force_volume is None:
            display_error('no_fv')
            return
        if self.force_volume.get_array('Event length') is None:
            display_error('no_compute', 'event length')
            return
        if not exist(self.win_plot['Event Length Map']):
            [window, widget] = self.regenerate_plot()
            self.win_plot['Event Length Map'] = plot_gtk.plotGride(
                                    window, widget,
                                    title=PLOT_WINDOW['ev_len array'][2],
                                    win_title=PLOT_WINDOW['ev_len array'][1])
            self.win_plot['Event Length Map'].figure.canvas.mpl_connect(
                                                        'button_press_event',
                                                        self.on_gride_click)
        self.win_plot['Event Length Map'].set_size(
                                    self.experiment.parameters['plot_size'])
        self.win_plot['Event Length Map'].plot(
                            self.force_volume.get_array('Event length'),
                            self.force_volume.header['scan_size'],
                            cmap=self.plot_properties.get_cmap('Event length'))

    def on_button_stiffness_tomo_clicked(self, widget):
        """
            Dispaly the stiffness tomography slices in x-y-z.
        """
        if self.force_volume is None:
            display_error('no_fv')
            return
        if not self.force_volume.has_stiffness:
            display_error('no_compute', 'stiffness')
            return
        self.force_volume.stiffness_tomography_array()
        if not exist(self.win_plot['Stiffness Tomo']):
            [window, widget] = self.regenerate_plot('tomography')
            self.win_plot['Stiffness Tomo'] = plot_gtk.plotTomo(
                            window, widget,
                            self.force_volume.get_array('Stiffness tomo'),
                            scan_size=self.force_volume.header['scan_size'],
                            title='Stiffness Tomo')
        self.win_plot['Stiffness Tomo'].set_size(
                                    self.experiment.parameters['plot_size'])
        self.win_plot['Stiffness Tomo'].get_pos = \
                                    self.on_stiffness_tomo_window_clicked

    def on_stiffness_tomo_window_clicked(self):
        """
            When clicking on the stiffness tomography window, it goes to the
            corresponding force-distance curve.
        """
        if self.force_volume is None:
            display_error('no_fv')
            return
        self.force_volume.go_to(self.win_plot['Stiffness Tomo'].slice[0],
                                self.win_plot['Stiffness Tomo'].slice[1])
        self.update('show gride position')
        self.plot_curve()

    def on_button_mean_stiffness_tomo_clicked(self, widget):
        """
            Distplay the mean stiffness tomography.
        """
        if self.force_volume is None:
            display_error('no_fv')
            return
        if not self.force_volume.has_stiffness:
            display_error('no_compute', 'stiffness')
            return
        if not exist(self.win_plot['Average Stiffness Tomo']):
            [window, widget] = self.regenerate_plot()
            self.win_plot['Average Stiffness Tomo'] = plot_gtk.errorBar(
                                                window, widget)

        if self.apply_on_all:
            mean_tomo = self.experiment.average_stiffness
            self.win_plot['Average Stiffness Tomo'].average_stiffness(
                                        mean_tomo,
                                        group=self.experiment.file['group'],
                                        group_info=self.experiment.group)
        else:
            mean_tomo = self.force_volume.compute_average_stiffness()
            self.win_plot['Average Stiffness Tomo'].average_stiffness(
                                            mean_tomo)
        self.win_plot['Average Stiffness Tomo'].set_size(
                                    self.experiment.parameters['plot_size'])

    ## Mosaics
    def mosaic_clicked(self, atype, widget=None):
        """
            Generic function to display mosaic view.
        """
        if self.force_volume is None:
            display_error('no_fv')
            return
        if DEBUG:
            print "Mosaic plot. Called from :"
            print widget
        array_list = self.experiment.get_arrays(atype)
        mask_list = self.experiment.get_masked_list()
        # MOSAIC_PLOT is a dict which contains the values for mosaic name :
        # mosaic_name[0] = name of the window plot
        # mosaic_name[1] = title of the window
        # mosaic_name[2] = title of the plot
        # mosaic_name[3] = cmap
        mosaic_name = MOSAIC_PLOT[atype]
        if not exist(self.win_plot[mosaic_name[0]]):
            [window, widget] = self.regenerate_plot()
            self.win_plot[mosaic_name[0]] = plot_gtk.plotMosaic(
                                                        window, widget,
                                                        title=mosaic_name[1])
            self.win_plot[mosaic_name[0]].figure.canvas.mpl_connect(
                                                        'button_press_event',
                                                        self.on_mosaic_click)
            self.win_plot[mosaic_name[0]].figure.canvas.mpl_connect(
                                                        'pick_event',
                                                        self.on_mosaic_pick)
        self.win_plot[mosaic_name[0]].set_size(
                                    self.experiment.parameters['plot_size'])
        self.win_plot[mosaic_name[0]].plot(array_list,
                                        title=mosaic_name[2],
                                        list_name=self.experiment.file['name'],
                                        cmap=mosaic_name[3],
                                        list_mask=mask_list)

    def on_button_mosaic_piezo_clicked(self, widget):
        """
            Display the mosaic view of piezo image.
        """
        self.mosaic_clicked('Piezo', widget)

    def on_button_mosaic_topography_clicked(self, widget):
        """
            Display the mosaic view of topography.
        """
        self.mosaic_clicked('Topography', widget)

    def on_button_mosaic_stiffness_clicked(self, widget):
        """
            Display the mosaic view of stiffness.
        """
        self.mosaic_clicked('Stiffness', widget)

    def on_button_mosaic_event_clicked(self, widget):
        """
            Display the mosaic view of event nbr.
        """
        self.mosaic_clicked('Event', widget)

    def on_button_mosaic_event_force_clicked(self, widget):
        """
            Display the mosaic view of event force.
        """
        self.mosaic_clicked('Event force', widget)

    def on_button_opengl_clicked(self, widget):
        """
            Display the stiffness tomography using OpenGL.
        """
        if DEBUG:
            print "OpenGL. Called from :"
            print widget
        self.force_volume.stiffness_tomography_array()
        # get the min and max values from the tomography display :
        if exist(self.win_plot['Stiffness Tomo']):
            clut = (self.win_plot['Stiffness Tomo'].colorScaleMin,
                    self.win_plot['Stiffness Tomo'].colorScaleMax)
        else:
            clut = None
        topogl = TopoGL(self.force_volume.get_array('Stiffness tomo'),
                        clut=clut)
        topogl.run()

    ###########################################################################
    ## Compute tab
    def update_stiffness_param(self, force_volume, widget=None):
        """
            Update the stiffness parameters.
        """
        if DEBUG:
            print "Updating stiffness parameters. Called by :"
            print widget
        # Update the Hertz model and the tip caracteristics.
        _hertz_model = self.widget['combo_hertz_model'].get_active_text()
        _stiff_method = self.widget['combo_stiff_method'].get_active_text()
        _tip_carac = self.widget['spin_tip_carac'].get_value()
        force_volume.change_model(model=_hertz_model, carac=_tip_carac)
        force_volume.set_parameters('stiffness_fit_method', _stiff_method)
        # Update the segmentations parameters.
        force_volume.change_segment_number(
                            self.widget['entry_number_deep'].get_text())
        force_volume.change_segment_depth(
                            self.widget['entry_size_deep'].get_text())
        # Update the point of contact detection algorythm.
        _poc_model = self.widget['combo_poc_method'].get_active_text()
        force_volume.stiffness['poc_method'] = _poc_model
        _limit_poc_slide = self.widget['check_limit_poc_slide'].get_active()
        force_volume.stiffness['limit_slide'] = _limit_poc_slide
        _fit_seg_size = self.widget['spin_fit_seg_size'].get_value()
        force_volume.change_poc_seg_size(_fit_seg_size)
        # Update whether to recompute or not.
        _recompute_poc = self.widget['check_compute_poc'].get_active()
        force_volume.stiffness['recompute_poc'] = _recompute_poc

    def on_button_compute_stiffness_clicked(self, widget):
        """
            Launch the stiffness computation.
        """
        if DEBUG:
            print "Compute stiffness. Called from :"
            print widget
        if self.apply_on_all:
            number_of_files = len(self.experiment.file['list'])
            this_file_nb = 0
            for force_volume, file_name in zip(self.experiment.file['list'],
                                              self.experiment.file['name']):
                counter_begin = float(this_file_nb) / number_of_files
                counter_end = float(this_file_nb + 1) / number_of_files
                counter_text = " of file " + file_name
                self._compute_stiffness(force_volume,
                                        counter_begin,
                                        counter_end,
                                        counter_text)
                this_file_nb += 1
        else:
            self._compute_stiffness(self.force_volume)
        # updates the histogram tabs according to the latest computations.
        self.update('hist entry')
        self.display_progressbar(None)

    def on_check_compute_poc_toggled(self, widget):
        """
            Change the state of forcing the detection of point of contact.
        """
        if self.force_volume is not None:
            self.update_stiffness_param(self.force_volume, widget)

    def _compute_stiffness(self, force_volume, counter_begin=0,
                           counter_end=1, counter_text=''):
        """
            Compute the stiffness. This is for one file.
        """
        # Update the computation properties
        self.update_stiffness_param(force_volume)
        # Start the computation
        _thread = threading.Thread(target=force_volume.compute_stiffness)
        _thread.start()
        while _thread.isAlive():
            self.display_progressbar(counter_begin +
                                float(force_volume.counter['num']) *
                                (counter_end - counter_begin),
                                force_volume.counter['text'] + counter_text)
            time.sleep(0.1)
        force_volume.counter['num'] = 0

    def update_evt_parameters(self, force_volume):
        evt_fit_model = self.widget['combo_ev_fit_model'].get_active_text()
        if evt_fit_model == 'None':
            evt_fit_model = None
        force_volume.set_parameters('evt_fit', evt_fit_model)
        force_volume.header['event_detect_weight'] = \
                        self.experiment.parameters['event_detect_weight']

    def on_button_detect_events_clicked(self, widget):
        """
            Launch the event detection.
        """
        if DEBUG:
            print "Detect events. Called from :"
            print widget
        if self.apply_on_all:
            number_of_files = len(self.experiment.file['list'])
            this_file_nb = 0
            for force_volume, file_name in zip(self.experiment.file['list'],
                                              self.experiment.file['name']):
                counter_begin = float(this_file_nb) / number_of_files
                counter_end = float(this_file_nb + 1) / number_of_files
                counter_text = " of file " + file_name
                self._compute_evt(force_volume,
                                        counter_begin,
                                        counter_end,
                                        counter_text)
                this_file_nb += 1
        else:
            self._compute_evt(self.force_volume)
        self.update('hist entry')
        self.display_progressbar(None)

    def _compute_evt(self, force_volume, counter_begin=0,
                           counter_end=1, counter_text=''):
        """
            Compute the stiffness. This is for one file.
        """
        # Update the computation properties
        self.update_evt_parameters(force_volume)
        # Start the computation
        _thread = threading.Thread(target=force_volume.compute_events)
        _thread.start()
        while _thread.isAlive():
            self.display_progressbar(counter_begin +
                                float(force_volume.counter['num']) *
                                (counter_end - counter_begin),
                                force_volume.counter['text'] + counter_text)
            time.sleep(0.1)
        force_volume.counter['num'] = 0

    def on_combo_ev_fit_model_changed(self, widget):
        if self.force_volume is not None:
            self.experiment.parameters['event_fit_model'] = \
                            self.widget['combo_ev_fit_model'].get_active_text()
            self.plot_curve()

    def on_button_regenerate_random_clicked(self, widget):
        """
            Regenerate the random event array.
        """
        if DEBUG:
            print "Regenerate random. Called from :"
            print widget
        nbr_rand_event = self.widget['spin_nbr_rand_event'].get_value()
        if self.apply_on_all:
            for force_volume in self.experiment.file['list']:
                force_volume.generate_random_event(nbr_rand_event)
            else:
                self.force_volume.generate_random_event(nbr_rand_event)

    def on_button_compute_rel_stiffness_clicked(self, widget):
        """
            Compute the relative stiffness
        """
        if DEBUG:
            print "Compute rel stiff. Called from :"
            print widget
        self.experiment.parameters['rel_stiff_dist'] = \
                        int(self.widget['spin_max_rel_stiffness'].get_value())
        if self.apply_on_all:
            self.experiment.compute_relative_stiffness()
        else:
            self.force_volume.change_relative_distance(
                            int(self.experiment.parameters['rel_stiff_dist']))
            self.force_volume.compute_relative_stiffness()

    ###########################################################################
    ## Parameter tab
    def on_spin_poisson_ratio_value_changed(self, widget):
        """
            Change the poisson ratio of the material.
        """
        if self.experiment is not None:
            self.experiment.set_parameters('poisson_ratio', widget.get_value())

    def on_button_event_calib_clicked(self, widget):
        """
            Modify the calibration of the event detection.
        """

        if DEBUG:
            print "Calib event. Called from :"
            print widget
        # Turn on the "Retraction force curve" and "Event on curve display"
        self.widget['check_display_fc_retrace'].set_active(True)
        self.force_volume.set_to_plot('Re', 1)
        self.widget['check_display_event'].set_active(True)
        self.force_volume.set_to_plot('Event', 1)
        self.modify_window('Curve Plot', 'Event detection')
        self.plot_curve()

    def on_spin_ev_threshold_dist_value_changed(self, widget):
        """
            Change the event filter based on its distance on the curve.
        """
        self.experiment.set_switch('ev_dist', widget.get_value())
        adjustment = widget.get_adjustment()
        adjustment.set_upper(widget.get_value() + 1000)
        self.update_switch()

    def on_spin_ev_length_thresh_min_value_changed(self, widget):
        """
            Change the event filter based on its minimum length computed from
            the fit.
        """
        self.update_ev_length_thresh()
        adjustment = widget.get_adjustment()
        adjustment.set_upper(widget.get_value() + 1000)

    def on_spin_ev_length_thresh_max_value_changed(self, widget):
        """
            Change the event filter based on its maximum length computed from
            the fit.
        """
        self.update_ev_length_thresh()
        adjustment = widget.get_adjustment()
        adjustment.set_upper(widget.get_value() + 1000)

    def update_ev_length_thresh(self):
        """
            Update the threshold fit based on the length computed from the fit.
        """
        min_length = self.widget['spin_ev_length_thresh_min'].get_value()
        max_length = self.widget['spin_ev_length_thresh_max'].get_value()
        self.experiment.set_switch('ev_fit_length', [min_length, max_length])

    def on_button_poc_calib_clicked(self, widget):
        """
            Change the calibration of the point of contact detection
        """
        if DEBUG:
            print "Calib poc. Called from :"
            print widget
        # Turn on the "Approach force curve" and "PoC on curve display"
        self.widget['check_display_fc_trace'].set_active(True)
        self.force_volume.set_to_plot('Av', 1)
        self.widget['check_display_poc'].set_active(True)
        self.force_volume.set_to_plot('PoC', 1)
        self.modify_window('Curve Plot', 'PoC detection')
        self.plot_curve()

    def on_combo_poc_method_changed(self, widget):
        """
            Change the method of the point of contact detection
        """

        if DEBUG:
            print "Change poc method. Called from :"
            print widget
        if self.force_volume is not None:
            self.update_stiffness_param(self.force_volume, widget)

    def on_check_limit_poc_slide_toggled(self, widget):
        """
            Limits the sliding of point of contact detection.
        """

        if DEBUG:
            print "Check limit poc. Called from :"
            print widget
        if self.force_volume is not None:
            self.update_stiffness_param(self.force_volume, widget)

    def on_spin_fit_seg_size_value_changed(self, widget):
        """
            Change the segment size of indentation curve
        """
        if DEBUG:
            print "Spin fit seg. Called from :"
            print widget
        if self.force_volume is not None:
            self.update_stiffness_param(self.force_volume)
        self.plot_curve()
    ##

    #############################################################
    ## Histograms tab
    def disp_hist(self, data_type, widget=None):
        """
            This is the generic function called to plot the histogram.

            data_type is a string that describes the data you want to plot.

            Can be :

              * 'force' to plot the force.
              * 'lr' to plot the loading rate
              * 'dist' to plot the distance of the unbinding
              * 'stiff' to plot the stiffness on pixel where event occured
              * 'relstiff' to plot the relative stiffness of pixels where event
                occured
        """
        if DEBUG:
            print "Disp histogram. Called by :"
            print widget
        if self.force_volume is None:
            return
        if data_type not in PLOT_LABEL.keys():
            raise TypeError
        if widget in [self.widget['spin_ev_force_hist_max'],
                      self.widget['spin_ev_lr_hist_max'],
                      self.widget['spin_ev_length_hist_max'],
                      self.widget['spin_ev_plength_hist_max'],
                      self.widget['spin_ev_stiff_hist_max'],
                      self.widget['spin_ev_relstiff_hist_max']]:
            curr_val = widget.get_value()
            widget.set_range(0, curr_val * 10)
        # Set the label of the plot
        [title, xlabel, window_name] = PLOT_LABEL[data_type]
        # Set the range and type of the plot
        # data_hist is a PlotData object that stores the plot properties
        self.experiment.data_hist.set_range(data_type,
                                            self.get_histo_range(data_type))
        self.experiment.data_hist.set_type(data_type)
        # We have a special case for the stiffness where we can compare event
        # versus non event. We check thank's to a checkbox :
        stiff_case = self.widget['check_stiff_ev_vs_notev'].get_active() and \
                     (data_type == 'stiff')
        if self.apply_on_all or self.plot_properties.plot_compare:
            # When we compare different experiments, we take the whole files
            if stiff_case:
                [event_att, event_group, fid, coord] = \
                                self.experiment.get_event_att('stiff_ev')
                [noevent_att, noevent_group, noevent_fid, noevent_coord] = \
                                self.experiment.get_event_att('stiff_notev')
            else:
                [event_att, event_group, fid, coord] = \
                                    self.experiment.get_event_att(data_type)
                self.experiment.data_hist.set_data(data_type, event_att)
                self.experiment.data_hist.set_group(data_type, event_group)
                self.experiment.data_hist.nbr_curves = \
                                    self.experiment.parameters['number_curves']
                if self.plot_properties.plot_compare:
                    self.experiment_compare.data_hist.set_range(data_type,
                                            self.get_histo_range(data_type))
                    self.experiment_compare.data_hist.nbr_curves = \
                            self.experiment_compare.parameters['number_curves']
                    self.experiment_compare.data_hist.set_type(data_type)
                    self.experiment_compare.data_hist.set_data(data_type,
                            self.experiment_compare.get_event_att(data_type))
                    self.experiment_compare.event_gauss_fit(data_type)
        else:
            if stiff_case:
                event_att = self.force_volume.get_event_prop('stiff_ev')
                noevent_att = self.force_volume.get_event_prop('stiff_notev')
            else:
                # Reinitialize the group, because the number of event can
                # change.
                self.experiment.data_hist.set_group(data_type, None)
                try:
                    data = self.force_volume.get_event_prop(data_type)
                except TypeError:
                    display_error('no_compute', data_type)
                    return
                self.experiment.data_hist.set_data(data_type, data)
                self.experiment.data_hist.nbr_curves = \
                                    self.force_volume.header['number_curves']
                #self.experiment.event_gauss_fit(data_type)
                #ev_prop = self.experiment.event_stat[data_type]
        ### Set the groups.
        if self.apply_on_all and not self.plot_properties.plot_compare:
            # We put the group properties to the histogram.
            self.experiment.data_hist.set_group_prop('all',
                                                     self.experiment.group)
        if not exist(self.win_plot[window_name]):
            [window, widget] = self.regenerate_plot()
            self.win_plot[window_name] = plot_gtk.plotHist(
                            window, widget,
                            title=title,
                            xlabel=xlabel,
                            y_rel=self.experiment.parameters['histo_y_rel'])
        if not self.plot_properties.plot_compare:
            if stiff_case:
                self.win_plot[window_name].hist(event_att, noevent_att,
                                label='event', label2='not event',
                                hist_range=self.get_histo_range(data_type),
                                y2=True)
            else:
                try:
                    self.win_plot[window_name].hist(self.experiment.data_hist)
                except IndexError:
                    pass
                else:
                    if self.experiment.parameters['plot_hist_gauss']:
                        self.experiment.event_gauss_fit(data_type)
                        self.win_plot[window_name].gauss(
                                        self.experiment.event_stat[data_type])
        else:
            # we compare two experiments :
            self.win_plot[window_name].hist(
                                self.experiment.data_hist,
                                self.experiment_compare.data_hist,
                                label=self.plot_properties.main_exp_label,
                                label2=self.plot_properties.comp_exp_label,
                                color_type=self.plot_properties.color_type)
        # Set the size of the window...
        self.win_plot[window_name].set_size(
                                self.experiment.parameters['plot_size'])

    def on_button_ev_force_hist_clicked(self, widget):
        """
            Display the histogram of force.
        """
        self.disp_hist('force', widget)

    def on_button_ev_lr_hist_clicked(self, widget):
        """
            Display the histogram of loading rate.
        """
        self.disp_hist('lr', widget)

    def on_button_ev_dist_hist_clicked(self, widget=None):
        """
            Display the histogram of distance.
        """
        self.disp_hist('dist', widget)

    def on_button_ev_stiff_hist_clicked(self, widget):
        """
            Display the histogram of stiffness.
        """
        self.disp_hist('stiff', widget)

    def on_button_ev_relstiff_hist_clicked(self, widget):
        """
            Display the histogram of relative stiffness.
        """
        self.disp_hist('relstiff', widget)

    def on_button_ev_length_hist_clicked(self, widget):
        """
            Display the histogram of relative stiffness.
        """
        self.disp_hist('length', widget)

    def on_button_ev_plength_hist_clicked(self, widget):
        """
            Display the histogram of relative stiffness.
        """
        self.disp_hist('plength', widget)

    def disp_scatter(self, data_type, widget=None):
        """
            The generic function to display the scatter plot.
        """
        if DEBUG:
            print "Disp scatter. Called by :"
            print widget
        self.experiment.data_hist.set_range(data_type[0],
                                            self.get_histo_range(data_type[0]))
        self.experiment.data_hist.set_range(data_type[1],
                                            self.get_histo_range(data_type[1]))
        if self.apply_on_all:
            [data, group, fid, coord] = \
                                    self.experiment.get_event_att(data_type[0])
            del fid, coord
            self.experiment.data_hist.set_data(data_type[0], data)
            self.experiment.data_hist.set_group(data_type[0], group)
            [data, group, fid, coord] = \
                                    self.experiment.get_event_att(data_type[1])
            self.experiment.data_hist.set_data(data_type[1], data)
            self.experiment.data_hist.set_group(data_type[1], group)
            self.experiment.data_hist.nbr_curves = \
                                    self.experiment.parameters['number_curves']
            # We put the group properties to the histogram.
            self.experiment.data_hist.set_group_prop('all',
                                                    self.experiment.group)
        else:
            self.experiment.data_hist.set_group(data_type[0], None)
            self.experiment.data_hist.set_group(data_type[1], None)
            self.experiment.data_hist.set_data(data_type[0],
                                self.force_volume.get_event_prop(data_type[0]))
            self.experiment.data_hist.set_data(data_type[1],
                                self.force_volume.get_event_prop(data_type[1]))
            self.experiment.data_hist.nbr_curves = \
                                self.force_volume.header['number_curves']
        # The labels
        label_x = PLOT_LABEL[data_type[0]]
        label_y = PLOT_LABEL[data_type[1]]
        [plot_name, plot_title] = PLOT_WINDOW[data_type[0] + data_type[1]]
        if not exist(self.win_plot[plot_name]):
            [window, widget] = self.regenerate_plot()
            self.win_plot[plot_name] = plot_gtk.plotScatter(
                                                window, widget,
                                                title=plot_title,
                                                xlabel=label_x[1],
                                                ylabel=label_y[1])
            #if data_type[0] == 'lr':
            #    self.win_plot[plot_name].set_xscale('log')
        self.win_plot[plot_name].display(self.experiment.data_hist, data_type)
        self.win_plot[plot_name].set_size(
                                    self.experiment.parameters['plot_size'])

    def on_button_scatter_Force_LR_clicked(self, widget):
        """
            Display the scatter plot of force vs loading rate.
        """
        self.disp_scatter(['force', 'lr'], widget)

    def on_button_scatter_force_dist_clicked(self, widget):
        """
            Display the scatter plot of force vs distance.
        """
        self.disp_scatter(['dist', 'force'], widget)

    def on_button_scatter_force_stiffness_clicked(self, widget):
        """
            Display the scatter plot of force vs stiffness.
        """
        self.disp_scatter(['force', 'stiff'], widget)

    def on_button_scatter_LR_Dist_clicked(self, widget):
        """
            Display the scatter plot of loading rate vs distance.
        """
        self.disp_scatter(['dist', 'lr'], widget)

    def on_button_scatter_LR_Stiffness_clicked(self, widget):
        """
            Display the scatter plot of loading rate vs stiffness.
        """
        self.disp_scatter(['lr', 'stiff'], widget)

    def on_button_scatter_Dist_Stiffness_clicked(self, widget):
        """
            Display the scatter plot of distance vs stiffness.
        """
        self.disp_scatter(['dist', 'stiff'], widget)

    def on_button_scatter_Force_RelStiff_clicked(self, widget):
        """
            Display the scatter plot of force vs relative stiffness.
        """
        self.disp_scatter(['relstiff', 'force'], widget)

    def on_button_scatter_LR_RelStiff_clicked(self, widget):
        """
            Display the scatter plot of loading rate vs relative stiffness.
        """
        self.disp_scatter(['relstiff', 'lr'], widget)

    def on_button_scatter_Dist_RelStiff_clicked(self, widget):
        """
            Display the scatter plot of distance vs rel.stiffness.
        """
        self.disp_scatter(['relstiff', 'dist'], widget)

    def get_histo_range(self, d_type):
        """
            Get the histogram range of the specified type.
        """
        if d_type in ['force', 'lr', 'dist', 'stiff', 'relstiff',
                      'length', 'plength']:
            base_name = d_type
        elif d_type in ['force_base']:
            base_name = d_type[:-5]
        min_val = self.widget[
                            'spin_ev_' + base_name + '_hist_min'].get_value()
        max_val = self.widget[
                            'spin_ev_' + base_name + '_hist_max'].get_value()
        col_size = self.widget[
                            'spin_ev_' + base_name + '_hist_size'].get_value()
        if d_type is 'relstiff':
            dist = int(self.widget['spin_ev_relstiff_hist_dist'].get_value())
            return [min_val, max_val, col_size, dist]
        return [min_val, max_val, col_size]

    def on_button_regenerate_event_properties_clicked(self, widget):
        """
            Regenerate the event properties.
        """
        if DEBUG:
            print "Regenerate event. Call by :"
            print widget
        self.experiment.clear_event_prop_list()

    ###########################################################################
    ## Plot tab
    def on_adj_plot_rel_dist_value_changed(self, widget):
        """
            Changing the relative distance value for the plot.
        """
        self.plot_rel_dist(widget.get_value())

    def plot_rel_dist(self, dist):
        """
            Plot the relative stiffness at a specified distance.
        """
        # We put the group properties to the histogram.
        self.experiment.data_hist.set_group_prop('all',
                                                    self.experiment.group)
        dist = int(dist)
        [data, group, fid, coord] = self.experiment.get_event_att('relstiff')
        del coord
        self.experiment.data_hist.set_type('relstiff')
        self.experiment.data_hist.set_data('relstiff', data)
        self.experiment.data_hist.set_range('relstiff',
                                            [None, None, None, dist])
        self.experiment.data_hist.set_group('relstiff', group)
        self.experiment.data_hist.set_fid('relstiff', fid)
        [data, group, fid, coord] = self.experiment.get_event_att(
                                                                'relstiffctl')
        self.experiment.data_hist.set_type('relstiffctl')
        self.experiment.data_hist.set_data('relstiffctl', data)
        self.experiment.data_hist.set_range('relstiffctl',
                                            [None, None, None, dist])
        self.experiment.data_hist.set_group('relstiffctl', group)
        self.experiment.data_hist.set_fid('relstiffctl', fid)
        if not exist(self.win_plot['TL Rel Stiffness']):
            [window, widget] = self.regenerate_plot()
            self.win_plot['TL Rel Stiffness'] = plot_gtk.TimeLapse(
                                            window, widget,
                                            title='Event relative stiffness',
                                            win_title='Relative Stiffness')
        self.win_plot['TL Rel Stiffness'].displayPlot(
                                            self.experiment.data_hist)

    def on_adj_rel_stiff_limite_value_changed(self, widget):
        """
            Modification of limit values of relative stiffness.
        """
        range_val = widget.get_value()
        self.experiment.compute_relative_stiffness(exclude=range_val)
        self.plot_rel_dist(self.widget['spin_rel_stiff_plot_dist'].get_value())

    def on_button_plot_nbr_event_tl_clicked(self, widget):
        """
            Plots the number of event per scans.
        """
        event_nbr_list = self.experiment.event_nbr_list()
        _px = range(len(event_nbr_list))
        if not exist(self.win_plot['TL Nbr Event']):
            [window, widget] = self.regenerate_plot()
            self.win_plot['TL Nbr Event'] = plot_gtk.plotCurves(
                                                window, widget,
                                                xlabel='Time',
                                                ylabel='Number of events',
                                                title='Number of events',
                                                win_title='Number of events')
            self.win_plot['TL Nbr Event'].ylim = 0

        self.win_plot['TL Nbr Event'].displayPlot(_px, event_nbr_list,
                                                  linestyle='bo-')

    ###########################################################################
    ## Tomography tab
    def on_button_compute_tomo_clicked(self, widget=None):
        """
            Computes the stiffness tomography.
        """
        if DEBUG:
            print "Compute tomo. Call from :"
            print widget
        self.force_volume.stiffness_tomography_array()

    def on_MainWindow_destroy(self, widget, data=None):
        '''
        Destroys the window
        '''
        if DEBUG:
            print "Destroy main window :"
            print widget, data
        # saves the preferences...
        if not(os.path.isdir(os.path.split(self.config['config'])[0])):
            os.makedirs(os.path.split(self.config['config'])[0])
        conf_id = open(self.config['config'], 'wb')
        for keys in self.config.keys():
            if keys == 'msgid':
                conf_id.write(keys + ' = ' +
                            self.config[keys].strftime('%Y, %m, %d') + '\n')
            elif keys in ['linewidth', 'connect-server']:
                conf_id.write(keys + ' = ' +
                            str(self.config[keys]) + '\n')
            else:
                conf_id.write(keys + ' = ' + self.config[keys] + '\n')
        conf_id.write('')
        conf_id.close()
        gtk.main_quit()

    ###########################################################################
    ## Mechanical prop. tab
    def hist_mech_prop(self, dtype, depth=0):
        """
            Display histogram of mechanical properties.
        """
        [title, xlabel, window_name] = PLOT_LABEL[dtype]
        self.experiment.set_data_hist(dtype, self.get_mech_histo_range(dtype),
                                      depth=depth)
        if not exist(self.win_plot[window_name]):
            [window, widget] = self.regenerate_plot()
            self.win_plot[window_name] = plot_gtk.plotHist(
                            window, widget,
                            title=title,
                            xlabel=xlabel,
                            y_rel=self.experiment.parameters['histo_y_rel'])
        self.win_plot[window_name].hist(self.experiment.data_hist)
        if self.experiment.parameters['plot_hist_gauss']:
            self.win_plot[window_name].gauss(
                            self.experiment.event_stat['stiff'])
        self.win_plot[window_name].set_size(
                            self.experiment.parameters['plot_size'])

    def on_button_young_modulus_hist_clicked(self, widget):
        """
            Display histogram of young modulus.
        """
        if DEBUG:
            print "Young modulus :"
            print widget
        # Get the depth value :
        depth = self.widget['spin_ym_hist_depth_nb'].get_value()
        self.hist_mech_prop('young_modulus', depth=depth)

    def get_mech_histo_range(self, dtype):
        """
            Get the range of histogram of mechanical properties.
        """
        if dtype in ['young_modulus']:
            base_name = dtype
        min_val = self.widget['spin_' + base_name + '_hist_min'].get_value()
        max_val = self.widget['spin_' + base_name + '_hist_max'].get_value()
        col_size = self.widget['spin_' + base_name + '_hist_size'].get_value()
        return [min_val, max_val, col_size]

    ###########################################################################
    #                               About dialog
    def on_about_activate(self, widget):
        '''
        Display the about dialog.
        '''
        if DEBUG:
            print "About dialog called from :"
            print widget
        self.about_dialog.run()
        self.about_dialog.hide()

    ###########################################################################
    #                       Compare Experiment Dialog
    def on_compare_exp_activate(self, widget):
        """
            Displays the compare experiment dialog
        """
        if DEBUG:
            print "Compare experiment. Call from :"
            print widget
        if self.experiment is not None:
            self.widget_exp_compare['label_main_filename'].set_label(
                                            self.experiment.short_name)
        self.compare_exp_dialog.run()
        self.compare_exp_dialog.hide()

    def on_open_exp_compare_clicked(self, widget):
        """
            Opens the compare experiment
        """
        if DEBUG:
            print "Compare experiment. Call from :"
            print widget
        self.temp_exp_compare = self.load_exp()
        self.widget_exp_compare['comp_exp_filename'].set_label(
                                            self.temp_exp_compare.short_name)
        self.widget_exp_compare['label_comp_filename'].set_label(
                                            self.temp_exp_compare.short_name)

    def on_button_compare_exp_validate_clicked(self, widget):
        """
            Compare experiments.
        """
        if DEBUG:
            print "validation of compare expexperiment :"
            print widget
        self.experiment_compare = self.temp_exp_compare
        del self.temp_exp_compare
        self.plot_properties.main_exp_label = \
                        self.widget_exp_compare['entry_main_label'].get_text()
        self.plot_properties.comp_exp_label = \
                        self.widget_exp_compare['entry_comp_label'].get_text()
        col_type = self.widget_exp_compare[
                                         'combo_plot_color_type'].get_active()
        if col_type == 0:
            self.plot_properties.color_type = 'color'
        elif col_type == 1:
            self.plot_properties.color_type = 'bw'
        self.plot_properties.plot_compare = True

    def on_button_compare_exp_cancel_clicked(self, widget):
        """
            Cancel the modification of the comparison
        """
        if DEBUG:
            print "compare experiment called by : "
            print widget
        if hasattr(self, 'temp_exp_compare'):
            del self.temp_exp_compare
        if hasattr(self, 'experiment_compare'):
            del self.experiment_compare
        self.plot_properties.comp_exp_label = None
        self.plot_properties.plot_compare = False

    def plot_curve(self):
        """
            Plot the curve according to the user choice.
        """
        [_px, _py] = [self.force_volume.trace_x, self.force_volume.trace_y]
        if self.force_volume.to_plot('Av'):
            if not exist(self.win_plot['Curve Plot']):
                [window, widget] = self.regenerate_plot()
                self.win_plot['Curve Plot'] = plot_gtk.plotCurves(
                                          window, widget,
                                          title='Deflection-Distance Curve',
                                          linewidth=self.config['linewidth'])
            self.win_plot['Curve Plot'].displayPlot(_px, _py)
            if self.force_volume.to_plot('PoC'):
                [poc, _cx, _cy, error, seg_slice, deriv] = \
                                    self.force_volume.get_poc_display()
                self.win_plot['Curve Plot'].hold('On')
                if deriv is not None:
                    self.win_plot['Curve Plot'].displayPlot(deriv[0],
                                                              deriv[1])
                self.win_plot['Curve Plot'].displayPlot(_cx, _cy,
                                                          'g-', alpha=0.3)
                self.win_plot['Curve Plot'].displayPlot(_px[poc: poc + 1],
                                                          _py[poc: poc + 1],
                                                          'go')
                self.win_plot['Curve Plot'].displayPlot(_cx, _cy + error,
                                                          'r-', alpha=0.5)
                self.win_plot['Curve Plot'].displayPlot(_cx, _cy - error,
                                                          'r-', alpha=0.5)
                self.win_plot['Curve Plot'].displayPlot(_px[seg_slice],
                                                          _py[seg_slice],
                                                          'r-', alpha=1)
                self.win_plot['Curve Plot'].hold('Off')
        if self.force_volume.to_plot('Re'):
            [_rx, _ry] = [self.force_volume.retrace_x,
                          self.force_volume.retrace_y]
            if not exist(self.win_plot['Curve Plot']):
                [window, widget] = self.regenerate_plot()
                self.win_plot['Curve Plot'] = plot_gtk.plotCurves(
                                          window, widget,
                                          title='Deflection-Distance Curve',
                                          linewidth=self.config['linewidth'])
            if self.force_volume.to_plot('Av'):
                self.win_plot['Curve Plot'].hold('On')
            self.win_plot['Curve Plot'].displayPlot(_rx, _ry, 'r-')
            self.win_plot['Curve Plot'].hold('Off')
            if self.force_volume.to_plot('Event'):
                det_event = self.force_volume.current_curve(
                        'Event',
                        [self.experiment.parameters['event_detect_weight'],
                        self.experiment.parameters['event_fit_model']])
                self.win_plot['Curve Plot'].hold('On')
                if det_event:
                    for event in det_event:
                        self.win_plot['Curve Plot'].displayPlot(
                                                    _rx[event['Slice']],
                                                    _ry[event['Slice']], 'g')
                        self.win_plot['Curve Plot'].displayPlot(
                                                    [_rx[event['min']]],
                                                    [_ry[event['min']]], 'go')
                        self.win_plot['Curve Plot'].displayPlot(
                                                    [_rx[event['max']]],
                                                    [_ry[event['max']]], 'wo')
                        self.win_plot['Curve Plot'].displayPlot(
                                                    event['fit_x'],
                                                    event['fit_y'],
                                                    'c')
                self.win_plot['Curve Plot'].hold('Off')
        if exist(self.win_plot['Curve Plot']):
            self.win_plot['Curve Plot'].set_size(
                                    self.experiment.parameters['plot_size'])
        if self.force_volume.to_plot('Indent'):
            all_plot = False
            [poc, _cx, _cy, error, seg_slice, deriv] = \
                                        self.force_volume.get_poc_display()
            [_px, _py] = self.force_volume.get_current_indent_curve('trace')
            [part_x, part_y] = self.force_volume.get_segments_indent()
            # Prepare dots coordinates to plot
            dot_x = [item[-1] for item in part_x]
            dot_y = [item[-1] for item in part_y]
            if not exist(self.win_plot['Indent Plot']):
                [window, widget] = self.regenerate_plot()
                self.win_plot['Indent Plot'] = plot_gtk.plotCurves(
                                          window, widget,
                                          title='Indention Curve',
                                          xlabel='Indentation [nm]',
                                          ylabel='Force [nN]',
                                          linewidth=self.config['linewidth'])
            self.win_plot['Indent Plot'].set_size(
                                    self.experiment.parameters['plot_size'])
            if all_plot:
                self.win_plot['Indent Plot'].displayPlot(_px, _py)
            else:
                self.win_plot['Indent Plot'].displayPlot(
                                                    _px[:poc] - _px[poc - 1],
                                                    _py[:poc] - _py[poc - 1])
            self.win_plot['Indent Plot'].hold('On')
            if all_plot:

                self.win_plot['Indent Plot'].displayPlot(dot_x, dot_y,
                                                         'ko', alpha=0.4)
            else:
                self.win_plot['Indent Plot'].displayPlot(dot_x - _px[poc - 1],
                                                         dot_y - _py[poc - 1],
                                                         'ko', alpha=0.4)
            if DEBUG and self.force_volume.to_plot('Re'):
                [_rx, _ry] = self.force_volume.get_current_indent_curve(
                                                                    'retrace')
                self.win_plot['Indent Plot'].displayPlot(_rx, _ry, 'r-')
                if self.force_volume.to_plot('Event'):
                    det_event = self.force_volume.current_curve(
                            'Event',
                            self.experiment.parameters['event_detect_weight'])
                    self.win_plot['Indent Plot'].hold('On')
                    if det_event:
                        for event in det_event:
                            self.win_plot['Indent Plot'].displayPlot(
                                                    _rx[event['Slice']],
                                                    _ry[event['Slice']], 'g')
                            self.win_plot['Indent Plot'].displayPlot(
                                                    [_rx[event['min']]],
                                                    [_ry[event['min']]], 'go')
                            self.win_plot['Indent Plot'].displayPlot(
                                                    [_rx[event['max']]],
                                                    [_ry[event['max']]], 'wo')
            self.win_plot['Indent Plot'].hold('Off')


def exist(win_plot):
    """
        Test the existance of a window plot.
    """
    if win_plot == 0:
        return 0
    return not win_plot.destroyed


def create_header_tree(header):
    """
        >>> test = create_header_tree(None)
    """
    treestore = gtk.TreeStore(str)
    for parent in HEADER.keys():
        piter = treestore.append(None, ['%s' % parent])
        for child in HEADER[parent]:
            treestore.append(piter, ['%s : %s' % (child, str(header[child]))])
    return treestore


def display_error(message, mtype=''):
    """
        Display an error message.
    """
    if message == 'no_fv':
        msg = ('%s\n%s\n%s') % (
              "No force volume file loaded.\n",
              "Please load fv file using file>new to create new experiment",
              "or file>load to load a saved experiment.")
    if message == 'no_compute':
        msg = "The %s is not compute. Please compute it first." % mtype
    dialog = gtk.MessageDialog(type=gtk.MESSAGE_ERROR,
                               buttons=gtk.BUTTONS_OK,
                               message_format=msg)
    dialog.set_title('Error')

    try:
        _icon = resource_filename('openfovea', 'Icon/openfovea.png')
    except NotImplementedError:
        _icon = 'openfovea/Icon/openfovea.png'
    try:
        dialog.set_icon_from_file(_icon)
    except GError:
        pass
    if dialog.run():
        dialog.destroy()


def display_message(message):
    """
        Display a message box with the given message.
    """
    _msg = ''

    for _line in message[1:]:
        _msg += _line
        _msg += '\n'
    _msg = _msg[:-2]
    dialog = gtk.MessageDialog(type=gtk.MESSAGE_INFO, buttons=gtk.BUTTONS_OK)
    dialog.set_properties('secondary-use-markup', True)
    dialog.set_title(message[0])
    dialog.set_markup(_msg)
    try:
        _icon = resource_filename('openfovea', 'Icon/openfovea.png')
    except NotImplementedError:
        _icon = 'openfovea/Icon/openfovea.png'
    dialog.set_icon_from_file(_icon)
    if dialog.run():
        dialog.destroy()


def ask_message(message):
    """
        Display a
    """
    dialog = gtk.MessageDialog(type=gtk.MESSAGE_QUESTION,
                               buttons=gtk.BUTTONS_YES_NO)
    dialog.set_default_response(gtk.RESPONSE_YES)
    dialog.set_properties('secondary-use-markup', True)
    dialog.set_title(message[0])
    dialog.set_markup(message[1])
    try:
        _icon = resource_filename('openfovea', 'Icon/openfovea.png')
    except NotImplementedError:
        _icon = 'openfovea/Icon/openfovea.png'
    dialog.set_icon_from_file(_icon)
    response = dialog.run()
    dialog.destroy()
    return response
WIN_TITLE = "Open Fovea"
HEADER = {
'Equipment': ['Microscope',
               'spring_constant'],
'Parameters': [  # 'force_bytes_per_pixel',
                 # 'hard_z_scale',
                 # 'image_scale',
                'sensit_deflection',
                 # 'sens_z_scan'
               ],
'Scan': [  # 'image_mode',
          'scan_size',
           # 'force_samples_per_curve',
           # 'image_number_lines',
          'number_curves',
           # 'ramp_size',
           # 'size_x',
           # 'size_y'
          'size'],
'Misc': ['date', 'note'],
#'Data': [  # 'force_data_length',
#           # 'force_data_offset',
#           # 'image_length',
#           # 'image_offset',
#          'image_samps_per_line',
#          'matrix_length',
#          'x_factor']
}
# Label = label : [title, x_label, window_name]
PLOT_LABEL = {
'force': ['Event force', 'Force [nN]', 'Hist Event Force'],
'force_base': ['Event force relative to baseline', 'Force [nN]',
                'Hist Event Force'],
'lr': ['Loading rate', 'Loading rate [nN/s]', 'Hist Event LR'],
'dist': ['Event distance', 'distance [nm]', 'Hist Event Dist'],
'stiff': ['Event stiffness', 'Stiffness [Pa]', 'Hist Event Stiff'],
'relstiff': ['Event relative stiffness', 'Stiffness [Pa]',
              'Hist Event Rel Stiff'],
'young_modulus': ['Young modulus', 'Stiffness [Pa]', 'Hist Young Modulus'],
'length': ['Event length', 'Length [nm]', 'Hist Event Length'],
'plength': ['Event persistent length', 'Persistent length [nm]',
                'Hist Event Persistent Length']}
# Window = window : [window name, window title, graph title]
PLOT_WINDOW = {
'forcelr': ['Scatter_FvsLR', 'Scatter Force VS Loading rate'],
'lrforce': ['Scatter_FvsLR', 'Scatter Force VS Loading rate'],
'forcedist': ['Scatter_FvsDist', 'Scatter Force VS Distance'],
'distforce': ['Scatter_FvsDist', 'Scatter Force VS Distance'],
'forcestiff': ['Scatter_FvsStiff', 'Scatter Force VS Stiffness'],
'stiffforce': ['Scatter_FvsStiff', 'Scatter Force VS Stiffness'],

'force_baselr': ['Scatter_FvsLR', 'Scatter Force VS Loading rate'],
'lrforce_base': ['Scatter_FvsLR', 'Scatter Force VS Loading rate'],
'force_basedist': ['Scatter_FvsDist', 'Scatter Loading rate VS Stiffness'],
'distforce_base': ['Scatter_FvsDist', 'Scatter Loading rate VS Stiffness'],
'force_basestiff': ['Scatter_FvsStiff', 'Scatter Force VS Stiffness'],
'stiffforce_base': ['Scatter_FvsStiff', 'Scatter Force VS Stiffness'],

'lrdist': ['Scatter_LRvsDist', 'Scatter Loading rate VS Distance'],
'distlr': ['Scatter_LRvsDist', 'Scatter Loading rate VS Distance'],
'lrstiff': ['Scatter_LRvsStiff', 'Scatter Loading rate VS Stiffness'],
'stifflr': ['Scatter_LRvsStiff', 'Scatter Loading rate VS Stiffness'],
'diststiff': ['Scatter_DistvsStiff', 'Scatter Distance VS Stiffness'],
'stiffdist': ['Scatter_DistvsStiff', 'Scatter Distance VS Stiffness'],
'forcerelstiff': ['Scatter_FvsRelStiff',
                   'Scatter Force VS Relative Stiffness'],
'relstiffforce': ['Scatter_FvsRelStiff',
                   'Scatter Force VS Relative Stiffness'],
'lrrelstiff': ['Scatter_LRvsRelStiff',
                'Scatter Loading Rate VS Realtive Stiffness'],
'relstifflr': ['Scatter_LRvsRelStiff',
                'Scatter Loading Rate VS Realtive Stiffness'],
'distrelstiff': ['Scatter_DistvsRelStiff',
                  'Scatter Distance VS Relative Stiffness'],
'relstiffdist': ['Scatter_DistvsRelStiff',
                  'Scatter Distance VS Relative Stiffness'],
'poc array': [None, 'poc_map', 'Point of contact'],
'piezo array': ['Piezo Height', 'Piezo_Map', 'Piezo Height'],
'topography array': ['Topography Map', 'Topo_map',
                      'Topography (zero force image)'],
'stiffness array': ['Stiffness Map', 'Stiff_map', 'Stiffness'],
'ev_pos array': ['Event Map', 'Ev_pos_map', 'Event position'],
'ev_force array': ['Event Force Map', 'Ev_force_map', 'Event force'],
'ev_dist array': ['Event Distance Map', 'Ev_dist_map', 'Event distance'],
'ev_len array': ['Event Length Map', 'Ev_len_map', 'Event length']}

WINDOW2ARRAY = {
    'Topography Map': 'Topography',
    'Piezo Height': 'Piezo',
    'Stiffness Map': 'Stiffness',
    'Event Map': 'Event',
    'Event Force Map': 'Event force',
    'Event Distance Map': 'Event dist'}

# MOSAIC_PLOT is a dict which contains the values for mosaic name :
# mosaic_name[0] = name of the window plot
# mosaic_name[1] = title of the window
# mosaic_name[2] = title of the plot
# mosaic_name[3] = cmap
MOSAIC_PLOT = {
    'Piezo': ['Mosaic Piezo', 'Mosaic of piezo', 'Piezo images', 'afm image'],
    'Topography': ['Mosaic Topo', 'Mosaic of topos', 'Topographies',
                    'afm image'],
    'Stiffness': ['Mosaic Stiffness', 'Mosaic of stiffness', 'Stiffness',
                   'jet'],
    'Event': ['Mosaic Event', 'Mosaic of events', 'Events', 'gray'],
    'Event force': ['Mosaic Event Force', 'Mosaic of event force',
                     'Event forces', 'gray']}


def main(arg=None):
    '''
        OpenFovea main function
    '''
    if os.name == 'posix':
        try:
            if os.uname()[0] == 'Linux':
                try:
                    locale.setlocale(locale.LC_ALL, 'en_GB.UTF8')
                except locale.Error:
                    locale.setlocale(locale.LC_ALL, 'en_US.UTF8')
            elif os.uname()[0] == 'Darwin':
                locale.setlocale(locale.LC_ALL, 'en_GB')
        except locale.Error:
            gtk.MessageDialog(type=gtk.MESSAGE_ERROR)
            message = \
                "Locale en_GB does not seem to be installed on your system." +\
                "\nPlease install it."
            message_sec = "On Linux system : change the file" +\
                  "\n/etc/locale.gen" +\
                  "\nto uncomment the corresponding line and run, as root :" +\
                  "\nlocale-gen"
            error = gtk.MessageDialog(buttons=gtk.BUTTONS_CLOSE,
                                  type=gtk.MESSAGE_ERROR,
                                  message_format=message)
            error.format_secondary_text(message_sec)
            error.set_title('Installation Error')
            error.run()
    elif os.name == 'nt':
        locale.setlocale(locale.LC_ALL, 'english')
    app = OpenFoveaMainGui(arg)
    app.window.show()
    gtk.main()

if __name__ == '__main__':
    locale.setlocale(locale.LC_ALL, 'en_GB.UTF8')
    APP = OpenFoveaMainGui()
    APP.window.show()
    gtk.main()

    #toto = open('../welcome')
    #message = toto.readlines()
    #display_message(message)
