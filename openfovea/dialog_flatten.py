#!/usr/bin/env python
# -*- coding: UTF-8 -*-import pygtk
"""
    The gui to flatten the images.
"""

import gtk
import pygtk
# for script installation
from pkg_resources import resource_filename
##############
## Graphic library ##
##############
try:
    matplotlib.__version__
except NameError:
    import matplotlib
    matplotlib.use('Agg', warn=False)
from matplotlib.figure import Figure
from matplotlib.backends.backend_gtkagg import FigureCanvasGTKAgg as FigureCanvas
from matplotlib.backends.backend_gtkagg import NavigationToolbar2GTKAgg as NavigationToolbar

from fovea_toolbox import misc

class FlattenDialog():
    def __init__(self, array, _glade_filename=None):
    
        self.array = [array, misc.flatten(array)]
        
        builder = gtk.Builder()
        if _glade_filename == None:
            try:
                _glade_filename = resource_filename(
                                    'openfovea',
                                    'glade/dialog_flatten.glade')
            except NotImplementedError:
                _glade_filename = 'openfovea/glade/dialog_flatten.glade'
        builder.add_from_file(_glade_filename)
        self.window = builder.get_object('FlattenDialog')
        self.widget = {
            'button_apply' : builder.get_object('button_apply'),
            'button_cancel' : builder.get_object('button_cancel'),
            'plot_before_flatten' : builder.get_object('view_before_flatten'),
            'plot_after_flatten' : builder.get_object('view_after_flatten'),
            }
        # The plots
        self.flatten = {'figure' : None,
                        'axis' : None,
                        'canvas' : None}
                        
        self.flatten['figure'] = [
                Figure(figsize=(100, 100), dpi=75, facecolor=(0.93,0.92,0.9,1)),
                Figure(figsize=(100, 100), dpi=75, facecolor=(0.93,0.92,0.9,1))
                ]
        self.flatten['axis'] = [
                self.flatten['figure'][0].add_subplot(111),
                self.flatten['figure'][1].add_subplot(111)
        ]
        self.flatten['canvas'] = [
                FigureCanvas(self.flatten['figure'][0]), # a gtk.DrawingArea 
                FigureCanvas(self.flatten['figure'][1]) # a gtk.DrawingArea 
        ]
        self.flatten['canvas'][0].show() 
        self.flatten['canvas'][1].show() 
        self.widget['plot_before_flatten'].pack_start(self.flatten['canvas'][0], True, True)
        self.widget['plot_after_flatten'].pack_start(self.flatten['canvas'][1], True, True)
        self.plot()
    def plot(self):
        #vmin = min([self.array[0].min(), self.array[1].min()])
        #vmax = max([self.array[0].max(), self.array[1].max()])
        self.flatten['axis'][0].pcolormesh(self.array[0].transpose(), cmap='copper')
        self.flatten['axis'][0].axis([0, self.array[0].shape[0], 0, self.array[0].shape[0]])
        self.flatten['axis'][0].set_xticks([])
        self.flatten['axis'][0].set_yticks([])
        self.flatten['axis'][1].pcolormesh(self.array[1].transpose(), cmap='copper')
        self.flatten['axis'][1].axis([0, self.array[1].shape[0], 0, self.array[1].shape[0]])
        self.flatten['axis'][1].set_xticks([])
        self.flatten['axis'][1].set_yticks([])
    def run(self):
        self.window.show()
        response = self.window.run()
        if response == 1:
            return self.array[1]
        else:
            return self.array[0]
    def destroy(self):
        self.window.destroy()
if __name__ == '__main__':
    import numpy
    test = main_win(numpy.array([range(10) for i in range(10)]), 
                    _glade_filename='glade/dialog_flatten.glade')
    toto = test.run()
    print toto
