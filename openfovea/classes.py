#! /usr/bin/python
# -*- coding: utf-8 -*-

'''
This module contains the classes that are usefull for OpenFovea. Currently,
there is only the class ForceVolume, but we will soon implement an experiment
class
'''

import os
import csv
from datetime import datetime
# import pdb

import numpy as num
import math

from fovea_toolbox import curve
from fovea_toolbox.file_util import afm_file, aex
from fovea_toolbox.post_proc import rel_values, rand_dot, \
                                    add_stiffness_to_event, resize_array
from fovea_toolbox import post_proc, misc
from plot_generic import PlotData
import plot_generic as plot

__author__ = "Charles Roduit <charles.roduit@gmail.com>"
__date__ = "25 juin 2005"
__version__ = "0.1.alpha.1"
__credits__ = "Rewrite of the 'Succellus' software developed during my thesis\
               at the EPFL under the direction of Sandor Kasas."


class ForceVolume(object):
    """
    This class contains all attributes and methods used to post-process force
    curve. The needed input is the name of the file to open.

    Attributes :

        * name: the original file name
        * indent: indentation vector of the force indentation curve
        * force: force vector of the force indentation curve
        * curve_nb: indice of the current curve
        * pox_x: x coordinate of the current curve
        * pos_y: y coordinate of the current curve
        * stiffness: dictionnary that contains the parameters to compute the
                      stiffness
          - hertz_model: the hertz model used
          - poisson_ratio: Poisson ratio of the material.
          - point_carac: caracteristic of the tip (radius or semi-opening
                      angle)
          - nb_parts: the number of segment to compute
          - size_parts: the size of each segments
          - glass: the slope of the "non indenting part" of the curve
          - claibSens: ???
          - relative_max_dist: the maximum distance to compute relative
                      stiffness
          - poc_threshold: The threshold to detect the point of contact
          - poc_method: Stores the method to find the point of contact. (see
                          curve.find_poc for the available methods.)
          - limit_slide: Wether to limit or not the sliding of the slice when
                           finding point of contact.
          - recompute_poc: Wether to recompute poc or not.
          - poc_len_slice: Length of the fit curve portion to find point of
                      contact
        * array: dictionnary that contains the arrays computed on the scan.
          - 'Piezo': The height of the piezo at the end of the indentation.
          - 'PoCIndice': The indice of the point of contact.
          - 'Topography': The zero force image.
          - 'Stiffness': The computed stiffness.
          - 'Stiffness tomo': Same as "Stiffness", but in an array corrected
                               with the zero force image.
          - 'Event': The number of events.
          - 'Event force': The force of events (max if several).
          - 'Event random': The position of random selected pixels.
          - 'Relative pos': ??
          - 'Relative rand pos': ??
          - 'Path': This defines a path to display info.
          - 'Masked': True | False (default: False) To apply or not a mask to
                       the array.
        * event: dictionnary that possess information on the computation and
                  display of the events.
          - nbr_random: the number of random event that has to be generated.
          - rel_stiff and rel_stiff_ctl: the result of the relative stiffness.
        * header: TO COMPLETE
          - ev_dist_thresh: the distance from which we have to display the
                                 events.
        * plot_who: curve to plot
        * exist: to check the existence of computed things
        * array: the 2D arrays computed
        * header: the header of the file
        * deflection_av
        * deflection_re: advance and retraction curves
        * plotx: the scanner extention part for all the deflection curves
        * event_list: list of all events
    """
    # TODO: change self.event_list to self.event['list']
    def __init__(self, fv_file):
        self.indent = []  # The indentation and force curve of the
        self.force = []  # trace curve.
        self.r_indent = []  # The indentation and force curve of the
        self.r_force = []  # retrace curve.
        self.curve_nb = 0
        self.pos_x = 0
        self.pos_y = 0
        self.counter = {'num': 0,
                        'increment': 0,
                        'text': 'Please wait...'}
        self.stiffness = {'hertz_model': 'Sphere',  # Hertz model used.
                        'fit_method': 'Raw',  # The fit method used.
                        'poisson_ratio': 0.3,  # Poisson ratio of the material.
                        'point_carac': 40,  # Caracteristic of the tip (depend
                        # on the Hertz model).
                        'nb_parts': 4,  # Number of FI segment.
                        'size_parts': 50,  # The size of each FI segment.
                        'glass': -1.,  # Correction factor for the sensitivity
                        # calibration.
                        'calib_sens': 1,  # ?? Dead code ??
                        'relative_max_dist': 3,  # When computing the relative
                        # stiffness, what is the maximum distance to compute.
                        'poc_threshold': 2,  # The dependency on noise.
                        'poc_method': 'curve_fit',  # Stores the method to find
                        # the point of contact. (see curve.find_poc for the
                        # available methods.)
                        'limit_slide': False,  # Wether to limit or not the
                        # sliding of the slice when finding point of contact.
                        'recompute_poc': True,  # Wether to recompute poc or
                                               # not.
                        'poc_len_slice': 0.3,  # Length of the fit curve
                        # portion to find point of contact
                        }
        self.event = {'nbr_random': 15,
                      'rel_stiff': None,
                      'rel_stiff_ctl': None}
        self.event_list = []
        self.event_stat = {'force': None,
                           'lr': None,
                           'dist': None,
                           'stiff': None,
                           'rel_stiff': None}
        self.event_list_original = None
        self.plot_who = {'Av': 1, 'Re': 0, 'Indent': 0,
                         'PoC': 0, 'Event': 0}
        self.exist = {'PoC': 0, 'Stiffness': 0}
        ######################
        # Define the arrays :
        self.__array = dict()
        for item in ARRAY_TYPE:
            self.__array[item] = None
        # special case :
        #self.__array['Masked'] = False
        #######################
        self.__fdcurves = {
                    'trace_x': None,
                    'trace_y': None,
                    'retrace_x': None,
                    'retrace_y': None,
        }
        if type(fv_file) in [str, unicode]:
        #if type(fv_file) == str:
            self.name = os.path.split(fv_file)[1]
            _data = afm_file.load(fv_file)
            self.header = _data['header']
            self.__array['Piezo'] = _data['piezo']
            if self.header['data_location'] == 'local':
                # we are in the case where the data are loaded at once
                self.__fdcurves['trace_x'] = _data['trace_x']
                self.__fdcurves['retrace_x'] = _data['retrace_x']
                self.__fdcurves['trace_y'] = _data['trace_y']
                self.__fdcurves['retrace_y'] = _data['retrace_y']
            else:
                self.__data_fid = _data['data_fid']
            if self.header['Microscope'] == 'csem':
                self.stiffness['glass'] = -0.01
        elif type(fv_file) == dict:
            # we are in the presence of a xml tree readed from aex file
            self.name = fv_file['name']
            self.header = fv_file['header']
            self.__fdcurves['trace_y'] = fv_file['trace_array']
            self.__fdcurves['retrace_y'] = fv_file['retrace_array']
            self.__fdcurves['trace_x'] = fv_file['trace_x_array']
            self.__fdcurves['retrace_x'] = fv_file['retrace_x_array']
            self.__array['Piezo'] = fv_file['piezo_array']
            if self.header['data_location'] not in ['local', u'local']:
                self.__data_fid = fv_file['data_fid']
            if len(fv_file['stiffness_header']):
                self.stiffness = fv_file['stiffness_header']
            # Load stiffness results
            if fv_file['stiffness'] is not None:
                # recover attributes...
                self.__array['Stiffness'] = fv_file['stiffness']
            # Load events results
            if 'event' in fv_file and 'array' in fv_file['event']:
                self.__array['Event'] = fv_file['event']['array']
                self.event_list = fv_file['event']['detail']
            if 'event_rand_array' in fv_file:
                self.__array['Event random'] = fv_file['event_rand_array']
            if 'poc_array' in fv_file and fv_file['poc_array'] is not None:
                self.__array['PoCIndice'] = fv_file['poc_array']
                self.exist['PoC'] = 1
            if fv_file['pocnoise_array'] is not None:
                self.__array['PoCNoise'] = fv_file['pocnoise_array']
            if 'topo_array' in fv_file:
                self.__array['Topography'] = fv_file['topo_array']
            if 'mask_array' in fv_file:
                self.__array['Mask'] = fv_file['mask_array']
            self.set_switch('mask', eval(fv_file['switch']['mask']))
        if self.header['pixel_size'] is None:
            try:
                self.header['pixel_size'] = \
                    tuple([float(size) / nb_pix
                        for size, nb_pix in zip(self.header['scan_size'],
                                                self.header['size'])])
            except (ZeroDivisionError, TypeError):
                self.header['pixel_size'] = (None, None)

    def __iter__(self):
        self.curve_nb = -1
        return self

    def __cmp__(self, other):
        if isinstance(other, ForceVolume):
            return cmp(self.header['date'], other.header['date'])

    def __getattribute__(self, name, **toto):
        """
            Control the attribute given form the object.
        """
        if name == 'event_list':
            event_list = object.__getattribute__(self, name)
            if self.get_switch('mask'):
                # If data are masked, get only the events that are on the mask.
                event_list = misc.remove_from_mask(event_list,
                                                   self.__array['Mask'])
            if self.get_switch('ev_dist'):
                # If there is an threshold on the event distance.
                event_list = misc.remove_from_threshold(
                                    event_list,
                                    'dist',
                                    self.get_switch('ev_dist'))
            if self.get_switch('ev_fit_length'):
                event_list = misc.remove_from_threshold(
                                    event_list,
                                    'fit_length',
                                    self.get_switch('ev_fit_length'))
            if self.get_switch('ev_fit_plength'):
                event_list = misc.remove_from_threshold(
                                    event_list,
                                    'fit_plength',
                                    self.get_switch('ev_fit_plength'))
            return event_list
        elif name == 'has_stiffness':
            if self.get_array('Stiffness') is not None:
                return True
            else:
                return False
        elif name == 'spring_constant':
            if type(self.header['spring_constant']) is list:
                return self.header['spring_constant'][self.pos_x]
            else:
                return self.header['spring_constant']
        elif name == 'trace_x':
            if self.header['data_location'] == 'local':
                if self.__fdcurves['trace_x'].ndim == 1:
                    return self.__fdcurves['trace_x']
                else:
                    return self.__fdcurves['trace_x'][
                                                    self.pos_x, self.pos_y, :]
            else:
                # the data are loaded on the fly.
                return afm_file.load_curve(self.__data_fid,
                                          pos=[self.pos_x, self.pos_y],
                                          dtype='trace_x',
                                          header=self.header)
        elif name == 'trace_y':
            if self.header['data_location'] == 'local':
                return self.__fdcurves['trace_y'][self.pos_x, self.pos_y, :]
            else:
                # the data are loaded on the fly.
                return afm_file.load_curve(self.__data_fid,
                                          pos=[self.pos_x, self.pos_y],
                                          dtype='trace_y',
                                          header=self.header)
        elif name == 'retrace_x':
            if self.header['data_location'] == 'local':
                if self.__fdcurves['retrace_x'].ndim == 1:
                    return self.__fdcurves['retrace_x']
                else:
                    return self.__fdcurves['retrace_x'][
                                                    self.pos_x, self.pos_y, :]
            else:
                # the data are loaded on the fly.
                return afm_file.load_curve(self.__data_fid,
                                          pos=[self.pos_x, self.pos_y],
                                          dtype='retrace_x',
                                          header=self.header)
        elif name == 'retrace_y':
            if self.header['data_location'] == 'local':
                return self.__fdcurves['retrace_y'][self.pos_x, self.pos_y, :]
            else:
                # the data are loaded on the fly.
                return afm_file.load_curve(self.__data_fid,
                                          pos=[self.pos_x, self.pos_y],
                                          dtype='retrace_y',
                                          header=self.header)
        else:
            return object.__getattribute__(self, name)

    def next(self):
        '''
        Go to the next pixel
        '''
        self.go_next()
        return [self.pos_x, self.pos_y]

    def current_curve(self, what, parameters=None):
        if what == 'Event':
            if parameters is not None:
                event_detect_weight = parameters[0]
                event_fit_model = parameters[1]
            else:
                event_detect_weight = self.header['event_detect_weight']
                event_fit_model = self.header['event_fit_model']
                # event_fit_from_poc = self.header['event_fit_from_poc']
            if event_fit_model is not None:
                # we compute the point of contact from the current curve
                poc = self.current_curve('poc')
                poc['Poly Fit'][1] += (self.retrace_y[0] - self.trace_y[0])
            else:
                poc = {'PoC': 0, 'Poly Fit': None}
            return curve.event_find(self.retrace_x,
                                self.retrace_y,
                                self.stiffness['glass'],
                                self.spring_constant,
                                weight=event_detect_weight,
                                fit_model=event_fit_model,
                                baseline=poc['Poly Fit'],
                                poc=poc['PoC'])
        elif what == 'poc':
            poc = curve.find_poc(self.trace_x, self.trace_y,
                                 threshold=self.stiffness['poc_threshold'],
                                 method=self.stiffness['poc_method'],
                                 limit_slide=self.stiffness['limit_slide'],
                                 len_slice=self.stiffness['poc_len_slice'])
            return poc

    ##### Get stuffs from the object.
    def get_parameter(self, which):
        # TODO Put here all parameters that can be get. These parameters are
        # not switches. Meaning that they have influences in the computation,
        # but not in the display.
        if which == 'poisson_ratio':
            return self.stiffness['poisson_ratio']

    def get_current_curve(self, direction='trace'):
        """
            Get the current trace curve.

            * Parameters :

                direction: str
                    'trace' or 'retrace'

            * Returns :

                plotx: numpy.array
                    The x part of the deflection curve
                deflection: numpy.array
                    The deflection part of the deflection curve
        """
        if direction == 'trace':
            return [self.trace_x, self.trace_y]
        elif direction == 'retrace':
            return [self.retrace_x, self.retrace_y]

    def get_switch(self, stype):
        """
            Get the value of the switch
        """
        if stype == 'list':
            return ['mask', 'ev_dist']
        elif stype == 'mask':
            return self.header['Masked']
        elif stype == 'ev_dist':
            return self.header['event_dist_thresh']
        elif stype == 'ev_thresh':
            # This is a generic switch for all event thresholds.
            return self.header['event_dist_thresh']
        elif stype == 'ev_fit_length':
            return self.header['thresh_event_fit_length']
        elif stype == 'ev_fit_plength':
            return self.header['thresh_event_fit_plength']
        elif stype == 'restore':
            return [self.get_switch('mask'), self.get_switch('ev_dist')]

    def set_switch(self, stype, state=None):
        """
            Set the value of the switch.
        """
        if stype == 'mask':
            if state == True and (self.__array['Mask'] is not None):
                # Control if we have a mask
                self.header['Masked'] = state
            else:
                self.header['Masked'] = False
        elif stype == 'ev_dist':
            if type(state) in [float, int]:
                self.header['event_dist_thresh'] = float(state)
            else:
                raise AttributeError('Expecting numerical value.')
        elif stype == 'ev_fit_length':
            if type(state) == list and len(state) == 2:
                if state[0] == 0:
                    state[0] = None
                if state[1] == 0:
                    state[1] = None
                self.header['thresh_event_fit_length'] = state
            else:
                raise AttributeError('Expecting list of length 2.')
        elif stype == 'reset':
            self.set_switch('mask', None)
            self.set_switch('ev_dist', 0.0)
        elif stype == 'restore':
            self.set_switch('mask', state[0])
            self.set_switch('ev_dist', state[1])

    def get_array(self, atype, raw=False):
        """
            Get array from the forcevolume object.
        """
        if atype == 'keys':
            # Return the valid keys for array.
            return ARRAY_TYPE.keys()
        if raw:
            return self.__array[atype]
        if ARRAY_TYPE[atype] == 'event':
        #if atype in ['Event', 'Event force', 'Event length'] :
            # A threshold is applyed on events.
            ev_lst = self.event_list
            #ev_lst = object.__getattribute__(self, 'event_list')
            #ev_lst = misc.remove_from_threshold(ev_lst, 'dist',
            #                                    self.get_switch('ev_dist'))

            # Get the name in the event dictionnary that correspond to the one
            # in array.
            if atype == 'Event':
                _gen_type = 'count'
            elif atype == 'Event force':
                _gen_type = 'force'
            elif atype == 'Event length':
                _gen_type = 'fit_length'
            elif atype == 'Event distance':
                _gen_type = 'dist'
            array = misc.generate_array(ev_lst, _gen_type, self.header['size'])
        elif atype == 'Piezo':
            array = self.__array[atype]
            if array is None: #num.array_equal(array, num.zeros(array.shape)):
                raise NotReadyError('Topography array is not loaded')
        else:
            array = self.__array[atype]
        if array is None:
            return array
        if self.get_switch('mask'):
            # Return a masked array corresponding to the generated mask.
            mask = self.__array['Mask'].copy()
            mask = mask.astype(bool)
            if type(array) == num.ma.core.MaskedArray:
                if array.ndim == 3:
                    mask = num.array([mask for i in range(array.shape[2])])
                    mask = mask.transpose(1, 2, 0)
                mask = ((array.mask - 1) * (mask - 1)) - 1
                array = array.data
            array = num.ma.masked_array(array, mask=mask)
        return array

    def force_piezo_loading(self):
        """
            Force the loading of the piezo array. Used in the case of AFM files
            with no piezo arrays saved. The method is to get the coordinate of
            the end of indentation of each curves. As this can take time, we do
            not this automatically, but only if the user think its important.
        """
        arrays = afm_file.force_load_array(self.__data_fid,
                                          header=self.header)
        self.__array['Piezo'] = arrays['Piezo']

    def set_array(self, atype, array):
        """
            Set the array with the new value.
        """
        if atype in self.__array.keys():
            self.__array[atype] = array

    def get_current_indent_curve(self, direction='trace'):
        """
            Get the current indentation curve.

            * Parameters :
                direction: str
                  'trace' or 'retrace'

            * Returns :
                indent: numpy.array
                    The indentation part of the indentation curve
                force: numpy.array
                    The force part of the indentation curve
        """
        if direction == 'trace':
            self.compute_indentation()
            return [self.indent, self.force]
        elif direction == 'retrace':
            self.compute_indentation('retrace')
            return [self.r_indent, self.r_force]

    def get_poc_display(self):
        """
            Get all parmeters to display the point of contact.

            [poc, plotx, ploty, error, slice, deriv] = fv.get_poc_display()

            * Parameters :
                None

            * Returns :
                PoC: int
                    The index of the point of contact.
                plotx: numpy.array
                    The x part of the curve.
                ploty: numpy.array
                    The y part of the curve.
                error: float
                    The standard error detected when finding the poc.
                slice: slice
                    The curve part that was used for the fit.
                deriv: ??
                    Have to find what's this...
        """
        ## build the curves...
        _result = curve.find_poc(self.trace_x,
                                self.trace_y,
                                self.stiffness['poc_threshold'],
                                method=self.stiffness['poc_method'],
                                limit_slide=self.stiffness['limit_slide'],
                                len_slice=self.stiffness['poc_len_slice'])
        vect_y = _result['Poly Fit'][0] * self.trace_x + _result['Poly Fit'][1]
        return[_result['PoC'], self.trace_x, vect_y, _result['Error'],
                _result['Slice'], _result['deriv']]

    def get_segments_indent(self):
        """
            get the segment of the indentation curve that are used to compute
            the stiffness or the stiffness tomography.
        """
        if not self.exist['PoC']:
            _result = curve.find_poc(self.trace_x,
                                self.trace_y,
                                self.stiffness['poc_threshold'],
                                method=self.stiffness['poc_method'],
                                limit_slide=self.stiffness['limit_slide'],
                                len_slice=self.stiffness['poc_len_slice'])
            poc_indice = _result['PoC']
        else:
            poc_indice = self.__array['PoCIndice'][self.pos_x, self.pos_y]
        [x_parts, y_parts] = curve.segment_curve(self.indent[0:poc_indice],
                                             self.force[0:poc_indice],
                                             self.stiffness['nb_parts'],
                                             self.stiffness['size_parts'])
        return [x_parts, y_parts]

    def get_path(self, atype, path):
        """
            Get the path from arrays.

            * Parameters :
                atype: str
                    This is the array type to slice. This has to be an item
                    from fv.get_array('keys')
                    If atype is "all", it will return all possible values,
                    with the corresponding label
                pos_a: array
                    correspond to the first point of the path[x, y]
                pos_b: array
                    correspond to the last point of the path [x,y]

            * Returns :
                x_slice: 1D numpy.array
                    The x value of the path
                y_slice: 1D numpy.array or list of numpy.array
                    * If atype is an array type, this is the y value of the
                      path
                    * If atype is "all" this is a list of y value of the path
                label: str
                    If atype is "all", this is a list of label that desing each
                    y_slices.
                    Else, this is the atype as a string

            >>> fv = ForceVolume(BIOSCOPE_FILE)
            >>> #piezo_slice = fv.get_path('Piezo', [0, 14], [15, 14])
        """
        array_list = ['Piezo', 'PoCIndice', 'Topography']

        if atype == 'all':
            atype = array_list
        elif atype in array_list:
            atype = [atype]
        else:
            raise ValueError('Array type ' + atype + ' not known.')

        self.__array['Path'] = path

        y_slice = [
                post_proc.generate_slice(self.__array['Path'],
                                         self.__array[item])
                for item in atype]
        x_slice = list(num.arange(len(y_slice[0])) * self.header['pixel_size'])

        return x_slice, y_slice, atype
    #
    #### Navigation dans le scan ####

    def curve2xy(self):
        '''
        Conversion of the current curveNb into the x and y coordinates.
        '''
        if self.header['size'][1] - 1:
            self.pos_x = (self.curve_nb) % (self.header['size'][0])
            self.pos_y = (self.curve_nb) / (self.header['size'][0])
        else:
            self.pos_x = self.curve_nb
            self.pos_y = 0

    def curve2nb(self):
        '''
        Conversion of the current x and y coordinate into the curveNb.
        '''
        self.curve_nb = self.pos_x + \
                        self.pos_y * self.header['size'][0]

    def go_next(self):
        '''
        Go to the next pixel.
        '''
        if self.curve_nb == self.header['number_curves'] - 1:
            raise StopIteration
        self.curve_nb = self.curve_nb + 1
        self.curve2xy()

    def go_prev(self):
        '''
        Go to the previous pixel.
        '''
        if self.curve_nb == 0:
            return
        self.curve_nb = self.curve_nb - 1
        self.curve2xy()

    def go_up(self):
        '''
        Go tho the upper pixel.
        '''
        if self.pos_y == self.header['size'][1] - 1:
            return
        self.pos_y = self.pos_y + 1
        self.curve2nb()

    def go_down(self):
        '''
        Go to the bellow pixel.
        '''
        if self.pos_y <= 0:
            return
        self.pos_y = self.pos_y - 1
        self.curve2nb()

    def go_first(self):
        '''
        Go the the first pixel.
        '''
        self.pos_y = 0
        self.pos_x = 0
        self.curve_nb = 0

    def go_to(self, pos_x, pos_y):
        '''
        Go to a defined pixel
        '''
        self.pos_x = int(pos_x)
        self.pos_y = int(pos_y)
        self.curve2nb()

    # TODO: All these change should be in a single set_param function
    #       Look if it's better or not...
    # e.g. set_param('hertz_model', 'sphere', carac=None)
    #       set_param('segm_depth', 50, carac='nm')
    #       set_param('segm_nbr', 10, carac='begin')
    #       set_param('rel_dist', 4)
    def set_parameters(self, param, value):
        """
            Change the caracteristic of a parameter.

            param: str
                    Which parameter to change. Possible values are
                    * 'tip_carac' to change the tip caracteristic (i.e. the
                      semi-opening angle if the cone model is used, or the
                      radius of the sphere if the sphere model is used)
                      In this case, value is a float.

                    * 'evt_fit' to change the event fit model.
                      In this case, value is a string. 'wlc' and 'fjc' are
                      possible strings. For more detailed information, see
                      documentation of the event_fit function in
                      fovea_toolbox/curve documentation.

                    * 'stiff_method' to change the stiffness computation
                      method.
                      'Raw' indicates the use of the raw data to fit with the
                            chosen model.
                      'Extrema' uses the points at the extremities of the
                                segments in order to make a simple fit. This
                                method is faster, but less precise. This is
                                historically the first method introduced.

            value: float or str (depending on the parameters.)
        """

        if param == 'tip_carac':
            print "Changing carac of %s from %f to %f" % (
                                        self.name,
                                        self.stiffness['point_carac'],
                                        value)
            self.stiffness['point_carac'] = value
        elif param == 'evt_fit':
            self.header['event_fit_model'] = value
        elif param == 'stiffness_fit_method':
            if value in ['Raw', 'Linear', 'Extrema', 'Median']:
                self.stiffness['fit_method'] = value
            else:
                raise AttributeError(
                    'Method %s not recognized as a stiffness method') % value

    def change_poc_seg_size(self, size):
        """
            Change the size of the curve portion used to find the point of
            contact. The size is expressed as a fraction of the total force
            curve.
        """
        if size == 'default':
            self.stiffness['poc_len_slice'] = 0.3
        elif 0 < size < 1:
            self.stiffness['poc_len_slice'] = size
        else:
            raise ValueError('Value must be between 0 and 1.')

    def change_model(self, model, carac=None):
        '''
        change_model changes the model used for the stiffness computation.

        model: 'Sphere' or 'Cone'.

        carac: sphere radius or cone semi opening angle.
        '''
        if model == 'Sphere':
            if carac == None:
                self.stiffness['point_carac'] = 40
            else:
                self.stiffness['point_carac'] = carac
            self.stiffness['hertz_model'] = 'Sphere'
        elif model == 'Cone':
            if carac == None:
                self.stiffness['point_carac'] = 20 * math.pi / 180
            else:
                self.stiffness['point_carac'] = carac
            self.stiffness['hertz_model'] = 'Cone'

    def change_segment_depth(self, value):
        '''
        Changes the depth (in nm) of the force curve segmentation used to
        compute the stiffness
        '''
        try:
            self.stiffness['size_parts'] = float(value)
        except ValueError:
            pass

    def change_segment_number(self, value):
        '''
        Changes the number of segment used to compute the stiffness
        '''
        try:
            self.stiffness['nb_parts'] = int(value)
        except ValueError:
            pass

    def change_relative_distance(self, value):
        '''
        Changes the maximum relative stiffness distance to compute.
        '''
        if type(value) == int or (int(value) - value == 0):
            self.stiffness['relative_max_dist'] = int(value)

    def change_poisson_ratio(self, value):
        """
            Changes the poisson ratio of the material.

            * Parameters :
                value: float
                    The new poisson ratio.
        """
        if type(value) == float:
            self.stiffness['poisson_ratio'] = value
        else:
            raise TypeError('Float value expected.')

    def compute_poc(self):
        '''
        Computes the point of contact between the tip and the sample in the
        whole array.
        '''
        self.counter['text'] = "Computing point of contact"
        self.__array['Topography'] = num.empty(self.header['size'])
        self.__array['PoCIndice'] = num.empty(self.header['size'])
        self.__array['PoCNoise'] = num.empty(self.header['size'])

        if self.__array['Piezo'] is None:
            regenerate_piezo = True
            self.__array['Piezo'] = num.empty(self.header['size'])
        else:
            regenerate_piezo = False

        for pixels in self:
            trace_x, trace_y = self.trace_x, self.trace_y
            if regenerate_piezo:
                self.__array['Piezo'][self.pos_x, self.pos_y] = trace_x[-1]
            _result = curve.find_poc(trace_x, trace_y,
                                    threshold=self.stiffness['poc_threshold'],
                                    method=self.stiffness['poc_method'],
                                    limit_slide=self.stiffness['limit_slide'],
                                    len_slice=self.stiffness['poc_len_slice'])
            if _result is not None:
                self.__array['PoCIndice'][
                                self.pos_x, self.pos_y] = _result['PoC']
                self.__array['Topography'][self.pos_x, self.pos_y] = (
                                self.__array['Piezo'][self.pos_x, self.pos_y] +
                                trace_x[_result['PoC']])
                self.__array['PoCNoise'][
                                self.pos_x, self.pos_y] = _result['Error']
            else:
                self.__array['PoCIndice'][self.pos_x, self.pos_y] = 0
                self.__array['Topography'][self.pos_x, self.pos_y] = 0
                self.__array['PoCNoise'][self.pos_x, self.pos_y] = 0
            self.counter['num'] += self.counter['increment']
        self.go_first()
        self.exist['PoC'] = 1

    def compute_indentation(self, direction='trace'):
        '''
        Computes the indentation curves on the current position
        '''
        if direction == 'trace':
            [self.indent, self.force] = curve.compute_indentation(
                                        self.trace_x, self.trace_y,
                                        self.stiffness['glass'],
                                        self.spring_constant)
        elif direction == 'retrace':
            [self.r_indent, self.r_force] = curve.compute_indentation(
                                        self.retrace_x, self.retrace_y,
                                        self.stiffness['glass'],
                                        self.spring_constant)

    def compute_stiffness(self):
        '''
        Compute the stiffness array on the scan
        '''
        ## Defines the variables
        ##

        self.counter['num'] = 0
        self.counter['text'] = "Creating matrices"
        if self.stiffness['nb_parts'] < 0:
            # case where we compute stiffness on whole curve instead of segments.
            __size_array = 1
        else:
            __size_array = self.stiffness['nb_parts']
        self.__array['Stiffness'] = num.zeros([
                                    self.header['size'][0],
                                    self.header['size'][1],
                                    __size_array
                                    ], num.float)
        _number_of_curves = self.header['size'][0] * \
                            self.header['size'][1] * 2 + 1
        self.counter['increment'] = 1 / float(_number_of_curves - 1)

        if (not self.exist['PoC']) or self.stiffness['recompute_poc']:
            self.compute_poc()

        self.counter['text'] = "Computing stiffness"
        ## Computation begins by navigation in the array
        for pixel in self:
            self.compute_indentation()
            if self.indent is None:
                # There is no data here. We increment the counter and go to the
                # next pixel.
                self.counter['num'] += self.counter['increment']
                continue
            poc_indice = self.__array['PoCIndice'][self.pos_x, self.pos_y]
            [x_parts, y_parts] = curve.segment_curve(
                                    self.indent[0:poc_indice],
                                    self.force[0:poc_indice],
                                    self.stiffness['nb_parts'],
                                    self.stiffness['size_parts'],
                                    )
            tmp = curve.compute_stiffness(
                                x_parts,
                                y_parts,
                                model=self.stiffness['hertz_model'],
                                tip_carac=self.stiffness['point_carac'],
                                poisson_ratio=self.stiffness['poisson_ratio'],
                                method=self.stiffness['fit_method'])
            ## We complement the array in the case the newly computed stiffness
            ## contains more depth. That can happend when performing stiffness
            ## tomography.
            if self.__array['Stiffness'].shape[2] < len(tmp):
                array_shape = self.__array['Stiffness'].shape
                _to_fill = num.empty((array_shape[0], array_shape[1],
                                      len(tmp) - array_shape[2]))
                _to_fill[:, :, :] = num.nan
                self.__array['Stiffness'] = num.append(
                                                self.__array['Stiffness'],
                                                _to_fill, 2)
            elif self.__array['Stiffness'].shape[2] > len(tmp):
                array_shape = self.__array['Stiffness'].shape
                _to_fill = num.empty((array_shape[2] - tmp.shape[0]))
                _to_fill[:] = num.nan
                tmp = num.append(tmp, _to_fill, 0)
            ## Curves are in nm and nN. The result in tmp is then in nN/nm^2
            ## To convert the result in Pa, we multiply by 1e9
            self.__array['Stiffness'][self.pos_x, self.pos_y, ] = tmp * 1e9
            self.counter['num'] += self.counter['increment']
        ##
        ## Stores the result in a matrix that supports the mask, to get rid
        ## of the nan values
        ##
        self.counter['text'] = 'Store results'
        self.__array['Stiffness'] = num.ma.array(
                                    self.__array['Stiffness'],
                                    mask=num.isnan(self.__array['Stiffness']))
        self.exist['Stiffness'] = 1
        self.exist['PoC'] = 1
        self.go_first()
        self.counter['num'] = 1

    def compute_events(self):
        '''
        Computes the event of all the curves in the object. The event_list and
        event matrix (in array['Event']) are refreshed.
        '''
        # Set the switches to 0 in order to get all event
        _init_mask = self.get_switch('mask')
        _init_ev_dist = self.get_switch('ev_dist')
        self.set_switch('mask', False)
        self.set_switch('ev_dist', 0.0)
        self.counter['num'] = 0
        self.counter['text'] = "Detecting events"
        _number_of_curves = self.header['size'][0] * \
                            self.header['size'][1] + 1
        self.event_list = []
        self.__array['Event'] = num.zeros((self.header['size'][0],
                                        self.header['size'][0]))
        self.__array['Event force'] = num.zeros((self.header['size'][0],
                                                self.header['size'][0]))
        for pixels in self:
            det_event = self.current_curve('Event')
            if det_event:
                for event in det_event:
                    event['pos_x'] = self.pos_x
                    event['pos_y'] = self.pos_y
                    self.event_list.append(event)
                    self.__array['Event'][self.pos_x, self.pos_y] += 1
            self.counter['num'] = float(self.curve_nb) / _number_of_curves
        self.go_first()
        # Generate the random matrix.
        self.generate_random_event()
        self.counter['num'] = 1

        # Set back the original switches...
        self.set_switch('mask', _init_mask)
        self.set_switch('ev_dist', _init_ev_dist)

    def generate_random_event(self, nbr_event=None):
        if nbr_event is not None:
            self.event['nbr_random'] = nbr_event
        self.__array['Event random'] = rand_dot(self.__array['Event'],
                                              self.event['nbr_random'])

    def generate_mask(self, mtype, mdata, depth=0):
        """
            Generate a mask for the arrays.

            * Parameters :
                mtype: string
                    'Piezo': generate mask based to the piezo image array.
                    'Topography': generate mask based on the topography array.
                    'Stiffness': generate mask based on the stiffness array.
                    'Array': generate mask based on the mdata array.
                mdata: float | 2D numpy.array
                    float if mtype is 'piezo', 'topography', 'stiffness'
                    2D numpy.array if mtype is 'array'
                depht: int
                    In the case the array is 3d. It generates the mask from the
                    specified depth, array[:,:,depth].
                    Default value: 0
        """
        if mtype == 'Array':
            self.__array['Mask'] = mdata
        else:
            if self.__array[mtype].ndim == 3:
                self.__array['Mask'] = self.__array[mtype][:, :, depth] < mdata
            else:
                self.__array['Mask'] = self.__array[mtype] < mdata
        # Creating a mask automatically switch it on.
        self.set_switch('mask', True)

    def compute_relative_stiffness(self, exclude=0):
        '''
        Computes the relative stiffness of the events
        '''
        if self.__array['Event'] is None:
            raise NotReadyError('Event is not yet computed.')
        if self.__array['Stiffness'] is None:
            raise NotReadyError('Stiffness is not yet computed.')
        # event and stiffness have to be computed
        # TODO: change self.event_list to self.event['list']
        [self.event['rel_stiff'], coordinate] = rel_values(
                                        self.__array['Stiffness'][:, :, 0],
                                        self.__array['Event'],
                                        self.stiffness['relative_max_dist'],
                                        coord=True)
        self.event['rel_stiff_ctl'] = rel_values(
                                        self.__array['Stiffness'][:, :, 0],
                                        self.__array['Event random'],
                                        self.stiffness['relative_max_dist'])
        # Is it really necessary ??
        self.event_list = add_stiffness_to_event(self.event_list,
                                                 self.event['rel_stiff'],
                                                 coordinate)

    def compute_average_stiffness(self):
        """
            Computes the average stiffness in function of the depth.

            returns a dictionary with the deep, mean, std and count.

            >>> fv = ForceVolume(BIOSCOPE_FILE)
            >>> fv.compute_stiffness()
            >>> result = fv.compute_average_stiffness()
            >>> result.keys()
            ['std', 'count', 'deep', 'mean']
        """
        if self.__array['Stiffness'] is None:
            start = 0
            data_count = 0
        else:
            step = self.stiffness['size_parts']
            start = (self.__array['Stiffness'].shape[2] - 1) * step
            #raise NotReadyError, 'Stiffness is not yet computed'
            #start = (self.__array['Stiffness'].shape[2] -1) * step
            deep = range(start, -step, -step)
            stiffness = self.get_array('Stiffness')
            data_count = stiffness.count()
        if data_count:
            _result = [[deep.pop(), i.mean(), i.std(), i.count()] for i in
                                self.get_array('Stiffness').transpose(2, 0, 1)]
        else:
            _result = [[0, num.nan, num.nan, 0]]
        _result = num.ma.asarray(_result)
        return dict([(key, value) for key, value in
                                    zip(['deep', 'mean', 'std', 'count'],
                                         _result.transpose(1, 0))])

    def event_force_list(self, dtype='rel'):
        '''
        Returns the list of the event force
        '''
        force_list = []
        if dtype == 'rel':
            prop = 'force'
        elif dtype == 'baseline':
            prop = 'force_base'
        else:
            raise TypeError('dtype ' + prop + ' is not supported.')
        for event in self.event_list:
            force_list.append(event[prop])
        return force_list

    def number_event(self):
        return len(self.event_list)

    def event_lr_list(self):
        '''
        Returns the list of the loading rates
        '''
        lr_list = []
        for event in self.event_list:
            lr_list.append(event['loading_rate'])
        return lr_list

    def event_dist_list(self):
        '''
        Returns the list of the distance of the events
        '''
        dist_list = []
        for event in self.event_list:
            dist_list.append(event['dist'])
        return dist_list

    def event_stiff_list(self):
        '''
        Returns the list of the stiffness of the events
        '''
        stiff_list = []
        if self.__array['Stiffness'] is None:
            return [num.nan for i in range(self.number_event())]
        for event in self.event_list:
            if not self.__array['Stiffness'].mask[event['pos_x'],
                                                event['pos_y'], 0]:
                stiff_list.append(
                            self.__array['Stiffness'].data[event['pos_x'],
                            event['pos_y'],
                            0])
            else:
                stiff_list.append(num.nan)
        return stiff_list

    def event_rel_stiff_list(self, dist=1):
        '''
        Returns the list of the relative stiffness of the events
        '''
        stiff_list = []
        if self.event_list is None:
            return stiff_list
        elif not 'Stiffness' in self.event_list[0]:
            return [num.nan for i in range(self.number_event())]
        for event in self.event_list:
            if event['Stiffness'].size and not event['Stiffness'].mask[dist]:
                stiff_list.append(event['Stiffness'].data[dist])
            else:
                stiff_list.append(num.nan)
        return stiff_list

    def event_rand_rel_stiff_list(self, dist=1):
        '''
            Returns the list of the relative stiffness of the events.
        '''
        _list = []
        for item in self.event['rel_stiff_ctl'][dist]:
            if isinstance(item, num.ma.core.MaskedArray):
                _list.append(num.nan)
            else:

                _list.append(item)
        return _list

    def event_pos_list(self, dim=None):
        """
            Returns the position list of the events.
        """
        if dim in ['x', 'X']:
            pos_list = [event['pos_x'] for event in self.event_list]
        elif dim in ['y', 'Y']:
            pos_list = [event['pos_y'] for event in self.event_list]
        else:
            pos_list = [self.event_pos_list('x'), self.event_pos_list('y')]
        return pos_list

    def event_nbr_per_curve(self):
        """
            Returns the list of number of event per curves.

            >>> fv = ForceVolume(BIOSCOPE_FILE)
            >>> fv.compute_events()
            >>> nbr_event = fv.event_nbr_per_curve()
            >>> print nbr_event
            [79, 86, 55, 26, 10]

            This means that 79 pixels (or force curves) have 0 events, 85 have
            1, 56 have 2 26 have 3 and 10 force curves contains 4 events.

            * Parameters: None

            * Returns :
                nbr_events_list: list
                    The number of force curves that contains 0, 1, ... events.
        """

        _list = []
        for nbr_ev in range(int(self.__array['Event'].max()) + 1):
            with_current = num.nonzero(self.get_array('Event') == nbr_ev)
            _list.append(len(with_current[0]))
        return _list

    def get_event_prop(self, data_type, dist=1):
        """
            Get the specified properties of the event.

            * Parameters :
                data_type: str
                    Describe the event property to get.
                    'force': return the list of event forces.
                    'force_base': return the list of event forces, computed
                        relative to the baseline.
                    'lr': the loading rate.
                    'dist': the distance from the end of the indentation.
                    'stiff': the stiffness, computed from the corresponding
                        trace curve.
                    'stiff_ev': same as stiff, but not ordered, and one per
                        pixel. If a pixel contain two event, it will be
                        reported once.
                    'stiff_notev': list of stiffness on pixels with no event.
                    'relstiff': the list of relative stiffness on event.
                    'pos_x': the list of the x position in the array.
                    'pos_y': the list of the y position in the array.
                    'nbr_ev_per_curve': the list of the number of events per
                        force curves. From 0 event to the maximum detected.

                    'all': A dictionnary of list with all the parameters
                        related to events. This will not return list from
                        "stiff_ev" and "stiff_notev" as they are different.
                    'label': the label corresponding to "all". Usefull to have
                        an ordered label.
        """
        if data_type is 'force':
            return self.event_force_list()
        elif data_type is 'force_base':
            return self.event_force_list('baseline')
        elif data_type is 'lr':
            return self.event_lr_list()
        elif data_type is 'dist':
            return self.event_dist_list()
        elif data_type is 'stiff':
            return self.event_stiff_list()
        elif data_type is 'stiff_ev':
            return self.stiff_list('event')
        elif data_type is 'stiff_notev':
            return self.stiff_list('not event')
        elif data_type is 'relstiff':
            return [self.event_rel_stiff_list(dist) \
                    for dist in range(self.stiffness['relative_max_dist'] + 1)]
        elif data_type is 'nbr_ev_per_curve':
            return self.event_nbr_per_curve()
        elif data_type is 'length':
            return [event['fit_length'][0] for event in self.event_list]
        elif data_type is 'plength':
            return [event['fit_plength'][0] for event in self.event_list]
        elif data_type is 'label':
            return ['pos_x', 'pos_y', 'force', 'lr',
                    'dist', 'stiff', 'relstiff']
        elif data_type is 'all':
            return {'force': self.event_force_list(),
                    'lr': self.event_lr_list(),
                    'dist': self.event_dist_list(),
                    'stiff': self.event_stiff_list(),
                    'relstiff': self.event_rel_stiff_list(),
                    'pos_x': self.event_pos_list('x'),
                    'pos_y': self.event_pos_list('y')}

    def stiff_list(self, dtype, depth=0):
        """
            Get the list of the stiffness at the given depth.

            dtype = string
                'all'
                    to have stiffness of all pixels
                'event'
                    to have stiffness of event pixels
                'not event'
                    to have stiffness of pixels with no event
                'random'
                    to have stiffness of randomly selected no event pixels

            The difference between event_stiff_list() and stiff_list('event')
            is that for the first, there is the list of stiffness for each
            events. Which means there can be two stiffness that belongs to the
            same pixel. For the second (stiff_list('event')), the list does not
            contain twice the same pixel value.

            For example :

            >>> fv = ForceVolume(BIOSCOPE_FILE)
            >>> fv.compute_events()
            >>> fv.compute_stiffness()

            To get stiffness of pixels where no event were detected :

            >>> stiffness_list = fv.stiff_list('not event')

            To get stiffness of pixels with event :

            >>> event_stiffness_list = fv.stiff_list('event')

            To get stiffness of all pixels :

            >>> all_stiffness_list = fv.stiff_list('all')

            Just to verify that the sum of list with and without event give the
            correct length :

            >>> len(event_stiffness_list) + len(stiffness_list) == \
                                                        len(all_stiffness_list)
            True

            ... and so on.
        """
        array = self.get_array('Stiffness')

        depth = int(depth)
        #print('Line 1165 class.py Depth = %f, type')%depth
        if array is None:
            return []
        else:
            try:
                array = array[:, :, depth]
            except IndexError:
                array = num.ma.array(num.zeros(array.shape[:2]),
                                       mask=num.ones(array.shape[:2]))
                array[:] = num.nan
        if dtype == 'all':
            cache = num.ones(array.shape)
        elif dtype == 'event':
            cache = self.get_array('Event') > 0
        elif dtype == 'random':
            cache = self.get_array('Event random') > 0
        elif dtype == 'not event':
            cache = self.get_array('Event') == 0
        else:
            raise TypeError(dtype + ' is not recognized.')
        # Want's negative mask (initially, 1 is masked and 0 is visible)
        mask = array.mask == False
        # * is the same as 'and' but 'and' operator does not work
        mask = cache * mask
        pix_indice = num.nonzero(mask)
        stiffness_list = [array[x, y] for x, y in zip(pix_indice[0],
                                                      pix_indice[1])]

        return stiffness_list

    def event_gauss_fit(self, data_type, min_val, max_val, dist=1):
        '''
        Automatically finds the best gaussian fit of the event properties.

        data_type can be :

            * 'force' to considere the event forces.
            * 'lr' to considere the event loading rates.
            * 'distance' to considere the event distance in the curve
            * 'stiff' to considere the stiffness in the trace curve where event
              was detected.
            * 'relstiff' to considere the relative stiffness of the pixel where
              event was detected.

        Results are stored in event_stat[data_type] attribute.

        For example :

        >>> fv = ForceVolume(BIOSCOPE_FILE)
        >>> fv.compute_events()
        >>> fv.event_gauss_fit('force', min_val=0, max_val=1)
        >>> fv.event_stat['force'].keys()
        ['std', 'kde', 'maxima', 'count', 'mean']

        * kde is the kernel density estimation. This is an array composed of
          two vector, respectively the x and y coordinate.
        * 'mean' is the list of detected gaussian mean
        * 'std' is the corresponding standard deviation.
        * 'count' is the corresponding number of elements for the mean and std.
        * 'maxima' is the detected maxima from the kde.
        '''

        fit, mean, std, count = None, None, None, None
        if data_type == 'force':
            values = num.asanyarray(self.event_force_list())
        elif data_type == 'lr':
            values = num.asanyarray(self.event_lr_list())
        elif data_type == 'distance':
            values = num.asanyarray(self.event_dist_list())
        elif data_type == 'stiff':
            values = num.asanyarray(self.event_stiff_list())
        elif data_type == 'relstiff':
            values = num.asanyarray(self.event_rel_stiff_list(dist))
        else:
            raise StandardError(data_type + ' is not recognize.')
        index = (min_val < values) * (values < max_val)
        self.event_stat[data_type] = post_proc.find_gauss_fit(values[index])

    def to_plot(self, curve_type):
        '''
        Returns the curves to plot
        '''
        if curve_type in self.plot_who:
            return self.plot_who[curve_type]
        else:
            raise KeyError

    def set_to_plot(self, curve_type, value):
        '''
        Sets the plotting status of the curve type
        '''
        if curve_type in self.plot_who:
            self.plot_who[curve_type] = value
        else:
            raise KeyError

    def stiffness_tomography_array(self):
        '''
        Generates the stiffness tomography array
        '''
        floor = num.floor(self.__array['Piezo'] / self.stiffness['size_parts'])

        self.__array['Stiffness tomo'] = post_proc.tomography_array(
                                                self.__array['Stiffness'],
                                                floor=floor)

    def add_point(self, fdcurves, piezo):
        '''
        Add a force distance curve to the file.
        '''
        # Calculate the shape of the new array, after we add a new point.
        nbr_line = num.round(num.sqrt(self.header['number_curves'] + 1))
        nbr_col = num.ceil(num.sqrt(self.header['number_curves'] + 1))
        if (nbr_line != self.header['size_x']) or (nbr_col !=
                                                   self.header['size_y']):
            self.__fdcurves['trace_y'] = resize_array(
                                self.__fdcurves['trace_y'],
                                (nbr_line, nbr_col,
                                 self.header['force_samples_per_curve']))
            self.__fdcurves['retrace_y'] = resize_array(
                                self.__fdcurves['retrace_y'],
                                 (nbr_line, nbr_col,
                                  self.header['force_samples_per_curve']))
            if self.__fdcurves['trace_x'].ndim == 3:
                self.__fdcurves['trace_x'] = resize_array(
                                self.__fdcurves['trace_x'],
                                (nbr_line, nbr_col,
                                 self.header['force_samples_per_curve']))
                self.__fdcurves['retrace_x'] = resize_array(
                                self.__fdcurves['retrace_x'],
                                (nbr_line, nbr_col,
                                 self.header['force_samples_per_curve']))
            self.__array['Piezo'] = resize_array(self.__array['Piezo'],
                                               (nbr_line, nbr_col))

        # The coordinate of the added point.
        pos_x = (self.header['number_curves']) % nbr_line
        pos_y = (self.header['number_curves']) / nbr_line
        # Add the new point.
        self.__fdcurves['trace_y'][pos_x, pos_y] = fdcurves['trace_y'][0, 0]
        self.__fdcurves['retrace_y'][pos_x, pos_y] = \
                                                    fdcurves['retrace_y'][0, 0]
        if self.__fdcurves['trace_x'].ndim == 3:
            self.__fdcurves['trace_x'][pos_x, pos_y] = \
                                                    fdcurves['trace_x'][0, 0]
            self.__fdcurves['retrace_x'][pos_x, pos_y] = \
                                                    fdcurves['retrace_x'][0, 0]
        self.__array['Piezo'][pos_x, pos_y] = piezo
        # Update the data.
        self.header['number_curves'] += 1
        self.header['matrix_length'] = int(nbr_line)
        self.header['size_x'] = int(nbr_line)
        self.header['size_y'] = int(nbr_col)

    def export_map_stiffness(self, file_name, depth=None):
        title = 'Stiffness File ' + self.name
        plot_array = plot.Array(title=title)
        if depth == None:
            #export all depth
            plot_array.color_scale['min'] = 0
            plot_array.color_scale['max'] = self.__array['Stiffness'].max()
            depth = 0
            for item in self.__array['Stiffness'].transpose(2, 0, 1):
                plot_array.plot(item, scan_size=self.header['scan_size'])
                plot_array.title = '%s, depth %03f nm' % (title,
                                                self.stiffness['size_parts'])
                plot_array.save('%s%03i.png' % (file_name, depth))
                depth += 1

    def export_curve(self, file_name, direction='trace'):
        """
            Export the current curve to a csv file.

            * Parameters :
                file_name: str
                    The complete path and file name to save the curve in.
                direction: str
                    The direction of the curve. This can be: 'trace',
                    'retrace', 'both'. Default is 'trace'

            * Returns :
                None

            >>> file_name = EXPORT_FOLDER + '/export_curve.csv'
            >>> fv_file = ForceVolume(BIOSCOPE_FILE)
            >>> fv_file.export_curve(file_name, 'trace')
            >>> file_name = EXPORT_FOLDER + '/export_curve_both.csv'
            >>> fv_file.export_curve(file_name, 'both')
        """
        legend = []
        curves = []
        if direction in ['trace', 'both']:
            legend.append('trace x')
            curves.append(self.trace_x)
            legend.append('trace y')
            curves.append(self.trace_y)
        if direction in ['retrace', 'both']:
            legend.append('retrace x')
            curves.append(self.retrace_x)
            legend.append('retrace y')
            curves.append(self.retrace_y)

        preambule = ['Force distance curve of file :' + self.name,
                    'Curve [' + str(self.pos_x) + ': ' + str(self.pos_y) + ']']
        curves = misc.export_list(file_name, curves,
                                  legend, preambule, transpose=True)

    def export_events(self, to_file, firstfile=False):
        """
            Export the event properties to a csv file.

            * Parameters :
                to_file: str or csv.writer object
                    If str, it describes the file pathname.
                    If csv.writer object, the function just adds events
                    properties of this ForceVolume object at the end of the
                    file.
                firstfile: bool (default: False)
                    If True, will add labels to the first line. This has
                    incidence only whith csv.writer object as to_file.

            * Retruns :
                None.

            >>> file_name = EXPORT_FOLDER + '/export_event.csv'
            >>> fv_file = ForceVolume(BIOSCOPE_FILE)
            >>> fv_file.compute_events()
            >>> fv_file.export_events(file_name)
        """
        nbr_evt = self.number_event()
        evt_list = self.get_event_prop('all')
        legend = self.get_event_prop('label')
        if type(to_file) == str:
            # Create the file.
            fid = open(to_file, 'wb')
            fcsv = csv.writer(fid)
        elif str(type(to_file)) == "<type '_csv.writer'>":
            # we got a csv writer, then directly write on it.
            fcsv = to_file
        if type(to_file) == str or firstfile:
            fcsv.writerow(['filename'] + legend)
        # Now, complement the fcsv...
        for item in range(nbr_evt):
            fcsv.writerow([self.name] + [evt_list[leg][item]
                                                        for leg in legend])
        if type(to_file) == str():
            fid.close()

    def export_nbr_events(self, to_file, firstfile=False):

        nbr_evt = self.get_event_prop('nbr_ev_per_curve')
        legend = ['nbr_evt', 'nbr_curves']
        if type(to_file) == str:
            # Create the file
            fid = open(to_file, 'wb')
            fcsv = csv.writer(fid)
        elif str(type(to_file)) == "<type '_csv.writer'>":
            # we got a csv writer, then, directly write on it.
            fcsv = to_file
        if type(to_file) == str or firstfile:
            fcsv.writerow(['filename'] + legend)
        # Now, complement the fcsv...
        for nbr_curve, nbr_evt in zip(nbr_evt, range(len(nbr_evt))):
            fcsv.writerow([self.name, nbr_evt, nbr_curve])
        if type(to_file) == str():
            fid.close()

    def test_fct(self):
        pass


class FVFolder(object):
    '''
    In this class are stored all the ForceVolume files of a single experiment.

    Important attribute that are read-write accessible :

        filename
            Stores the name of the original aex file.
        short_name
            Same as filename, but without the tree.
        file
            Dictionnary that contains informations about the stored files :

            'list'
                is the list of stored files.
            'name'
                is the name of stored files.
            'group'
                is the group to which files respectively belongs to.
        data_hist
            Is a PlotData instance. Used to plot histograms and scatter with
            plot_generic module.
        author
            String informing about the author of the experiment.
        comment
            String that contains information usefull for user.
        date
            Datetime instance. Stores the creation date.
        counter, counter_text
            Integer and string to be displayed in the progressbar for the gui.
        parameters
            Dictionnary that contains informations about some parameters :

            'event_detect_weight'
                Float. [0:inf] How much the noise has to be considered for the
                event detection. 0 means no influence (detection won't work)
            'rel_stiff_dist'
                Integer. Is the maximum distance to consider for the relative
                stiffness computation.
            'PoC_threshold'
                Float. [0:inf] Threshold to detect the point of contact.
            'spring_constant'
                Float. Is the spring constant of the used cantilever.
            'glass'
                Float. Is the correction for the slope recorded on stiff
                substrate.
            'injection_index' [Deprecated]
                Integer. Is the index of the first file after injection of
                chemical.
            'number_curves'
                Integer. The total number of curves in the experiment.
        event_stat
            Dictionnary that contais event properties, ordered.

            'force', 'lr', 'dist', 'stiff', 'rel_stiff'
                Are list containing the computed force, loading rate, distance
                from the end of indentation, stiffness and relative stiffness
                respectively
        relative_stiffness
            Dictionnary that contains information about relative stiffness.

            'data'
                List containing the relative stiffness of event.
            'data_ctl'
                List containing the relative stiffness of randomly selected
                pixels
            'indice'
                First file after chemical injection.

    >>> #fv_folder = FVFolder()
    >>> #fv_folder.load()
    '''

    def __init__(self):
        # TODO eliminate self.relative_stiffness['mean'] or use it...

        self.filename = None  # stores the filename of aex file where it is
                              # saved
        self.short_name = None  # the short name to display
        self.file = {'list': [],
                     'name': [],
                     'group': [],
                     'current': 0}  # Stores the indice of the current used
                                    # file.
        self.group = {'id': [],  # List of int. The id of the group.
                      'label': [],  # List of str. Label corresponding to
                                    # group to be displayed in the plots.
                      'display': []}  # List of bool. Informs wether to display
                                      # or not the correspinding group.
        self.data_hist = PlotData()
        self.author = 'Author name'
        self.comment = 'Your comment'
        self.date = datetime.now()
        self.counter = {'num': 0,
                        'text': 'Please wait ...'}
        self.parameters = {
            'event_detect_weight': 2,
            # The fit use for the events (can be wlc, fjc or None)
            'event_fit_model': None,
            'rel_stiff_dist': 3,
            'PoC_threshold': 2,
            'spring_constant': None,
            'glass': None,
            'injection_index': 0,
            'number_curves': 0,
            'histo_y_rel': False,  # The y axis in histogram. If False, the
            # histogram y axis reflects the number of data. If true, it
            # reflects the frequency of the data.
            'plot_size': [800, 600],  # The size, in pixel, of the plot to
                                      # save.
            'plot_hist_gauss': True,
        }
        self.event_stat = {'force': None,
                           'lr': None,
                           'dist': None,
                           'stiff': None,
                           'rel_stiff': None}
        self.relative_stiffness = {'data': None,
                                    'data_ctl': None,
                                    'indice': None}
        # The list of switch to enable or disable functions. Use self.switch()
        # to change the state.
        self.switch_dict = {'mask': 'No',
                            'all': 'No'}

    def __setattr__(self, att, value):
        if att is 'PoC_threshold':
            for fv in self.file['list']:
                fv.stiffness['poc_threshold'] = value
            self.parameters['PoC_threshold'] = value
        else:
            self.__dict__[att] = value

    def __getattr__(self, att):
        '''
        Called if requested attribute does not exist.
        '''
        if att == 'event_force':
            __list = []
            for force_volume in self.file['list']:
                __list = __list + force_volume.event_force_list()
            self.event_force = __list
            return self.event_force
        elif att == 'event_distance':
            __list = []
            for force_volume in self.file['list']:
                __list = __list + force_volume.event_dist_list()
            self.event_distance = __list
            return self.event_distance
        elif att == 'event_lr':
            __list = []
            for force_volume in self.file['list']:
                __list = __list + force_volume.event_lr_list()
            self.event_lr = __list
            return self.event_lr
        elif att == 'event_stiffness':
            __list = []
            for force_volume in self.file['list']:
                __list = __list + force_volume.event_stiff_list()
            self.event_stiffness = __list
            return self.event_stiffness
        elif att == 'event_relstiff':
            __list = []
            for dist in range(self.parameters['rel_stiff_dist'] + 1):
                __dist = []
                for force_volume in self.file['list']:
                    __dist = __dist + force_volume.event_rel_stiff_list(dist)
                __list.append(__dist)
            self.event_relstiff = __list
            return self.event_relstiff
        elif att == 'average_stiffness':
            """
                This is the average stiffness in function of the depth
            """
            return [fv.compute_average_stiffness() for fv in self.file['list']]
        else:
            print att
            raise AttributeError

    def get_file(self, nbr):
        """
            Get the forcevolume file from the list.

            * Parameters :
                nbr: int
                    The index number of the force volume object.
        """
        self.file['current'] = nbr
        self.update_switch()
        return self.file['list'][nbr]

    def get_masked_list(self):
        """
            Get the list of masked switch for each forcevolume files.

            fvfolder.get_masked_list()
            [True, False, False, False, True, True]
        """

        return [fv.get_switch('mask') for fv in self.file['list']]

    def get_event_att(self, att):
        if att in ['force', 'force_base', 'dist', 'stiff_ev', 'stiff_notev',
                   'lr', 'stiff', 'length', 'plength']:
            __list = []
            __group = []
            __fid = []
            __coord = None  # to be done...
            for force_volume, group in zip(self.file['list'],
                                           self.file['group']):
                __add_to_list = force_volume.get_event_prop(att)
                __add_to_group = [group for i in range(len(__add_to_list))]

                __this_fid = self.file['list'].index(force_volume)
                __add_to_fid = [__this_fid for i in range(len(__add_to_list))]

                __list = __list + __add_to_list
                __group = __group + __add_to_group
                __fid = __fid + __add_to_fid
        if att in ['force', 'force_base']:
            self.event_force = __list
        elif att == 'dist':
            self.event_distance = __list
        elif att == 'lr':
            self.event_lr = __list
        elif att == 'stiff':
            self.event_stiffness = __list
        elif att == 'length':
            self.event_length = __list
        elif att == 'plength':
            self.event_plength = __list
        elif att in ['relstiff', 'relstiffctl']:
            __list = []
            __group = []
            __fid = []
            __coord = None  # to be done...
            for dist in range(self.parameters['rel_stiff_dist'] + 1):
                __dist = []
                __dist_group = []
                __dist_fid = []
                # __dist_coord = []
                for force_volume, group in zip(self.file['list'],
                                               self.file['group']):
                    if att == 'relstiff':
                        __add_to_list = force_volume.event_rel_stiff_list(dist)
                    elif att == 'relstiffctl':
                        __add_to_list = \
                                   force_volume.event_rand_rel_stiff_list(dist)
                        __add_to_list = list(__add_to_list)
                    __add_to_group = [group for i in range(len(__add_to_list))]
                    __this_fid = self.file['list'].index(force_volume)
                    __add_to_fid = [__this_fid
                                    for i in range(len(__add_to_list))]
                    __dist = __dist + __add_to_list
                    __dist_group = __dist_group + __add_to_group
                    __dist_fid = __dist_fid + __add_to_fid
                __list.append(__dist)
                __group.append(__dist_group)
            self.event_relstiff = __list
            __fid = __dist_fid
        return [__list, __group, __fid, __coord]

    def load_file(self, fv_file):
        '''
        Loads the file specified
        '''
        self.file['list'].append(ForceVolume(fv_file))
        self.file['list'].sort()
        self.__regenerate_filename_list()
        self.parameters['spring_constant'] = \
                            self.file['list'][0].header['spring_constant']
        self.parameters['glass'] = \
                            self.file['list'][0].stiffness['glass']

    def load_folder(self, fv_folder):
        '''
        Loads all the files in a folder
        '''
        list_files = os.listdir(fv_folder)
        self.counter['num'] = 0
        counter_total = len(list_files) + 1
        counter_this = 0
        for files in list_files:
            self.counter['text'] = 'Loading ' + files + '...'
            try:
                self.load_file(os.path.join(fv_folder, files))
            except TypeError as detail:
                if detail.args[0] == "Not a supported AFM file format.":
                    pass
                elif 'asylum' in detail.args[0]:
                    pass
                else:
                    raise TypeError(detail)
                counter_this += 1
            finally:
                counter_this += 1
                self.counter['num'] = float(counter_this) / counter_total
        self.file['list'].sort()
        try:
            self.convert_v6()
        except KeyError:
            pass
        self.__regenerate_filename_list()
        self.__regenerate_comment()
        self.counter['num'] = 1

    def convert_v6(self):
        new_file_list = []
        container = None
        self.counter['text'] = 'Reshaping data from V6...'
        for counter_this in range(1, len(self.file['list'])):
            if (6100000 <
                self.file['list'][counter_this].header['di_version'] <
                6200000):
                name0 = os.path.splitext(self.file['list']
                                                    [counter_this - 1].name)[0]
                name1 = os.path.splitext(self.file['list']
                                                    [counter_this].name)[0]
                if name0 == name1:
                    if container is None:
                        container = self.file['list'][counter_this - 1]
                    container = self.merge_single(container,
                                            self.file['list'][counter_this])
                else:  # A new serie is comming
                    new_file_list.append(container)
                    container = self.file['list'][counter_this]
        new_file_list.append(container)
        new_file_list.sort()
        if container is not None:
            self.file['list'] = new_file_list

    def merge_single(self, container, fv_file):
        '''
        Merge force volume files that contains only one fv curve.
        The first fv_file is the container that can contain already several
        force curve.
        The second is the force curve that will be added.
        '''
        ## Variable to update :
        ##
        if fv_file._ForceVolume__fdcurves['trace_y'].shape[0:2] == (1, 1):
            container.add_point(fv_file._ForceVolume__fdcurves,
                                fv_file.get_array('Piezo')[0, 0])
        return container

    def load_aex(self, aex_file):
        '''
        Loads a AFM Experiment XML file
        '''
        self.filename = aex_file
        self.short_name = os.path.split(aex_file)[1]
        self.counter['num'] = None
        self.counter['text'] = 'Uncompressing files ...'
        [experiment, file_tree] = aex.load(aex_file)
        self.author = experiment['author']
        self.comment = experiment['comment']
        self.date = experiment['date']
        self.parameters = experiment['parameters']
        self.counter['text'] = 'Loading files ...'
        for item in file_tree:
            self.load_file(item)
        self.file['list'].sort()
        self.__regenerate_filename_list
        self.counter['num'] = 1
        self.update_switch()
        if len(experiment['group']['id']):
            self.group = experiment['group']

    def save(self, file_name):
        self.filename = file_name
        self.short_name = os.path.split(file_name)[1]
        self.counter['num'] = None
        self.counter['text'] = 'Saving files ...'
        aex.save(self, file_name)
        self.counter['num'] = 1

    def set_parameters(self, param, value):
        """
            Change the parameters of the force volume files in the experiments.

            * Parameters :
                param: str
                    Defines which parameter to change.
                    'spring_constant'
                    'glass'
                    'poisson_ratio'
                value: depending on the parameters.
                    float if 'spring_constant', 'glass' or 'poisson_ratio'
        """
        if param == 'spring_constant':
            for fc in self.file['list']:
                fc.header['spring_constant'] = value
            self.parameters['spring_constant'] = value
        elif param == 'glass':
            for fv in self.file['list']:
                fv.stiffness['glass'] = value
            self.parameters['glass'] = value
        elif param == 'poisson_ratio':
            if self.get_switch('all') == 'Yes':
                [fv.change_poisson_ratio(value) for fv in self.file['list']]
            else:
                self.file['list'][self.file['current']].change_poisson_ratio(
                                                                        value)
        elif param == 'tip_carac':
            if self.get_switch('all') == 'Yes':
                [fv.set_parameters('tip_carac', value)
                                                for fv in self.file['list']]
            else:
                self.file['list'][self.file['current']].set_parameters(
                                                            'tip_carac', value)
        elif param == 'plot_size':
            self.parameters['plot_size'] = [int(value[0]), int(value[1])]
        elif param == 'plot_hist_gauss':
            # bool to display gauss fit on histograms.
            if isinstance(value, bool):
                self.parameters['plot_hist_gauss'] = value
            elif isinstance(eval(value), bool):
                self.parameters['plot_hist_gauss'] = eval(value)
            else:
                raise AttributeError(
                        '%s is not a correct type for this parameter' %
                        type(value))

    def set_group(self, group_list):
        """
            Change the group appartenance for the fv files, and update the
            group list.
        """

        for fv, group in zip(self.file['list'], group_list):
            fv.header['group'] = group
        self.__regenerate_filename_list()

    def set_group_prop(self, prop, which, value):
        """
            Change the properties of the group.

            * Parameters :
                prop: str
                    'label': to change the label
                    'visible': to switch the visibility of the group
                which: int
                    The group id to change. if -1, will change all the label
                value: str or bool
                    if prop is label: str
                    if prop is visible: True or False
                    if which is -1, need a list of prop. the length has to be
                                    the same as the number of groups.
        """
        if prop not in ['label', 'display']:
            raise TypeError('%s is not recognized property.' % prop)
        if which is not -1:
            if prop == 'label' and type(value) is not str:
                raise TypeError('Property %s expect string value.' % prop)
            if prop == 'display' and type(value) is not bool:
                raise TypeError('Property %s expect boolean value' % prop)
            # we change only one group property
            index = self.group['id'].index(which)
            self.group[prop][index] = value
        if which == -1:
            if type(value) is not list:
                raise TypeError('Expect list of values.')
            for [gid, gval] in zip(self.group['id'], value):
                self.set_group_prop(prop, gid, gval)

    def compute_relative_stiffness(self, exclude=0):
        # initialize temporary variables
        tmp_indice = num.zeros(len(self.file['list']))
        max_dot = 0
        max_rand = 0
        for fvfile in self.file['list']:
            max_dot = max((fvfile.get_array('Event') > 0).sum(), max_dot)
            max_rand = max((fvfile.get_array('Event random') > 0).sum(),
                           max_dot)

        array_shape = (len(self.file['list']),
                       self.parameters['rel_stiff_dist'] + 1,
                       max_dot)
        self.relative_stiffness['data'] = num.ma.array(
                                                num.empty(array_shape),
                                                mask=num.ones(array_shape))
        array_shape = (len(self.file['list']),
                       self.parameters['rel_stiff_dist'] + 1,
                       max_rand)
        self.relative_stiffness['data_ctl'] = num.ma.array(
                                                num.empty(array_shape),
                                                mask=num.ones(array_shape))
        file_nbr = 0
        for fvfile in self.file['list']:
            fvfile.change_relative_distance(self.parameters['rel_stiff_dist'])
            fvfile.compute_relative_stiffness(exclude)
            t_shape = fvfile.event['rel_stiff'].shape

            self.relative_stiffness['data'].data[file_nbr,
                                                 :t_shape[0],
                                                 :t_shape[1]] = \
                                            fvfile.event['rel_stiff'].data
            self.relative_stiffness['data'].mask[file_nbr,
                                                 :t_shape[0],
                                                 :t_shape[1]] = \
                                            fvfile.event['rel_stiff'].mask

            t_shape = fvfile.event['rel_stiff_ctl'].shape
            self.relative_stiffness['data_ctl'].data[file_nbr,
                                                     :t_shape[0],
                                                     :t_shape[1]] = \
                                            fvfile.event['rel_stiff_ctl'].data
            self.relative_stiffness['data_ctl'].mask[file_nbr,
                                                     :t_shape[0],
                                                     :t_shape[1]] = \
                                            fvfile.event['rel_stiff_ctl'].mask
            if file_nbr < self.parameters['injection_index']:
                tmp_indice[file_nbr] = 0
            else:
                tmp_indice[file_nbr] = 0
            file_nbr += 1
        self.relative_stiffness['indice'] = num.array(tmp_indice)
        self.relative_stiffness['data'] = \
                            self.relative_stiffness['data'].transpose(0, 2, 1)
        self.relative_stiffness['data_ctl'] = \
                        self.relative_stiffness['data_ctl'].transpose(0, 2, 1)

    def generate_mask(self, mtype, mdata):
        """
            See FV classe.
        """

        for fvfile in self.file['list']:
            fvfile.generate_mask(mtype, mdata)

    def update_switch(self):
        """
            Update the switches.
        """

        convbool = {True: 'Yes', False: 'No'}
        if self.switch_dict['all'] == 'Yes':
            # We update other switches to reflect the whole experiment.
            #
            ## First the mask :
            mask = [fv.get_switch('mask') for fv in self.file['list']]
            if mask.count(True) == len(mask):
                self.switch_dict['mask'] = 'Yes'
            elif mask.count(False) == len(mask):
                self.switch_dict['mask'] = 'No'
            else:
                self.switch_dict['mask'] = 'Some'
            ## Second, the event distance threshold :
            ev_dist = [fv.get_switch('ev_dist') for fv in self.file['list']]
            if ev_dist.count(ev_dist[0]) == len(mask):
                self.switch_dict['ev_dist'] = ev_dist[0]
            else:
                self.switch_dict['ev_dist'] = '~~'
            ## Third, the event lenght computed from the fit :
            curr_switch = [fv.get_switch('ev_fit_length')
                                for fv in self.file['list']]
            if curr_switch.count(curr_switch[0]) == len(curr_switch):
                self.switch_dict['ev_fit_length'] = curr_switch[0]
            else:
                self.switch_dict['ev_fit_length'] = '~~'
        if self.switch_dict['all'] == 'No':
            mask = self.file['list'][self.file['current']].get_switch('mask')
            self.switch_dict['mask'] = convbool[mask]
            self.switch_dict['ev_dist'] = \
                self.file['list'][self.file['current']].get_switch('ev_dist')
            self.switch_dict['ev_fit_length'] = \
                self.file['list'][self.file['current']].get_switch(
                                                            'ev_fit_length')

    def get_switch(self, what):
        if what == 'all':
            return self.switch_dict['all']
        elif what == 'mask':
            self.update_switch()
            return self.switch_dict['mask']
        elif what == 'ev_dist':
            self.update_switch()
            return self.switch_dict['ev_dist']
        elif what == 'ev_fit_length':
            self.update_switch()
            return self.switch_dict[what]

    def set_switch(self, stype, state):
        """
            Switch to True or False several stuffs or indicate the state of
            internal components.

            * Parameters:
                stype: str
                    'all': Switch the FVFolder to consider one or all the
                            forcevolume object.
                    'ev_dist': Threshold on the distance where event occured.
                    'ev_fit_length': Threshold on the length of event.
                state: str
                    if stype is 'all' :
                        'Yes' if the feature is enabled.
                        'No' if not.
                    if stype is 'ev_dist':
                        float
                    if stype is 'ev_fit_length' :
                        [float, float]: first is the min, second is the max.
        """
        if not stype in ['all', 'ev_dist', 'ev_fit_length']:
            raise AttributeError(stype +
                                 ' is not recognized as a FVFolder switch.')

        if stype == 'all':
            convbool = {True: 'Yes', False: 'No'}
            state = convbool[state]
            self.switch_dict[stype] = state
        elif stype in ['ev_dist', 'ev_fit_length']:
            if self.get_switch('all') == 'Yes':
                [fv.set_switch(stype, state) for fv in self.file['list']]
            else:
                self.file['list'][self.file['current']].set_switch(stype,
                                                                   state)
        self.update_switch()

    def set_data_hist(self, dtype, drange=None, depth=0):
        """
            Sets the plot data type and refresh it.

            * Parameters :
                dtype: str
                    The data type that has to be set on the plot_data instance.
                depth: int
                    For data dependent to depth. Specify the depth at which the
                    data has to be taken.

            >>> exp = FVFolder()
            >>> exp.load_aex(STIFFNESS_COMPUTED_FILE)
            >>> #fv = exp.get_file(1)
            >>> exp.set_data_hist('young_modulus', \
                                  drange=[0, 100000000, 10000000])
            >>> exp.set_data_hist('young_modulus', drange=[0, 1000000, 100000])
        """
        self.__regenerate_filename_list()
        if dtype == 'young_modulus':
            flat_list = []
            flat_group = []
            if self.get_switch('all') == 'Yes':
                stiff_list = self.get_stiffness_list('all', depth)
                file_group = self.file['group']
                for i in range(len(stiff_list)):
                    flat_list += stiff_list[i]
                    flat_group += [num.unique(file_group)[i] \
                                   for nb in range(len(stiff_list[i]))]
                self.data_hist.set_group_prop('all', self.group)
            else:
                fvfile = self.file['list'][self.file['current']]
                flat_list = fvfile.stiff_list('all', depth)
                flat_group = [0 for i in range(len(flat_list))]
                self.data_hist.set_group_prop('all', None)
            self.data_hist.set_data('stiff', flat_list)
            self.data_hist.set_group('stiff', flat_group)
            if drange == None:
                drange = [0, max(flat_list) / 10, max(flat_list) / 500]
            self.data_hist.set_range('stiff', drange)
            self.data_hist.set_type('stiff')
            self.event_gauss_fit('stiff')
            self.data_hist.nbr_curves = self.parameters['number_curves']

    def get_rel_stiff(self, dist):
        """
            Returns the dictionnary of relative stiffness at the desired
            distance. It gives it in a format that is friendly to plot with
            plot_generic.
        """
        rl_dict = self.relative_stiffness
        to_return = dict()
        for key in rl_dict.keys():
            to_return[key] = rl_dict[key][dist]
        return to_return

    def get_relative_stiffness_random(self, dist):
        """
            Returns the dictionnary of relative stiffness at the desired
            distance. It gives it in a format that is friendly to plot with
            plot_generic.
        """
        to_return = dict()
        rl_dict = self.relative_stiffness_random
        for key in rl_dict.keys():
            to_return[key] = rl_dict[key][dist]
        return to_return

    def get_arrays(self, which):
        '''
        Returns the list of the desired arrays:

        which :
          String definding the array to get. Possible values are :

          * 'Event' to get the event array
          * 'Topography' to get the topography array
        '''
        if which not in self.file['list'][0].get_array('keys'):
            raise TypeError('"' + which + '" is not supported')
        array_list = list()
        for fvfile in self.file['list']:
            array_list.append(fvfile.get_array(which))
        return array_list

    def get_stiffness_list(self, dtype, depth=0):
        """
            Get the list of the stiffness of the files contain in the
            experiment.

            It takes advantage of the group definition.

            Returned list is a 2d list which look like this :
            [Stiffness list group 1, Stiffness list group 2, ...]

            Files belonging to group 0 are not
            considered.

            >>> exp = FVFolder()
            >>> exp.load_aex(ALL_COMPUTED_FILE)
            >>> stiff_list = exp.get_stiffness_list('event')
            >>> stiff_list2 = exp.get_stiffness_list('not event')

            In our example file, we have 5 + 191 pixels with no computed
            stiffness for the first depth. Then, we would expect
            (16 * 16) + (32 * 32) - 5 - 191 = 1078 number of points

            >>> len(stiff_list[0]) + len(stiff_list2[0])
            1084

        """
        if sum(self.file['group']) == 0:
            # no group was defined, so do not consider it.
            group = [1 for item in self.file['group']]
        else:
            group = self.file['group']
        glist = list(num.unique(group))
        stiff_list = [[] for item in glist]
        for fvfile, gid in zip(self.file['list'], group):
            # gid means group id...
            index = glist.index(gid)
            for item in fvfile.stiff_list(dtype, depth):
                stiff_list[index].append(item)
            #stiff_list[index].append(fvfile.get_stiffness_list(dtype))
        return stiff_list

    def event_nbr_list(self):
        '''
        Retuns a list containing the number of detected event per files.
        '''

        event_list = []
        for fv in self.file['list']:
            event_list.append(fv.number_event())
        return event_list

    def clear_event_prop_list(self):
        '''
        Clear the event properties.
        To do to refresh the list if a new computation was done.
        '''

        att_list = ['event_force', 'event_distance', 'event_lr',
                    'event_stiffness', 'event_relstiff']
        for item in att_list:
            if hasattr(self, item):
                delattr(self, item)

    def event_gauss_fit(self, data_type=None, min_val=None, max_val=None,
                        dist=1):
        '''
            Automatically finds the best gaussian fit of the event properties.

            data_type can be :

                * 'force' to considere the event forces.
                * 'lr' to considere the event loading rates.
                * 'distance' to considere the event distance in the curve
                * 'stiff' to considere the stiffness in the trace curve where
                  event was detected.
                * 'relstiff' to considere the relative stiffness of the pixel
                  where event was detected.

            Results are stored in event_stat[data_type] attribute.

            For example :

            >>> exp = FVFolder()
            >>> exp.load_aex(EVENT_COMPUTED_FILE)
            >>> exp.data_hist.set_range('force', [0, 1, 0.1])
            >>> exp.data_hist.set_type('force')
            >>> [data, group, fid, coord] = exp.get_event_att('force')
            >>> exp.data_hist.set_data('force', data)
            >>> exp.event_gauss_fit(data_type='force')#, min_val=0, max_val=1)
            >>> exp.event_stat['force'].keys()
            ['std', 'kde', 'maxima', 'count', 'mean']

            * kde is the kernel density estimation. This is an array composed
              of two vector, respectively the x and y coordinate.
            * 'mean' is the list of detected gaussian mean
            * 'std' is the corresponding standard deviation.
            * 'count' is the corresponding number of elements for the mean and
              std.
            * 'maxima' is the detected maxima from the kde.

            To fit loading rate, distance, stiffness and relative stiffness :

            >>> exp.data_hist.set_range('lr', [0, 1, 0.1])
            >>> exp.data_hist.set_type('lr')
            >>> [data, group, fid, coord] = exp.get_event_att('lr')
            >>> exp.data_hist.set_data('lr', data)
            >>> exp.event_gauss_fit(data_type='lr')

            >>> exp.data_hist.set_range('dist', [0, 1000, 10])
            >>> exp.data_hist.set_type('dist')
            >>> [data, group, fid, coord] = exp.get_event_att('dist')
            >>> exp.data_hist.set_data('dist', data)
            >>> exp.event_gauss_fit(data_type='dist')
        '''
        # defines the groups to be displayed.
        group = num.array(self.group['id'])[
                            num.array(self.group['display'])]
        fit, mean, std, count = None, None, None, None
        if data_type in ['force', 'force_base', 'lr', 'dist', 'stiff',
                         'relstiff', 'length', 'plength']:
            min_val = self.data_hist.get_range(data_type).min
            max_val = self.data_hist.get_range(data_type).max
            values = self.data_hist.get_data(data_type, group)
        index = (min_val < values) * (values < max_val)
        self.event_stat[data_type] = post_proc.find_gauss_fit(values[index])

    def export_stiffness(self, filename):
        """
            Export the stiffness in a set of csv file. Each file contain the
            stiffness at

            >>> exp = FVFolder()
            >>> exp.load_aex(STIFFNESS_COMPUTED_FILE)
            >>> exp.export_stiffness(os.path.join(EXPORT_FOLDER, \
                                     'stiffness_export'))
        """
        # Get all the stiffness
        #stiffness_list = list()
        split_filename = os.path.splitext(filename)
        if split_filename[1] in ['.csv', '.txt']:
            filename = split_filename[0]
        else:
            # It is probably not an extention, then restore it
            filename = split_filename[0] + split_filename[1]
        old_masked_print_option = num.ma.masked_print_option.display()
        num.ma.masked_print_option.set_display('')
        shapes = list()
        size_parts = list()
        for fv in self.file['list']:
            if fv.has_stiffness:
                shapes.append(fv.get_array('Stiffness').shape)
                size_parts.append(fv.stiffness['size_parts'])
        # The shape are in the forme :
        # (size_x, size_y, depth)
        # to take the max depth: shape[:,2].max()
        shapes = num.array(shapes)
        max_depth = shapes[:, 2].max()
        max_size = max(shapes[:, 0] * shapes[:, 1])
        for depth in range(max_depth):
            this_file = filename + '_Deep_' + str(depth) + '.csv'
            this_array = num.ma.array(
                                num.empty((max_size, len(self.file['list']))),
                                           mask=num.ones((max_size,
                                                    len(self.file['list']))))
            # this_averate will contain 1) mean, 2) std and 3) count.
            this_average = num.empty((3, len(self.file['list'])))
            for fv in self.file['list']:
                if fv.has_stiffness:
                    if fv.get_array('Stiffness').shape[2] >= depth:
                        # Get the data and create the mask according to it.
                        try:
                            vector = fv.get_array(
                                            'Stiffness')[:, :, depth].flatten()
                        except IndexError:
                            print "SHIT"
                            vector = num.ma.array(num.empty(5),
                                                  mask=num.ones(5))
                        # Put the data in the masked array.
                        index = self.file['name'].index(fv.name)
                        this_array[:vector.shape[0], index] = vector
                        this_array.mask[:vector.shape[0], index] = vector.mask
                        this_average[0, index] = vector.mean()
                        this_average[1, index] = vector.std()
                        this_average[2, index] = vector.count()
            # Write the file.
            fid = open(this_file, 'wb')
            fcsv = csv.writer(fid)
            fcsv.writerow(['Stiffness data of file :', self.filename])
            fcsv.writerow(['Injection time : ',
                            self.parameters['injection_index']])
            fcsv.writerow(self.file['name'])
            fcsv.writerow(['Group'])
            fcsv.writerow(self.file['group'])
            fcsv.writerow(['Depth size : [nm]'])
            fcsv.writerow(num.array(size_parts))
            fcsv.writerow(['Global: mean'])
            fcsv.writerow(this_average[0, :])
            fcsv.writerow(['Global: std'])
            fcsv.writerow(this_average[1, :])
            fcsv.writerow(['Global: number'])
            fcsv.writerow(this_average[2, :])
            fcsv.writerow(['Global: all values'])
            fcsv.writerows(this_array)
            fid.close()
        # restore the old display option of masked arrays
        num.ma.masked_print_option.set_display(old_masked_print_option)

    def export_average_stiffness_tomo(self, filename):
        """
            Exports the average stiffness tomography to the specified file.

            * Parameters :
                filename: str
                    path-name to save the file.

            * Output :
                None
        """

        ## generate the filename
        split_filename = os.path.splitext(filename)
        if split_filename[1] in ['.csv', '.txt']:
            filename = split_filename[0]
        else:
            # It is probably not an extention, then restore it
            filename = split_filename[0] + split_filename[1]
        filename = filename + '.csv'
        old_masked_print_option = num.ma.masked_print_option.display()
        num.ma.masked_print_option.set_display('')

        # get the data to export
        mean_stiffness = self.average_stiffness
        # the data are [{'deep': [], 'mean': [], 'std': [], 'count': []}]
        # Rearrange the data to be an array...
        deep = [i['deep'] for i in mean_stiffness]
        mean = [i['mean'] for i in mean_stiffness]
        std = [i['std'] for i in mean_stiffness]
        count = [i['count'] for i in mean_stiffness]

        sizes = [len(i) for i in deep]
        max_size = max(sizes)
        max_index = sizes.index(max_size)

        array_size = (3 * len(mean_stiffness) + 1, max_size)

        this_array = num.ma.array(num.empty(array_size),
                                  mask=num.ones(array_size))

        # Fill the array with the data
        this_array[0][:len(deep[max_index])] = deep[max_index]
        this_array[0].mask[:len(deep[max_index])] = 0
        add = 1
        for i, this_mean in zip(range(len(mean_stiffness)), mean):
            this_array[i + add][:len(this_mean)] = this_mean
            this_array[i + add].mask[:len(this_mean)] = 0
        add = 1 + len(mean_stiffness)
        for i, this_std in zip(range(len(mean_stiffness)), std):
            this_array[i + add][:len(this_std)] = this_std
            this_array[i + add].mask[:len(this_std)] = 0
        add = 1 + 2 * len(mean_stiffness)
        for i, this_count in zip(range(len(mean_stiffness)), count):
            this_array[i + add][:len(this_count)] = this_count
            this_array[i + add].mask[:len(this_count)] = 0
        #this_array = this_array.transpose(1,0)
        label = ['deep'] + \
                ['mean of file ' + fvname for fvname in self.file['name']] +\
                ['std of file' + fvname for fvname in self.file['name']] +\
                ['count of file' + fvname for fvname in self.file['name']]
        # Write the csv
        fid = open(filename, 'wb')
        fcsv = csv.writer(fid)
        fcsv.writerow(['Stiffness data of file :', self.filename])
        fcsv.writerow(label)
        fcsv.writerows(this_array.transpose(1, 0))
        num.ma.masked_print_option.set_display(old_masked_print_option)

    def export_events(self, filename):
        """
            Export the events of all files.

            * Parameters :
                filename: str
                    The file pathname to store the data

            * Retruns :
                None.

            >>> exp = FVFolder()
            >>> exp.load_aex(EVENT_COMPUTED_FILE)
            >>> exp.export_events(EXPORT_FOLDER + '/experiment_events.csv')
        """

        fid = open(filename, 'wb')
        fcsv = csv.writer(fid)
        firstfile = True
        for fv in self.file['list']:
            fv.export_events(fcsv, firstfile=firstfile)
            firstfile = False
        fid.close()

    def export_nbr_events(self, filename):
        """
            Export the number of events per curves of all files.

            * Parameters :
                filename: str
                    The file pathname to store the data

            * Retruns :
                None.

            >>> exp = FVFolder()
            >>> exp.load_aex(EVENT_COMPUTED_FILE)
            >>> exp.export_nbr_events(EXPORT_FOLDER + \
                                      '/experiment_nbr_events.csv')
        """
        fid = open(filename, 'wb')
        fcsv = csv.writer(fid)
        firstfile = True
        for fv in self.file['list']:
            fv.export_nbr_events(fcsv, firstfile=firstfile)
            firstfile = False
        fid.close()

    def __regenerate_filename_list(self):
        '''
        Regenerates the filename list
        '''
        self.file['name'] = []
        self.file['group'] = []
        nbr_curves = 0
        for item in self.file['list']:
            self.file['name'].append(item.name)
            self.file['group'].append(item.header['group'])
            nbr_curves = nbr_curves + item.header['number_curves']
        self.parameters['number_curves'] = nbr_curves
        self.data_hist.total_nbr_curves = nbr_curves
        # For the group labels and display...
        unique_group = num.unique(self.file['group'])
        if len(self.group['label']) > len(unique_group):
            _to_remove = [item not in unique_group
                                for item in self.group['id']]
            self.__remove_group(_to_remove)
        elif len(self.group['label']) < len(unique_group):
            _to_add = [item not in self.group['id'] for item in unique_group]
            self.__add_group(_to_add)

    def __remove_group(self, to_remove):
        """
            Remove item from group.

            * Parameter :
                to_remove: list
                    list of boolean to tell which remove and which one to keep.
        """
        for item in range(len(to_remove)):
            if to_remove[item]:
                self.group['label'].pop(item)
                self.group['id'].pop(item)
                self.group['display'].pop()

    def __add_group(self, _to_add):
        """
            Add object from group.

            * Parameter :
                to_add: list
                    list of boolean to tell which group to add.
        """

        new_id = []
        new_label = []
        new_disp = []
        unique_group = num.unique(self.file['group'])
        for i in range(len(unique_group)):
            if _to_add[i]:
                _add = unique_group[i]
                # Adds this group and put default value.
                new_id.append(_add)
                new_label.append('Groupe %i' % _add)
                new_disp.append(True)
            else:
                # This group exist. Recover the user defined value.
                _gr = unique_group[i]
                index = self.group['id'].index(_gr)
                new_id.append(self.group['id'][index])
                new_label.append(self.group['label'][index])
                new_disp.append(self.group['display'][index])
        self.group['id'] = new_id
        self.group['label'] = new_label
        self.group['display'] = new_disp

    def __regenerate_comment(self):
        if self.comment == 'Your comment':
            comment = ''
            for fvfile in self.file['list']:
                if fvfile.header['note'] != '\n':
                    comment = comment + fvfile.name + '\n' + \
                              fvfile.header['note'] + '\n'
            if comment != '':
                # We update only if there was a comment in the file headers,
                # and if it's the first (i.e. self.comment == 'Your comment')
                comment = comment[:-1]  # we erase the last \n
                self.comment = comment


class NotReadyError(Exception):
    pass

ARRAY_TYPE = {'Piezo': 'topo',
              'PoCIndice': 'topo',
              'PoCNoise': 'topo',
              'Topography': 'topo',
              'Stiffness': 'topo',
              'Stiffness tomo': '3d',
              'Event': 'event',
              'Event force': 'event',
              'Event length': 'event',
              'Event distance': 'event',
              'Event random': 'revent',
              'Relative pos': 'rel',
              'Relative rand pos': 'rel',
              'Path': 'misc',  # This defines a path to display info.
              'Masked': 'misc',  # This option is to apply or not a mask to the
                                 # array.
              'Mask': 'misc'}

if __name__ == '__main__':  # pragma: no cover
#    import plot_generic
#    test_file = '/home/charles/AFM/Macrophages/Living_cytB2.aex'
#    exp_1 = FVFolder()
#    exp_1.load_aex(test_file)
#    win_plot = plot_generic.ErrorBar()
#    win_plot.average_stiffness(exp_1.average_stiffness,
#                               group=exp_1.file['group'],
#                               group_info=exp_1.group,
#                               style = 'isol')
#    win_plot.save('test.png')

    import doctest
    BIOSCOPE_FILE2 = '../docs/data/files/FR20074A.001'
#    fvfile = ForceVolume(BIOSCOPE_FILE2)
    BIOSCOPE_FILE = '../docs/data/files/FR2310B.004'
    BIOSCOPE_FOLDER = '../docs/data/files/'
    STIFFNESS_COMPUTED_FILE = '../docs/data/Stiffness_computed.aex'
    EVENT_COMPUTED_FILE = '../docs/data/Event_computed.aex'
    ALL_COMPUTED_FILE = '../docs/data/Stiffness_and_Event_computed.aex'
    EXPORT_FOLDER = '../docs/data/Garbage'
    doctest.testmod()
