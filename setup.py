#!/usr/bin/env python
"""
    Setup.py package the source in order to distriburte the software.
    Just run python setup.py to install the software in your computer.
"""

from distutils.core import setup
from openfovea.common import DEB_NAME, APP_VERSION

setup(name=DEB_NAME,
      version=APP_VERSION,
      license='GNU Public License v3',
      description='Postprocessing of force volume data acquired with AFM./n'+\
                   'OpenFovea is a software to postprocess AFM force volume/n'+\
                   'experiment. It is able to compute the stiffness of the/n'+\
                   'scanned area, detect unbinding event and the zero force/n'+\
                   'topography. It comes with a user friendly graphical interface/n'+\
                   'that allow the user to select and display the data.',
      author="Charles Roduit",
      author_email="charles.roduit@gmail.com",
      url="http://www.freesbi.ch/openfovea",
      packages = ['openfovea',
                  'openfovea.fovea_toolbox',
                  'openfovea.fovea_toolbox.file_util'],
      package_data={'openfovea':['glade/*.glade', 'Icon/*']},
      data_files=[('share/applications', ['openfovea.desktop']),
                  ('share/pixmaps', ['openfovea/Icon/openfovea.png'])],
      scripts = ['script/openfovea']
      )
